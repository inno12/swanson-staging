<?php
	include_once 'inc_top_script.php';

	$rid = $_REQUEST["id"];

	$titleId = $_REQUEST["title_id"];

	if(!empty($_REQUEST["id"]) && !is_numeric($_REQUEST["id"]) ){
		header("Location: recipe-list.php");
	}

	if(!empty($_REQUEST["id"]) && strip_tags($_REQUEST["title_id"]) != $_REQUEST["title_id"] ){
		header("Location: recipe-list.php");
	}

	if(empty($rid) && empty($titleId) ){
		header("Location: recipe-list.php");
	}
?>

<!DOCTYPE html>
<html>
	<head>

		<?php


			include_once 'include_script.php';

			if(!empty($rid)){
				$rRs = $pdo->prepare("select r.*, p.image_thumb, p.product_key , p.id as productId, p.seo_title, p.seo_desc, p.seo_keywords from recipe r left join product p on r.product_id = p.id where r.id = :id");
				$rRs->bindValue("id", $rid, PDO::PARAM_INT);
			}else if(!empty($titleId)){
				$rRs = $pdo->prepare("select r.*, p.image_thumb, p.product_key , p.id as productId, p.seo_title, p.seo_desc, p.seo_keywords from recipe r left join product p on r.product_id = p.id where r.title_id = :title_id");
				$rRs->bindValue("title_id", $titleId, PDO::PARAM_STR);
			}
			$rRs->execute();

			$rRs_row = $rRs->fetch();
			$title = $rRs_row["title"];
			$steps = $rRs_row["steps"];
			$image = $rRs_row["image"];


			$productId1 = $rRs_row["product_id"];
			$productId2 = $rRs_row["product_id2"];
			if(!empty($productId2)){
				$productRs2 = $pdo->prepare("select id, product_key, image_thumb3  from `product` where id = :pid2");
				$productRs2 -> bindValue("pid2", $productId2, PDO::PARAM_INT);
				$productRs2 ->execute();
				$productRs2_row = $productRs2 ->fetch();
				$product2Key = $productRs2_row["product_key"];
				$product2Image3 = $productRs2_row["image_thumb3"];
			}


			$otherDesc_content = $rRs_row["otherDesc_content"];
			$otherDesc_title = $rRs_row["otherDesc_title"];
			$otherDesc_content2 = $rRs_row["otherDesc_content2"];
			$otherDesc_title2 = $rRs_row["otherDesc_title2"];

			$ingredients = $rRs_row["ingredients"];
			$ingredients_desc = $rRs_row["ingredients_desc"];
			$sauce = $rRs_row["sauce"];
			$productImage = $rRs_row["image_thumb"];
			$productKey = $rRs_row["product_key"];
			$productId = $rRs_row["productId"];

			$related_recipe_1 = $rRs_row["related_recipe_1"];
			$related_recipe_2 = $rRs_row["related_recipe_2"];
			$related_recipe_3 = $rRs_row["related_recipe_3"];
			$related_recipe_4 = $rRs_row["related_recipe_4"];



			$seoTitle = $rRs_row["seo_title"];
			$seoDesc = $rRs_row["seo_desc"];
			$seoKeywords = $rRs_row["seo_keywords"];

			$tips = $rRs_row["tips"];


			$related_recipeList = array();
// 			if(!empty($related_recipe_1)) array_push($related_recipeList, $related_recipe_1);
// 			if(!empty($related_recipe_2)) array_push($related_recipeList, $related_recipe_2);
// 			if(!empty($related_recipe_3)) array_push($related_recipeList, $related_recipe_3);
// 			if(!empty($related_recipe_4)) array_push($related_recipeList, $related_recipe_4);


			//fill related recipe from random recipe
			$relatedRecipeNum = sizeof($related_recipeList);
			$pendingRecipeNum = 4 - $relatedRecipeNum;
			if( $pendingRecipeNum > 0){

				if(sizeof($related_recipeList)>0){
					$rRs2 = $pdo->prepare("select id from recipe where id not in (".implode(",", $related_recipeList).")  and isHide = 'N' order by rand() limit :limit");
				}else{
 					if(strpos($productId, "9") || strpos($productId, "10") || strpos($productId, "11") ){//for scs
						$rRs2 = $pdo->prepare("select r.id from recipe r join product p on r.product_id = p.id where r.id <> :rid and isHide = 'N' and p.id in ('9','10','11') order by rand() limit :limit");
					}else{
						$rRs2 = $pdo->prepare("select r.id from recipe r join product p on r.product_id = p.id where r.id <> :rid and isHide = 'N' and p.id in ('".str_replace(";",",",$productId)."') order by rand() limit :limit");

					}


				}
				$rRs2->bindValue("rid", $rid, PDO::PARAM_INT);
				$rRs2->bindValue("limit", $pendingRecipeNum, PDO::PARAM_INT);
				$rRs2->execute();
				while($rRs2_row = $rRs2->fetch()){
					array_push($related_recipeList, $rRs2_row["id"]);
				}
			}

				$shareText =

				"史雲生食譜 - {$title} :".
				" %0A" .
				"http://".$_SERVER["HTTP_HOST"].PATH_TO_ULFILE."recipe_share/".$rid.".jpg" .
				" %0A" .
				" %0A" .
				"更多食譜 :%0Ahttp://".$_SERVER["HTTP_HOST"].ROOT_PATH."recipe-list.php"
				;




			$updateRs = $pdo->prepare("update recipe set viewCount = viewCount +1 where id = :rid");
			$updateRs ->bindValue("rid", $rid, PDO::PARAM_INT);
			$updateRs ->execute();

		?>
		<meta property="og:title" content="史雲生食譜 - <?=$title?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH?>recipe-detail.php?id=<?=$rid?>"/>
		<meta property="og:image" content="http://<?=$_SERVER["HTTP_HOST"].PATH_TO_ULFILE.$image?>"/>
		<meta property="og:site_name" content="<?=APP_TITLE?>"/>
		<meta property="og:description" content="Click入嚟睇更多食譜啦！"/>

		<script>
			function sharePage(){
				/*
				track.send("RECIPE_DETAIL", "SHARE_TO_FACEBOOK", "<?=$title?>");
				*/
				var u = "http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH?>recipe-detail.php?id=<?=$rid?>";
				window.open("https://www.facebook.com/sharer/sharer.php?u="+u , "pop", "width=600, height=400, scrollbars=no");
			}
			// $( document ).ready(function() {
			// 	if ($('.productThumbBgAsImg').css('background-image') === 'url(/_ul/1515059642.png)') {
			// 		alert('hi');
			// $('.productThumbBgAsImg').css("background-position", "-30px 10px");
			// }
});

		</script>
		<style>

		div[style^="background-image:url(/_ul/1516778519.png)"]{
		/*	background-position: -25px 15px !important; */
			margin-bottom: 2.5px;
			margin-right: 10px;
		}

		div[style^="background-image:url(/_ul/1507858435.png)"]{
		/*	background-position: -25px 15px !important; */
			margin-bottom: 2.5px;
		}

		div[style^="background-image:url(/_ul/1522728281.png)"]{
		/*	background-position: -25px 15px !important; */
			margin-bottom: 10px !important;
		}



		img[src*="1516864215"] {
			max-height: 80px;
		}

		/* img[src*="1516778519"] {
			margin-top: 8px;

		}
		img[src*="1515059642"] {
			background-position: -30px 10px;

		} */
		.recipe-detail-2-product div.small{
			top:0px;
		}
		</style>

		<title>史雲生食譜 - <?=$title?></title>
		<meta name="keywords" content="史雲生食譜,今晚煮乜餸,火鍋推介,新手菜式,粥粉麵飯,家常小菜,節慶菜式,搜尋食譜,人氣食譜,今晚食乜餸,<?=$seoKeywords?>">
		<meta name="description" content="<?=$seoDesc?>">

	</head>

	<body>
		<?php
			include_once 'inc_beginbody_script.php';
		?>
		 <div id="wrapper" class="">
	        <?php
	        	include_once 'inc_sidebar.php';
	        ?>
			<div class="mainContainer "  id="page-content-wrapper" >
				<div style="position:relative;">
				<?php
					include_once 'inc_header.php';
				?>





				<div class="breadcrumb" style="margin-bottom:0px;" >
					<div class="container swansonBreadcrumb" >
						<img src="<?=ROOT_PATH?>images/breadcrumb_home_icon.png"/>&nbsp;<a href="<?=ROOT_PATH?>index.php">主頁</a>

						<span class="glyphicon glyphicon-menu-right " aria-hidden="true" ></span>
						 <a href="<?=ROOT_PATH?>recipe-list.php">食譜搜尋</a>

						 <span class="glyphicon glyphicon-menu-right " aria-hidden="true" ></span>
						<?=$title?>
					</div>

				</div>







				<div class="container recipeDetail">
					<div id="product_share" style="" class="product_share hidden-xs hidden-sm hidden">
						<a onclick ="sharePage()" style="margin-right:1px;cursor:pointer;margin-bottom:12px;display:block;">
							<img src="<?=ROOT_PATH?>images/share_facebook_icon.png"/>
						</a>
						<div style="clear:both">
						</div>

					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row" style="background-color:#FFF;border-bottom:15px solid #0e847a;margin-bottom:15px;">

						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bigimage">
								<img src="<?=PATH_TO_ULFILE.$image?>" style="width:100%;" alt="<?=$title?>">
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 content" style=""  >
								<div class="title textBold">
									<?=$title?>
								</div>
								<div class="shareContainer">
									<span style="padding-right:10px;padding-top:2px;font-weight:bold;">分享到</span>

									<a href="#" onclick="sharePage()" ><img src="<?=ROOT_PATH?>images/share_facebook_icon.png"/></a>
									<a class="whatsappShareBtn" href="whatsapp://send?text=<?=$shareText?>" data-action="share/whatsapp/share" style="margin-right:1px;cursor:pointer;" class="hidden-lg" > <!-- onclick='track.send("RECIPE_DETAIL", "SHARE_TO_WHATSAPP", "<?=$title?>");' -->
										<img src="<?=ROOT_PATH?>images/share_whatsapp_icon.png"/>
									</a>
								</div>


								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 description" style="padding-top:30px;" >
									<?php
										if(!empty($ingredients_desc)){
									?>
										<div style="font-size:24px;color: #0F847B;font-weight:bold; " class="textBold">
											材料
										</div>
										<?=$ingredients_desc?>
									<?php
										}
									?>

									<?php
										if(!empty($sauce)){
									?>
										<div style="font-size:24px;color: #0F847B;font-weight:bold;  padding-top: 40px; " class="textBold">
											醃料
										</div>
										<?=$sauce?>
									<?php
										}
									?>

									<?php
										if(!empty($otherDesc_content)){
									?>
										<div style="font-size:24px;color: #0F847B;font-weight:bold;   padding-top: 40px; " class="textBold">
											<?=$otherDesc_title?>
										</div>
										<?=$otherDesc_content?>
									<?php
										}
									?>

									<?php
										if(!empty($otherDesc_content2)){
									?>
										<div style="font-size:24px;color: #0F847B;font-weight:bold;    padding-top: 40px; " class="textBold">
											<?=$otherDesc_title2?>
										</div>
										<?=$otherDesc_content2?>
									<?php
										}
									?>





								</div>

								<div style="clear: both;">
								</div>



								<?php
									//only restrict to 250mL Chicken & any CCS
									if( in_array("1", explode(";", $productId1)) && ( in_array("9", explode(";", $productId2)) || in_array("10", explode(";", $productId2)) || in_array("11", explode(";", $productId2)))   ){
										$isP1Small = true;
									}else if( in_array("1", explode(";", $productId2)) && ( in_array("9", explode(";", $productId1)) || in_array("10", explode(";", $productId1)) || in_array("11", explode(";", $productId1)))   ){
										$isP2Small = true;
									}


								?>


								<div class="row productThumb <?=!empty($product2Key) ? "product2" : ""?>" >


									<a href="<?=ROOT_PATH?>product-list.php?p=<?=$productKey?>" class="<?=$isP1Small ? "small" :""?>">
										<div class="productThumbBgAsImg <?=$rid == "218" ? "special " : ""?><?=$rid ==  "217" ? "special " : ""?><?=$rid ==  "224" ? "special " : ""?><?=$rid ==  "230" ? " special1 " : ""?><?=!empty($product2Key) ? "product2_image" : "product1_image"?>" style="background-image:url(<?=PATH_TO_ULFILE.$productImage?>);">

										</div>
									</a>
									<?php
										if(!empty($product2Key)){
									?>

										<a href="<?=ROOT_PATH?>product-list.php?p=<?=$product2Key?>" class="<?=$isP2Small ? "small" :""?>">
											<div class="productThumbBgAsImg product2_image  <?=$rid ==  "230" ? "special" : ""?><?=$rid ==  "91" ? "special" : ""?><?=$rid ==  "195" ? "special1" : ""?>" style="background-image:url(<?=PATH_TO_ULFILE.$product2Image3?>);">

											</div>
										</a>
									<?php
										}
									?>




								</div>

							</div>
						</div>

						<div class="row stepsContainer" style="position:relative">

							<div class="title2 textBold">
								做法
							</div>
							<div class="steps">


								<?php
									$stepsList = explode(";", $steps);
									foreach($stepsList as $stepNum => $step){
								?>
									<div>
										<div class="number" ><?=($stepNum + 1)?></div>
										<div class="stepContent"><?=$step?></div>
									</div>
									<hr/>
								<?php
									}
								?>



							</div>


							<?php
								if(!empty($tips)){
							?>
								<div style="font-size:24px;color: #0F847B;font-weight:bold;  font-size:18px;   padding-top: 40px; " class="textBold">
									小提示
								</div>
								<?=html_entity_decode($tips)?>
							<?php
								}
							?>
						</div>
						<div class="row detailEnd">
							<img src="<?=ROOT_PATH?>images/detail_bottom_image.gif"/>
						</div>
					</div>

					<?php
						if(sizeof($related_recipeList)>0){
					?>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row lineContainer ">
							<div class="line" >
								<div><div class="border"></div></div>
							</div>
							<div class="titleImg" >
								<img src="<?=ROOT_PATH?>images/related_recipe.png" alt="相關食譜"/>
							</div>
							<div class="line" >
								<div><div class="border"></div></div>
							</div>
						</div>

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row recipeContainer">
							<?php
								$relatedRecipeInfoList = array();
								$rRs2 = $pdo -> prepare("
												select distinct r.id, r.title, r.image,r.isPopular, r.product_id2, p.image_thumb from recipe r left join product p on r.product_id = p.id
												where r.id in (".implode(",", $related_recipeList).")
												order by r.display_priority desc");
								$rRs2->execute();
								while($rRs2_row = $rRs2->fetch()){
									$rId = $rRs2_row["id"];
									$title = $rRs2_row["title"];
									$image = $rRs2_row["image"];
									$image = empty($rRs2_row["image"])? "<?=ROOT_PATH?>images/empty_recipe.png" : PATH_TO_ULFILE.$rRs2_row["image"];
									$productImage = $rRs2_row["image_thumb"];
									$isPopular = $rRs2_row["isPopular"] == "Y";
									$productId2 = $rRs2_row["product_id2"];
									$product2Image3 = "";
									if(!empty($productId2)){
										$productRs2 = $pdo->prepare("select id, title, image_thumb, image_thumb2, image_thumb3  from `product` where id = :pid2");
										$productRs2 -> bindValue("pid2", $productId2, PDO::PARAM_INT);
										$productRs2 ->execute();
										$productRs2_row = $productRs2 ->fetch();
										$product2Image3 = $productRs2_row["image_thumb3"];
										$isSCS = $productRs2_row["title"] == "調味雞汁8包" ? "product-scs" : "";
 									}
									$relatedRecipeInfoList[$rId] = array("id"=>$rId, "title"=>$title, "image"=>$image, "p_image"=>$productImage, "p_image2"=>$product2Image3, "isPop"=>$isPopular, "isSCS"=>$isSCS);
								}
								foreach($related_recipeList as $relatedRecipeId){
									if(empty($relatedRecipeId)){
										continue;
									}
							?>
								<div class="recipeItem col-lg-3 col-md-3 col-sm-6 col-xs-6">
									<div>
										<div class="image hoverzoom">
											<a href="<?=ROOT_PATH?>recipe-detail.php?id=<?=$relatedRecipeInfoList[$relatedRecipeId]["id"]?>">
												<img src="<?=$relatedRecipeInfoList[$relatedRecipeId]["image"]?>" alt="<?=$relatedRecipeInfoList[$relatedRecipeId]["title"]?>"/>
											</a>
											<?php
												if($relatedRecipeInfoList[$relatedRecipeId]["isPop"]){
											?>
												<div class="hotRecipe">
													<img src="<?=ROOT_PATH?>images/hot_recipe.png"/>
												</div>
											<?php
												}
											?>

										</div>

										<div class="caption">
											<div class="title">
												<a href="<?=ROOT_PATH?>recipe-detail.php?id=<?=$relatedRecipeInfoList[$relatedRecipeId]["id"]?>">
													<?=$relatedRecipeInfoList[$relatedRecipeId]["title"]?>
												</a>
											</div>
											<div class="productIcon productListIcon">
												<? //var_dump($relatedRecipeInfoList); ?>
												<? if(!empty($relatedRecipeInfoList[$relatedRecipeId]["p_image2"])){ ?>
												<div class="main <?=$relatedRecipeInfoList[$relatedRecipeId]["id"] == "91" ? "special " : ""?><?=$relatedRecipeInfoList[$relatedRecipeId]["id"] == "195" ? "special " : ""?><?=$relatedRecipeInfoList[$relatedRecipeId]["id"] == "107" ? "special " : ""?><?=$relatedRecipeInfoList[$relatedRecipeId]["isSCS"];?>">
													<img src="<?=PATH_TO_ULFILE.$relatedRecipeInfoList[$relatedRecipeId]["p_image"]?>" />
												</div>
												<div class="<?=$relatedRecipeInfoList[$relatedRecipeId]["id"] == "230" ? "special " : ""?><?=$relatedRecipeInfoList[$relatedRecipeId]["id"] == "223" ? "special " : ""?><?=$relatedRecipeInfoList[$relatedRecipeId]["id"] == "218" ? "special " : ""?><?=$relatedRecipeInfoList[$relatedRecipeId]["id"] == "191" ? "special " : ""?><?=$relatedRecipeInfoList[$relatedRecipeId]["id"] == "217" ? "special " : ""?><?=$relatedRecipeInfoList[$relatedRecipeId]["id"] == "224" ? "special " : ""?><?=$relatedRecipeInfoList[$relatedRecipeId]["id"] == "91" ? "special " : ""?><?=$relatedRecipeInfoList[$relatedRecipeId]["id"] == "195" ? "special " : ""?><?=$relatedRecipeInfoList[$relatedRecipeId]["id"] == "107" ? "special " : ""?><?=$relatedRecipeInfoList[$relatedRecipeId]["isSCS"];?>">
													<img src="<?=PATH_TO_ULFILE.$relatedRecipeInfoList[$relatedRecipeId]["p_image2"]?>" />
												</div>
												<? } else { ?>
												<div>
													<img src="<?=PATH_TO_ULFILE.$relatedRecipeInfoList[$relatedRecipeId]["p_image"]?>" />
												</div>
												<? } ?>
											</div>
										</div>
									</div>
								</div>
							<?php
								}
							?>
						</div>
					<?php
						}
					?>

					<div style="clear:both;height:45px;">
					</div>


				</div>
			</div>
			<?php
				include_once 'inc_footer.php';
			?>
		</div>
	</body>
</html>


<div id="shareToWhatsapp" style="display:none">
	<div class="title">
		分享到Whatsapp:

		<?php
			$shareText =

			"史雲生食譜 - {$title} :".
			" %0A" .
			"http://".$_SERVER["HTTP_HOST"].PATH_TO_ULFILE."recipe_share/".$rid.".jpg" .
			" %0A" .
			" %0A" .
			"更多食譜 :%0Ahttp://".$_SERVER["HTTP_HOST"].ROOT_PATH."recipe-list.php"
			;
		?>


		<a href="whatsapp://send?text=<?=$shareText?>" data-action="share/whatsapp/share">
			<img src="<?=ROOT_PATH?>images/share_whatsapp_icon2.png" class="whatsappBtn" style="display:inline-block;padding:0px 2px;"/>
		</a>
	</div>
	<div class="input" 						style="display:none">
		<input type="text" class="form-control" placeholder="電話號碼" style="width:calc( 100% - 45px);display:inline-block; "/>
		<img src="<?=ROOT_PATH?>images/share_whatsapp_icon2.png" class="whatsappBtn" style="display:inline-block;padding:0px 2px;"/>
	</div>
	<div class="image">
		<img src="<?=PATH_TO_ULFILE."/recipe_share/".$rid.".jpg"?>"/>
	</div>
</div>
<?php
	include_once 'inc_footer_script.php';
?>

<script>

	function shareToWhatsapp(){

		$.fancybox( $("#shareToWhatsapp") ,{
			padding:20,
			margin:0,


			minWidth: '300',
			maxWidth: '300',
			afterShow:function(){


				$.fancybox.reposition()
			}
		});

	}


var isGettingNewRecipe = false;

var isShareBtnShown = false;


var isAndroid = (/android/gi).test(navigator.appVersion);
var isIDevice = (/iphone|ipad/gi).test(navigator.appVersion);
if(!isAndroid && !isIDevice){
	$(".whatsappShareBtn").hide();
}
	$(function() {



		$(window).on('scroll', function() {
			if($("body").height() - ($(this).scrollTop() + $(this).height()) < $(".footerListContainer").height() ){
				if(isShareBtnShown){
					isShareBtnShown = false;
					$("#product_share").clearQueue().addClass("hidden", 300, "easeInCubic").removeClass("shown");
				}
		    }else if((isAndroid || isIDevice) && $(this).scrollTop() >= $(".navBtnContainer").height() +15  ) {

				if(!isShareBtnShown){
					isShareBtnShown = true;
					$("#product_share").clearQueue().removeClass("hidden").addClass("shown", 200, "easeOutCubic");

				}

	        }else{

	        	if(isShareBtnShown){
					isShareBtnShown = false;
					$("#product_share").clearQueue().addClass("hidden", 300, "easeInCubic").removeClass("shown");
				}



		    }
	    })

		$(".collapsible").click(function(e){

			if($(this).hasClass("active")){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}

			$(this).parent().find(".collapsibleContent").slideToggle(300);

		});
	});


</script>
<script>
	//track

	<?/*
// 		track.send("RECIPE_DETAIL", "VIEW", "<?=$title?>");
	*/
	?>
</script>



<script src="<?=ROOT_PATH?>js/jquery-1.11.3.min.js"></script>
<script src="<?=ROOT_PATH?>js/jquery-ui.min.js"></script>
<script src="<?=ROOT_PATH?>js/jquery-migrate-1.4.1.min.js"></script>
<script src="<?=ROOT_PATH?>js/bootstrap.min.js"></script>
<script src="<?=ROOT_PATH?>js/jquery.fancybox.pack.js?v=2.1.5"></script>
<script src="<?=ROOT_PATH?>js/owl.carousel.min.js"></script>



<script>
	$(function(){

		$('li.dropdown').on('click', function() {
		    var $el = $(this);
		    if ($el.hasClass('open')) {
		        var $a = $el.children('a.dropdown-toggle');
		        if ($a.length && $a.attr('href')) {
		            location.href = $a.attr('href');
		        }
		    }
		});
	});

	$(function() {
		$(".sideBarOverlay").on("touchstart", function(e){
		    $("#wrapper").toggleClass("toggled");
			e.stopPropagation();
			e.preventDefault();
		});

		$(".sidebar-cancel-btn").on("click", function(){
			$("#wrapper").toggleClass("toggled");
		});
		
		$(".sideBarOverlay").on("click", function(e){
		    $("#wrapper").toggleClass("toggled");
			e.stopPropagation();
			e.preventDefault();
		});
		
		$("#sidebar-wrapper").on("touchstart", function(e){

			if($(e.target).is("a") || $(e.target).is("li")|| $(e.target).is(".glyphicon-remove") || $(e.target).is(".sidebar-sharing")  ){
				
				
			}else{
				console.log(e.target);
				e.stopPropagation();
				e.preventDefault();
			}
		});


		 $("#menu-toggle").click(function(e) {
	        e.preventDefault();
	        $("#wrapper").toggleClass("toggled");
	    });
	});
</script>
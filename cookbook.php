<?php
	include_once 'inc_top_script.php';
?>
<!DOCTYPE html>
<html>
	<head>

		<?php
			include_once 'include_script.php';
		?>
		<title>史雲生1盒煮1餸 簡易食譜</title>
		<meta name="keywords" content="史雲生食譜,食譜下載食譜推薦,家常菜式,新手菜式,簡易食譜,一盒煮一餸,精選食譜,新手食譜">
		<meta name="description" content="史雲生新手食譜，精選一盒煮一餸菜式，簡單又方便。立即下載食譜，隨時隨地煮出滋味靚餸！">

		<style>
			.productInfo{
				display:none;
			}

			.fancybox-outer{
				background-image: url(images/cookbook/cookbook_popup_bg.jpg);
				margin-top: 5%;
				padding:100px 25px;
				background-size: cover;
				/*height: 95vh;*/
			}
			.fancybox-outer>*{
				padding:20px;
				background-color:#FFF;
				width:auto !important;
				height:auto !important;
			}

			.fancybox-opened{
				margin-bottom:200px;
			}
			.fancybox-wrap{
				width: 66% !important;
				left: 17% !important;
			}


			@media(min-width:768px){
				.cookbookContainer{
					padding:25px 0%;
				}
				.cookbookContainer .left, .cookbookContainer .right{
					width:20%;
				}
				.cookbookContainer .image{
					width:200px;
				}

				.cookbookContainer .right {
					text-align:right;
				}
				.cookbookContainer .left {
					text-align:left;
				}

				.cookbookContainer .title{
				    height: 28px;
		    		max-width: 100%;

				}

				.cookbookContainer a, .cookbookContainer a span{
					display:inline-block;
				}
			}
			@media(min-width:992px){

				.cookbookContainer{
					padding:25px 120px;
				}
				.cookbookContainer .title{
				    height: 30px;
		    		max-width: 100%;

				}

				.cookbookContainer .left, .cookbookContainer .right{
					min-width:135px;
					width:20%;
				}
			}
			@media(min-width:1200px){

				.cookbookContainer{
					padding:25px 120px;
				}
				.cookbookContainer .title{
				    height: 35px;
		    		max-width: 100%;
				}

				.cookbookContainer .left, .cookbookContainer .right{
					min-width:135px;
					width:20%;
				}
			}
			/*171027*/
			.cookbookContainer_m .image{
				max-width: 90%;
			}
			.cookbookItem .cookbook-line-bg{
				margin-left: 20px;
			}


		</style>

		<meta property="og:title" content="史雲生食譜網站 "/>
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH?>cookbook.php"/>
		<meta property="og:image" content="http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH."images/fb-share-banner-cookbook.jpg"?>"/>
		<meta property="og:site_name" content="<?=APP_TITLE?>"/>
		<meta property="og:description" content="史雲生食譜，教你用簡單食材煮出滋味靚餸。"/>

		<script>
			function sharePage(){
				var u = "http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH?>";
				window.open("https://www.facebook.com/sharer/sharer.php?u="+u , "pop", "width=600, height=400, scrollbars=no");
			}
		</script>
	</head>

	<body>
		<?php
			include_once 'inc_beginbody_script.php';
		?>
		 <div id="wrapper" class="">
	        <?php
	        	include_once 'inc_sidebar.php';
	        ?>
			<div class="mainContainer "  id="page-content-wrapper" >
				<div style="position:relative;">
				<?php
					include_once 'inc_header.php';
				?>

				<div class="breadcrumb" style="background-color:#f1dc00;margin-bottom:0px;">
					<div class="container swansonBreadcrumb" >
						<img src="images/breadcrumb_home_icon.png"/>&nbsp;<a href="index.php">主頁</a>

						<span class="glyphicon glyphicon-menu-right " aria-hidden="true" ></span>
						 食譜下載
					</div>

				</div>

				<div class="row cookbook col-lg-12 col-md-12 col-sm-12 hidden-xs" style="    padding-bottom: 25px;">
					<a href="doc/一盒煮一餸簡易食譜.pdf" download = "一盒煮一餸簡易食譜.pdf">
						<img src="images/Banner_250ml_1920x465_v2.jpg" style="width:100%;"/>
					</a>
				</div>

				<div class="row cookbook hidden-lg hidden-md hidden-sm col-xs-12" style="    padding-bottom: 25px;">
					<a href="doc/一盒煮一餸簡易食譜.pdf"  download = "一盒煮一餸簡易食譜.pdf">
						<img src="images/cookbook/cook-books-list-banner-01.jpg" style="width:100%;"/>
					</a>
				</div>

				<div class="container recipeDetail">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row lineContainer">
						<div class="line">
							<div><div class="border"></div></div>
						</div>
						<div class="titleImg">
							<img src="images/cookbook.png"  alt="下載食譜"/>
						</div>
						<div class="line">
							<div><div class="border"></div></div>
						</div>
					</div>
				</div>

				<div class="row cookbookContainer_m cookbook hidden-lg hidden-md" style="text-align:center; ">

					<?php
						$i=1;
						while($i<=15){
					?>
					<a class="cookbookItem" href="images/cookbook/250recipe/250-recipes-mobile-<?=$i?>.jpg" target="_blank">
						<div>
							<img class="image" src="images/cookbook/cookbook_img_<?=$i?>_1710.jpg">
						</div>
						<div class="titleContainer">
							<img class="title" src="images/cookbook/cookbook-mobile-text/cookbook<?=$i?>_1710.png">
						</div>
					</a>

					<?php
							$i++;
						}
					?>

				</div>


				<div class="container cookbookContainer hidden-xs hidden-sm" style=" ">
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe1.jpg" target="_blank" style="width:100%"><!-- 34 -->
							<span class="left">
								<img class="image" src="images/cookbook/250_img1.png">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name1.png">
								</div>
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe2.jpg" target="_blank" style="width:100%"><!-- 79 -->
							<span class="left">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name2.png">
								</div>
							</span>
							<span class="right">
								<img class="image" src="images/cookbook/250_img2.png">
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe3.jpg" target="_blank" style="width:100%"><!-- 96 -->
							<span class="left">
								<img class="image" src="images/cookbook/250_img3.png">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name3.png">
								</div>
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe4.jpg" target="_blank" style="width:100%"><!-- 98 -->
							<span class="left">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name4.png">
								</div>
							</span>
							<span class="right">
								<img class="image" src="images/cookbook/250_img4.png">
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe5.jpg" target="_blank" style="width:100%"><!-- 37 -->
							<span class="left">
								<img class="image" src="images/cookbook/250_img5.png">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name5.png">
								</div>
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe6.jpg" target="_blank" style="width:100%"><!-- 102 -->
							<span class="left">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name6.png">
								</div>
							</span>
							<span class="right">
								<img class="image" src="images/cookbook/250_img6.png">
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe7.jpg" target="_blank" style="width:100%"><!-- 122 -->
							<span class="left">
								<img class="image" src="images/cookbook/250_img7.png">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name7.png">
								</div>
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe8.jpg" target="_blank" style="width:100%"><!-- 55 -->
							<span class="left">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name8.png">
								</div>
							</span>
							<span class="right">
								<img class="image" src="images/cookbook/250_img8.png">
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe9.jpg" target="_blank" style="width:100%"><!-- 114 -->
							<span class="left">
								<img class="image" src="images/cookbook/250_img9.png">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name9.png">
								</div>
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe10.jpg" target="_blank" style="width:100%"><!-- 14 -->
							<span class="left">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name10.png">
								</div>
							</span>
							<span class="right">
								<img class="image" src="images/cookbook/250_img10.png">
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe11.jpg" target="_blank" style="width:100%"><!-- 49 -->
							<span class="left">
								<img class="image" src="images/cookbook/250_img11.png">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name11.png">
								</div>
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe12.jpg" target="_blank" style="width:100%"><!-- 87 -->
							<span class="left">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name12.png">
								</div>
							</span>
							<span class="right">
								<img class="image" src="images/cookbook/250_img12.png">
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe13.jpg" target="_blank" style="width:100%"><!-- 88 -->
							<span class="left">
								<img class="image" src="images/cookbook/250_img13.png">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name13.png">
								</div>
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe14.jpg" target="_blank" style="width:100%"><!-- 15 -->
							<span class="left">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name14.png">
								</div>
							</span>
							<span class="right">
								<img class="image" src="images/cookbook/250_img14.png">
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe15.jpg" target="_blank" style="width:100%"><!-- 88 -->
							<span class="left">
								<img class="image" src="images/cookbook/250_img15.png">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name15.png">
								</div>
							</span>
						</a>
					</div>
					<div style="clear: both;width:100%">
						<a class="cookbookItem" href="images/cookbook/250ml-recipe16.jpg" target="_blank" style="width:100%"><!-- 15 -->
							<span class="left">
							</span>
							<span style="width:58%">
								<div class="cookbook-line-bg">
									<img class="title" src="images/cookbook/name16.png">
								</div>
							</span>
							<span class="right">
								<img class="image" src="images/cookbook/250_img16.png">
							</span>
						</a>
					</div>
				</div>



				<div class="row cookbook_bottom" style="text-align:center;padding:35px 0px;display:none">
					<div>
						<a href="product-list.php">
							<img src="images/cookbook_bottom_p1.png" />
						</a>
					</div>
					<div>
						<img src="images/cookbook_bottom_p2.png" />
					</div>
				</div>



			</div>
			<?php
				include_once 'inc_footer.php';
			?>
		</div>
	</body>
</html>
<?php
	include_once 'inc_footer_script.php';
?>
<script>

	$(function(){

		if( navigator.userAgent.match(/Android/i)
				 || navigator.userAgent.match(/webOS/i)
				 || navigator.userAgent.match(/iPhone/i)
				 || navigator.userAgent.match(/iPad/i)
				 || navigator.userAgent.match(/iPod/i)
				 || navigator.userAgent.match(/BlackBerry/i)
				 || navigator.userAgent.match(/Windows Phone/i)
		 ){
			//
	  	}else{
			$(".cookbookItem").fancybox({
				fitToView: false,
			    maxWidth: "90%",
			    margin:[20, 200,200,200],
				openEffect	: 'fade',
				closeEffect	: 'fade',
				padding:0
			});
		}


	});
</script>

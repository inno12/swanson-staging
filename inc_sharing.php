<meta property="og:title" content="史雲生食譜網站 "/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH?>"/>
<meta property="og:image" content="http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH."images/fb-share-banner-01-new-2.jpg"?>"/>
<meta property="og:site_name" content="<?=APP_TITLE?>"/>
<meta property="og:description" content="史雲生食譜，教你用簡單食材煮出滋味靚餸。"/>

<script>
	function sharePage(){
		var u = "http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH?>";
		window.open("https://www.facebook.com/sharer/sharer.php?u="+u , "pop", "width=600, height=400, scrollbars=no");
	}
</script>

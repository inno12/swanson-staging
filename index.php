<?php
	include_once 'inc_top_script.php';
?>
<!DOCTYPE html>
<html>
	<head>

		<?php
			require_once 'include_script.php';
		?>

		<?php
			include_once 'inc_sharing.php';
		?>
		<title>史雲生食譜網站</title>
		<meta name="keywords" content="史雲生,鮮魚湯食譜,今晚煮乜餸,Swanson,調味雞汁食譜,清雞湯食譜,金華火腿上湯食譜,豬骨湯食譜,瑤柱上湯食譜,今晚食乜餸">
		<meta name="description" content="史雲生助你輕易煮出各款美味家常菜。全新食譜搜尋器，讓你輕鬆找到簡單、美味的心水食譜；每日精選菜式推介，幫你輕鬆加餸。">

	</head>

	<body>
		<?php
			include_once 'inc_beginbody_script.php';
		?>
		<div id="wrapper" class="">

		 <?php
        	include_once 'inc_sidebar.php';
        ?>

		<div class="mainContainer" id="page-content-wrapper" >
			<div style="position:relative;">
			<?php
				include_once 'inc_header.php';
			?>
			<div class="breadcrumb_mp" style="background-color:#f1dc00;margin-bottom:0px;">

			</div>
			<div class="row mainpage">

				<div class="top-container container boxDistribute" style="">
					<div class="sliderContainer row col-lg-9 col-md-8 col-sm-8 col-xs-12">
						<div class="mainSlider_carousel hidden-xs hidden-sm hidden-md" style="clear:both">

							<?php
								$rRs = $pdo->prepare("select * from pagesetting_coverimage order by display_priority asc;  ");
								$rRs->execute();
								$resultSet = $rRs->fetchAll();

								foreach($resultSet as $i=>$v){

									if(!empty($v["hyperlink"])){
							?>
										 <div class="item">
										 	<a href="http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH.$v["hyperlink"]?>"/>
												<img class="owl-lazy" data-src="<?=PATH_TO_ULFILE.$v["image"]?>"/>
										 	</a>
									     </div>
							<?php

									}else{
							?>
									     <div class="item">
											<img class="owl-lazy" data-src="<?=PATH_TO_ULFILE.$v["image"]?>"/>
									     </div>
							<?php
									}
								}
							?>
						</div>


						<div class="mainSlider_carousel_mobile hidden-lg col-xs-12 col-sm-12 col-md-12"  style="clear:both;">

							<?php
								$rRs = $pdo->prepare("select * from pagesetting_coverimage2 order by display_priority asc;  ");
								$rRs->execute();
								$resultSet = $rRs->fetchAll();

								foreach($resultSet as $i=>$v){

									if(!empty($v["hyperlink"])){
							?>
										 <div class="item">
										 	<a href="http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH.$v["hyperlink"]?>"/>
												<img class="owl-lazy" data-src="<?=PATH_TO_ULFILE.$v["image"]?>"/>
										 	</a>
									     </div>
							<?php

									}else{
							?>
									     <div class="item">
											<img class="owl-lazy" data-src="<?=PATH_TO_ULFILE.$v["image"]?>"/>
									     </div>
							<?php
									}
								}
							?>
						</div>

					</div>




					<div class=" col-lg-3 col-md-4 col-sm-4 col-xs-12 mp_cookbook_partContainer" style="">

						<div class="mp_cookbook_part">
							<div class="cookbook_container row">
								<div class=" lineContainer">
									<div class="line hidden-lg hidden-md hidden-sm" style="width:calc(50% - 116px)">
										<div><div class="border"></div></div>
									</div>
									<div class="titleImg" style="">
										<img src="images/cook_video.png" alt="入廚教學短片"/>
									</div>
									<div class="line  hidden-lg hidden-md hidden-sm" style="width:calc(50% - 116px)">
										<div><div class="border"></div></div>
									</div>
								</div>

								<?php
									$rRs2 = $pdo->prepare("select * from recipe_video order by rand() limit 2");
									$rRs2->execute();

									while($rRs2_row = $rRs2->fetch()){

										$image = $rRs2_row["image"];
										$title = $rRs2_row["title"];
// 										$vid = $rRs2_row["video_link"];
										$youtubeId = $rRs2_row["video_link"];
								?>
									<!-- video item -->
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 videoItem ani" >
										<a class="fancybox-media" href="http://www.youtube.com/watch?v=<?=$youtubeId?>?rel=0" rel="media-gallery">
											<div style="position:relative">

												<div class="thumb">
													<img src="<?=PATH_TO_ULFILE.$image?>">
												</div>
												<div class="videoOverlay" style="position:absolute;">
													<div class="bg">
													</div>

													<div>
														<div>
															<img src="images/playVideoBtn.png" style="width:40px">
														</div>
													</div>

												</div>
											</div>
										</a>
										<a href="#">
											<div class="title" style="font-size:16px;line-height:18px;">
												<?=$title?>
											</div>
										</a>
									</div>
									<!-- video item -->
								<?php
									}
								?>


								<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<a href="recipe-video.php" class="moreRecipeVideoBtn">
										更多 <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
									</a>
								</div>
							</div>
						</div>














					</div>




				</div>

				<div class="container" style="padding-left:0px;padding-right:0px;">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row lineContainer">
						<div class="line">
							<div><div class="border"></div></div>
						</div>
						<div class="titleImg">
							<img src="images/find_recipe.png" alt="搜尋食譜"/>
						</div>
						<div class="line">
							<div><div class="border"></div></div>
						</div>
					</div>
				</div>




				<div class="container hidden-md hidden-lg col-md-12 col-sm-12 col-xs-12">

					<div class="mobile_ingredients_carousel" >

						<a  href="recipe-list.php?a" class="item">
							<img src="images/i_search.png" class="shadow" />
						</a>
						<a  href="recipe-list.php?i=1"class="item">
							<img src="images/i1.png" class="shadow" />
						</a>
						<a  href="recipe-list.php?i=3" class="item">
							<img src="images/i2.png" class="shadow" />
						</a>
						<a  href="recipe-list.php?i=2" class="item">
							<img src="images/i3.png" class="shadow" />
						</a>
						<a  href="recipe-list.php?i=5"class="item">
							<img src="images/i5.png" class="shadow" />
						</a>
						<a  href="recipe-list.php?i=6" class="item">
							<img src="images/i6.png" class="shadow" />
						</a>
						<a  href="recipe-list.php?i=7" class="item">
							<img src="images/i7.png" class="shadow" />
						</a>
					</div>
				</div>






				<div class="container boxDistribute hidden-xs hidden-sm">

					<div class="quickSearchBox" style="position:relative">
						<img src="images/r_search.png"   class="shadow" style="width:100%;"/>



						<div  style="position:absolute;width: 100%;top:0px;padding:6%;">
							<div style="">
								<div>
									<div style="width:calc(100% - 35px);float:left;">
										<input type="text" class=" form-control" id="searchRecipeTxt" placeholder="食譜搜尋">
									</div>
									<div style="width:35px;float:left">
										<a href="#">
											<img src="images/search_icon2.png" id="searchRecipeImg" style="height:34px;margin-left:-2px">
										</a>
									</div>

								</div>
							</div>
							<div style="clear:both">
							</div>
							<a href="recipe-list.php?a">
								<div class="mp_search" style="">
									<img src="images/search_icon3.png" style="padding:4px;padding-top: 0px;"/> 進階搜尋
								</div>
							</a>
						</div>


					</div>

					<a class="index-ingredient-item" href="recipe-list.php?i=1">
						<img src="images/i1.png" class="shadow"/>
					</a>
					<a class="index-ingredient-item"  href="recipe-list.php?i=3">
						<img src="images/i2.png" class="shadow"/>
					</a>
					<a class="index-ingredient-item" href="recipe-list.php?i=2">
						<img src="images/i3.png" class="shadow"/>
					</a>
					<a class="index-ingredient-item" href="recipe-list.php?i=5">
						<img src="images/i5.png" class="shadow"/>
					</a>
					<a class="index-ingredient-item" href="recipe-list.php?i=6">
						<img src="images/i6.png" class="shadow"/>
					</a>
					<a class="index-ingredient-item" href="recipe-list.php?i=7">
						<img src="images/i7.png" class="shadow"/>
					</a>
				</div>


				<div class="row detailEnd" style="text-align:center;">
					<img src="images/detail_bottom_image.gif"/>
				</div>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row lineContainer" id="weeklySuggestionHeader">
				<div class="line" style="width:calc(50% - 110px)">
					<div><div class="border"></div></div>
				</div>
				<div class="titleImg" style="width:220px;">
					<img src="images/weekly_suggest.png" alt="今日食乜餸"/>
				</div>
				<div class="line" style="width:calc(50% - 110px)">
					<div><div class="border"></div></div>
				</div>
			</div>




			<?php
				$rRs3 = $pdo->prepare("select * from weekly_highlight limit 1");
				$rRs3->execute();

				$rRs3_row = $rRs3->fetch();
				$weekday1 = $rRs3_row["r1"];
				$weekday2 = $rRs3_row["r2"];
				$weekday3 = $rRs3_row["r3"];
				$weekday4 = $rRs3_row["r4"];
				$weekday5 = $rRs3_row["r5"];

				$weekday6 = $rRs3_row["r6"];
				$weekday7 = $rRs3_row["r7"];

				$recipeList = array("1"=>$weekday1,
									"2"=>$weekday2,
									"3"=>$weekday3,
									"4"=>$weekday4,
									"5"=>$weekday5,
									"6"=>$weekday6,
									"0"=>$weekday7);

				$weeklyRecipeInfoList = array();
				$rRs4 = $pdo -> prepare("
					select distinct r.id, r.title, r.image,r.isPopular, p.image_thumb from recipe r left join product p on r.product_id = p.id
					where r.id in (".implode(",", $recipeList).")
					order by r.display_priority desc");
				$rRs4->execute();
				while($rRs4_row = $rRs4->fetch()){
					$rId = $rRs4_row["id"];
					$title = $rRs4_row["title"];
					$image = empty($rRs4_row["image"])? "images/empty_recipe.png" : PATH_TO_ULFILE.$rRs4_row["image"];
					$productImage = $rRs4_row["image_thumb"];
					$isPopular = $rRs4_row["isPopular"] == "Y";
					$weeklyRecipeInfoList[$rId] = array("id"=>$rId, "title"=>$title, "image"=>$image, "p_image"=>$productImage, "isPop"=>$isPopular);
				}


				$weekDay_today = date("w");
			?>


			<div class="container">
				<div class="row boxDistribute">


					<?php
						$wd1 = $weekDay_today++%7;
						$wd2 = $weekDay_today++%7;
						$wd3 = $weekDay_today++%7;
						$wd4 = $weekDay_today++%7;
						$wd5 = $weekDay_today++%7;
						$wd6 = $weekDay_today++%7;
						$wd7 = $weekDay_today++%7;

						$rid1 = $recipeList[$wd1];
						$rid2 = $recipeList[$wd2];
						$rid3 = $recipeList[$wd3];
						$rid4 = $recipeList[$wd4];
						$rid5 = $recipeList[$wd5];
						$rid6 = $recipeList[$wd6];
						$rid7 = $recipeList[$wd7];
					?>
					<!-- today -->
					<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 day_recipe today">
						<div class="image hoverzoom" style="float: left;">
							<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid1]["id"]?>"/>
								<img src="<?=$weeklyRecipeInfoList[$rid1]["image"]?>" style="width:100%;" alt="<?=$weeklyRecipeInfoList[$rid1]["title"]?>"/>
							</a>
						</div>
						<?php
							if($weeklyRecipeInfoList[$rid1]["isPop"]){
						?>
							<div class="hotRecipe">
								<img src="images/hot_recipe.png"/>
							</div>
						<?php
							}
						?>
						<div class="weekdayContent">
							<div class="weekday_title">
								<img src="images/recipe_week_<?=$wd1?>.png"/>
							</div>
							<div class="title">
								<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid1]["id"]?>" style="padding-bottom: 120px;"/>
									<?=$weeklyRecipeInfoList[$rid1]["title"]?>
								</a>
							</div>
							<div class="product_thumb">
								<img src="<?=PATH_TO_ULFILE.$weeklyRecipeInfoList[$rid1]["p_image"]?>"/>
							</div>
						</div>
					</div>
					<!-- today -->




					<div class="row hidden-lg col-md-12 col-sm-12 col-xs-12 weekly_more" >

						<div class="moreBtn" onclick="showWeeklyMore();">
							更多推介
						</div>
						<!-- mobile wd2 -->
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 day_recipe today">
								<div class="image hoverzoom" style="float: left;">
									<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid2]["id"]?>"/>
										<img src="<?=$weeklyRecipeInfoList[$rid2]["image"]?>" style="width:100%;"  alt="<?=$weeklyRecipeInfoList[$rid2]["title"]?>" />
									</a>
								</div>
								<?php
									if($weeklyRecipeInfoList[$rid2]["isPop"]){
								?>
									<div class="hotRecipe">
										<img src="images/hot_recipe.png"/>
									</div>
								<?php
									}
								?>
								<div class="weekdayContent">
									<div class="weekday_title">
										<img src="images/recipe_week_<?=$wd2?>.png"/>
									</div>
									<div class="title">
										<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid2]["id"]?>" style="padding-bottom: 120px;"/>
											<?=$weeklyRecipeInfoList[$rid2]["title"]?>
										</a>
									</div>
									<div class="product_thumb">
										<img src="<?=PATH_TO_ULFILE.$weeklyRecipeInfoList[$rid2]["p_image"]?>"/>
									</div>
								</div>
							</div>
						<!-- mobile wd2 -->

						<!-- mobile wd2 -->
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 day_recipe today">
								<div class="image hoverzoom" style="float: left;">
									<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid3]["id"]?>"/>
										<img src="<?=$weeklyRecipeInfoList[$rid3]["image"]?>" style="width:100%;"  alt="<?=$weeklyRecipeInfoList[$rid3]["title"]?>"/>
									</a>
								</div>
								<?php
									if($weeklyRecipeInfoList[$rid3]["isPop"]){
								?>
									<div class="hotRecipe">
										<img src="images/hot_recipe.png"/>
									</div>
								<?php
									}
								?>
								<div class="weekdayContent">
									<div class="weekday_title">
										<img src="images/recipe_week_<?=$wd3?>.png"/>
									</div>
									<div class="title">
										<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid3]["id"]?>" style="padding-bottom: 120px;"/>
											<?=$weeklyRecipeInfoList[$rid3]["title"]?>
										</a>
									</div>
									<div class="product_thumb">
										<img src="<?=PATH_TO_ULFILE.$weeklyRecipeInfoList[$rid3]["p_image"]?>"/>
									</div>
								</div>
							</div>
						<!-- mobile wd2 -->

						<!-- mobile wd2 -->
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 day_recipe today">
								<div class="image hoverzoom" style="float: left;">
									<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid4]["id"]?>"/>
										<img src="<?=$weeklyRecipeInfoList[$rid4]["image"]?>" style="width:100%;"  alt="<?=$weeklyRecipeInfoList[$rid4]["title"]?>"/>
									</a>
								</div>
								<?php
									if($weeklyRecipeInfoList[$rid4]["isPop"]){
								?>
									<div class="hotRecipe">
										<img src="images/hot_recipe.png"/>
									</div>
								<?php
									}
								?>
								<div class="weekdayContent">
									<div class="weekday_title">
										<img src="images/recipe_week_<?=$wd4?>.png"/>
									</div>
									<div class="title">
										<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid4]["id"]?>" style="padding-bottom: 120px;"/>
											<?=$weeklyRecipeInfoList[$rid4]["title"]?>
										</a>
									</div>
									<div class="product_thumb">
										<img src="<?=PATH_TO_ULFILE.$weeklyRecipeInfoList[$rid4]["p_image"]?>"/>
									</div>
								</div>
							</div>
						<!-- mobile wd2 -->

						<!-- mobile wd2 -->
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 day_recipe today">
								<div class="image hoverzoom" style="float: left;">
									<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid5]["id"]?>"/>
										<img src="<?=$weeklyRecipeInfoList[$rid5]["image"]?>" style="width:100%;"  alt="<?=$weeklyRecipeInfoList[$rid5]["title"]?>"/>
									</a>
								</div>
								<?php
									if($weeklyRecipeInfoList[$rid5]["isPop"]){
								?>
									<div class="hotRecipe">
										<img src="images/hot_recipe.png"/>
									</div>
								<?php
									}
								?>
								<div class="weekdayContent">
									<div class="weekday_title">
										<img src="images/recipe_week_<?=$wd5?>.png"/>
									</div>
									<div class="title">
										<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid5]["id"]?>" style="padding-bottom: 120px;"/>
											<?=$weeklyRecipeInfoList[$rid5]["title"]?>
										</a>
									</div>
									<div class="product_thumb">
										<img src="<?=PATH_TO_ULFILE.$weeklyRecipeInfoList[$rid5]["p_image"]?>"/>
									</div>
								</div>
							</div>
						<!-- mobile wd2 -->

						<!-- mobile wd2 -->
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 day_recipe today">
								<div class="image hoverzoom" style="float: left;">
									<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid6]["id"]?>"/>
										<img src="<?=$weeklyRecipeInfoList[$rid6]["image"]?>" style="width:100%;"  alt="<?=$weeklyRecipeInfoList[$rid6]["title"]?>"/>
									</a>
								</div>
								<?php
									if($weeklyRecipeInfoList[$rid6]["isPop"]){
								?>
									<div class="hotRecipe">
										<img src="images/hot_recipe.png"/>
									</div>
								<?php
									}
								?>
								<div class="weekdayContent">
									<div class="weekday_title">
										<img src="images/recipe_week_<?=$wd6?>.png"/>
									</div>
									<div class="title">
										<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid6]["id"]?>" style="padding-bottom: 120px;"/>
											<?=$weeklyRecipeInfoList[$rid6]["title"]?>
										</a>
									</div>
									<div class="product_thumb">
										<img src="<?=PATH_TO_ULFILE.$weeklyRecipeInfoList[$rid6]["p_image"]?>"/>
									</div>
								</div>
							</div>
						<!-- mobile wd2 -->


						<!-- mobile wd2 -->
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 day_recipe today">
								<div class="image hoverzoom" style="float: left;">
									<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid7]["id"]?>"/>
										<img src="<?=$weeklyRecipeInfoList[$rid7]["image"]?>" style="width:100%;"  alt="<?=$weeklyRecipeInfoList[$rid7]["title"]?>"/>
									</a>
								</div>
								<?php
									if($weeklyRecipeInfoList[$rid7]["isPop"]){
								?>
									<div class="hotRecipe">
										<img src="images/hot_recipe.png"/>
									</div>
								<?php
									}
								?>
								<div class="weekdayContent">
									<div class="weekday_title">
										<img src="images/recipe_week_<?=$wd7?>.png"/>
									</div>
									<div class="title">
										<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid7]["id"]?>" style="padding-bottom: 120px;"/>
											<?=$weeklyRecipeInfoList[$rid7]["title"]?>
										</a>
									</div>
									<div class="product_thumb">
										<img src="<?=PATH_TO_ULFILE.$weeklyRecipeInfoList[$rid7]["p_image"]?>"/>
									</div>
								</div>
							</div>
						<!-- mobile wd2 -->
						<div style="clear: both"></div>
						<div class="hideBtn" onclick="hideWeeklyMore();">
							隱藏推介
						</div>
					</div>

					<!-- weekday -->
					<div class="col-lg-3 day_recipe hidden-xs hidden-sm hidden-md">
						<div class="image hoverzoom">
							<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid2]["id"]?>"/>
								<img src="<?=$weeklyRecipeInfoList[$rid2]["image"]?>" style="width:100%;"  alt="<?=$weeklyRecipeInfoList[$rid2]["title"]?>"/>
							</a>
						</div>
						<?php
							if($weeklyRecipeInfoList[$rid2]["isPop"]){
						?>
							<div class="hotRecipe">
								<img src="images/hot_recipe.png"/>
							</div>
						<?php
							}
						?>
						<div class="weekday_title">
							<img src="images/recipe_week_<?=$wd2?>.png" />
						</div>
						<div class="title">
							<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid2]["id"]?>"/>
								<?=$weeklyRecipeInfoList[$rid2]["title"]?>
							</a>
						</div>
						<div class="product_thumb">
							<img src="<?=PATH_TO_ULFILE.$weeklyRecipeInfoList[$rid2]["p_image"]?>"/>
						</div>

					</div>
					<!-- weekday -->

					<!-- weekday -->
					<div class="col-lg-3 day_recipe hidden-xs hidden-sm hidden-md">
						<div class="image hoverzoom">
							<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid3]["id"]?>"/>
								<img src="<?=$weeklyRecipeInfoList[$rid3]["image"]?>" style="width:100%;"  alt="<?=$weeklyRecipeInfoList[$rid3]["title"]?>"/>
							</a>
						</div>
						<?php
							if($weeklyRecipeInfoList[$rid3]["isPop"]){
						?>
							<div class="hotRecipe">
								<img src="images/hot_recipe.png"/>
							</div>
						<?php
							}
						?>

						<div class="weekday_title">
							<img src="images/recipe_week_<?=$wd3?>.png" />
						</div>
						<div class="title">
							<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid3]["id"]?>"/>
								<?=$weeklyRecipeInfoList[$rid3]["title"]?>
							</a>
						</div>
						<div class="product_thumb">
							<img src="<?=PATH_TO_ULFILE.$weeklyRecipeInfoList[$rid3]["p_image"]?>"/>
						</div>

					</div>
					<!-- weekday -->
				</div>

				<div class="row boxDistribute">
					<!-- weekday -->
					<div class="col-lg-3 day_recipe hidden-xs hidden-sm hidden-md">
						<div class="image hoverzoom">
							<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid4]["id"]?>"/>
								<img src="<?=$weeklyRecipeInfoList[$rid4]["image"]?>" style="width:100%;"  alt="<?=$weeklyRecipeInfoList[$rid4]["title"]?>"/>
							</a>
						</div>
						<?php
							if($weeklyRecipeInfoList[$rid4]["isPop"]){
						?>
							<div class="hotRecipe">
								<img src="images/hot_recipe.png"/>
							</div>
						<?php
							}
						?>

						<div class="weekday_title">
							<img src="images/recipe_week_<?=$wd4?>.png" />
						</div>
						<div class="title">
							<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid4]["id"]?>"/>
								<?=$weeklyRecipeInfoList[$rid4]["title"]?>
							</a>
						</div>
						<div class="product_thumb">
							<img src="<?=PATH_TO_ULFILE.$weeklyRecipeInfoList[$rid4]["p_image"]?>"/>
						</div>

					</div>
					<!-- weekday -->
					<!-- weekday -->
					<div class="col-lg-3 day_recipe hidden-xs hidden-sm hidden-md">
						<div class="image hoverzoom">
							<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid5]["id"]?>"/>
								<img src="<?=$weeklyRecipeInfoList[$rid5]["image"]?>" style="width:100%;" alt="<?=$weeklyRecipeInfoList[$rid5]["title"]?>"/>
							</a>
						</div>
						<?php
							if($weeklyRecipeInfoList[$rid5]["isPop"]){
						?>
							<div class="hotRecipe">
								<img src="images/hot_recipe.png"/>
							</div>
						<?php
							}
						?>

						<div class="weekday_title">
							<img src="images/recipe_week_<?=$wd5?>.png" />
						</div>
						<div class="title">
							<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid5]["id"]?>"/>
								<?=$weeklyRecipeInfoList[$rid5]["title"]?>
							</a>
						</div>
						<div class="product_thumb">
							<img src="<?=PATH_TO_ULFILE.$weeklyRecipeInfoList[$rid5]["p_image"]?>"/>
						</div>

					</div>
					<!-- weekday -->
					<!-- weekday -->
					<div class="col-lg-3 day_recipe hidden-xs hidden-sm hidden-md">
						<div class="image hoverzoom">
							<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid6]["id"]?>"/>
								<img src="<?=$weeklyRecipeInfoList[$rid6]["image"]?>" style="width:100%;"  alt="<?=$weeklyRecipeInfoList[$rid6]["title"]?>"/>
							</a>
						</div>
						<?php
							if($weeklyRecipeInfoList[$rid6]["isPop"]){
						?>
							<div class="hotRecipe">
								<img src="images/hot_recipe.png"/>
							</div>
						<?php
							}
						?>

						<div class="weekday_title">
							<img src="images/recipe_week_<?=$wd6?>.png" />
						</div>
						<div class="title">
							<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid6]["id"]?>"/>
								<?=$weeklyRecipeInfoList[$rid6]["title"]?>
							</a>
						</div>
						<div class="product_thumb">
							<img src="<?=PATH_TO_ULFILE.$weeklyRecipeInfoList[$rid6]["p_image"]?>"/>
						</div>

					</div>
					<!-- weekday -->
					<!-- weekday -->
					<div class="col-lg-3 day_recipe hidden-xs hidden-sm hidden-md">
						<div class="image hoverzoom">
							<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid7]["id"]?>"/>
								<img src="<?=$weeklyRecipeInfoList[$rid7]["image"]?>" style="width:100%;"  alt="<?=$weeklyRecipeInfoList[$rid7]["title"]?>"/>
							</a>
						</div>
						<?php
							if($weeklyRecipeInfoList[$rid7]["isPop"]){
						?>
							<div class="hotRecipe">
								<img src="images/hot_recipe.png"/>
							</div>
						<?php
							}
						?>

						<div class="weekday_title">
							<img src="images/recipe_week_<?=$wd7?>.png" />
						</div>
						<div class="title">
							<a href="recipe-detail.php?id=<?=$weeklyRecipeInfoList[$rid7]["id"]?>"/>
								<?=$weeklyRecipeInfoList[$rid7]["title"]?>
							</a>
						</div>
						<div class="product_thumb">
							<img src="<?=PATH_TO_ULFILE.$weeklyRecipeInfoList[$rid7]["p_image"]?>"/>
						</div>

					</div>
					<!-- weekday -->
				</div>
			</div>


			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row lineContainer">
				<div class="line">
					<div><div class="border"></div></div>
				</div>
				<div class="titleImg">
					<img src="images/cookbook.png" alt="下載食譜"/>
				</div>
				<div class="line">
					<div><div class="border"></div></div>
				</div>
			</div>

			<div class="container boxDistribute recipeDownload" >
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<a href="cookbook.php">
						<img src="images/Banner_250ml_582x247.png" style="width: 100%;"/>
					</a>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<a href="cookbook-scs.php">
						<img src="images/Banner_ccs_582x247.png" style="width: 100%;"/>
					</a>
				</div>
			</div>




		</div>
		<?php
			include_once 'inc_footer.php';
		?>
		</div>
	</body>
</html>

<div class="mobile_ingredients_carousel_btn" style="display:none;">
	<img class="left sliderArrowBtns" src="images/mobile_ingredients_left_arrow.png"/>
	<img class="right sliderArrowBtns" src="images/mobile_ingredients_rigft_arrow.png"/>
</div>

<div class="mainSlider_carousel_btns" style="display:none;">
	<img class="left sliderArrowBtns" src="images/arrow_left.png"/>
	<img class="right sliderArrowBtns" src="images/arrow_right.png"/>
</div>

<div class="mainSlider_carousel_mobile_btns" style="display:none;">
	<img class="left sliderArrowBtns" src="images/arrow_left.png"/>
	<img class="right sliderArrowBtns" src="images/arrow_right.png"/>
</div>



<?php
	include_once 'inc_footer_script.php';
?>
<script src="js/jquery.fancybox-media.js"></script>

<script>

	$(window).resize(function() {

		if($(this).width()>= 1200){
// 			$(".mp_cookbook_part").height($(".mainSlider_carousel .owl-stage-outer").height());
		}else if($(this).width()> 750){
// 			$(".mp_cookbook_part").height($(".mainSlider_carousel_mobile .owl-stage-outer").height());
		}

	});


	 $( window ).load(function() {

		var isAndroid = (/android/gi).test(navigator.appVersion);
		var isIDevice = (/iphone|ipad/gi).test(navigator.appVersion);

		var h = $(".mobile_ingredients_carousel .owl-stage").height();
		$(".mobile_ingredients_carousel .sliderArrowBtns").css("height", h -(isIDevice ? h * 0.3  : 0));

		$(".mobile_ingredients_carousel .owl-prev,.mobile_ingredients_carousel .owl-next").css("margin-top", -1*h + (isIDevice ? h *0.3  : 0));

    });
	 window.addEventListener("resize", function() {
		 setTimeout(function () {
			 var isAndroid = (/android/gi).test(navigator.appVersion);
				var isIDevice = (/iphone|ipad/gi).test(navigator.appVersion);

				var h = $(".mobile_ingredients_carousel .owl-stage").height();
				$(".mobile_ingredients_carousel .sliderArrowBtns").css("height", h );

				$(".mobile_ingredients_carousel .owl-prev,.mobile_ingredients_carousel .owl-next").css("margin-top", -1*h);

		    }, 1000);
		}, false);

	$(function(){

		$('.mainSlider_carousel').owlCarousel({
			nav:true,
			items : 1,
			loop:true,
			dots:true,
			dotsEach:1,
			autoplay:true,
			autoplayTimeout:5000,
			lazyLoad:true,
			navText:[$(".mainSlider_carousel_btns>.left"), $(".mainSlider_carousel_btns>.right")],
		});



		$('.mainSlider_carousel_mobile').owlCarousel({
			nav:true,
			items : 1,
			loop:true,
			dots:true,
			dotsEach:1,
			autoplay:true,
			autoplayTimeout:5000,
			lazyLoad:true,
			navText:[$(".mainSlider_carousel_mobile_btns>.left"), $(".mainSlider_carousel_mobile_btns>.right")],
		});


		$('.mobile_ingredients_carousel').owlCarousel({

			nav:true,
			items : 3,
			loop:true,
			dots:false,
			margin:16,
			navText:[$(".mobile_ingredients_carousel_btn>.left"), $(".mobile_ingredients_carousel_btn>.right")],
		});


		$('.mobile_ingredients_carousel').on('resized.owl.carousel',function(e){

			var isAndroid = (/android/gi).test(navigator.appVersion);
			var isIDevice = (/iphone|ipad/gi).test(navigator.appVersion);

			 var h = $(".mobile_ingredients_carousel .owl-stage").height();
			 $(".mobile_ingredients_carousel .sliderArrowBtns").css("height", h -(isIDevice ? h * 0.3  : 0));

			$(".mobile_ingredients_carousel .owl-prev,.mobile_ingredients_carousel .owl-next").css("margin-top", -1*h + (isIDevice ? h *0.3  : 0));


		});


	});


	function showWeeklyMore(){

		$(".weekly_more .day_recipe").each(function(i,v){

			$(v).slideDown(i*200);

		});

		$(".weekly_more .moreBtn").hide();
		$(".weekly_more .hideBtn").show();
	}


	function hideWeeklyMore(){
		$(".weekly_more .day_recipe").each(function(i,v){

			$(v).slideUp(i*100);

		});

		$(".weekly_more .moreBtn").show();
		$(".weekly_more .hideBtn").hide();
		$('html, body').animate({
	        scrollTop: $("#weeklySuggestionHeader").offset().top - $(".navbar ").height()
	    }, 400, "easeOutExpo");
	}

	$(function(){


		var isAndroid = (/android/gi).test(navigator.appVersion);
	    var isIDevice = (/iphone|ipad/gi).test(navigator.appVersion);

		    if (isAndroid || isIDevice || $(window).width()<768) {

	    }else{

	    	$('.fancybox-media')
				.fancybox({
					openEffect : 'fade',
					closeEffect : 'fade',
					prevEffect : 'fade',
					nextEffect : 'fade',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
			});
	  	}



		$("#searchRecipeImg").on("click", function(){

			var val = $("#searchRecipeTxt").val();
			if(val != ""){
				window.location = "recipe-list.php?k=" + val;
			}

		});


		$("#searchRecipeTxt").keyup(function (e) {
		    if (e.keyCode == 13) {
		    	var val = $("#searchRecipeTxt").val();
				if(val != ""){
					window.location = "recipe-list.php?k=" + val;
				}
		    }
		});
	});


</script>

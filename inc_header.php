<nav class = "navbar navbar-default" role = "navigation" style="">
   <div class="row " >
	   <div class = "navbar-header">
	      <button type = "button" class = "navbar-toggle" style="margin-top:20px;margin-bottom:24px;z-index: 5;"

	      	 id="menu-toggle"

	         data-toggle = "collapse" >
	         <span class = "sr-only">Toggle navigation</span>
	         <span class = "icon-bar"></span>
	         <span class = "icon-bar"></span>
	         <span class = "icon-bar"></span>
	      </button>



			<a class="navbar-brand" rel="home"  title="" style="position:relative;">
		        <div>
			        <img src="<?=ROOT_PATH?>images/logo.png">
		        </div>
		        <div class="hidden-xs">
		        	<img src="<?=ROOT_PATH?>images/logo_btm_bg.png" style="position: absolute;margin:0px;">
		        </div>
		    </a>
	   </div>

	   <div class = "collapse navbar-collapse"  id ="tecm-navbar-collapse" style="padding:0px;position:relative">

			<div class="hidden-xs navBtnContainer" style="padding: 15px 0px 0px 0px;text-align: right;">
				<a class="" href="https://www.youtube.com/channel/UCAQGuP8ViNiIZIb8CcDQ3nQ/videos" target="_blank" style="margin-right:8px;">
					<img src="<?=ROOT_PATH?>images/youtubeBtn.png" />
				</a>

				<a onclick ="sharePage()" style="margin-right:1px;cursor:pointer;">
					<img src="<?=ROOT_PATH?>images/facebookBtn.png" />
				</a>
			</div>

	      	<ul class = "nav navbar-nav">

		      	 <li ><a href="<?=ROOT_PATH?>index.php">主頁</a></li>

				 <li><a href="<?=ROOT_PATH?>recipe-list.php">食譜搜尋</a></li>
				 <li><a href="<?=ROOT_PATH?>recipe-video-tsui.php" >鮮味炒教室</a></li>


				 <li><a href="<?=ROOT_PATH?>recipe-video.php">入廚教學短片</a></li>
				 <li><a href="<?=ROOT_PATH?>product-list.php">史雲生系列</a></li>

				  <li class="dropdown ">
		         	<a class="dropdown-toggle" data-toggle="dropdown" >
		         		食譜下載<span class="caret"></span>
		         	</a>
		         	<ul class="dropdown-menu">
						<li><a href="<?=ROOT_PATH?>cookbook.php">1盒煮1餸 簡易食譜</a></li>
						<li><a href="<?=ROOT_PATH?>cookbook-scs.php">調味雞汁鮮味炒食譜</a></li>
					</ul>
				</li>

				 <li><a href="https://docs.google.com/forms/d/e/1FAIpQLSfEQ5Zj9McgBkJweKYRy7eF7IplUTScDRqdi4MHmCPciCTMOg/viewform?usp=sf_link" target="_blank">有獎問卷</a></li>
				 <li><a href="<?=ROOT_PATH?>event_list.php">鮮味炒試食地點</a></li>
	      	</ul>
	   </div>
   </div>
</nav>


<div style="clear: both"></div>

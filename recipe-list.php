<?php

	include_once 'inc_top_script.php';
	$captialize_alpha_pattern = '^[A-Z]+$';
	$numeric_only_pattern = '^[0-9]+$';
	if(!empty($_REQUEST["i"]) && !is_numeric($_REQUEST["i"]) ){
// 		$_REQUEST["i"] = null;
		unset($_REQUEST["i"]);
	}


	if(!empty($_REQUEST["t"]) && !is_numeric($_REQUEST["t"]) ){
// 		$_REQUEST["t"] = null;
		unset($_REQUEST["t"]);
	}


	if(!empty($_REQUEST["p"]) && preg_match($numeric_only_pattern , $_REQUEST["p"])){
// 		$_REQUEST["p"] = null;
		unset($_REQUEST["p"]);
	}

	if(!empty($_REQUEST["c"]) && preg_match($captialize_alpha_pattern , $_REQUEST["c"])){
// 		$_REQUEST["c"] = null;
		unset($_REQUEST["c"]);
	}




	if(!empty($_REQUEST["k"]) && htmlspecialchars($_REQUEST["k"], ENT_QUOTES) != $_REQUEST["k"] ){
		$_REQUEST["k"] = htmlspecialchars($_REQUEST["k"], ENT_QUOTES);
	}


?>

<!DOCTYPE html>
<html>
	<head>

		<?php
			define(ITEM_CNT, 18);
			$selectedIngredient = $_REQUEST["i"];
			$selectedTheme = $_REQUEST["t"];
			$selectedProduct = $_REQUEST["p"];
			$selectedCategory = $_REQUEST["c"];
			$selectedKeyword = $_REQUEST["k"];
			$selectedMethod = $_REQUEST["m"];

			$isAdv = array_key_exists("a", $_GET);
			include_once 'include_script.php';

		?>


		<style>
			.lazy, .recipeContainer{
				transition: initial !important;
			}


		</style>

		<?php
			include_once 'inc_sharing.php';
		?>

		<title>史雲生食譜搜尋 </title>
		<meta name="keywords" content="史雲生食譜,今晚煮乜餸,火鍋推介,新手菜式,粥粉麵飯,家常小菜,節慶菜式,搜尋食譜,人氣食譜,今晚食乜餸">
		<meta name="description" content="史雲生助你輕易煮出各款美味家常菜。全新食譜搜尋器，讓你輕鬆找到簡單、美味的心水食譜；每日精選菜式推介，幫你輕鬆加餸。">
		<style>
			.collapsibleContent{
				position:relative;
			}
			.desktopLoadingBlock{
				display:none;
				position:absolute;
				background-color: rgba(225,225,225,0.5);
				width:100%;
				height:100%;
				left:0px;
				top:0px;
			}
			.desktopLoadingBlock>div{
				display:table;
				height:100%;
				width:100%;
			}
			.desktopLoadingBlock>div>div{
				display:table-cell;
				vertical-align:middle;
				text-align: center;
			}
			.desktopLoadingBlock>div>div>img{
				width:40px;
			}

			li.notInGroup.loading, li.inGroup.loading{
				background-color: rgb(225,225,225);
  				opacity: 0.5;
			}
			.mobileLoading{
				display:none;
				position:absolute;
				right:0px;
			  padding: 8px;
				top:0px;
			}
			.mobileLoading img{
				width:30px;
			}

	/*		div.recipe-detail-2-product > div.main > img{
				width: 80% !important;
				margin-top: 5px;
			}


			img[src*="1516778519"] {
				margin-top: 9px;
				margin-left: 5px;
				 margin-top: -8px;
				max-height: 110px !important;
				max-width: 110px !important;

			}
*/

	/*
			img[src*="1515059642"] {
				margin-top: 12px;
				margin-left: 8px;

			}

			.recipe-detail-2-product div.small img[src*="1516774081"]{
				max-height: 70px;
				margin-top: 20px;
				margin-left: 14px;

			}
			.recipe-detail-2-product > div.main img[src*="1507858435"] {
				max-height: 102px;
				margin-top: -6px;
				max-width: 105px;
    		margin-left: 5px;
			}

			.recipe-detail-2-product>div:last-child img[src*="1516778519"] {
				max-height: 105px !important;
				max-width: 105px;
				margin-top: -3px;
				margin-left: 2px;
			}

	*/

	}
			 @media (min-width: 375px){
				 .recipe-detail-2-product>div:last-child img[src*="1515059542"] {
					 margin-top: 5px;
				 }

			   }





		 @media (min-width: 768px){
			.recipe-detail-2-product div.small img {
			    max-height: 84px;
			}
			/*.recipe-detail-2-product>div:last-child img[src*="1515059642"] {
		  		margin-top: 6px;
		   }*/

			/*.recipeItem>div .productIcon>div img[src*="1515059642"] {
				margin-top: 6px;
			} */


	 		}



		 }
		 @media (min-width: 1200px){

    		.recipe-detail-2-product div.small {
    				top: 0px;
			}
			.recipe-detail-2-product>div:last-child img[src*="1516774012"] {
  				margin-left: 5px;
			}
	 		.recipe-detail-2-product>div:last-child img[src*="1515059642"] {
	 			margin-top: 13px !important;
	 		}
	 		.recipe-detail-2-product>div:last-child img[src*="1507858435"] {
	 			margin-top: 5px;
	 		}
			.recipe-detail-2-product>div:last-child img[src*="1515059542"] {
	 			margin-top: 15px;
	 		}






 }

		</style>
	</head>

	<body>
		<?php
			include_once 'inc_beginbody_script.php';
		?>

		 <div id="wrapper" class="">
	        <?php
	        	include_once 'inc_sidebar.php';
	        ?>
			<div class="mainContainer "  id="page-content-wrapper" >
				<div style="position:relative;">
				<?php
					include_once 'inc_header.php';
				?>

				<div class="breadcrumb" style="background-color:#f1dc00">
					<div class="container swansonBreadcrumb" >
						<img src="images/breadcrumb_home_icon.png"/>&nbsp;<a href="index.php">主頁</a>

						<span class="glyphicon glyphicon-menu-right " aria-hidden="true" ></span>
						食譜搜尋
					</div>

				</div>


				<div class="container" style="min-height:800px;">


					<div class="hidden-lg hidden-md hidden-sm col-xs-12 row menuContainer_m">

						<div class="hidden-lg hidden-md hidden-sm col-xs-12 searchTool">
							<div style="font-size:24px;padding-bottom:5px;">食譜搜索</div>
							<div>

								<div style="width:79%;display:inline-block;">
									<input type="text" class=" form-control" id="searchRecipeTxt2" value="">
								</div>
								<div style="width:19%;float:right;text-align:right;">
									<a href="#">
										<img src="images/search_icon.png" id="searchRecipeImg" style="text-align:center;max-width:33px;width:100%;margin:1px 11px;">
									</a>
								</div>
							</div>
						</div>


						<select id="ingredients-menu" multiple="multiple" class="form-control recipeListMenu">
						    <option name="ingredients" value="1" >豬 </option>
						    <option name="ingredients" value="3" >雞  </option>
						    <option name="ingredients" value="2" >牛  </option>
						    <option name="ingredients" value="4" >其他肉類 </option>
						    <option name="ingredients" value="5" >蛋及豆腐 </option>
						    <option name="ingredients" value="6" >海鮮 </option>
						    <option name="ingredients" value="7" > 菜及菇類  </option>

						</select>

						<select id="theme-menu" multiple="multiple" class="form-control recipeListMenu">
							<option name="theme" value="1" >新手菜式</option>
							<option name="theme" value="2" >家常小菜</option>
							<option name="theme" value="3" >節慶菜式 </option>
							<option name="theme" value="4" >粥粉麵飯 </option>
							<option name="theme" value="5" >火鍋推介</option>
							<option name="theme" value="6" >輕盈健康浸菜 </option>
							<option name="theme" value="7" >湯/羹 </option>
						</select>


						<select id="product-menu" multiple="multiple" class="form-control recipeListMenu product-menu">
							<optgroup data-name="productCat" label='清雞湯' value="CCB">
							    <option name="product" value="3" >清雞湯1L</option>
							    <option name="product" value="1" >清雞湯250mL </option>
							    <option name="product" value="2" >清雞湯500mL </option>
							</optgroup>
					    	<option name="product" value="4" >瑤柱上湯250mL </option>
						    <option name="product" value="6" >金華火腿上湯250mL</option>
						    <optgroup data-name="productCat" label='豬骨湯' value="PBB">
							    <option name="product" value="7" >豬骨湯1L</option>
							    <option name="product" value="5">豬骨湯500mL</option>
						    </optgroup>

						    <option name="product" value="8" >鮮魚湯500mL</option>
						    <option name="product" value="9,10,11">調味雞汁</option>
						</select>




						<select id="method-menu" multiple="multiple" class="form-control recipeListMenu">
							<option name="method" value="1" >煎</option>
							<option name="method" value="2" >炒</option>
							<option name="method" value="3" >蒸</option>
							<option name="method" value="4" >炆</option>
							<option name="method" value="6" >燴</option>
							<option name="method" value="7" >湯浸</option>
							<option name="method" value="8" >煮</option>
							<option name="method" value="9" >煲/火鍋</option>
							<option name="method" value="10">其他</option>
						</select>

					</div>



					<div class="col-lg-3 col-md-3 col-sm-4 hidden-xs" style="padding-right:16px;">

						<div id="leftMenu">

							<div class="searchTool">

								<div style="font-size:25px;padding-bottom:5px;">食譜搜索</div>
								<div>

									<div style="width:79%;display:inline-block;">
										<input type="text" class=" form-control" id="searchRecipeTxt" value="<?=$selectedKeyword?>"  style="font-weight:normal;"/>
									</div>
									<div style="width:19%;float:right;text-align:right;">
										<a href="#">
											<img src="images/search_icon.png" id="searchRecipeImg" style="text-align:center;max-width:33px;width:100%;margin:1px 11px;"/>
										</a>
									</div>
								</div>
							</div>


							<div>

								<div class="title collapsible active">
									<div >
										所有食材
									</div>
									<div >
										<span class="glyphicon glyphicon-chevron-down activeIcon" aria-hidden="true"></span>
										<span class="glyphicon glyphicon-chevron-right inActiveIcon" aria-hidden="true" ></span>
									</div>
								</div>
								<div class="collapsibleContent">
									<div class="desktopLoadingBlock">
										<div>
											<div>
												<img src="images/loading_icon.gif"/>
											</div>
										</div>
									</div>
									<div>
										<label>
											<input type="checkbox" name="ingredients" value="1" data-label="豬"/>豬
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="ingredients" value="3" data-label="雞 " />雞
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="ingredients" value="2" data-label="牛" />牛
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="ingredients" value="4" data-label="其他肉類"/>其他肉類
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="ingredients" value="5"  data-label="蛋及豆腐"/>蛋及豆腐
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="ingredients" value="6"  data-label="海鮮"/>海鮮
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="ingredients" value="7" data-label="菜及菇類 " />菜及菇類
										</label>
									</div>


						  		</div>
					  		</div>

					  		<div>
						  		<div class="title collapsible active">
									<div >
										菜式
									</div>
									<div >
										<span class="glyphicon glyphicon-chevron-down activeIcon" aria-hidden="true"></span>
										<span class="glyphicon glyphicon-chevron-right inActiveIcon" aria-hidden="true" ></span>
									</div>
								</div>
								<div class="collapsibleContent">
									<div class="desktopLoadingBlock">
										<div>
											<div>
												<img src="images/loading_icon.gif"/>
											</div>
										</div>
									</div>
										<label>
											<input type="checkbox" name="theme" value="1" data-label="新手菜式"/>新手菜式
										</label>

									<div>
										<label>
											<input type="checkbox" name="theme" value="2" data-label="家常小菜" />家常小菜
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="theme" value="3" data-label="節慶菜式"/>節慶菜式
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="theme" value="4" data-label="粥粉麵飯" />粥粉麵飯
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="theme" value="5" data-label="火鍋推介"/>火鍋推介
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="theme" value="6"  data-label="輕盈健康浸菜"/>輕盈健康浸菜
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="theme" value="7" data-label="湯/羹" />湯/羹
										</label>
									</div>
						  		</div>
					  		</div>






					  		<div>
						  		<div class="title collapsible active">
									<div >
										史雲生系列
									</div>
									<div >
										<span class="glyphicon glyphicon-chevron-down activeIcon" aria-hidden="true"></span>
										<span class="glyphicon glyphicon-chevron-right inActiveIcon" aria-hidden="true" ></span>
									</div>
								</div>
								<div class="collapsibleContent">

									<div class="desktopLoadingBlock">
										<div>
											<div>
												<img src="images/loading_icon.gif"/>
											</div>
										</div>
									</div>

									<div class="checkboxGroup">
										<div class="checkboxGroupTitle">
											<span class="checkboxInput"><input type="checkbox" value="CCB" name="productCat"/>  <span style="margin-left:-5px">清雞湯</span></span>
											<span class="glyphicon glyphicon-chevron-down arrow " aria-hidden="true"  style="display:none"></span>
											<span class="glyphicon glyphicon-chevron-right arrow shown" aria-hidden="true" ></span>
										</div>
										<div class="checkboxGroupContent"  style="display:none">

											<div>
												<label>
													<input type="checkbox" name="product" value="1" data-label="清雞湯250mL"/>清雞湯250mL
												</label>
											</div>
											<div>
												<label>
													<input type="checkbox" name="product" value="2"  data-label="清雞湯500mL"/>清雞湯500mL
												</label>
											</div>
											<div>
												<label>
													<input type="checkbox" name="product" value="3" data-label="清雞湯1L"/>清雞湯1L
												</label>
											</div>
										</div>
									</div>




									<div>
										<label>
											<input type="checkbox" name="product" value="4" data-label="瑤柱上湯250mL"/>瑤柱上湯250mL
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="product" value="6"  data-label="金華火腿上湯250mL" />金華火腿上湯250mL
										</label>
									</div>


									<div class="checkboxGroup">
										<div class="checkboxGroupTitle">
											<span class="checkboxInput"><input type="checkbox" value="PBB" name="productCat"/> <span style="margin-left:-5px">豬骨湯</span></span>
											<span class="glyphicon glyphicon-chevron-down arrow " aria-hidden="true"  style="display:none"></span>
											<span class="glyphicon glyphicon-chevron-right arrow shown" aria-hidden="true" ></span>
										</div>
										<div class="checkboxGroupContent"  style="display:none">
											<div>
												<label>
													<input type="checkbox" name="product" value="5"  data-label="豬骨湯500mL" />豬骨湯500mL
												</label>
											</div>

											<div>
												<label>
													<input type="checkbox" name="product" value="7" data-label="豬骨湯1L"/>豬骨湯1L
												</label>
											</div>

										</div>
									</div>






									<div>
										<label>
											<input type="checkbox" name="product" value="8"   data-label="鮮魚湯500mL"/>鮮魚湯500mL
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="product" value="9,10,11" data-label="調味雞汁"/>調味雞汁
										</label>
									</div>
						  		</div>
						  	</div>


					  		<div>
						  		<div class="title collapsible active">
									<div >
										煮法
									</div>
									<div >
										<span class="glyphicon glyphicon-chevron-down activeIcon" aria-hidden="true"></span>
										<span class="glyphicon glyphicon-chevron-right inActiveIcon" aria-hidden="true" ></span>
									</div>
								</div>
								<div class="collapsibleContent">
									<div class="desktopLoadingBlock">
										<div>
											<div>
												<img src="images/loading_icon.gif"/>
											</div>
										</div>
									</div>
										<label>
											<input type="checkbox" name="method" value="1" data-label="煎"/>煎
										</label>

									<div>
										<label>
											<input type="checkbox" name="method" value="2"  data-label="炒"/>炒
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="method" value="3" data-label="蒸"/>蒸
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="method" value="4" data-label="炆" />炆
										</label>
									</div>


									<div>
										<label>
											<input type="checkbox" name="method" value="6" data-label="燴" />燴
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="method" value="7" data-label="湯浸" />湯浸
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="method" value="8" data-label="煮" />煮
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="method" value="9" data-label="煲/火鍋" />煲/火鍋
										</label>
									</div>

									<div>
										<label>
											<input type="checkbox" name="method" value="10" data-label="其他" />其他
										</label>
									</div>
						  		</div>
					  		</div>




						  	<BR/>

						</div>

					</div>

					<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 " style="padding-bottom:40px;">

						<?php

							$rRs = $pdo -> prepare("select SQL_CALC_FOUND_ROWS distinct r.id, r.title, r.image,r.isPopular, p.product_key, p.image_thumb, r.product_id, r.product_id2, r.display_priority  from recipe r left join product p on r.product_id = p.id where r.isHide = 'N' order by r.display_priority desc limit ".ITEM_CNT);
							$rRs->execute();
							$totalItems = $pdo->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
						?>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 listResult"  >
							<span style="color:#888;font-weight:bold;padding-left: 4px;font-size: 18px;">搜尋結果 :</span>
							<span style="color:#0f847b;font-weight:bold;font-size: 19px;">共 <span id="totalItemsCount"><?=$totalItems?></span> 款食譜</span>
						</div>

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="loading"    style="text-align: center;padding:100px 0px;">
							<img src="images/loading_icon.gif"/>
						</div>
						<div class="recipeContainer" >

							<?php

								if(empty($selectedIngredient) && empty($selectedKeyword) && empty($selectedTheme) && empty($selectedMethod) && empty($selectedProduct) ){

									while($rRs_row = $rRs->fetch()){
										$rId = $rRs_row["id"];
										$title = $rRs_row["title"];
										$image = empty($rRs_row["image"])? "images/empty_recipe.png" : PATH_TO_ULFILE.$rRs_row["image"];
										$productImage = $rRs_row["image_thumb"];

										$productId1 =  $rRs_row["product_id"];
										$isPopular = $rRs_row["isPopular"] == "Y";


										$productId2 = $rRs_row["product_id2"];

										if(!empty($productId2)){
											$productRs2 = $pdo->prepare("select id,  image_thumb, image_thumb2, image_thumb3  from `product` where id = :pid2");
											$productRs2 -> bindValue("pid2", $productId2, PDO::PARAM_INT);
											$productRs2 ->execute();
											$productRs2_row = $productRs2 ->fetch();
											$product2Image3 = $productRs2_row["image_thumb3"];
										}

							?>
									<div class="recipeItem col-lg-4 col-md-4 col-sm-6 col-xs-6 lazy" data-ga-recipetitle="<?=$title?>">
										<div>
											<div class="image hoverzoom">
												<a href="recipe-detail.php?id=<?=$rId?>">
													<img src="<?=$image?>"  alt="<?=$title?>"/>
												</a>
											</div>
											<?php
												if($isPopular){
											?>
												<div class="hotRecipe">
													<img src="images/hot_recipe.png"/>
												</div>
											<?php
												}
											?>

											<a href="recipe-detail.php?id=<?=$rId?>">
												<div class="caption">
													<div class="title">
														<?=$title?>
													</div>
													<div class="productIcon">

														<?php
															if(!empty($productId2)){
																$isP1Small = false;
																$isP2Small = false;

																//only restrict to 250mL Chicken & any CCS
																if( (in_array("1", explode(";", $productId1)) || in_array("4", explode(";", $productId1))) && ( in_array("9", explode(";", $productId2)) || in_array("10", explode(";", $productId2)) || in_array("11", explode(";", $productId2)))   ){
																	$isP1Small = true;
																}else if( (in_array("1", explode(";", $productId2)) || in_array("4", explode(";", $productId2)) )  && ( in_array("9", explode(";", $productId1)) || in_array("10", explode(";", $productId1)) || in_array("11", explode(";", $productId1)))   ){
																	$isP2Small = true;
																}



														?>
															<div class="recipe-detail-2-product">
																<div class="<?=$isP1Small ? "small" :""?> main">
																	<img src="<?=PATH_TO_ULFILE.$productImage?>"/>
																</div>
																<div class="<?=$isP2Small ? "small" :""?>"  >
																	<img src="<?=PATH_TO_ULFILE.$product2Image3?>"/>
																</div>
															</div>
														<?php
															}else{
														?>
															<div>
																<img src="<?=PATH_TO_ULFILE.$productImage?>"/>
															</div>
														<?php
															}
														?>

													</div>
												</div>
											</a>
											<div style="clear: both"></div>
										</div>
									</div>




							<?php
									}
								}
							?>
						</div>
						<div style="clear:both">
						</div>
						<div class="pagingContainer">
							<div class="left pageItem" onclick="prevPage()">
								<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							</div>

							<div class="pageItemContainer">

							</div>
							<div class="right pageItem" onclick="nextPage()">
								<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							</div>
						</div>




					</div>


				</div>
			</div>
			<?php
				include_once 'inc_footer.php';
			?>
		</div>
	</body>
</html>
<?php
	include_once 'inc_footer_script.php';
?>
<style media="screen">

</style>

<script src="js/bootstrap-multiselect.js"></script>
<script>

	var isTriggerAdd = false;
	var isRecipeEnd = false;
	var currentPage = 1;
	var maxPage;

	function scrollToCount(){

		var isAndroid = (/android/gi).test(navigator.appVersion);
	    var isIDevice = (/iphone|ipad/gi).test(navigator.appVersion);

	    if (isAndroid || isIDevice || $(window).width()< 768) {
	       offset = $(".navbar-brand>div").height()+ 10;
	    }else{
		   offset = 0;
	  	}



		 $('html, body').animate({
	        scrollTop: $("#totalItemsCount").offset().top -offset
	    }, 300, "easeOutExpo");
	}

	function nextPage(){
		currentPage+=1;

		if(currentPage > maxPage){

		}else{
			showLoading();
			$(".pagingContainer").fadeOut(300);
			$(".recipeContainer").fadeOut(300, function(){
				$(this).html("").show();
				getNewRecipe();
				scrollToCount();

				ga("send", "pageview", "/"+currentPage);

// 				track.send("RECIPE_LIST", "VIEW", "P"+currentPage);
			});
		}

	}

	function prevPage(){
		currentPage-=1;
		if(currentPage <= 0){

		}else{
			showLoading();
			$(".pagingContainer").fadeOut(300);
			$(".recipeContainer").fadeOut(300, function(){
				$(this).html("").show();
				getNewRecipe();
				scrollToCount();
ga("send", "pageview", "/"+currentPage);
// 				track.send("RECIPE_LIST", "VIEW", "P"+currentPage);
			});
		}
	}

	function navToPage(num){

		if(num != currentPage){
			currentPage = num;
			showLoading();
			$(".pagingContainer").fadeOut(300);
			$(".recipeContainer").fadeOut(300, function(){
				$(this).html("").show();
				getNewRecipe();
				scrollToCount();

// 				track.send("RECIPE_LIST", "VIEW", "P"+currentPage);
			});
		}

	}

	function updatePageNum(total){

		$(".pageItemContainer").html("");

		var isPrintPrefixDot = false;
		var isPrintPostfixDot = false;

		var toShow = [1, currentPage-1, currentPage, currentPage+1, Math.ceil(total / <?=ITEM_CNT?>)];

		maxPage =  Math.ceil(total / <?=ITEM_CNT?>);
		var pageNum = 1;
		var cnt = 0;
		while(pageNum <= Math.ceil(total / <?=ITEM_CNT?>)){


			if(toShow.indexOf(pageNum) != -1){

				var div = $("<div>").addClass("pageItem").data("num", pageNum).html(pageNum);

				if(currentPage ==pageNum){
					div.addClass("current");

				}
				div.click(function(e){
					var num = $(e.target).data("num");
					navToPage(num);
				});

				$(".pageItemContainer").append(div);
			}else{

				if(pageNum < currentPage && !isPrintPrefixDot){
					var s = $("<span>").html("...");
					$(".pageItemContainer").append(s);
					isPrintPrefixDot = true;
				}else if(pageNum > currentPage && !isPrintPostfixDot){
					var s = $("<span>").html("...");
					$(".pageItemContainer").append(s);
					isPrintPostfixDot =true;
				}
			}

			pageNum++;
			cnt++;
		}

		if(currentPage == 1){
			$(".pagingContainer .pageItem.left").hide();
		}else{
			$(".pagingContainer .pageItem.left").show();
		}

		if(currentPage == maxPage){
			$(".pagingContainer .pageItem.right").hide();
		}else{
			$(".pagingContainer .pageItem.right").show();
		}


		if(total>0){
			$(".pagingContainer").fadeIn(300);
		}


	}

	function refreshResult(){
		isTriggerAdd = false;
		isRecipeEnd = false;
		currentPage = 1;
		showLoading();
		$("#totalItemsCount").html("-");
		$(".pagingContainer").fadeOut(300);
		$(".recipeContainer").fadeOut(300, function(){

			$(this).html("").show();

			getNewRecipe();


// 		    $('html, body').animate({
// 		        scrollTop: $("#totalItemsCount").offset().top
// 		    }, 400, "easeOutExpo");
		});

	}

	$(function() {


		$("#loading").hide();

		updatePageNum(<?=$totalItems?>);

		$('#ingredients-menu').multiselect({
        	nonSelectedText:"所有食材",
        	nSelectedText: '項',
        	numberDisplayed:10,
        	allSelectedText: null,
			checkboxName:"ingredients",
	    });
        $('#theme-menu').multiselect({
        	nonSelectedText:"菜式",
        	nSelectedText: '項',
        	numberDisplayed:10,
        	allSelectedText: null,
			checkboxName:"theme",
		});
        $('#product-menu').multiselect({
        	nonSelectedText:"史雲生系列",
        	nSelectedText: '項',
        	numberDisplayed:10,
        	allSelectedText: null,
//         	selectableOptgroup: true,
        	enableClickableOptGroups: true,
            enableCollapsibleOptGroups: true,
            onDropdownShown: function(){
// 				$(".multiselect-group .caret-container .caret").toggleClass("active");
				$(".caret-container:not(.active)").click();
            },
            onDropdownHidden: function(){
// 				$(".multiselect-group .caret-container .caret").toggleClass("active");
            },
			checkboxName:"product",
			multiple:true,

	    });
        $('#method-menu').multiselect({
        	nonSelectedText:"煮法",
        	nSelectedText: '項',
        	numberDisplayed:10,
        	allSelectedText: null,
			checkboxName:"method",
		});


		$(".caret-container").click(function(e){
			$(this).toggleClass("active");
		});

		$(".multiselect-group label b").click(function(){
			$(this).parent().parent().find(".caret-container").click();
 		});

		$(".multiselect-container").each(function(i,v){$("li:first",v).before('<div class="mobileLoading"><img src="images/loading_icon.gif"/></div>')});




	    $(".checkboxGroup .checkboxGroupTitle .arrow").click(function(e){
			$(e.target).parent().parent().find(".checkboxGroupContent").slideToggle(300);
			$(e.target).parent().find(".arrow").addClass("shown").show();
			$(e.target).hide().removeClass("shown");
		});

	    $(".checkboxGroup .checkboxGroupTitle .checkboxInput span").click(function(e){
			$(this).parent().parent().find("input").click();
			$(this).parent().parent().find(".arrow.shown").click();
		});

 	    $(".checkboxGroup .checkboxGroupTitle input").click(function(e){
 			var t = $(e.target).prop("checked");
			var catValue = $(e.target).val();




			var list = [];
			var vName = $(e.target).parent().parent().parent().find(".checkboxGroupContent input[type=checkbox]:eq(0)").attr("name");


 	    	$(e.target).parent().parent().parent().find(".checkboxGroupContent input[type=checkbox]").each(function(i,v){
 				$(v).prop("checked", t);

				var v_v = $(v).val();
				if(t){
					list.push(v_v);
				}

				var v_name = $(v).attr("name");
				$("[name="+v_name+"][value="+v_v+"]").prop("checked", t).prop("selected", t);


 		    });

			$('#'+vName+'-menu').val(list);
			$('#'+vName+'-menu').multiselect('refresh');
			$("[value="+catValue+"]").prop("checked",t);
 		});

// 	    $(".checkboxGroup .checkboxGroupTitle .checkboxInput").click(function(e){
// 		    e.stopPropagation();

// 			var t = $(e.target).find("input").prop("checked");
// 			$(e.target).find("input").prop("checked", !t);
// 	    	$(e.target).parent().parent().find(".checkboxGroupContent input[type=checkbox]").each(function(i,v){
// 				$(v).prop("checked", !t);
// 		    });

// 	    	refreshResult();
// 		});
		$("[type=checkbox]").on("change", function(e){

			var v = $(e.target).val();
			var name = $(e.target).attr("name");
			var isChecked =  $(e.target).prop("checked");

			$("[name="+name+"][value='"+v+"']").prop("checked", isChecked).prop("selected", isChecked);

			$('#'+name+'-menu').multiselect('refresh');

			refreshResult();
		});
		$("#searchRecipeTxt").on("change", function(){
			refreshResult();
		});
		$("#searchRecipeTxt2").on("change", function(){
			refreshResult();
		});
		$("#searchRecipeImg").on("click", function(){
			refreshResult();
		});

		$(".collapsible").click(function(e){

			if($(this).hasClass("active")){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}

			$(this).parent().find(".collapsibleContent").slideToggle(300);

		});
/*
		$(window).on('scroll', function() {

	        if( !isTriggerAdd && !isRecipeEnd &&  $(this).scrollTop() + $(this).height() -  $("#endoflist").offset().top >= $(window).height()*0 && $("#endoflist").offset().top > $(window).height()) {
	        	isTriggerAdd = true;

				getNewRecipe();


	        }

	    })
*/
		<?php

			if(!empty($selectedIngredient) || !empty($selectedKeyword)  || !empty($selectedTheme) || !empty($selectedProduct) || !empty($selectedMethod) || !empty($selectedCategory)){
		?>
			refreshResult();
		<?php
			}
		?>

		<?php
			if(!empty($selectedCategory)){
		?>


		<?php

			if($selectedCategory == "CCB"){
		?>
			$('#product-menu').val(["1", "2", "3"]);
			$("[name=product][value=1]").prop("checked", true);
			$("[name=product][value=2]").prop("checked", true);
			$("[name=product][value=3]").prop("checked", true);

		<?php
			}else if($selectedCategory == "PBB"){
		?>
			$('#product-menu').val(["5", "7"]);
			$("[name=product][value=5]").prop("checked", true);
			$("[name=product][value=7]").prop("checked", true);
		<?php
			}
		?>
			$('#product-menu').multiselect('refresh');
			$("[value='<?=$selectedCategory?>']").prop("checked", true);
		<?php
			}
		?>


		<?php
			if(!empty($selectedIngredient)){
		?>
			$("input[name=ingredients][value='<?=$selectedIngredient?>']").prop("checked", true);
			$('#ingredients-menu').val('<?=$selectedIngredient?>');
			$("#ingredients-menu + div input[type=checkbox][value='<?=$selectedIngredient?>']").prop("checked",true);
			$('#ingredients-menu').multiselect('refresh');
		<?php
			}
		?>



		<?php
			if(!empty($selectedTheme)){
		?>
			$("input[name=theme][value='<?=$selectedTheme?>']").prop("checked", true);
			$('#theme-menu').val('<?=$selectedTheme?>');
			$("#theme-menu + div input[type=checkbox][value='<?=$selectedTheme?>']").prop("checked",true);
			$('#theme-menu').multiselect('refresh');
		<?php
			}
		?>

		<?php
			if(!empty($selectedProduct)){
		?>
			$("input[name=product][value='<?=$selectedProduct?>']").prop("checked", true);
			$('#product-menu').val('<?=$selectedProduct?>');
			$("#product-menu + div input[type=checkbox][value='<?=$selectedProduct?>']").prop("checked",true);
			$('#product-menu').multiselect('refresh');
		<?php
			}
		?>

		<?php
			if(!empty($selectedMethod)){
		?>
			$("input[name=method][value='<?=$selectedMethod?>']").prop("checked", true);
			$('#method-menu').val('<?=$selectedMethod?>');
			$("#method-menu + div input[type=checkbox][value='<?=$selectedMethod?>']").prop("checked",true);
			$('#method-menu').multiselect('refresh');
		<?php
			}
		?>

		<?php
			if($isAdv){
		?>
				onclickBottomSearchBtn();
		<?php
			}
		?>


		$("[name=productCat]").click(function(e){

			var v = $(e.target).val();
			var t = $(e.target).prop("checked");






		});

	});

	function resetMenu(){
		$("[type=checkbox]:checked").prop("checked", false);
		$("#searchRecipeTxt").val("");
		refreshResult();
	}

	function onclickBottomSearchBtn(){
		$("#searchRecipeTxt").focus();
	}

	function showLoading(){
		$("#loading").show();
		$("[type=checkbox]").attr("disabled", "disabled")
		$("li.inGroup, li.notInGroup").addClass("loading");
		$(".desktopLoadingBlock").show();
		$(".mobileLoading").show();
	}

	function hideLoading(){
		$("#loading").hide();
		$("[type=checkbox]").removeAttr("disabled");
		$(".desktopLoadingBlock").hide();
		$("li.inGroup, li.notInGroup").removeClass("loading");
		$(".mobileLoading").hide();
	}

	function getNewRecipe(){

// 		$("#loading").parent().css("display", "block");

    	var nextPage = currentPage;

    	var productIds = "";
    	$(".multiselect-container [name=product]:checked").each(function(i,v){
        	if(productIds ==""){
        		productIds += $(v).val();
            }else{
	    		productIds += "," + $(v).val();

			}
//     		track.send("RECIPE_LIST", "SEARCH_PRODUCT", $("#leftMenu [name=product][value='"+$(v).val()+"']").data("label"));
        });


    	var themeIds = "";
    	$(".multiselect-container [name=theme]:checked").each(function(i,v){
        	if(themeIds ==""){
        		themeIds += $(v).val();
            }else{
	    		themeIds += "," + $(v).val();
			}

//         	track.send("RECIPE_LIST", "SEARCH_THEME", $("#leftMenu [name=theme][value='"+$(v).val()+"']").data("label"));
        });

    	var ingredientsIds = "";
    	$(".multiselect-container [name=ingredients]:checked").each(function(i,v){
        	if(ingredientsIds ==""){
        		ingredientsIds += $(v).val();
            }else{
	    		ingredientsIds += "," + $(v).val();
			}

//         	track.send("RECIPE_LIST", "SEARCH_INGREDIENTS", $("#leftMenu [name=ingredients][value='"+$(v).val()+"']").data("label"));
        });

    	var methodIds = "";
    	$(".multiselect-container [name=method]:checked").each(function(i,v){
        	if(methodIds ==""){
        		methodIds += $(v).val();
            }else{
            	methodIds += "," + $(v).val();
			}

//         	track.send("RECIPE_LIST", "SEARCH_METHOD", $("#leftMenu [name=method][value='"+$(v).val()+"']").data("label"));
        });

		var keyword = encodeURI($("#searchRecipeTxt").val());
		if($("#searchRecipeTxt").val()==""){
			var keyword = encodeURI($("#searchRecipeTxt2").val());
		}

        if(keyword.length > 0){
//         	track.send("RECIPE_LIST", "SEARCH_KEYWORD", keyword);
        }

		$.post("recordUtils.php?getRecipe", {
				product: productIds,
				theme : themeIds,
				ingredients: ingredientsIds,
				method: methodIds,
				count: <?=ITEM_CNT?>,
				key : keyword,
				page: currentPage
			}, function(d){

				var data = JSON.parse(d);
//					console.log(data.status);
//					console.log(data.r);

				if(data.status=="SUCCESS"){
					var result = data.r;
					var totalCnt = data.count;
					$("#totalItemsCount").html(totalCnt);

					updatePageNum(totalCnt);

					if(result.length > 0){


						for(var x in result){
							var obj = result[x];

							var id = obj.id;
							var title = obj.t;
							var image ;
							if(obj.image == ""){
								image = "images/empty_recipe.png";
							}else{
								image = "<?=PATH_TO_ULFILE?>" + obj.image;
							}

							var thumb = obj.thumb;
							var thumb2 = obj.thumb2;
							var isPop = obj.isPop == "Y";
							var html =

								'<div class="recipeItem col-lg-4 col-md-4 col-sm-6 col-xs-6 lazy" style="display:none" data-ga-recipetitle="'+title+'">' +
								'	<div>                                                                      ' +
								'		<div class="image hoverzoom">                                                    ' +
								'			<a href="recipe-detail.php?id='+  id+'">                           ' +
								'				<img src="'+ image +'" alt="' + title + '" />                   ' +
								'			</a>                                                               ' +
								'		</div>                                                                 ';

							if(isPop){
							html+=
								'<div class="hotRecipe">														' +
								'	<img src="images/hot_recipe.png"/>											' +
								'</div>																			';
							}
							html +=
								'		                                                                       ' +
								'		<a href="recipe-detail.php?id='+  id +'">                              ' +
								'			<div class="caption">                                              ' +
								'				<div class="title">                                            ' +
								'					'+  title  +'                                              ' +
								'				</div>                                                         ' +
								'				<div class="productIcon">                                      ';

							if(typeof(thumb2) == "undefined" || thumb2 == null){
								html +=
									'					<div>                                                      ' +
									'						<img src="<?=PATH_TO_ULFILE?>'+  thumb   +'"/>         ' +
									'					</div>                                                     ';
							}else{


								var isP1Small = obj.isP1Small;
								var isP2Small = obj.isP2Small;
								html +=
									'				<div class="recipe-detail-2-product">                          ' +
									'					<div class="'+(isP1Small ? "small" : "")  +' main">         ' +
									'						<img src="<?=PATH_TO_ULFILE?>'+  thumb   +'"/>       ' +
									'					</div>                                                     ' +
									'					<div class="'+(isP2Small ? "small" : "")  +'">               ' +
									'						<img src="<?=PATH_TO_ULFILE?>'+  thumb2   +'"/>     ' +
									'					</div>                                                     ' +
									'				</div>                                                         ';
							}





							html +=
								'				</div>                                                         ' +
								'			</div>                                                             ' +
								'		</a>                                                                   ' +
								'		<div style="clear: both"></div>                                        ' +
								'	</div>                                                                     ' +
								'</div>                                                                        ';



							$(".recipeContainer").append(html);
//								console.log("append " + id);
						}
						isTriggerAdd = false;
						currentPage = nextPage;
// 						$("#loading").css("visibility", "hidden");
						hideLoading();
						$(".lazy").each(function(i,v){

							$(v).fadeIn(600);
						});

					}else{
						isRecipeEnd = true;
						$(".recipeContainer").append('<div style="color:#888;font-weight:bold;font-size: 18px;padding:4px;text-align:center;">沒有相關食譜</div>');
						hideLoading();
// 						$("#loading").parent().css("display", "none");
					}

				}

		});
	}
</script>
<script>
	//track

// 	track.send("RECIPE_LIST", "VIEW", "P1");
</script>

<?php
	include_once 'inc_top_script.php';
?>
<!DOCTYPE html>
<html>
	<head>

		<?php
			include_once 'include_script.php';
		?>
		<style>
			#video-menu{
				overflow:hidden;
			}
			.fancybox-nav{
				height:90%;
			}

			@media(min-width: 1200px){

				.recipeVideoItem:nth-child(5) {
				/*	margin-left:20px;*/
				}

				.recipeVideoItemContainer:after {
					display: block;
				}
			}


		</style>
		<?php
			include_once 'inc_sharing.php';
		?>

		<title>鮮味炒教室</title>
		<meta name="keywords" content="初學 烹飪,煮食教學影片,烹飪教學,簡易食譜短片,入廚教學短片,新手食譜,新手烹飪教學,簡易食譜">
		<meta name="description" content="史雲生入廚教學短片，除提供完整食譜教學，亦為新手解決平日烹飪時遇到的困難，煮食時更得心應手！">

	</head>

	<body>
		<?php
			include_once 'inc_beginbody_script.php';
		?>
		 <div id="wrapper" class="">
	        <?php
	        	include_once 'inc_sidebar.php';
	        ?>
			<div class="mainContainer "  id="page-content-wrapper" >
				<div style="position:relative;">
				<?php
					include_once 'inc_header.php';
				?>

				<div class="breadcrumb" style="background-color:#f1dc00;">
					<div class="container swansonBreadcrumb" >
						<img src="images/breadcrumb_home_icon.png"/>&nbsp;<a href="index.php">主頁</a>

						<span class="glyphicon glyphicon-menu-right " aria-hidden="true" ></span>
						鮮味炒教室
					</div>

				</div>


				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row lineContainer">
					<div class="line" style="width: calc(50% - 127.5px);">
						<div><div class="border"></div></div>
					</div>
					<div class="titleImg" style="width:255px">
						<img src="images/cook_video_tsui.png" alt="鮮味炒教室"/>
					</div>
					<div class="line" style="width: calc(50% - 127.5px);">
						<div><div class="border"></div></div>
					</div>
				</div>





				<div class="container ">


					<div class="hidden-lg hidden-md hidden-sm col-xs-12 row menuContainer_m">


						<select id="video-menu"  class="form-control recipeListMenu">
							<?php
								$rRs = $pdo->prepare("select * from recipe_video_tsui order by display_priority desc");
								$rRs->execute();
								while($rRs_row = $rRs->fetch()){
									$id  = $rRs_row["id"];
									$title = $rRs_row["title"];
							?>
							    <option name="video" value="<?=$id?>" ><?=str_replace("史雲生教室之","", $title); ?> </option>
							<?php
								}
							?>
						</select>

					</div>



					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row recipeVideoItemContainer">

						<?php

							$rRs = $pdo->prepare("select * from recipe_video_tsui order by display_priority desc");
							$rRs->execute();

							while($rRs_row = $rRs->fetch()){
								$id = $rRs_row["id"];
								$title = $rRs_row["title"];
								$image = $rRs_row["image"];
								$description = $rRs_row["description"];
								$youtubeId = $rRs_row["video_link"];
								$recipe_link = $rRs_row["recipe_link"];
								$recipe_title = str_replace("史雲生冇失手食譜【","",$title);
								$recipe_title = str_replace("】","",$recipe_title);
								$final_recipe = "【" . $recipe_title . "】";
								$copy = $title;
								$copy = str_replace($final_recipe, "", $copy);
								if ($copy === $recipe_title) {
									$final_recipe = "";
								}
						?>

						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 recipeVideoItem" id="<?=$id?>" data-ga-recipetitle="<?php echo $recipe_title; ?>" >
							<div>
								<div>

									<div class="image">
										<a class="fancybox-media" href="http://www.youtube.com/watch?v=<?=$youtubeId?>" rel="media-gallery1">
											<img src="<?=PATH_TO_ULFILE.$image?>" style="width:100%;"/>
										</a>
									</div>
									<div class="title">
										 <p style = "display: block"><?=$copy?></p>
										 <p style = "display: block; margin-left: -13px;"><?=$final_recipe?></p>
									</div>
									<div class="description">
										<?=$description?>
									</div>

									<div class="buttonHolder">
										<div style="text-align: left">
											<a target="_blank" href="recipe-detail.php?id=<?=$recipe_link?>" >
												<div class="recipe-link-btn button">
													<img src="images/recipe_video_btn_1.png"/>食譜
												</div>
											</a>
										</div>
										<div style="text-align: right">
											<a class="fancybox-media" href="http://www.youtube.com/watch?v=<?=$youtubeId?>" rel="media-gallery2">
												<div class="button">
													<img src="images/recipe_video_btn_2.png"/>煮食影片
												</div>
											</a>
										</div>




									</div>
								</div>
							</div>
						</div>
						<?php
							}
						?>


					    <span class="stretch"></span>
					</div>

					<div style="clear:both;height:45px;">
					</div>
					<div class="recipeVideoBackToTopBtn hidden-lg hidden-md hidden-sm " onclick="backToTop()">
						<img src="images/btn-backtotop.png"/>
					</div>
				</div>
			</div>
			<?php
				include_once 'inc_footer.php';
			?>
		</div>
	</body>
</html>
<?php
	include_once 'inc_footer_script.php';
?>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/bootstrap-multiselect.js"></script>
<script>



	var isBackToTopBtnShown = false;
	function backToTop(){
	    $('html, body').animate({
	        scrollTop: 0
	    }, 400, "easeOutExpo");

	}
	$(function(){

		$('#video-menu').multiselect({
        	nonSelectedText:"所有教學",
        	nSelectedText: '項',
        	numberDisplayed:1,
        	maxHeight:200,
	    });


		var isAndroid = (/android/gi).test(navigator.appVersion);
	    var isIDevice = (/iphone|ipad/gi).test(navigator.appVersion);
	    if (isAndroid || isIDevice || $(window).width()<768) {

	    }else{
	    	$('.fancybox-media').fancybox({
				openEffect : 'none',
				closeEffect : 'none',
				width: 1000,
				height: 560,
// 				prevEffect : 'none',
// 				nextEffect : 'none',

				arrows : true,
				helpers : {
					media : {},
					buttons : {}
				}
			});
	  	}

		$("#video-menu").on("change", function(){




			var isAndroid = (/android/gi).test(navigator.appVersion);
		    var isIDevice = (/iphone|ipad/gi).test(navigator.appVersion);

		    if (isAndroid || isIDevice || $(window).width()<768) {
		       offset = $(".navbar-brand>div").height()+ 10;
		    }else{
			   offset = 0;
		  	}

			$('html, body').animate({
		        scrollTop: $('#'+$(this).val()).offset().top -offset
		    }, 300, "easeOutExpo");
		});


	});





		$(window).on('scroll', function() {


			var isAndroid = (/android/gi).test(navigator.appVersion);
		    var isIDevice = (/iphone|ipad/gi).test(navigator.appVersion);

		    if (isAndroid || isIDevice || $(window).width()<768) {
		       offset = $(".navbar-brand>div").height()+ 10;
		    }else{
			   offset = 0;
		  	}


			if($("#page-content-wrapper").height() - ($(this).scrollTop() + $(this).height()) < $("#footerContainer").height() - offset){
				if(isBackToTopBtnShown){
					isBackToTopBtnShown = false;
					$(".recipeVideoBackToTopBtn").animate({"bottom":"-100px"},800, "easeOutCubic");

				}
		    }else if($(this).scrollTop() >= $("#video-menu").offset().top - offset) {

				if(!isBackToTopBtnShown){
					isBackToTopBtnShown = true;

					$(".recipeVideoBackToTopBtn").animate({"bottom":"0px"},800, "easeInCubic");
				}

	        }else{

	        	if(isBackToTopBtnShown){
					isBackToTopBtnShown = false;
					$(".recipeVideoBackToTopBtn").animate({"bottom":"-100px"},800, "easeOutCubic");

				}



		    }
	    });



</script>


<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="<?=ROOT_PATH?>css/bootstrap.min.css">
<link rel="stylesheet" href="<?=ROOT_PATH?>css/simple-sidebar.css">
<link rel="stylesheet" href="<?=ROOT_PATH?>css/owl.theme.css" >
<link rel="stylesheet" href="<?=ROOT_PATH?>css/owl.transitions.css" >
<link rel="stylesheet" href="<?=ROOT_PATH?>css/jquery-ui.min.css">
<link rel="stylesheet" href="<?=ROOT_PATH?>css/jquery-ui.theme.min.css"> 
<link rel="stylesheet" href="<?=ROOT_PATH?>css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="<?=ROOT_PATH?>css/bootstrap-multiselect.css">

<link rel="stylesheet" type="text/css" href="<?=ROOT_PATH?>css/fancybox/jquery.fancybox.css?v=2.1.5"  media="screen" />
<link rel="stylesheet" href="<?=ROOT_PATH?>css/owl.carousel.css" >
<link rel="stylesheet" href="<?=ROOT_PATH?>css/style.css" >

<link rel="shortcut icon" href="<?=ROOT_PATH?>favicon.ico" type="image/x-icon" />


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '161735800907277');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=161735800907277&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57656614-5', 'auto');
  ga('send', 'pageview');

</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth=uiJJvxvFrXtkhGWbS0VaIQ&gtm_preview=env-12&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W3TB6SV');</script>
<!-- End Google Tag Manager -->

<!-- Lightning Bolt Begins -->

<script type="text/javascript">

	var lbTrans = '[TRANSACTION ID]';

	var lbValue = '[TRANSACTION VALUE]';

	var lbData = '[Attribute/Value Pairs for Custom Data]';

	var lb_rn = new String(Math.random()); var lb_rns = lb_rn.substring(2, 12);

	var boltProtocol = ('https:' == document.location.protocol) ? 'https://' : 'http://';

	try {

		var newScript = document.createElement('script');

		var scriptElement = document.getElementsByTagName('script')[0];

		newScript.type = 'text/javascript';

		newScript.id = 'lightning_bolt_' + lb_rns;

		newScript.src = boltProtocol + 'cdn-akamai.mookie1.com/LB/LightningBolt.js';

		scriptElement.parentNode.insertBefore(newScript, scriptElement);

		scriptElement = null; newScript = null;

	} catch (e) { }

</script>

<!-- Lightning Bolt Ends -->    
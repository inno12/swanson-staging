<?php
	include_once 'inc_top_script.php';
?>
<!DOCTYPE html>
<html>
	<head>

		<?php
			include_once 'include_script.php';
		?>
		<title>史雲生新手x新意食譜</title>
		<meta name="keywords" content="史雲生食譜,食譜下載食譜推薦,家常菜式,新手菜式,簡易食譜,一盒煮一餸,精選食譜,新手食譜">
		<meta name="description" content="史雲生新手食譜，精選一盒煮一餸菜式，簡單又方便。立即下載食譜，隨時隨地煮出滋味靚餸！">

		<style>
		  .baiJia {
        position: relative;
				top: 40px;
			}
			.huiKang {
				position: relative;
				top: 40px;
			}
			.about_date {
				position: relative;
				left: 163px;
			}
			.about_date_content {
				position: relative;
				left: 163px;
			}
			.about_time {
        position: relative;
				left: 20px;
			}
			.about_time_content {
				position: relative;
				left: 20px;
			}
			.about_store {
				position: relative;
			  right: 80px;
			}
			.about_store_content {
				position: relative;
				right: 80px;
			}
			.about_address {
				position: relative;
				right: 140px;
			}
			.about_address_content{
				position: relative;
				right: 140px;
			}
			.eventTable{
				width: 100%;
				position: relative;
			  margin-bottom: 40px;
			}
			.eventDetail{
				margin-top: 50px;
			}
			.event-data-row td{
				display: inline-block;
			}
			.label-header{
				width: 50%;
			}
			.label-header span{
				font-size: 24px;
				font-weight: bold;

			}
			.label-mobile-hide{
				display: none;

			}
			.event-storeName{
				border-radius: 4px;
				padding: 5px 10px;
				background-color: #0f847b;
				display: inline-block;
				color: #ffffff;
				margin-bottom: 15px;
				font-size: 30px;
				position: relative;
				left: 163px;
				top: 30px;
			}
			.eventData-row:last-child .eventData{
				padding-bottom: 40px;
			}
			.eventData span{
				display: block;
				font-size: 16px;

			}
			.eventData{
				padding-bottom: 15px;
			}
			.eventData-row{
				vertical-align: top;
			}
			.label-header span,.eventData-row span{
				color: #0f847b;
			}
			@media(min-width:992px){
				.eventTable{
					/*border-bottom: 2px solid #0f847b; */
				}
				.event-data-row td{
					display: table-cell;
				}
				.label-odd{
					width: auto;
				}
				.label-even{
					display: block;
				}
				.label-header .label-desktop-hide{
						display: none;
				}
				.label-header span,.eventData-row span{
					display: inline-block;
					width: 50%;
					vertical-align: top;
					min-height:44px;
				}
				/*.eventData-row .eventData:nth-child(2) span:nth-child(1){
					width: 30%;
				}
				.eventData-row .eventData:nth-child(2) span:nth-child(2){
					width: 70%;
				}*/
			}

			@media only screen and (min-width: 992px) and (max-width: 1440px) {
				.about_time {
				  position: relative;
					left: 80px;
				}
				.about_time_content {
					position: relative;
					left: 80px;
				}
				.about_store {
					position: relative;
					right: 0px;
				}
				.about_store_content {
					position: relative;
					right: 0px;
				}
				.about_address {
					position: relative;
					right: 80px;
				}
				.about_address_content {
					position: relative;
					right: 80px;
				}
			}
			@media only screen and (min-width: 768px) and (max-width: 991px) {
				.about_time_content {
					position: relative;
					left: 160px;
				}
				.about_store_content {
					position: relative;
					right: 0px;
				}
				.about_address_content {
					position: relative;
					right: 0px;
				}
				.mainContainer {
					position: relative;
				}
			}
			@media only screen and (min-width: 425px) and (max-width: 767px) {
				.event-storeName {
					left: 50px;
				}
				.about_date {
					left: 50px;
				}
				.about_date_content {
					left: 50px;
				}
				.about_time_content {
					position: relative;
					left: 47px;
				}
				.about_store_content {
					position: relative;
					right: 0px;
				}
				.about_address_content {
					position: relative;
					right: 0px;
				}
				.mainContainer {
					position: relative;
				}
			}
			@media only screen and (min-width: 0px) and (max-width: 424px) {
				.event-storeName {
					left: 20px;
				}
				.label-header span {
					font-size: 16px;
				}
				.eventData span {
					font-size: 12px;
				}
				.about_date {
					left: 20px;
				}
				.about_date_content {
					left: 20px;
				}
				.about_time_content {
					position: relative;
					left: 18px;
				}
				.about_store_content {
					position: relative;
					right: 0px;
				}
				.about_address_content {
					position: relative;
					right: 0px;
				}
				.mainContainer {
					position: relative;
				}
			}
		</style>

		<meta property="og:title" content="史雲生食譜網站 "/>
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH?>cookbook.php"/>
		<meta property="og:image" content="http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH."images/fb-share-banner-02.jpg"?>"/>
		<meta property="og:site_name" content="<?=APP_TITLE?>"/>
		<meta property="og:description" content="史雲生食譜，教你用簡單食材煮出滋味靚餸。"/>

		<script>
			function sharePage(){
				var u = "http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH?>";
				window.open("https://www.facebook.com/sharer/sharer.php?u="+u , "pop", "width=600, height=400, scrollbars=no");
			}
		</script>
	</head>

	<body>
		<?php
			include_once 'inc_beginbody_script.php';
		?>
		<div id="wrapper" class="">
		<?php
		include_once 'inc_sidebar.php';
		?>
			<div class="mainContainer "  id="page-content-wrapper" style="position: relative;">
				<div style="position:relative;">
					<?php
					include_once 'inc_header.php';
					?>
					<div class="breadcrumb" style="background-color:#f1dc00;margin-bottom:0px;">
						<div class="container swansonBreadcrumb" >
							<img src="images/breadcrumb_home_icon.png"/>&nbsp;<a href="index.php">主頁</a>

							<span class="glyphicon glyphicon-menu-right " aria-hidden="true" ></span>
							鮮味炒試食地點
						</div>
					</div>
<!--
					<div class="container eventDetail">

									<table class="eventTable">
										<tr class="label-row">
											<th class="label-header" style="text-align:center"><span>暫無試食活動</span></th>

										</tr>

position: relative;
				right: 40px;
									</table>

								</div> -->
								<?=$rRs3;?>

			<!--			<div class="event-storeName">惠康超級市場</div>
						<table class="eventTable">
							<tr class="label-row">
								<th class="label-header"><span>活動日期</span><span class="label-mobile-hide">活動時間</span></th>
								<th class="label-header"><span class="label-desktop-hide">商鋪名稱及地址</span><span class="label-mobile-hide">商鋪名稱</span><span class="label-mobile-hide">地址</span></th>
							</tr>
							<tr class="eventData-row">
								<td class="eventData"><span>2018年6月15-17日</span><span class="eventData-mobile-break">上午11時至下午7時</span></td>
								<td class="eventData"><span>健威花園</span><span class="eventData-mobile-break">北角英皇道 560 號健威商場 UG 層 14 號舖</span></td>
							</tr>
							<tr class="eventData-row">
								<td class="eventData"><span>2018年6月15-17日</span><span class="eventData-mobile-break">上午11時至下午7時</span></td>
								<td class="eventData"><span>鯉魚門</span><span class="eventData-mobile-break">鯉魚門道鯉魚門廣場 1 樓 123 及 124 號舖</span></td>
							</tr>
							<tr class="eventData-row">
								<td class="eventData"><span>2018年6月15-17日</span><span class="eventData-mobile-break">上午11時至下午7時</span></td>
								<td class="eventData"><span>秀茂坪廣場</span><span class="eventData-mobile-break">秀茂坪村秀茂坪購物商場 1 樓 102 舖</span></td>
							</tr>
							<tr class="eventData-row">
								<td class="eventData"><span>2018年6月15-17日</span><span class="eventData-mobile-break">上午11時至下午7時</span></td>
								<td class="eventData"><span>天澤</span><span class="eventData-mobile-break">天水圍天澤邨天澤商場 204 號舖</span></td>
							</tr>
							<tr class="eventData-row">
								<td class="eventData"><span>2018年6月15-17日</span><span class="eventData-mobile-break">上午11時至下午7時</span></td>
								<td class="eventData"><span>安定超級廣場</span><span class="eventData-mobile-break">屯門鄉事會路 2A 安定邨酒樓大厦 3 樓 M301 號舖</span></td>
							</tr>
							<tr class="eventData-row">
								<td class="eventData"><span>2018年6月15-17日</span><span class="eventData-mobile-break">上午11時至下午7時</span></td>
								<td class="eventData"><span>翠屏</span><span class="eventData-mobile-break">觀塘翠屏道 19 號翠屏北邨翠屏商場 1F 號舖</span></td>
							</tr>
							<tr class="eventData-row">
								<td class="eventData"><span>2018年6月15-17日</span><span class="eventData-mobile-break">上午11時至下午7時</span></td>
								<td class="eventData"><span>上水超級廣場</span><span class="eventData-mobile-break">上水智昌路 9 號上水名都中心地下 15 及 17 號舖</span></td>
							</tr>
							<tr class="eventData-row">
								<td class="eventData"><span>2018年6月15-17日</span><span class="eventData-mobile-break">上午11時至下午7時</span></td>
								<td class="eventData"><span>頌富</span><span class="eventData-mobile-break">天水圍天頌苑頌富廣場地下 017 號舖</span></td>
							</tr>
							<tr class="eventData-row">
								<td class="eventData"><span>2018年6月15-17日</span><span class="eventData-mobile-break">上午11時至下午7時</span></td>
								<td class="eventData"><span>蝴蝶</span><span class="eventData-mobile-break">屯門蝴蝶村廣場 2 樓</span></td>
							</tr>

						</table>

					</div>   -->


								<div class="event-storeName">百佳超級市場</div>
								<div class="baiJia">
								<table class="eventTable">
									<tr class="label-row">
										<th class="label-header"><span class="about_date">活動日期</span><span class="label-mobile-hide"><span class="about_time">活動時間</span></span></th>
										<th class="label-header"><span class="label-desktop-hide">商鋪名稱及地址</span><span class="label-mobile-hide"><span class="about_store">商鋪名稱</span></span><span class="label-mobile-hide"><span class="about_address">地址</span></span></th>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月10-12日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">第一城</span><span class="eventData-mobile-break"><div class="about_address_content">沙田銀城街 2 號 沙田第一城銀城商場地下 7B & 46-80 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月10-12日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">晴朗商場</span><span class="eventData-mobile-break"><div class="about_address_content">九龍城晴朗商場 1 樓 B101 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月10-12日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">天耀商場</span><span class="eventData-mobile-break"><div class="about_address_content">天水圍天湖路天耀邨天耀商場 1 樓 L109 及 L110 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月10-12日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">海怡半島海怡廣場</span><span class="eventData-mobile-break"><div class="about_address_content">鴨脷洲海怡半島海怡廣場西座 3 樓 302 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月10-12日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">慈雲山</span><span class="eventData-mobile-break"><div class="about_address_content">慈雲山毓華街 23 號慈雲山中心 5 樓 528-535 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月10-12日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">寶達商場</span><span class="eventData-mobile-break"><div class="about_address_content">秀茂平寶達商場 1 樓 116 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月17-19日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">利東商場</span><span class="eventData-mobile-break"><div class="about_address_content">利東邨道 5 號利東邨利東商場 1 期 4 樓 501 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月17-19日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">淘大廣場</span><span class="eventData-mobile-break"><div class="about_address_content">牛頭角道77號淘大商場第 1 期 1 樓 33-45 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月17-19日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">石籬</span><span class="eventData-mobile-break"><div class="about_address_content">石籬(第一)邨石籬購物中心 1 樓 107 號舖</div></span></td>
									</tr>

									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月17-19日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">置富花園</span><span class="eventData-mobile-break"><div class="about_address_content">薄扶林置富花園商場 2 樓 201-202 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月17-19日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">青衣城(盈翠半島)(Taste)</span><span class="eventData-mobile-break"><div class="about_address_content">青衣盈翠半島 313 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月17-19日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">顯徑村</span><span class="eventData-mobile-break"><div class="about_address_content">大圍車公廟道 69 號顯徑邨顯徑商場地下 101-103 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月24-26日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">翠林邨</span><span class="eventData-mobile-break"><div class="about_address_content">將軍澳翠林邨翠林商場 5 樓 132 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月24-26日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">馬鞍山廣場</span><span class="eventData-mobile-break"><div class="about_address_content">沙田馬鞍山廣場 3 樓 343-363 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月24-26日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">馬坑涌</span><span class="eventData-mobile-break"><div class="about_address_content">土瓜灣馬坑涌道 3A-3C 號中華大廈地下 2-3 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月24-26日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">啟德花園</span><span class="eventData-mobile-break"><div class="about_address_content">黃大仙彩虹道 121 號啟德花園第 2 期地下</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月24-26日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">廣源邨</span><span class="eventData-mobile-break"><div class="about_address_content">沙田廣源邨 2 座地下 5B 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月24-26日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">天瑞邨</span><span class="eventData-mobile-break"><div class="about_address_content">天水圍天瑞路 9 號天瑞商場 2 樓 L101 號舖</div></span></td>
									</tr>

								</table>
              </div>
							</div>

								<div class="event-storeName">惠康超級市場</div>
								<div class="huiKang">
								<table class="eventTable">
									<tr class="label-row">
										<th class="label-header"><span class="about_date">活動日期</span><span class="label-mobile-hide"><div class="about_time">活動時間</div></span></th>
										<th class="label-header"><span class="label-desktop-hide">商鋪名稱及地址</span><span class="label-mobile-hide"><span class="about_store">商鋪名稱</span></span><span class="label-mobile-hide"><span class="about_address">地址</span></span></th>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月10-12日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">翠屏</span><span class="eventData-mobile-break"><div class="about_address_content">觀塘翠屏道 19 號翠屏北邨翠屏商場 1F 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月10-12日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">花都廣場</span><span class="eventData-mobile-break"><div class="about_address_content">粉嶺地段 113 號百和路 88 號花都廣場地下 132 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月10-12日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">美孚超級廣場</span><span class="eventData-mobile-break"><div class="about_address_content">美孚新村百老匯街地下 69-71 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月10-12日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">栢蕙苑</span><span class="eventData-mobile-break"><div class="about_address_content">鰂魚涌英皇道 1060 號栢惠苑購物商場 G 地下 A 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月10-12日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">頌富</span><span class="eventData-mobile-break"><div class="about_address_content">天水圍天頌苑頌富廣場地下 017 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月10-12日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">紅磡超級廣場 </span><span class="eventData-mobile-break"><div class="about_address_content">紅磡湖光街 1-7 號聯盛大廈地下 10C-10G 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月17-19日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">環翠</span><span class="eventData-mobile-break"><div class="about_address_content">柴灣環翠邨環翠商場 3 樓</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月17-19日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">秀茂坪廣場</span><span class="eventData-mobile-break"><div class="about_address_content">秀茂坪村秀茂坪購物商場 1 樓 102 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月17-19日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">海悅豪園</span><span class="eventData-mobile-break"><div class="about_address_content">將軍澳海悅豪園購物中心 16-18 號舖</div></span></td>
									</tr>

									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月17-19日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">上水超級廣場</span><span class="eventData-mobile-break"><div class="about_address_content">上水智昌路 9 號上水名都中心地下 15及17 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月17-19日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">鯉魚門</span><span class="eventData-mobile-break"><div class="about_address_content">油塘鯉魚門道鯉魚門廣場 1 樓 123 及 124 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月17-19日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">藍田</span><span class="eventData-mobile-break"><div class="about_address_content">藍田啟田村啟田商場 1 樓 104 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月24-26日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">蝴蝶</span><span class="eventData-mobile-break"><div class="about_address_content">屯門蝴蝶村廣場 2 樓</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月24-26日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">大興</span><span class="eventData-mobile-break"><div class="about_address_content">屯門大興村第 2 期商場 43-53 號 1 樓</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月24-26日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">安定</span><span class="eventData-mobile-break"><div class="about_address_content">屯門鄉事會路 2A 安定邨酒樓大厦 3 摟 M301 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月24-26日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">良景</span><span class="eventData-mobile-break"><div class="about_address_content">屯門良景邨商場 4 樓 401 號</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月24-26日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">天澤</span><span class="eventData-mobile-break"><div class="about_address_content">天水圍天澤邨天澤商場 204 號舖</div></span></td>
									</tr>
									<tr class="eventData-row">
										<td class="eventData"><span class="about_date_content">2018年8月24-26日</span><span class="eventData-mobile-break"><span class="about_time_content">上午11時至下午7時</span></span></td>
										<td class="eventData"><span class="about_store_content">銅鑼灣</span><span class="eventData-mobile-break"><div class="about_address_content">銅鑼灣記利佐治街 25-29 號</div></span></td>
									</tr>

								</table>
              </div>
							</div>

					<?php
					include_once 'inc_footer.php';
					?>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
	include_once 'inc_footer_script.php';
?>
<script>

	$(function(){

		if( navigator.userAgent.match(/Android/i)
				 || navigator.userAgent.match(/webOS/i)
				 || navigator.userAgent.match(/iPhone/i)
				 || navigator.userAgent.match(/iPad/i)
				 || navigator.userAgent.match(/iPod/i)
				 || navigator.userAgent.match(/BlackBerry/i)
				 || navigator.userAgent.match(/Windows Phone/i)
		 ){
			//
	  	}else{
			$(".cookbookItem").fancybox({
				fitToView: false,
			    maxWidth: "90%",
			    margin:[20, 200,200,200],
				openEffect	: 'fade',
				closeEffect	: 'fade',
				padding:0
			});
		}


	});
</script>

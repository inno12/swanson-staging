<?php 
	include_once '_inc_/global_config.php';	
	
	global $pdo;
	try {
	
		$dbHost = DB_HOST;
		$dbDatabase = DB_INSTANCE;
		$dbUser =DB_USERNAME;
		$dbPassword = DB_PASSWORD;
		$pdo = new PDO("mysql:host=$dbHost;dbname=$dbDatabase", $dbUser, $dbPassword);
		$pdo->exec("set names utf8");
	}catch(PDOException $e) {
		echo $e->getMessage();
	}
	
	
	$rid = $_REQUEST["id"];
	if(empty($rid)){
		header("recipe-list.php");
		exit;
	}
	
	$rRs = $pdo->prepare("select r.*, p.image_thumb, p.product_key from recipe r left join product p on r.product_id = p.id where r.id = :id");
	$rRs->bindValue("id", $rid, PDO::PARAM_INT);
	$rRs->execute();
		
	$rRs_row = $rRs->fetch();
	$title = $rRs_row["title"];
	$steps = $rRs_row["steps"];
	$image = $rRs_row["image"];
	$otherDesc_content = $rRs_row["otherDesc_content"];
	$otherDesc_title = $rRs_row["otherDesc_title"];
	$otherDesc_content2 = $rRs_row["otherDesc_content2"];
	$otherDesc_title2 = $rRs_row["otherDesc_title2"];
		
	$ingredients = $rRs_row["ingredients"];
	$ingredients_desc = $rRs_row["ingredients_desc"];
	$sauce = $rRs_row["sauce"];
	$productImage = $rRs_row["image_thumb"];
	$productKey = $rRs_row["product_key"];
		
	$related_recipe_1 = $rRs_row["related_recipe_1"];
	$related_recipe_2 = $rRs_row["related_recipe_2"];
	$related_recipe_3 = $rRs_row["related_recipe_3"];
	$related_recipe_4 = $rRs_row["related_recipe_4"];
		
	$tips = $rRs_row["tips"];
		
?>

<Style>
html,body{
	margin: 0;
	padding: 0;
	font-family: "Arial", "微軟正黑體", Helvetica, sans-serif; 
	-webkit-font-smoothing: antialiased;
	border:none;
}
.text{
	font-family: "Arial", "微軟正黑體", Helvetica, sans-serif;
}
.steps .number {
    background-color: #0F847B;
    width: 35px;
    height: 35px;
    color: #FFF;
    font-weight: bold;
    text-align: center;
    line-height: 35px;
    font-size: 25px;
    border-radius: 30px;
    display: inline-block;
    vertical-align: top;
}

.steps .stepContent {
    display: inline-block;
    font-size: 28px;
    width: 90%;
}

.desc{
	font-size:28px;
}

.desc p{
	margin:auto;
}
</Style>
<div style="border:2px solid #BBB;border-radius:8px;">
	<div>
		<img src="images/swanson-banner01.jpg" style="width:100%;"/>
	</div>
	<div>
		<img src="<?=PATH_TO_ULFILE.$image?>" style="width:100%;"/>	
	</div>
	<div style="padding:30px 45px;">
	
		<div style="font-size: 60px;color: #0F847B;font-weight: bold;padding-bottom:30px">
			<?=$title?>
		</div>
		<div style="padding-bottom:30px">
			<div style="display:inline-block;width:33%;vertical-align:top;"> 
				<div class="text" style="font-size: 40px;color: #0F847B; padding-bottom:10px;">
					材料
				</div>
				<div class="text desc">
					<?=$ingredients_desc?>
				</div>
			</div>
			
			
			<?php 
				if(!empty($sauce)){
			?>
				<div style="display:inline-block;width:33%;vertical-align:top;">
					<div class="text" style="font-size: 40px;color: #0F847B; padding-bottom:10px;">
						醃料
					</div>
					<div class="text desc">
						<?=$sauce?>
					</div>
				</div>
			<?php 
				}
			?>
			
			<?php 
				if(!empty($otherDesc_title)){
			?>
				<div style="display:inline-block;width:33%;vertical-align:top;">
					<div class="text" style="font-size: 40px;color: #0F847B; padding-bottom:10px;">
						<?=$otherDesc_title?>
					</div>
					<div class="text desc">
						<?=$otherDesc_content?>
					</div>
				</div>
			<?php 
				}
			?>
			
			<?php 
				if(!empty($otherDesc_title2)){
			?>
				<div style="display:inline-block;width:33%;vertical-align:top;">
					<div class="text" style="font-size: 40px;color: #0F847B; padding-bottom:10px;">
						<?=$otherDesc_title2?>
					</div>
					<div class="text desc">
						<?=$otherDesc_content2?>
					</div>
				</div>
			<?php 
				}
			?>
		</div>
		
		<div class="text" style="font-size: 40px;color: #0F847B;padding-bottom:30px">
			做法
		</div>
		<div class="steps"> 
			<?php 
				$stepsList = explode(";", $steps);
				foreach($stepsList as $stepNum => $step){
			?>
				<div>
					<div class="number"  ><?=($stepNum + 1)?></div>
					<div class="stepContent"><?=$step?></div>
				</div>
				<hr style="margin:20px 0px;"/>
			<?php 		
				}
			?>
		</div>
		
		<div style="font-size: 25px;color: #0F847B;font-weight: bold;padding-bottom:10px;text-align: right;">
			史電生食譜 Swanson Cooking 
		</div>
		<div style="font-size: 25px;padding-bottom:10px;text-align: right;">
			http://swansoncooking.com.hk/
		</div>
		<div style="font-size: 25px;color: #0F847B;font-weight: bold;padding-bottom:10px;text-align: right;">
			<img src="images/youtubeBtn.png">
		</div>
	</div>
</div>




<?php 

	foreach($_REQUEST as $reqK => $reqV){
		
		$orgV = $reqV;
		$filteredV =  htmlspecialchars(strip_tags($reqV), ENT_QUOTES, "UTF-8") ;
		
		if($orgV != $filteredV) { 
			$_REQUEST[$reqK] = $filteredV;
		}
	}
	
	
	include_once '_inc_/global_config.php';	
	
	global $pdo;
	try {
	
		$dbHost = DB_HOST;
		$dbDatabase = DB_INSTANCE;
		$dbUser =DB_USERNAME;
		$dbPassword = DB_PASSWORD;
		$pdo = new PDO("mysql:host=$dbHost;dbname=$dbDatabase", $dbUser, $dbPassword);
		$pdo->exec("set names utf8");
	}catch(PDOException $e) {
		echo $e->getMessage();
	}
	
?>
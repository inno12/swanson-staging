<?php 
	include_once '_inc_/global_config.php';
	global $pdo;
	try {
	
		$dbHost = DB_HOST;
		$dbDatabase = DB_INSTANCE;
		$dbUser =DB_USERNAME;
		$dbPassword = DB_PASSWORD;
		$pdo = new PDO("mysql:host=$dbHost;dbname=$dbDatabase", $dbUser, $dbPassword);
		$pdo->exec("set names utf8");
	}catch(PDOException $e) {
		echo $e->getMessage();
	} 
	
	foreach($_REQUEST as $reqK => $reqV){
	
		$orgV = $reqV;
		$filteredV =  htmlspecialchars(strip_tags($reqV), ENT_QUOTES, "UTF-8") ;
	
		if($orgV != $filteredV) {
			$_REQUEST[$reqK] = $filteredV;
		}
	}  
	
	
	if(array_key_exists("getRecipe", $_GET)){
		
		$cnt = intval($_REQUEST["count"]);
		$page = empty($_REQUEST["page"]) ? 1 : intval($_REQUEST["page"]);
		$theme = $_REQUEST["theme"];
		$ingredients = $_REQUEST["ingredients"];
		$method = $_REQUEST["method"];
		$product = $_REQUEST["product"];
		$keyword = urldecode($_REQUEST["key"]);
		
		
		$sql = "select SQL_CALC_FOUND_ROWS distinct r.id, r.title, r.image,r.isPopular, p.image_thumb, r.product_id, r.product_id2  from recipe r left join product p on r.product_id = p.id"; 
		$queryList = array();
		$paramList = array();
		
		if(!empty($theme)){
			$themeQuery = array();
			foreach(explode(",", $theme) as $tIndex => $t){
				array_push($themeQuery,  " (concat(';', themes, ';') like :t".$tIndex.") ");
				$paramList["t".$tIndex] = "%;".$t.";%";
			}
			
			array_push($queryList, "(".implode(" or ", $themeQuery).")");
		}
		 
		if(!empty($ingredients)){
			$ingredientsQuery = array();
			foreach(explode(",", $ingredients) as $iIndex => $i){
				array_push($ingredientsQuery,  " (concat(';', ingredients, ';') like :i".$iIndex.") ");
				$paramList["i".$iIndex] = "%;".$i.";%";
			}
			
			array_push($queryList, "(".implode(" or ", $ingredientsQuery).")");
		}
		
		if(!empty($product)){
			$productQuery = array();
			foreach(explode(",", $product) as $pIndex => $p){
				array_push($productQuery,  " (concat(';', product_id, ';') like :p".$pIndex.") or (concat(';', product_id2, ';') like :p".$pIndex.") ");
				$paramList["p".$pIndex] = "%;".$p.";%";
			}
			
			array_push($queryList, "(".implode(" or ", $productQuery).")");
		}
		
		if(!empty($method)){
			$methodQuery = array();
			foreach(explode(",", $method) as $mIndex => $m){
				array_push($methodQuery,  " (concat(';', methods, ';') like :m".$mIndex.") ");
				$paramList["m".$mIndex] = "%;".$m.";%";
			}
				
			array_push($queryList, "(".implode(" or ", $methodQuery).")");
		}
		
		if(!empty($keyword)){
			array_push($queryList,  " r.title like :k  "); 
			$paramList["k"] = "%".$keyword."%";
		}
				
		
		
		$query = " where isHide ='N' ";
		if(sizeof($queryList) >0){
			$query .= " and ". implode(" and ", $queryList);
		}
				
				
		$sql .= $query. " order by r.display_priority desc limit :start, :readCount;";
		
//		var_dump($paramList);
		 
// 		echo $sql; 
		$rRs = $pdo -> prepare($sql);
		
		if(sizeof($queryList) >0){
			
			foreach($paramList as $p => $pValue){
				$rRs ->bindValue($p, $pValue, PDO::PARAM_STR);			
			}
			
		}
		
		$itemInPage = $cnt;
		$startItem =  ($page-1) * $itemInPage;
		$rRs->bindValue("start", $startItem, PDO::PARAM_INT);
		$rRs->bindValue("readCount", $itemInPage, PDO::PARAM_INT);
		
		$r = $rRs->execute();
		$totalItems = $pdo->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
		//var_dump($r);
		$resultList = array();
		while($rRs_row = $rRs->fetch()){
			$rId = $rRs_row["id"];
			$title = $rRs_row["title"];
			$image = $rRs_row["image"];
			$productImage = $rRs_row["image_thumb"];
			
			
			$productId1 =  $rRs_row["product_id"];
			$isPopular = $rRs_row["isPopular"] == "Y";
				
			
			$productId2 = $rRs_row["product_id2"];
			if(!empty($productId2)){
				$productRs2 = $pdo->prepare("select id, title, image_thumb, image_thumb2, image_thumb3  from `product` where id = :pid2");
				$productRs2 -> bindValue("pid2", $productId2, PDO::PARAM_INT);
				$productRs2 ->execute();
				$productRs2_row = $productRs2 ->fetch();
				$product2Image3 = $productRs2_row["image_thumb3"];
				$isSCS = $productRs2_row["title"] == "調味雞汁8包" ? "product-scs" : "";
				if($productId1 == 2 && $productId2 == 11){
					$isSCS = "product-scs270-special";
				}
				//only restrict to 250mL Chicken & any CCS
				if( (in_array("1", explode(";", $productId1)) || in_array("4", explode(";", $productId1))) && ( in_array("9", explode(";", $productId2)) || in_array("10", explode(";", $productId2)) || in_array("11", explode(";", $productId2)))   ){
					$isP1Small = true;
				}else if( (in_array("1", explode(";", $productId2)) || in_array("4", explode(";", $productId2)) )  && ( in_array("9", explode(";", $productId1)) || in_array("10", explode(";", $productId1)) || in_array("11", explode(";", $productId1)))   ){
					$isP2Small = true;
				}
				
				
				
				
			}else{
				$isP1Small = false;
				$isP2Small = false;
				$product2Image3 = null;
			}
			
			
			
			
			
			
			
			array_push($resultList ,  array("id"=>$rId, "t"=>$title, "image"=>$image, "thumb"=>$productImage, "isPop"=>$isPopular, "thumb2"=>$product2Image3, "isP1Small"=>$isP1Small, "isP2Small"=>$isP2Small, "isSCS"=>$isSCS ));
		} 
		
		echo json_encode(array("status"=>"SUCCESS", "r"=>$resultList, "count" =>$totalItems));
		
		
		
	}else if(array_key_exists("generateWeeklyRecipe", $_GET)){
		
		$selectedRecipeList = array(91,107,191,195,201,204);
		
		$rRs1 = $pdo->prepare("select id from recipe where  concat(';', ingredients, ';')  like '%1%'  order by rand() limit 2;");
		$rRs1->execute();
		while($rRs1_row = $rRs1->fetch()){
			array_push($selectedRecipeList, $rRs1_row["id"]);
		}

		$rRs2 = $pdo->prepare("select id from recipe where  concat(';', ingredients, ';')  like '%3%' and id not in (". implode(",", $selectedRecipeList) . ") order by rand() limit 2;");
		$rRs2->execute();
		while($rRs2_row = $rRs2->fetch()){
			array_push($selectedRecipeList, $rRs2_row["id"]);
		}
		

		$rRs3 = $pdo->prepare("select id from recipe where  ((concat(';', ingredients, ';')  like '%2%') or (concat(';', ingredients, ';')  like '%4%') or (concat(';', ingredients, ';')  like '%6%') ) and id not in (". implode(",", $selectedRecipeList) . ") order by rand() limit 1;");
		$rRs3->execute();
		while($rRs3_row = $rRs3->fetch()){
			array_push($selectedRecipeList, $rRs3_row["id"]);
		}
		
		$rRs4 = $pdo->prepare("select id from recipe where  concat(';', ingredients, ';')  like '%5%' and id not in (". implode(",", $selectedRecipeList) . ") order by rand() limit 1;");
		$rRs4->execute();
		while($rRs4_row = $rRs4->fetch()){
			array_push($selectedRecipeList, $rRs4_row["id"]);
		}
		
		$rRs5 = $pdo->prepare("select id from recipe where  concat(';', ingredients, ';')  like '%7%' and id not in (". implode(",", $selectedRecipeList) . ") order by rand() limit 1;");
		$rRs5->execute();
		while($rRs5_row = $rRs5->fetch()){
			array_push($selectedRecipeList, $rRs5_row["id"]);
		}
		
		shuffle($selectedRecipeList);
		$updateRs = $pdo->prepare("update weekly_highlight set lastUpdate =now(), r1=?, r2 = ?, r3=?, r4=?, r5=?, r6=?, r7=?");
		$r = $updateRs->execute($selectedRecipeList);
		
		if($r){
			echo "1";
		}else{
			echo "0";
		}
		
		exit;
	}
?>
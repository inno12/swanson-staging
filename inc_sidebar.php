

        <div id="sidebar-wrapper">

	            <ul class="sidebar-nav">
	            	<li style="text-indent: 0px;">
	                   <img src="<?=ROOT_PATH?>images/logo.png" style="max-height:50px;margin:10px 5px;">
	                   <div class="sidebar-cancel-btn">
		                   <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
	                   </div>
	                </li>
	                <li>
	                    <a href="<?=ROOT_PATH?>index.php">主頁</a>
	                </li>
	                <li>
	                    <a href="<?=ROOT_PATH?>recipe-list.php">食譜搜尋</a>
	                </li>
	                <li>
	                    <a href="<?=ROOT_PATH?>recipe-video-tsui.php">鮮味炒教室</a>
	                </li>
	                <li>
	                    <a href="<?=ROOT_PATH?>recipe-video.php">入廚教學短片</a>
	                </li>
	                <li>
	                    <a href="<?=ROOT_PATH?>product-list.php">史雲生系列</a>
	                </li>
	                <li>
	                    <a href="<?=ROOT_PATH?>cookbook.php">1盒煮1餸簡易食譜下載</a>
	                </li>
	                <li>
	                    <a href="<?=ROOT_PATH?>cookbook-scs.php">調味雞汁鮮味炒食譜下載</a>
	                </li>
	                <li>
	                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSfEQ5Zj9McgBkJweKYRy7eF7IplUTScDRqdi4MHmCPciCTMOg/viewform?usp=sf_link" target="_blank">有獎問卷</a>
	                </li>
	                <li>
	                    <a href="<?=ROOT_PATH?>cookbook-scs.php">鮮味炒試食地點</a>
	                </li>
	            </ul>

	            <div class="sidebar-bottom">
	            	<div>
			            <a href="https://www.youtube.com/channel/UCAQGuP8ViNiIZIb8CcDQ3nQ/videos" target="_blank" style="margin-right:8px;">
							<img class="sidebar-sharing"  src="<?=ROOT_PATH?>images/youtubeBtn.png">
						</a>
	            	</div>

	            	<div>
						<a onclick ="sharePage()" style="margin-right:1px;cursor:pointer;">
							<img class="sidebar-sharing"  src="<?=ROOT_PATH?>images/facebookBtn.png" />
						</a>
	            	</div>

	            	<div style="color:#FFF;min-width: 200px;">
	            		® Registered trademark of Campbell Soup Company.<br/>
						&copy; 2016 Campbell Soup Asia Limited.
	            	</div>
	            </div>



        </div>
		<div class="sideBarOverlay" >

		</div>

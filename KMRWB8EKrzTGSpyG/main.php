<?php 
	
	require_once "../_inc_/global_config.php";
	
	require_once DOC_ROOT.INC_PATH."init.php";
	require_once DOC_ROOT.ADMIN_ROOT_INC_PATH."admin_config.php";
	
	require_once DOC_ROOT.ADMIN_ROOT_INC_PATH."admin_utils.php";
	if(!isAdminLogin()){

		header("location:".ADMIN_ROOT_PATH);
		
	}
	
	$urlParam = parseUrl();
	if(sizeof($urlParam) == 0){
		//empty
	}else if(sizeof($urlParam) > 0 && sizeof($urlParam) <= 7){
		
		if(!empty($urlParam[0])){
			define(ADMIN_MODULE, strtolower($urlParam[0]));
		}
		
		if(!empty($urlParam[1])){
			define(ADMIN_SECTION, strtolower($urlParam[1]));
		}
		
		if(!empty($urlParam[2])){
			define(ADMIN_ACTION, strtolower($urlParam[2]));
		}
		
		if(!empty($urlParam[3])){
			define(ADMIN_ACTION_ID1, strtolower($urlParam[3]));
		}
		
		if(!empty($urlParam[4])){
			define(ADMIN_ACTION_ID2, strtolower($urlParam[4]));
		}
		
		if(!empty($urlParam[5])){
			define(ADMIN_ACTION_ID3, strtolower($urlParam[5]));
		}
		
		if(!empty($urlParam[6])){
			define(ADMIN_ACTION_ID4, strtolower($urlParam[6]));
		}
		
	
	}else{
		
		exit("Error: unexpected input");
		
	}
	
	
	
	
	
	function parseUrl(){
	
		if(isset($_GET['url'])){
				
			return $url = explode("/",rtrim($_GET['url']));
				
		}
	
	}
	
?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="UTF-8">
		<?php 
			include_once DOC_ROOT.ADMIN_ROOT_INC_PATH.'commonStyle.php';
			include_once DOC_ROOT.ADMIN_ROOT_INC_PATH.'commonScript.php';
		?>
		<title><?=APP_TITLE?> Admin Panel</title>
	</head>
	<body>
		<div style="width: 100%;height: 100%;overflow-y: hidden;">
		
			<div id="topmenuWrapper">
				<?php 
					include_once DOC_ROOT.ADMIN_ROOT_INC_PATH.'topmenu.php';
				?>
			</div>
		
			<div id="leftmenuWrapper">
				<?php 
					include_once  DOC_ROOT.ADMIN_ROOT_INC_PATH.'leftmenu.php';
				?>
			</div>
			 <div id="mainContentWrapper">
				 <?php 
				 	include_once  DOC_ROOT.ADMIN_ROOT_INC_PATH.'maincontent.php';
					
				 ?>
			</div>
		</div>
	</body>


</html>












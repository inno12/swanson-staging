<?php 
	require_once "../_inc_/global_config.php";
	require_once DOC_ROOT.INC_PATH."init.php";
	require_once DOC_ROOT.ADMIN_ROOT_INC_PATH."admin_utils.php";
	if(isAdminLogin()){
		header("location: home");
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="UTF-8">
		<?php 
			include_once DOC_ROOT.ADMIN_ROOT_INC_PATH.'commonStyle.php';
			include_once DOC_ROOT.ADMIN_ROOT_INC_PATH.'commonScript.php';
		?>
		<script type="text/javascript" src="<?=INC_PATH?>js/jquery.validate.min.js"></script>
		<link rel="stylesheet" type="text/css" href="<?=ADMIN_ROOT_INC_PATH?>css/login.css"/>
		
	</head>
	<body onload="bodyFadeIn();">

		<div style="display: table;width: 100%;height: 100%;">
			<div style="display: table-cell;vertical-align: middle;">
				<div id="formContainer">
					
					<h4>Welcome back! </h4>
					
					<form id="loginForm" class="form-horizontal" >
						<div class="fieldGroup">
							<input type="text" name="username" id="username" class="form-control" placeholder="Username"/> 
							<div class="errorContainer">Please input your username</div>
						</div>
						
						<div class="fieldGroup">
							<input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
							<div class="errorContainer">Please enter your password</div>
						</div>
						
						<div class="fieldGroup">
							<input type="submit" id="submitBtn" class="btn btn-info" value="Login" onclick="$('#loginForm').submit()" ></input>
						</div>
						
						<div id="loginIncorrect">
		               		Login incorrect. Please try again.
		               	</div>
					</form>
				
				
				</div>
			</div>
		</div>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	</body>



	<script>
		function bodyFadeIn(){
			$("html").fadeIn(300);
		}
		$(document).ready(function(){
			
			$("#loginForm").validate({
				rules:{
	   				username: "required",
					password:"required",
				}
				,submitHandler:function(form,e) {
					$("#loginIncorrect").fadeTo(300,0);
					e.preventDefault();
					$.post("<?=ADMIN_ROOT_INC_PATH."_api/user/user_api.php"?>?login",{
						username: $("[name=username]").val(),
		   				password: $("[name=password]").val(),

					}, function(data){

						if(data == "SUCCESS"){

							$("html").fadeOut(200, function(){
								window.location="home";
							});
						}else{
							$("#loginIncorrect").fadeTo(300, 1);
						}

					}); 
	   			}
	   			,errorPlacement: function(error, element) {
			   		$(element).parent().find(".errorContainer").css("opacity",1);
			    },
			    success: function (error, element) {
		    		$(element).parent().find(".errorContainer").css("opacity",0);
	            }
			});
		});
		    
				
				
	
	</script>

</html>
<?php

	if(ADMIN_MODULE == "home"){
		//home section
		$content = $homeMenu["home"];
		
	}else if(ADMIN_MODULE=="changepw"){
		$content = $generalContent ["changepw"];
		
	}else{
		
		//page content menu
		$content = $pageContentMenu[ADMIN_MODULE];
		
		//site config menu
		if(empty($content)){
			$content = $siteConfigMenu[ADMIN_MODULE];
		}
		
	}
	
	
	
	$totalSection = sizeof($content["section"]);
	$moduleType = $content["type"];
	

	if($totalSection == 1){
		//$tobeShownSection = array_keys ($content["section"])[0]; 
		$tobeShownSection = key($array);
	}
	
	
	if(!defined("ADMIN_SECTION")){
		
		if(array_key_exists("record", $content["section"]) && $moduleType =="CONTENT"){
				
			$sectionMain = "record";
			define(ADMIN_SECTION, $sectionMain);
				
		}else if( $moduleType =="SETTING"){
		
			$sectionMain = "record";
	
			define(ADMIN_SECTION, $sectionMain);
			define(ADMIN_ACTION, "edit");
			define(ADMIN_ACTION_ID1, 1);
			
		
		}else{
			$sectionMain = "home";
			
			define(ADMIN_SECTION, $sectionMain);
				
		}
		
	}else{
		$sectionMain = ADMIN_SECTION;
	}

	if(!defined("ADMIN_ACTION")){
		
		if($sectionMain =="record"){
			
			define(ADMIN_ACTION, "list");
	
		}else if($sectionMain =="setting"){
			
			define(ADMIN_ACTION, "edit");
		
		}else{

			define(ADMIN_ACTION, "list");
		}
	
	}
	
	
	include_once "section_top.php";
	include_once "section/".$sectionMain.".php";

	
?>
<?php
	if(DEBUG_MODE){
?>

	<pre class="xdebug-var-dump" dir="ltr">
<?php 
		echo "ADMIN_MODULE:  	". 		ADMIN_MODULE 		. "<br/>";
		echo "ADMIN_SECTION: 	". 		ADMIN_SECTION 		. "<br/>";
		echo "ADMIN_ACTION: 	". 		ADMIN_ACTION 		. "<br/>";
		echo "ADMIN_ACTION_ID1: ". 		ADMIN_ACTION_ID1 	. "<br/>";
		echo "ADMIN_ACTION_ID2: ". 		ADMIN_ACTION_ID2 	. "<br/>";
		echo "ADMIN_ACTION_ID3: ". 		ADMIN_ACTION_ID3 	. "<br/>";
		echo "ADMIN_ACTION_ID4: ". 		ADMIN_ACTION_ID4 	. "<br/>";
?>
	</pre>
<?php 
	}
?>
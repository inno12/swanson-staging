<div id="leftmenu_logo">
	<a href="<?=$logoContent["url"]?>" target="_blank">
		<img src="<?=ADMIN_ROOT_PATH.$logoContent["img"]?>"/>
	</a>
	<div>
	</div>
</div>






<div id="leftmenu_content">
	
	<?php 
	
		$displayName = $homeMenu["home"]["displayName"];
		$menuType=  $homeMenu["home"]["type"];
		$menuSections=  $homeMenu["home"]["section"];
	
	?>
	<div class="menuWrapper <?=(ADMIN_MODULE == "home")? "selected":""?>">
		<a href="<?=ADMIN_ROOT_PATH?>">
			<div class="menuContent">
				<span class="glyphicon glyphicon-home white" aria-hidden="true"></span>
				<?=$displayName?>
			</div>
		</a>
	</div>
	
	
	
	
	<div class="menuTitle">
		Page Content
	</div>
	
	<div class="menuSection">
	<?php
		$counter = 1;
		foreach($pageContentMenu as $module => $menuItem){

			if($menuItem["isShowInMenu"] === false){
				continue;
			}

			$displayName = $menuItem["displayName"];
			$menuType= $menuItem["type"];
			$menuSections= $menuItem["section"];
			
			if($menuType == "CONTENT"){
				$iconUrl = ADMIN_ROOT_PATH."include/images/icon_content.png";
				$icon = '<span class="glyphicon glyphicon-list white" aria-hidden="true"></span>';
			}else if($menuType == "HOME"){
				$iconUrl = ADMIN_ROOT_PATH."include/images/icon_home.png";
				$icon = '<span class="glyphicon glyphicon-home white" aria-hidden="true"></span>';
			}else if($menuType == "SETTING"){
				$iconUrl = ADMIN_ROOT_PATH."include/images/icon_setting.png";
				$icon = '<span class="glyphicon glyphicon-cog white" aria-hidden="true"></span>';
			}else if($menuType == "HIDDEN"){
				continue;
			}
				
			if(ADMIN_MODULE == $module){
				$class="selected";
			}else{
				$class="";
			}
			
			$counter++;
	?>
			<div class="menuWrapper <?=$class?>" 						style="position: relative">
				<a href="<?=ADMIN_ROOT_PATH.$module?>">
					<div class="menuContent">
						<?=$icon?>
						<?=$displayName?>
					</div>
				</a>
				
				
				
				<?php
				/* 
					foreach($menuSections as $sectionKey=>$section){
				?>
					<div class="submenu">
						<a href="<?=ADMIN_ROOT_PATH.$module."/".$sectionKey?>">
							<div>
								<?=$section["displayName"]?>
							</div>
						</a>
					</div>
				<?php 
					}
				?>
				
				
				
				
				
				
				
				
				
				
				<div style="" class="hoverMenu">
					<?php 
						foreach($menuSections as $sectionKey=>$section){
					?>
						<a href="<?=ADMIN_ROOT_PATH.$module."/".$sectionKey?>">
							<div>
								<?=$section["displayName"]?>
							</div>
						</a>
					<?php 
						}
					?>
				</div>
				*/
				?>
				
				
				
				
				
		</div>
	<?php 
		}
	?>
	</div>
	
	
	<div class="menuTitle"				>
		Settings
	</div>
	<div class="menuSection"		>
	<?php
		$counter = 1;
		foreach($siteConfigMenu as $module => $menuItem){
			$displayName = $menuItem["displayName"];
			$menuType= $menuItem["type"];
			$menuSections= $menuItem["section"];
			
			if($menuType == "SETTING"){
				$iconUrl = ADMIN_ROOT_PATH."include/images/icon_setting.png";
				$icon = '<span class="glyphicon glyphicon-cog white" aria-hidden="true"></span>';
			}else if($menuType == "EMAIL"){
				$iconUrl = ADMIN_ROOT_PATH."include/images/icon_email.png";
				$icon = '<span class="glyphicon glyphicon-envelope white" aria-hidden="true"></span>';
			}else if($menuType == "SETTING"){
				$iconUrl = ADMIN_ROOT_PATH."include/images/icon_setting.png";
				$icon = '<span class="glyphicon glyphicon-cog white" aria-hidden="true"></span>';
			}
			if(ADMIN_MODULE == $module){
				$class="selected";
			}else{
				$class="";
			}
			
			$counter++;
	?>
			<div class="menuWrapper <?=$class?>">
				<a href="<?=ADMIN_ROOT_PATH.$module?>">
					<div class="menuContent">
						<?=$icon?>
						<?=$displayName?>
					</div>
				</a>
			</div>
	<?php 
		}
	?>
		</div>
</div>

<script>

$(document).ready(function(){

	$(".menuWrapper").hover(
			function(){
				var hoverMenu = $(this).find(".hoverMenu");
				$(hoverMenu).addClass("menuHighlighted");		
			},
			function(){
				if($(".hoverMenu:hover").length==0){
					var hoverMenu = $(this).find(".hoverMenu");
					$(hoverMenu).removeClass("menuHighlighted");		
				}
			}	
	 );
});

</script>












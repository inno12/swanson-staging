
<div>

	<div id="pageTitle">
		<h1 style="display: inline-block"><?=$content["section"][$sectionMain]["displayName"]?>		</h1> &nbsp;&nbsp;
		<?php 
			if(ADMIN_ACTION == "new"){
				echo "(add)";
			}else if(ADMIN_ACTION=="edit"){
				echo "(edit)";
			}
		?>
	</div><!--  
--><div id="pageTopMenu">
		<?php 
			foreach(array_keys ($content["section"]) as $key){
				if($key != $sectionMain){
					
					if($key == "record"){
						
						$btnIcon = '<span class="glyphicon glyphicon-list-alt white" aria-hidden="true"></span> &nbsp;';
						
					}else if($key == "setting"){
						
						$btnIcon = '<span class="glyphicon glyphicon-cog white" aria-hidden="true"></span> &nbsp;';
						
					}else if($key == "manage_category"){
						
						$btnIcon = '<span class="glyphicon glyphicon-plus white" aria-hidden="true"></span> &nbsp;';
						
					}else{
						
						$btnIcon = '<span class="glyphicon glyphicon-file white" aria-hidden="true"></span> &nbsp;';

					}
					
					
					
		?>
				<a href="<?=ADMIN_ROOT_PATH.ADMIN_MODULE."/".$key?>">	
					<button type="button" class="btn btn-primary"><?=$btnIcon?><?=$content["section"][$key]["displayName"]?></button>
				</a>
		<?php 
				}
			}
		?>
	</div>
</div>

<script type="text/javascript" src="<?=ADMIN_ROOT_PATH?>include/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?=ADMIN_ROOT_PATH?>include/js/jquery.fancybox.pack.js"></script>
<?php include_once DOC_ROOT.INC_PATH.'js/utils.php';?>


<script type="text/javascript" src="<?=ADMIN_ROOT_PATH?>include/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=ADMIN_ROOT_PATH?>include/js/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="<?=ADMIN_ROOT_PATH?>include/js/jquery.nestable.js"></script>
<script type="text/javascript" src="<?=ADMIN_ROOT_PATH?>include/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=ADMIN_ROOT_PATH?>include/js/ckeditor/ckeditor.js"></script>
<script>
	CKEDITOR.config.width = "60%";
</script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.js"></script>


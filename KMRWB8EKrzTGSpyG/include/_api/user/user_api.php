<?php 
	require_once "../../../../_inc_/global_config.php";
	require_once DOC_ROOT.INC_PATH."init.php";
	
	if(array_key_exists("login", $_GET)){
		
		$loginUsername = $_POST["username"];
		$loginPw = $_POST["password"];
		
		$userSQL = "select id, password_salt, password, name from user where username = :username;";
		$params = array("username"=>$loginUsername);
		
		$result = DB::getInstance()->query($userSQL, $params);
		
		if(!$result->error()){
			$userRs = $result->singleResults();
			
			$userId = $userRs->id;
			$userSalt = $userRs -> password_salt;
			$userPassword = $userRs->password;
			$userName = $userRs->name;
			if(md5($loginPw) == $userPassword){
// 			if(md5($userSalt.md5($loginPw)) == $userPassword){
				$_SESSION[APP_ID."_LOGINID"] = $userId;
				$_SESSION[APP_ID."_NAME"] = $userName;
				echo "SUCCESS";
			}else{
				echo "UNSUCCESS";
			}
			
			
		}
		
		exit();
	}else if(array_key_exists("logout", $_GET)){
		unset($_SESSION[APP_ID."_LOGINID"]);
		unset($_SESSION[APP_ID."_NAME"]);
		
		echo "SUCCESS";
		
	}

?>

<?php 
	require_once "../../../../_inc_/global_config.php";
	
	require_once DOC_ROOT.INC_PATH."init.php";
	require_once DOC_ROOT.ADMIN_ROOT_INC_PATH."admin_config.php";
	
	if(array_key_exists("delete", $_GET)){
			
		if(!empty($_POST["module"])){
			define("ADMIN_MODULE", $_POST["module"]);
		}else{
			echo "error";
		}
		
		if(!empty($_POST["section"])){
			define("ADMIN_SECTION", $_POST["section"]);
		}else{
			echo "error";
		}
		
		if(!empty($_POST["id"])){
			$ids = $_POST["id"];
		}else{
			echo "error";
		}
		
		$content = $pageContentMenu[ADMIN_MODULE];
		
		if(empty($content)){
			$content = $siteConfigMenu[ADMIN_MODULE];
		}
		
		$deleteTable = $content ["section"] [ADMIN_SECTION] ["table"] ;
		
		$deleteKeyField = $content ["section"] [ADMIN_SECTION] ["keyField"];
		
		$query = "";
		$params = array();
// 		foreach($deleteKeyField as $keyField => $keyFieldValue){
// 			$query .= " AND `{$keyField}` = :{$keyField}";
// 			$params[$keyField] = constant($keyFieldValue);
// 		}
		
		
		
		$deleteSql = "delete from `{$deleteTable}`  where 1 = 1 AND id in ({$ids})";
		//Execute Query
		$deleteResultSet = DB::getInstance()->query($deleteSql, $params);
		
		if($deleteResultSet->error() ===false){
			echo "DELETE_DONE";
		}else{
			echo "ERROR";
		}
		
		exit;
	}else if(array_key_exists("updateDp", $_GET)){
		if(!empty($_POST["module"])){
			define("ADMIN_MODULE", $_POST["module"]);
		}else{
			echo "error";
		}
		
		if(!empty($_POST["section"])){
			define("ADMIN_SECTION", $_POST["section"]);
		}else{
			echo "error";
		}
		
		if(!empty($_POST["id"])){
			$id = $_POST["id"];
		}else{
			echo "error";
		}
		
		if( !empty($_POST["v"]) || ($_POST["v"] == "0")){
			$v = $_POST["v"];
		}else{
			echo "error";
		}
		
		$content = $pageContentMenu[ADMIN_MODULE];
		
		if(empty($content)){
			$content = $siteConfigMenu[ADMIN_MODULE];
		}
		
		$deleteTable = $content ["section"] [ADMIN_SECTION] ["table"] ;
		
		$updateKeyField = $content ["section"] [ADMIN_SECTION] ["keyField"];
		
		$query = "";
		
		$updateSql = "update `{$deleteTable}` set `display_priority`=:dp  where 1 = 1 AND id  = :id";
		$params = array("id" => $id, "dp"=>$v);
		//Execute Query
		$updateResultSet = DB::getInstance()->query($updateSql, $params);
		
		if($updateResultSet->error() ===false){
			echo "UPDATE_DONE";
		}else{
			echo "ERROR";
		}
	}
?>
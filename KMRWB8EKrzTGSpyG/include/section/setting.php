<?php

	if(ADMIN_ACTION == "edit" || ADMIN_ACTION == "new"){
		include_once "record_edit.php";
	}else if(ADMIN_ACTION == "list"){
		include_once "record_list.php";
	}else if(ADMIN_ACTION == "update"){
		include_once "record_update.php";
	}else if(ADMIN_ACTION == "add"){
		include_once "record_add.php";
	}else if(ADMIN_ACTION == "delete"){
		include_once "record_delete.php";
	}
	
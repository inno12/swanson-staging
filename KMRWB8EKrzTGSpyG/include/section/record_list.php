<?php
	$recordFields = $content ["section"] [$sectionMain] ["list"];
	//parse config
	$fieldsDisplayName = array();
	$fieldsName = array();
	$fieldsType = array();
	foreach($recordFields as $fieldInfo){
// 		array_push($fieldsDisplayName, $fieldInfo["displayName"]);
		if($fieldInfo["isShowInList"] !== false){
			array_push($fieldsName, $fieldInfo["fieldName"]);
		}
// 		array_push($fieldsType, $fieldInfo["type"]);
	}
	
	//prepare SQL
	$selectNameStr = implode("`,`",$fieldsName);
	$selectTable = $content ["section"] [$sectionMain] ["table"] ;
	$selectOrderBy = $content ["section"] [$sectionMain] ["orderby"];
	$selectSQL = "select `{$selectNameStr}` from {$selectTable} {$selectOrderBy}";

	//Execute Query
	$selectResultSet = DB::getInstance()->query($selectSQL)->results();
// 	echo $selectSQL;
	
// 	var_dump($selectResultSet);
	
// 	var_dump($recordFields);
?>



<div class="recordUtils">
	<a href="<?=ADMIN_ROOT_PATH.ADMIN_MODULE."/".ADMIN_SECTION."/new/"?>" type="button" class="btn btn-info">Add Record</a>
	<!-- <button type="button" class="btn btn-success">Export to Excel</button> -->
</div>



<div class="recordContainer">

	
	
	

	<table id="recordTable" class="table stripe hover"
		cellspacing="0" width="100%">
		<thead>
			<tr>
				<th width="20"> </th>
				<th width="40">Control</th>
				
				<?php 
					foreach($recordFields as $field){


						if($field["isShowInList"] === false){
							continue;
						}

						if(is_array($field["type"])){
							$fieldType = $field["type"][0];
						}else if(is_string($field["type"])){
							$fieldType = $field["type"];
						}
						
						if(in_array($fieldType, array("HIDDEN"))) continue;
				?>
							<th><?=$field["displayName"]?></th>				
				<?php
					}
				?>
			</tr>
		</thead>


		<tbody>
		
			<?php 
				//loop through record rows(row1, row2, row3..)
				foreach($selectResultSet as $index => $record){



			?>
				<tr>
					<td class="tableControl">
						<input class="tableRecordSelector" type="checkbox" value="<?=$record->id?>"/>
					</td>
					<td class="tableControl">
						<a href="<?=ADMIN_ROOT_PATH.ADMIN_MODULE."/".ADMIN_SECTION."/edit/".$record->id?>"><button type="button" class="btn btn-default" >Edit</button></a> 
						<a onclick="removeItem_popup('<?=$record->id?>')"><button type="button" class="btn btn-default">Delete</button></a>
					</td>
				<?php
					//loop through row column(name, code, price) 
					foreach($recordFields as $field){

						if($field["isShowInList"] === false){
							continue;
						}

						$fieldName = $field["fieldName"];
						$fieldValue = $record->$fieldName; 
						
						if(is_array($field["type"])){
							$fieldType = $field["type"][0];
							$fieldTypeOption = $field["type"][1];
							
						}else if(is_string($field["type"])){
							$fieldType = $field["type"];
						}

						//skip hidden value
						if(in_array($fieldType, array("HIDDEN"))) continue;
						
						
						$fieldWidth = $field["w"];
						$fieldSpWidth = !empty($fieldWidth) ? "style='width:".$fieldWidth.";min-width:".$fieldWidth."'":"";
						
				?>		
							<td <?=$fieldSpWidth?>>
								<?php 
									include "fields/list/{$fieldType}.php";
								?>
							</td>				
				<?php
					}
				?>
				</tr>
			<?php 
				}
			?>
		</tbody>
	</table>
	
	
	

	
	
</div>

<div class="recordUtils">
	<button type="button" class="btn btn-default" onclick="selectAllCheckbox()">Select All</button>
	<button type="button" class="btn btn-danger btn-enlarge" onclick="removeSelectedRecords();">Delete selected record(s)</button>

</div>








<script>

	$(document).ready(function() {
	    $('#recordTable').dataTable({
	    	 "pageLength": 30,
	    	 "lengthMenu": [ 30, 40, 50, 75, 100 ]
		});



		$(".editDp").click(function(d){
			var selectedId = $(d.target).data("id");
			var fieldV = $(d.target).data("v");
			var field = $(d.target).data("field");
			var t = d.target;
			$(d.target).html("");

			var input = $("<input>").addClass("form-control varcharInput").val(fieldV).appendTo(d.target).focus();
			var editLabel = $("<span>").addClass("editLabel").html("Edit").appendTo(d.target);
			
			input.on("blur", function(e){
				var v = $(e.target).val();
				onTriggerUpdateDp(t, selectedId,v);
			});

			input.on("keypress", function(e){
				if (e.keyCode == 13) {
					var v = $(e.target).val();
					onTriggerUpdateDp(t, selectedId,v);
				}
			});
		});
		
	});

	function onTriggerUpdateDp(t,id,v){
		if(isNaN(v)){
			createDialogConfirm("Invalid", "Please enter a number");
		}else{

			updateDp(
				id,
				v, 
				function(){
					$(t).html(v);
					$(t).data("v", v);
				}, function(){
					alert("Update failed, please try again later");
				}
			);
		
				
		}
	}

	
	function removeItem_popup(ids){
		
		var idList = ids.split(",");

		var title = "Delete record";
		var message = idList.length +" record(s) will be deleted, confirm?";
		
		createDialogConfirmCancel(	title,
									message,
									function(){
										removeItem(ids);
									},
									null);
	}

	

	

	function removeSelectedRecords(){
		var ids = [];
		$(".tableRecordSelector:checked").each(function(i,v){
			ids.push($(v).val());
		});
		
		removeItem_popup(ids.join());
	}

	function removeItem(ids){
		$.post("<?=ADMIN_ROOT_INC_PATH."_api/record/record_utils.php"?>?delete",
			{
				module:"<?=ADMIN_MODULE?>",
				section:"<?=ADMIN_SECTION?>",
				id:ids
			},
			function(data){
				if(data.trim() == "DELETE_DONE"){
					window.location = "<?=ADMIN_ROOT_PATH.ADMIN_MODULE."/".ADMIN_SECTION."/".ADMIN_ACTION."/"?>";
				}else{
					alert(data);
				}

			}
		);
	}

	function updateDp(id,v, trueEvent, falseEvent){
		$.post("<?=ADMIN_ROOT_INC_PATH."_api/record/record_utils.php"?>?updateDp",
			{
				module:"<?=ADMIN_MODULE?>",
				section:"<?=ADMIN_SECTION?>",
				id:id,
				v:v
			},
			function(data){
				if(data.trim() == "UPDATE_DONE"){
					if(trueEvent){
						trueEvent();
					}
				}else{
					if(falseEvent){
						falseEvent();
					}
				}

			}
		);
	}
	
	function selectAllCheckbox(){
		$(".tableRecordSelector").each(function() {
            this.checked = true;
        });
	}

	
</script>
<?php 
	// $sectionMain = "manage_category"
	
	//also include child
	$categoryDepth = $content ["section"] [$sectionMain]["level"];
	
	//prepare SQL
	$selectTable = $content ["section"] [$sectionMain] ["table"] ;
	
	$selectSQL = "select `id`, `name`, `parent_id`, `node_level` from {$selectTable} where 1 = 1 order by node_level asc,parent_id,  display_priority asc, id desc";
	
	//Execute Query
	$selectResultSet_cat = DB::getInstance()->query($selectSQL)->results();
	
	
	$nodeList = array();
	$nodeIdToParent = array();
	foreach($selectResultSet_cat as $nodeIndex => $nodeRecord){
		
		
		$nodeIdToParent[$nodeRecord->id] = $nodeRecord->parent_id;
		
		//top node
		if( $nodeRecord->node_level==0 && empty($nodeList[$nodeRecord->id])  ){
			$nodeList[$nodeRecord->id] = array("id"=>$nodeRecord->id, "name"=>$nodeRecord->name, "list"=>array());
		}
		
		if( $nodeRecord->node_level==1 ){
			
			$parent0_Id = $nodeRecord->parent_id;
			
			$nodeList[$parent0_Id]["list"][$nodeRecord->id] = array("id"=>$nodeRecord->id, "name"=>$nodeRecord->name, "list"=>array());
		}
		
		if( $nodeRecord->node_level==2 ){
			
			$parent1_Id = $nodeRecord->parent_id;
			$parent0_Id = $nodeIdToParent[$nodeRecord->parent_id];
			
			$nodeList[$parent0_Id]["list"][$parent1_Id]["list"][$nodeRecord->id] = array("id"=>$nodeRecord->id, "name"=>$nodeRecord->name, "list"=>array());
		}
		
		if( $nodeRecord->node_level==3 ){
			
			$parent2_Id = $nodeRecord->parent_id;
			$parent1_Id = $nodeIdToParent[$nodeRecord->parent_id];
			$parent0_Id = $nodeIdToParent[$parent1_Id];
			
			$nodeList[$parent0_Id]["list"][$parent1_Id]["list"][$parent2_Id]["list"][$nodeRecord->id] = array("id"=>$nodeRecord->id, "name"=>$nodeRecord->name, "list"=>array());
		}
		
		if( $nodeRecord->node_level==4 ){
				
			$parent3_Id = $nodeRecord->parent_id;
			$parent2_Id = $nodeIdToParent[$nodeRecord->parent_id];
			$parent1_Id = $nodeIdToParent[$parent2_Id];
			$parent0_Id = $nodeIdToParent[$parent1_Id];
				
			$nodeList[$parent0_Id]["list"][$parent1_Id]["list"][$parent2_Id]["list"][$parent3_Id]["list"][$nodeRecord->id] = array("id"=>$nodeRecord->id, "name"=>$nodeRecord->name, "list"=>array());
		}
		
	}
// 	var_dump($nodeList);
	
	
	
?>
<div class="recordUtils">
	<a href="<?=ADMIN_ROOT_PATH.$selectTable."/record/new/"?>" type="button" class="btn btn-info">Add Record</a>
	<button class="btn btn-default" onclick=" $('.node').nestable('expandAll');">
		<span class="glyphicon glyphicon-plus " aria-hidden="true"></span> &nbsp; Expand All
	</button>
	
	<button class="btn btn-default" onclick=" $('.node').nestable('collapseAll');">
		<span class="glyphicon glyphicon-minus " aria-hidden="true"></span> &nbsp; Collapse All
	</button>
</div>
<div>
</div>
<div class="node" >

	<?php 
		if(sizeof($nodeList)>0){
	?>
		<ol class="node-list">
	<?php 			
		}
	?>
	
	<?php 
		foreach($nodeList as $level1){
			$level1Name = $level1["name"];
			$level1Id = $level1["id"];
	?>
		<li class="node-item" data-id="<?=$level1Id?>">
			<div class="node-handle">
				<?=$level1Name?>
				<a onclick="removeItem_popup(<?=$level1Id?>)">
					<button type="button" class="btn btn-default deleteNodeBtn" >Delete</button>
				</a>
				<a href="<?=ADMIN_ROOT_PATH.$selectTable."/record/edit/".$level1Id?>" >
					<button type="button" class="btn btn-default">Edit</button>
				</a>
			</div>
			
			
			<?php 
				if(sizeof($level1["list"])>0){
			?>
				<ol class="node-list">
			<?php 			
				}
			?>
			
			<?php 
			foreach($level1["list"] as $level2){
					$level2Name = $level2["name"];
					$level2Id = $level2["id"];
			?>
				<li class="node-item" data-id="<?=$level2Id?>">
					<div class="node-handle">
						<?=$level2Name?>
						<a onclick="removeItem_popup(<?=$level2Id?>)">
							<button type="button" class="btn btn-default deleteNodeBtn" >Delete</button>
						</a>
						<a href="<?=ADMIN_ROOT_PATH.$selectTable."/record/edit/".$level2Id?>" >
							<button type="button" class="btn btn-default">Edit</button>
						</a>
					</div>
					
					
					
					
					
					
					
					
					<?php 
						if(sizeof($level2["list"])>0){
					?>
						<ol class="node-list">
					<?php 			
						}
					?>
					
					<?php 
					foreach($level2["list"] as $level3){
							$level3Name = $level3["name"];
							$level3Id = $level3["id"];
					?>
						<li class="node-item" data-id="<?=$level3Id?>">
							<div class="node-handle">
								<?=$level3Name?>
								<a onclick="removeItem_popup(<?=$level3Id?>)">
									<button type="button" class="btn btn-default deleteNodeBtn" >Delete</button>
								</a>
								<a href="<?=ADMIN_ROOT_PATH.$selectTable."/record/edit/".$level3Id?>" >
									<button type="button" class="btn btn-default">Edit</button>
								</a>
							</div>
							
							
							
							
							<?php 
								if(sizeof($level3["list"])>0){
							?>
								<ol class="node-list">
							<?php 			
								}
							?>
							
							<?php 
							foreach($level3["list"] as $level4){
									$level4Name = $level4["name"];
									$level4Id = $level4["id"];
							?>
								<li class="node-item" data-id="<?=$level4Id?>">
									<div class="node-handle">
										<?=$level4Name?>
										<a onclick="removeItem_popup(<?=$level4Id?>)">
											<button type="button" class="btn btn-default deleteNodeBtn" >Delete</button>
										</a>
										<a href="<?=ADMIN_ROOT_PATH.$selectTable."/record/edit/".$level4Id?>" >
											<button type="button" class="btn btn-default">Edit</button>
										</a>
									</div>
									
									
									
									
									
									
									
									
									
									
									
									
										<?php 
											if(sizeof($level4["list"])>0){
										?>
											<ol class="node-list">
										<?php 			
											}
										?>
										
										<?php 
										foreach($level4["list"] as $level5){
												$level5Name = $level5["name"];
												$level5Id = $level5["id"];
										?>
											<li class="node-item" data-id="<?=$level5Id?>">
												<div class="node-handle">
													<?=$level5Name?>
													<a onclick="removeItem_popup(<?=$level5Id?>)">
														<button type="button" class="btn btn-default deleteNodeBtn" >Delete</button>
													</a>
													<a href="<?=ADMIN_ROOT_PATH.$selectTable."/record/edit/".$level5Id?>" >
														<button type="button" class="btn btn-default">Edit</button>
													</a>
												</div>
											</li>
										<?php 
											}
										?>
										
										
										<?php 
											if(sizeof($level4["list"])>0){
										?>
											</ol>
										<?php
											} 
										?>
										
									
									
										
									
									
								</li>
							<?php 
								}
							?>
							
							
							<?php 
								if(sizeof($level3["list"])>0){
							?>
								</ol>
							<?php
								} 
							?>
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
						</li>
					<?php 
						}
					?>
					
					
					<?php 
						if(sizeof($level2["list"])>0){
					?>
						</ol>
					<?php
						} 
					?>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				</li>
			<?php 
				}
			?>
			
			
			<?php 
				if(sizeof($level1["list"])>0){
			?>
				</ol>
			<?php
				} 
			?>
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		</li>
	<?php 
		}
	?>
	
	
	<?php 
		if(sizeof($nodeList)>0){
	?>
		</ol>
	<?php
		} 
	?>
	
	
	
</div>
<form id="categoryForm" class="form-horizontal" action="<?=ADMIN_ROOT_PATH.ADMIN_MODULE."/category/update/"?>" method="post" enctype="multipart/form-data">
	<input name="nodeData" type="hidden" value=""/>
</form> 



<div id="formActionButton">
<!-- 	<button type="reset" class="btn btn-default"> -->
<!-- 	<span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> &nbsp; Reset 重設</button> -->
	<button class="btn btn-info" onclick="submitCategoryForm()">
	<span class="glyphicon glyphicon-ok white" aria-hidden="true"></span> &nbsp; Submit 提交</button>
</div>


 

<script>
	function submitCategoryForm(){
		 var listObj = $('.node').nestable('serialize');
		 
		$("[name=nodeData]").val(JSON.stringify(listObj));

		$("#categoryForm").submit();
	}
	$(function(){
		$('.node').nestable({
			group:1,
			 maxDepth:<?=$categoryDepth?> ,
			 rootClass       : 'node',
	         listClass       : 'node-list',
	         itemClass       : 'node-item',
	         dragClass       : 'node-dragel',
	         handleClass     : 'node-handle',
	         collapsedClass  : 'node-collapsed',
	         placeClass      : 'node-placeholder',
	         noDragClass     : 'node-nodrag',
	         emptyClass      : 'node-empty',
		});

		$('.node').on('change', function() {
				var listObj = $('.node').nestable('serialize');
				console.log(JSON.stringify(listObj));

				$(".node-item").each(function(i,v){
					if($(v).find(".node-list").length>0){
						$(v).find(".deleteNodeBtn").attr("disabled", "disabled");
					}else{
						$(v).find(".deleteNodeBtn").removeAttr("disabled", "disabled");
					}
			  });
		});

		$(".node a").on("mousedown", function(event) { // mousedown prevent nestable click
		    event.preventDefault();
		    return false;
		});

		$('.node').change();
	});


	function removeItem_popup(id){
		

		var title = "Delete record";
		var message = "Selected record will be deleted, confirm?";
		createDialogConfirmCancel(	title,
									message,
									function(){
										removeItem(id);
									},
									null);
	}

	

	function removeItem(id){
		$.post("<?=ADMIN_ROOT_INC_PATH."_api/record/record_utils.php"?>?delete",
			{
				module:"<?=$selectTable?>",
				section:"record",
				id:id
			},
			function(data){
				if(data.trim() == "DELETE_DONE"){
					window.location = "<?=ADMIN_ROOT_PATH.ADMIN_MODULE."/".ADMIN_SECTION."/"?>";
				}else{
					alert(data);
				}

			}
		);
	}
</script>

<?php 
	include_once UTILS_PATH."fieldUtils.php";
	
	// $fieldValue
	$pathToFile = PATH_TO_ULFILE;
	$_field_imageStyle = $fieldTypeOption["style"];
	$_field_imageWidth = empty($fieldTypeOption["w"]) ? "100%":$fieldTypeOption["w"];
	$_field_imageHeight = empty($fieldTypeOption["h"]) ? "auto":$fieldTypeOption["h"];
	$_field_imageClass = $fieldTypeOption["class"];
	$_field_imageId = $fieldTypeOption["id"];
	$_field_imageOther = $fieldTypeOption["other"];
	
	

	if(!empty($fieldValue)){
		if($_field_imageStyle == "AUTO"){
			
			outputImageByDimension($pathToFile, $fieldValue, $_field_imageWidth, $_field_imageHeight, $_field_imageClass, $_field_imageId, $_field_imageOther);
			
		}else if($_field_imageStyle =="FIT_W"){
			
			outputImageByDimension($pathToFile, $fieldValue, $_field_imageWidth, "auto", $_field_imageClass, $_field_imageId, $_field_imageOther);
			
		}else if($_field_imageStyle =="FIT_H"){
			
			outputImageByDimension($pathToFile, $fieldValue, "auto", $_field_imageHeight, $_field_imageClass, $_field_imageId, $_field_imageOther);
			
		}else if($_field_imageStyle =="CROP"){
			
			outputCropImageByDimension($pathToFile, $fieldValue, $_field_imageWidth, $_field_imageHeight, $_field_imageClass, $_field_imageId, $_field_imageOther);
			
		}else if($_field_imageStyle == "KEEP_DIMENSION"){
			
			outputImageIncludePadding($pathToFile, $fieldValue, $_field_imageWidth, $_field_imageHeight, $_field_imageClass, $_field_imageId, $_field_imageOther);
		}
		
	}else{
		
	}	
?>
	

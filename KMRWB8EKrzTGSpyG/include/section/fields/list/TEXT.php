<?php 
	if($fieldTypeOption["format"] == "TEXT" || $fieldTypeOption["format"] == "EDITOR"){
		if($fieldTypeOption["LANG"] == "EN"){

			$maxWord = 30;
			
			$rawText = strip_tags(html_entity_decode($fieldValue), "");
			$words = array();
			$words = explode(" ", $rawText);
		
			$slicedWords = array_slice($words, 0, $maxWord);
			
			echo implode(" ", $slicedWords);
			
			if(count($words)>$maxWord){
				echo "...";
			}
			
		}else if($fieldTypeOption["LANG"] == "ZH" || $fieldTypeOption["LANG"] == "CN"){
	
			$max = 20;
			
			echo mb_substr(strip_tags(html_entity_decode($fieldValue), ""), 0 , $max,"utf-8");
			
			if(strlen($fieldValue) > $max){
				echo "...";
			}
		}
	
	}
?>
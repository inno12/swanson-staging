<?php 

	$dot="";

	if($fieldTypeOption["src"] == "LIST"){
		$tempList = array();
		if(!empty($fieldValue)){
			$valueList = explode(";", $fieldValue);

			$cnt = 0;
			foreach($valueList as $v){
				if($cnt>=3){
					$dot="...";
					break;
				}
				array_push($tempList, $fieldTypeOption["data"][$v]);
				$cnt++;
			}
		}
		
	}else if($fieldTypeOption["src"] =="TABLE"){
		$tempList = array();
		if(!empty($fieldValue)){
			$valueList = explode(";", $fieldValue);
			$data = $fieldTypeOption["data"];
			$selectTable = $data["table"];
			$selectName = $data["name"];
			$selectId = $data["id"];
			$selectOrder = $data["order"];
				
			$selectDataSQL = "select {$selectId}, {$selectName} from {$selectTable} where 1 = 1 and {$selectId} IN(".implode(",",$valueList).") {$selectOrder}";
	// 		$param =array($selectId=>$fieldValue);
			$selectDataResultSet = DB::getInstance()->query($selectDataSQL)->results();

		
			foreach($selectDataResultSet as $i =>$obj){
				if($i>=3){
					$dot="...";
					break;
				}
				array_push($tempList, $obj->$selectName); 
			}
		}
	}
	
?>


<?=implode(", ", $tempList)?> <?=$dot?>


<label for="<?=$fieldName?>" class="formLabel control-label" ><?=$fieldDisplayName?> : </label>



<div class="radioBtnContainer">


<?php
	if(!empty($selectResultSet)){
		$valueList = explode(";", $selectResultSet->$fieldName);
	}

	if($fieldTypeOption["src"] == "LIST"){
		$data = $fieldTypeOption["data"];
		
		foreach ($data as $data_k =>$data_v){
			
			if(!empty($valueList)){
				$isSelected = in_array($data_k, $valueList);
			}
			
			if($isSelected){
				$s = "checked='checked'";
			}else{
				$s = "";
			}
		
?>
	<label class="formRadioLabel">
		<input type="checkbox" name="<?=$fieldName?>[]" value="<?=$data_k?>" <?=$s?>/> <?=$data_v?>
	</label>			

<?php 
		}

	}else if($fieldTypeOption["src"] =="TABLE"){

		$data = $fieldTypeOption["data"];
		$selectTable = $data["table"];
		$selectName = $data["name"];
		$selectId = $data["id"];
		$selectOrder = $data["order"];
		
		$selectDataSQL = "select {$selectId}, {$selectName} from {$selectTable} where 1 = 1 {$selectOrder}";
		$selectDataResultSet = DB::getInstance()->query($selectDataSQL)->results();
		foreach($selectDataResultSet as $dataRec){
			$selectData_id = $dataRec->id;
			$selectData_name = $dataRec->$selectName;

			if(!empty($valueList)){
				$isSelected = in_array($selectData_id, $valueList);
			}
			
			if($isSelected){
				$s = "checked='checked'";
			}else{
				$s = "";
			}


?>
	<label class="formRadioLabel">
		<input type="checkbox" name="<?=$fieldName?>[]" value="<?=$selectData_id?>" <?=$s?>/> <?=$selectData_name?>
	</label>			
			
<?php 
		}	

	}

	
?>

</div>




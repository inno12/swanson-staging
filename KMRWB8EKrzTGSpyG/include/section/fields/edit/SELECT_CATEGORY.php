<label for="<?=$fieldName?>" class="formLabel control-label" ><?=$fieldDisplayName?> : </label>



<div class="selectContainer">

	<select class="form-control" name="<?=$fieldName?>" >
	
	

<?php 
	if($fieldTypeOption["src"] == "LIST"){
		$data = $fieldTypeOption["data"];
		
		foreach ($data as $data_k =>$data_v){

		$isSelected = $selectResultSet->$fieldName == $data_k;
		if($isSelected){
			$s = "selected='selected'";
		}else{
			$s = "";
		}
		
?>
	<option value="<?=$data_k?>" <?=$s?>> <?=$data_v?>
<?php 
		}

	}else if($fieldTypeOption["src"] =="TABLE"){
?>
	<option value="0">TOP<?=$level1Name?>
<?php
		$data = $fieldTypeOption["data"];
		$selectTable = $data["table"];
		$selectName = $data["name"];
		
		$selectDataSQL = "select * from {$selectTable} where 1 = 1 order by node_level asc,parent_id,  display_priority asc, id desc";
		//Execute Query
		$selectResultSet_cat = DB::getInstance()->query($selectDataSQL)->results();
		
		$nodeList = array();
		$nodeIdToParent = array();
		foreach($selectResultSet_cat as $nodeIndex => $nodeRecord){
			$nodeIdToParent[$nodeRecord->id] = $nodeRecord->parent_id;
			
			//top node
			if( $nodeRecord->node_level==0 && empty($nodeList[$nodeRecord->id])  ){
				$nodeList[$nodeRecord->id] = array("id"=>$nodeRecord->id, "name"=>$nodeRecord->$selectName, "list"=>array());
			}
			
			if( $nodeRecord->node_level==1 ){
				
				$parent0_Id = $nodeRecord->parent_id;
				
				$nodeList[$parent0_Id]["list"][$nodeRecord->id] = array("id"=>$nodeRecord->id, "name"=>$nodeRecord->$selectName, "list"=>array());
			}
			
			if( $nodeRecord->node_level==2 ){
				
				$parent1_Id = $nodeRecord->parent_id;
				$parent0_Id = $nodeIdToParent[$nodeRecord->parent_id];
				
				$nodeList[$parent0_Id]["list"][$parent1_Id]["list"][$nodeRecord->id] = array("id"=>$nodeRecord->id, "name"=>$nodeRecord->$selectName, "list"=>array());
			}
			
			if( $nodeRecord->node_level==3 ){
				
				$parent2_Id = $nodeRecord->parent_id;
				$parent1_Id = $nodeIdToParent[$nodeRecord->parent_id];
				$parent0_Id = $nodeIdToParent[$parent1_Id];
				
				$nodeList[$parent0_Id]["list"][$parent1_Id]["list"][$parent2_Id]["list"][$nodeRecord->id] = array("id"=>$nodeRecord->id, "name"=>$nodeRecord->$selectName, "list"=>array());
			}
			
			if( $nodeRecord->node_level==4 ){
					
				$parent3_Id = $nodeRecord->parent_id;
				$parent2_Id = $nodeIdToParent[$nodeRecord->parent_id];
				$parent1_Id = $nodeIdToParent[$parent2_Id];
				$parent0_Id = $nodeIdToParent[$parent1_Id];
					
				$nodeList[$parent0_Id]["list"][$parent1_Id]["list"][$parent2_Id]["list"][$parent3_Id]["list"][$nodeRecord->id] = array("id"=>$nodeRecord->id, "name"=>$nodeRecord->$selectName, "list"=>array());
			}
			
		}

		foreach($nodeList as $level1){
				$level1Name = $level1["name"];
				$level1Id = $level1["id"];
				$s = $selectResultSet->$fieldName == $level1Id ? "selected='selected'" : "";
		?>
			<option value="<?=$level1Id?>" <?=$s?>>&nbsp;&nbsp;<?=$level1Name?>
		
				<?php 
					foreach($level1["list"] as $level2){
							$level2Name = $level2["name"];
							$level2Id = $level2["id"];
							$s = $selectResultSet->$fieldName == $level2Id ? "selected='selected'" : "";
				?>
				
					<option value="<?=$level2Id?>" <?=$s?>> &nbsp;--&nbsp; <?=$level2Name?>
						
				<?php 
						foreach($level2["list"] as $level3){
								$level3Name = $level3["name"];
								$level3Id = $level3["id"];
								$s = $selectResultSet->$fieldName == $level3Id ? "selected='selected'" : "";
				?>
						<option value="<?=$level3Id?>" <?=$s?>> &nbsp;----&nbsp; <?=$level3Name?>
								
				<?php 
							foreach($level3["list"] as $level4){
									$level4Name = $level4["name"];
									$level4Id = $level4["id"];
									$s = $selectResultSet->$fieldName == $level4Id ? "selected='selected'" : "";
				?>
						<option value="<?=$level4Id?>" <?=$s?>> &nbsp;------&nbsp;<?=$level4Name?>
						
				<?php 
								foreach($level4["list"] as $level5){
										$level5Name = $level5["name"];
										$level5Id = $level5["id"];
										$s = $selectResultSet->$fieldName == $level5Id ? "selected='selected'" : "";
				?>
						<option value="<?=$level5Id?>" <?=$s?>> &nbsp;--------&nbsp; <?=$level5Name?>
						
				<?php 
							}
						}
					}
				}
			}
	}
	?>
	</select>
</div>


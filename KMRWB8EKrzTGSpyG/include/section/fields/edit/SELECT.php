<label for="<?=$fieldName?>" class="formLabel control-label" ><?=$fieldDisplayName?> : </label>



<div class="selectContainer">

	<select class="form-control" name="<?=$fieldName?>" >
	
		<option value=""> -- Please select -- </option>
	

<?php 
	if($fieldTypeOption["src"] == "LIST"){
		$data = $fieldTypeOption["data"];
		
		foreach ($data as $data_k =>$data_v){

		$isSelected = $selectResultSet->$fieldName == $data_k;
		if($isSelected){
			$s = "selected='selected'";
		}else{
			$s = "";
		}
		
?>
	<option value="<?=$data_k?>" <?=$s?>> <?=$data_v?>
<?php 
		}

	}else if($fieldTypeOption["src"] =="TABLE"){

		$data = $fieldTypeOption["data"];
		$selectTable = $data["table"];
		$selectName = $data["name"];
		$selectId = $data["id"];
		$selectOrder = $data["order"];
		
		$selectDataSQL = "select {$selectId}, {$selectName} from {$selectTable} where 1 = 1 {$selectOrder}";
		$selectDataResultSet = DB::getInstance()->query($selectDataSQL)->results();
		foreach($selectDataResultSet as $dataRec){
			$selectData_id = $dataRec->id;
			$selectData_name = $dataRec->$selectName;


			$isSelected = $selectResultSet->$fieldName == $selectData_id;
			if($isSelected){
				$s = "selected='selected'";
			}else{
				$s = "";
			}


?>
	<option value="<?=$selectData_id?>" <?=$s?>> <?=$selectData_name?>
<?php 
		}	

	}

	
?>
	</select>

</div>




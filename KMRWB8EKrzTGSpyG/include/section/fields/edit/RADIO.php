<label for="<?=$fieldName?>" class="formLabel control-label" ><?=$fieldDisplayName?> : </label>



<div class="radioBtnContainer">


<?php 
	if($fieldTypeOption["src"] == "LIST"){
		$data = $fieldTypeOption["data"];
		
		foreach ($data as $data_k =>$data_v){
		$isSelected = $selectResultSet->$fieldName == $data_k;
		if($isSelected){
			$s = "checked='checked'";
		}else{
			$s = "";
		}
		
?>
	<label class="formRadioLabel">
		<input type="radio" name="<?=$fieldName?>" value="<?=$data_k?>" <?=$s?>/> <?=$data_v?>
	</label>			

<?php 
		}

	}else if($fieldTypeOption["src"] =="TABLE"){

		$data = $fieldTypeOption["data"];
		$selectTable = $data["table"];
		$selectName = $data["name"];
		$selectId = $data["id"];
		$selectOrder = $data["order"];
		
		$selectDataSQL = "select {$selectId}, {$selectName} from {$selectTable} where 1 = 1 {$selectOrder}";
		$selectDataResultSet = DB::getInstance()->query($selectDataSQL)->results();
		foreach($selectDataResultSet as $dataRec){
			$selectData_id = $dataRec->id;
			$selectData_name = $dataRec->$selectName;


			$isSelected = $selectResultSet->$fieldName == $selectData_id;
			if($isSelected){
				$s = "checked='checked'";
			}else{
				$s = "";
			}


?>
	<label class="formRadioLabel">
		<input type="radio" name="<?=$fieldName?>" value="<?=$selectData_id?>" <?=$s?>/> <?=$selectData_name?>
	</label>			
			
<?php 
		}	

	}

	
?>

</div>




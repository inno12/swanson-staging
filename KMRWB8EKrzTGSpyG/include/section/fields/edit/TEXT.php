<?php 
	if($isHideLabel ===true){
?>
	<label for="<?=$fieldName?>" class="formLabel control-label" style="opacity:0"><?=$fieldDisplayName?> : </label>
<?php 
	}else{
?>
	<label for="<?=$fieldName?>" class="formLabel control-label" ><?=$fieldDisplayName?> : </label>
<?php 
	}
?>




<?php 
if($fieldTypeOption["format"] == "TEXT"){
?>
		
	<textarea class="textInput" id="<?=$fieldName?>" name="<?=$fieldName?>" placeholder="<?=$fieldTips?>"><?=$selectResultSet->$fieldName?></textarea>

<?php 
	}else if($fieldTypeOption["format"] == "TEXT_URLS"){
?>
		
	https://<textarea class="textInput" id="<?=$fieldName?>" name="<?=$fieldName?>" placeholder="<?=$fieldTips?>"><?=$selectResultSet->$fieldName?></textarea>

<?php 
	}else if($fieldTypeOption["format"] == "EDITOR"){
?>
	
	<textarea class="textInput" id="<?=$fieldName?>" name="<?=$fieldName?>" placeholder="<?=$fieldTips?>" ><?=$selectResultSet->$fieldName?> </textarea>
	

	<script>
		$(function(){
			CKEDITOR.replace( '<?=$fieldName?>' );
		});

	</script>
	
<?php 
	}
?>
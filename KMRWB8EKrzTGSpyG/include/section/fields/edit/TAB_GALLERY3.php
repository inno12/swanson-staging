<?php 
include_once UTILS_PATH."fieldUtils.php";

?>


	
<input type="hidden" name="<?=$fieldName?>_gallerytable" value="<?=$fieldTypeOption["table"]?>" />
<input type="hidden" name="<?=$fieldName?>_gallerytable2" value="<?=$fieldTypeOption["table2"]?>" />
	
					
					
					


	
<?php
	$selectDataSQL = "select * from `{$fieldTypeOption["table"]}` where 1 = 1 and {$selectTable}_id = :id order by display_priority asc";
	$param = array();
	$param["id"] = ADMIN_ACTION_ID1;
	$selectDataResultSet = DB::getInstance()->query($selectDataSQL, $param)->results();
?>







<div style="margin:8px 0px;">
	<input type="text" id="newTabValue" />
	<a onclick="addTab()" class="btn btn-default"> 增加版面 </a>
</div>
<div id="formTabs2">
  <ul >
  	<?php 
		foreach($selectDataResultSet as $i=>$tabItem){
	?>
		<li>
			<input type="hidden" name="tabseq[<?=$i?>]" value="<?=$i?>"/>
			<a  style='cursor:move' href="#tab<?=$i?>"><?=$tabItem->name?></a>
		</li>
	<?php 
		}
	?>
  </ul>
  
  <?php 
		foreach($selectDataResultSet as $i=>$tabItem){
	?>
		<div id='tab<?=$i?>' class='editFormTabContainer'>
			
			<div class="galleryEditor">	
				<div class="galleryEditorContainer">	
					<div>
						<label for="eventMenu[<?=$i?>]" class="formLabel control-label" style="vertical-align:top"> 版面名稱 : </label>				
						<input type="text" name="eventMenu_tabName[<?=$i?>]" class="form-control varcharInput" value="<?=$tabItem->name?>">			
					</div>	<br>	
					
					<label for="eventMenu[<?=$i?>]" class="formLabel control-label" style="vertical-align:top"> 圖像 : </label>			
					<div class="galleryUploader">				
						<input type="hidden" name="eventMenu_tabId[<?=$i?>]" value="<?=$tabItem->id?>"> 		
						<input type="file" id="eventMenu[<?=$i?>]" name="eventMenu[<?=$i?>][]" accept="image/*" multiple="multiple" class="btn btn-default">			
					</div><br>	
				</div>	
				
				<div style="margin-top:30px;">			
					<label style="font-weight:100;cursor:pointer;opacity:1 !important;">		
						<a class="btn btn-danger " target="_blank" style="color:#FFF;">			
							<input type="checkbox" class="deleteThisPage" name="eventMenu_delete[<?=$i?>]" value="<?=$tabItem->id?>" style="margin:0px;vertical-align: middle;">Delete		
						</a>												
					</label>
				</div>
			</div>
			
			
			
			
			
			
			
			
			
			<?php
				$selectDataSQL2 = "select * from `{$fieldTypeOption["table2"]}` where 1 = 1 and {$fieldTypeOption["table"]}_id = :id order by display_priority asc";
				$param2 = array();
				$param2["id"] = $tabItem->id;
				$selectDataResultSet2 = DB::getInstance()->query($selectDataSQL2, $param2)->results();
			?>
			
			<div  style="margin: 16px 8px;display:inline-block;width: 100%;">
				<div id="<?=$fieldName?>_sortableContainer_<?=$i?>" class="sortableContainer">
					<?php 
						foreach($selectDataResultSet2 as $i2=>$item2){
							$itemId = $item2->id;
							$itemImg= $item2->image;
							$itemFile= $item2->file;
					?>
								<div class="ui-state-default itemWrapper">
										
									<div class="imageContainer" style="height:200px">
										<input type="hidden" name="<?=$fieldName?>_item_order[<?=$i?>][]" value="<?=$i2+1?>"/>
										<input type="hidden" name="<?=$fieldName?>_item_id[<?=$i?>][]" value="<?=$itemId?>"/>
										<div class="itemOrder" style="font-weight:bold;">
											#<?=$i2+1?>
										</div>
										<div style="height:185px">	
											<?=outputImageIncludePadding(PATH_TO_ULFILE, $itemImg, "160px", "auto", "", "", array("style"=>"max-width:80%;"))?>
											
										</div>
									</div>
									<div style="height:40px">
										File:
										<?php
											if(empty($itemFile)){
										?>
											<input type="file" name="<?=$fieldName?>_item_file[<?=$i?>][]" style="display:inline-block;width:75%;"/>
										<?php 
											}else{
										?>
											<input type="file" name="<?=$fieldName?>_item_file[<?=$i?>][]" style="display:none"/>
											<a href="<?=PATH_TO_ULFILE.$itemFile?>" target="_blank" style="padding:8px;">
												<?php 
												if(!empty($selectResultSet->$fieldName)){
														outputImageByDimension(ADMIN_ROOT_INC_PATH."/images/", "doc_file.png", "20px", "auto");
													}
												?>
											</a>
										<?php 
											}
										?>
									</div>
									
									<div class="imageOptions">
										<div style="display:inline-block;width:100%">
											<div>
												<div style="margin-top:5px;">
													<a class="btn btn-default"  href="<?=PATH_TO_ULFILE.$itemImg?>" target="_blank" >
														View
													</a>			
													
													<label style="font-weight:100;cursor:pointer;opacity:1 !important;">
														<a class="btn btn-danger "  target="_blank" style="color:#FFF;" >
															<input type="checkbox" name="<?=$fieldName?>_item_delete[<?=$i?>][]" value="<?=$itemId?>" style="margin:0px;vertical-align: middle;"/>Delete
														</a>											
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
					<?php 
						}
					?>						
				</div>
			</div>
				
			
		</div>
		
		
		
		
		
		
		<script>
			$(function(){


				$("[name='<?=$fieldName?>_delete[<?=$i?>]']").on("change", function(e){
					$(e.target).closest(".galleryEditor").toggleClass("tobeRemoved");
					$(e.target).closest(".editFormTabContainer").find(".itemWrapper input[type=checkbox]").click();

					if($(e.target).closest(".galleryEditor").hasClass("tobeRemoved")){
						$(e.target).closest(".editFormTabContainer").find(".itemWrapper input[type=checkbox]").prop("disabled", true);
					}else{
						$(e.target).closest(".editFormTabContainer").find(".itemWrapper input[type=checkbox]").prop("disabled", false).click();
					}
				});
				
		
				$("[name='<?=$fieldName?>_item_delete[<?=$i?>][]']").on("change",function(e){
					$(e.target).closest(".itemWrapper").toggleClass("tobeRemoved");
				});
				
				$( "#<?=$fieldName?>_sortableContainer_<?=$i?>" ).sortable({
					update: function(event, ui) {
						
						<?=$fieldName?>_<?=$i?>_reorder();
					}
		
				});
			    $( "#<?=$fieldName?>_sortableContainer_<?=$i?>" ).disableSelection();
			    <?=$fieldName?>_<?=$i?>_reorder();
			});
		
			function <?=$fieldName?>_<?=$i?>_reorder(){
				$("#<?=$fieldName?>_sortableContainer_<?=$i?> .itemWrapper .imageContainer").each(function(i,v){
					var id = $(v).data("id");
					$(".itemOrder",v).html("#" + (i+1));
					$("[name='<?=$fieldName?>_item_order[<?=$i?>][]']", v).val((i+1));
					
				});
		
		
			}
		
			
		</script>
		
		
		
		
		
		
		
	<?php 
		}
	?>
</div>





<script>
	<?php 
	
	?>
	function addTab(){
		var tabName = $("#newTabValue").val();
		if(tabName.length > 0){
			
			 var num_tabs = $("div#formTabs2 ul li").length;
			 
	        $("div#formTabs2 ul").append(
	            "<li><input type='hidden' name='tabseq["+num_tabs+"]' value='"+num_tabs+"'/>"+
	            	"<a  style='cursor:move' class='tabBtn' href='#tab"+num_tabs+"'>" + tabName + "</a></li>"
	        );
	        
			$("div#formTabs2").append(
	            "<div id='tab"+num_tabs+"' class='editFormTabContainer'>" + getUploadCode(num_tabs, tabName) + "</div>"
	        );

			 $("div#formTabs2").tabs("refresh");
			 $("#newTabValue").val("");

			 $("div#formTabs2 li:last-child a").click();
			 
			$("[name='<?=$fieldName?>_delete["+num_tabs+"]']").on("change", function(e){
				$(e.target).closest(".galleryEditor").toggleClass("tobeRemoved");
			});
			 
		}else{
			createDialogConfirm("Remark","請輸入版面名稱",null);
		}


	}

	function getUploadCode(num,tabName){
		 

		var t=
	'<div class="galleryEditor">'+
	'	<div class="galleryEditorContainer">'+
		'	<div><label for="<?=$fieldName?>['+num+']" class="formLabel control-label" style="vertical-align:top" > 版面名稱 : </label>		'+
		'		<input type="text" name="<?=$fieldName?>_tabName['+num+']" class="form-control varcharInput" value="'+tabName+'">		'+
		'	</div>	<br/>'+

		'	<label for="<?=$fieldName?>['+num+']" class="formLabel control-label" style="vertical-align:top" > 圖像 : </label>		'+
		
		'	<div class="galleryUploader">		'+
		'		<input type="hidden" name="<?=$fieldName?>_tabId['+num+']" value="0"> '+
	
		'		<input type="file" id="<?=$fieldName?>['+num+']" name="<?=$fieldName?>['+num+'][]"  accept="image/*" multiple="multiple" class="btn btn-default"  />		'+
		'	</div><br/>'+
	'	</div>' +
	

		'	<div style="margin-top:30px;">	'+
		'		<label style="font-weight:100;cursor:pointer;opacity:1 !important;">' +
		'		<a class="btn btn-danger "  target="_blank" style="color:#FFF;" >' +
		'			<input type="checkbox" class="deleteThisPage" name="<?=$fieldName?>_delete['+num+']" value="'+tabName+'" style="margin:0px;vertical-align: middle;"/>Delete' +
		'		</a>											' +
		'	</div>' +
		'</div>'+	
		'</label>'
		;

		return t;
	}
	
	$(function() {

		
		var tabs = $( "#formTabs2" ).tabs();
		tabs.find( ".ui-tabs-nav" ).sortable({
		      axis: "xy",
		      stop: function() {
		        tabs.tabs( "refresh" );

		        $("#formTabs2 li").each(function(i,v){
					var seq = $(v).index("#formTabs2 li");
					$(v).find("[name^=tabseq]").val(seq);
				});
				
		      }
	    });
	});


</script>


















	

					






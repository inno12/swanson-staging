<?php 
	include_once UTILS_PATH."fieldUtils.php";
	
	
	
	if(!empty($fieldTypeOption["path"])){
			
		$pathToFile = ROOT_PATH. $fieldTypeOption["path"];
		
	}else{
		$pathToFile = PATH_TO_ULFILE;
	}
	
	$isEnableDelete = $fieldTypeOption["enableDelete"]===false ? false:true;
?>	
<label for="<?=$fieldName?>" class="formLabel control-label" style="vertical-align:top" ><?=$fieldDisplayName?> : </label>
<div style="display:inline-block">
	<input type="hidden" name="<?=$fieldName?>" value="1">
	<input type="hidden" name="existing_<?=$fieldName?>" value="<?=$selectResultSet->$fieldName?>">
	<input type="file" id="<?=$fieldName?>" name="<?=$fieldName?>"   class="fileInput" value="<?=$selectResultSet->$fieldName?>">
	 
	
	<a href="<?=$pathToFile.$selectResultSet->$fieldName?>" target="_blank" style="padding:8px;">
		<?php 
		if(!empty($selectResultSet->$fieldName)){
				outputImageByDimension(ADMIN_ROOT_INC_PATH."/images/", "doc_file.png", "40px", "auto");
			}
		?>
	</a>
	
	<?php 
	
		if(!empty($selectResultSet->$fieldName) && $isEnableDelete){
	?>
		<label style="font-weight:100;cursor:pointer;opacity:1 !important;">
			<a class="btn btn-danger "  target="_blank" style="color:#FFF;" >
				<input type="checkbox" name="delete_<?=$fieldName?>" value="<?=$selectResultSet->$fieldName?>" style="margin:0px;vertical-align: middle;"/>Delete
			</a>											
		</label>
	<?php 
		}
	?>
	
</div>

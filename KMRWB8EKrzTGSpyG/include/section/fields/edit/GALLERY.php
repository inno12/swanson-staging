<?php 
include_once UTILS_PATH."fieldUtils.php";

?>

<label for="<?=$fieldName?>" class="formLabel control-label" style="vertical-align:top" ><?=$fieldDisplayName?> : </label>
<input type="hidden" name="<?=$fieldName?>" value="1">
<div class="galleryUploader">
	<input type="file" id="<?=$fieldName?>" name="<?=$fieldName?>[]"  accept="image/*" multiple="multiple" class="btn btn-default"  />
</div>


<div>

	
	
	<?php
		$selectDataSQL = "select * from `{$fieldTypeOption["table"]}` where 1 = 1 and {$selectTable}_id = :id order by display_priority asc";
		$param = array();
		$param["id"] = ADMIN_ACTION_ID1;
		$selectDataResultSet = DB::getInstance()->query($selectDataSQL, $param)->results();
	?>
	
		<input type="hidden" name="<?=$fieldName?>_gallerytable" value="<?=$fieldTypeOption["table"]?>" />
	
		<div  style="margin: 16px 8px;">
			
			<div id="<?=$fieldName?>_sortableContainer" class="sortableContainer">
				
				
				<?php 
					foreach($selectDataResultSet as $i=>$item){
						$itemId = $item->id;
						$itemImg= $item->image;
				?>
							
						<div class="ui-state-default itemWrapper">
						
							<div class="imageContainer" >
								<input type="hidden" name="<?=$fieldName?>_item_order[]" value="<?=$i+1?>"/>
								<input type="hidden" name="<?=$fieldName?>_item_id[]" value="<?=$itemId?>"/>
								<div class="itemOrder" style="font-weight:bold;">
									#<?=$i+1?>
								</div>
								<div style="height:225px">	
									<?php
									
										if($fieldTypeOption["type"]=="pdf"){
									?>
										<?=outputImageIncludePadding(ADMIN_ROOT_PATH."/images/", "pdf_file.png", "100px", "auto", "", "", array("style"=>"max-width:60%;"))?>
									<?php							
										}else{
									?>
										<?=outputImageIncludePadding(PATH_TO_ULFILE, $itemImg, "auto", "180px", "", "", array("style"=>"max-width:60%; "))?>
									<?php
										}
									?>
								
									
									
								</div>
							</div>
							
							<div class="imageOptions">
								<div style="display:inline-block;width:100%">
									<div>
										<div style="margin-top:5px;">
											<a class="btn btn-default"  href="<?=PATH_TO_ULFILE.$itemImg?>" target="_blank" >
												View
											</a>			
											
											<label style="font-weight:100;cursor:pointer;opacity:1 !important;">
												<a class="btn btn-danger "  target="_blank" style="color:#FFF;" >
													<input type="checkbox" name="<?=$fieldName?>_item_delete[]" value="<?=$itemId?>" style="margin:0px;vertical-align: middle;"/>Delete
													
													
												</a>											
											</label>
										</div>
										
										
									</div>
								</div>
							</div>
						</div>
				
				
				<?php 
					}
				?>
					
			</div>

		</div>
					
					
					
					
					
					
					
					
	
	
	
	
	
	

</div>









	<script>
	$(function(){

		$("[name='<?=$fieldName?>_item_delete[]']").on("change",function(e){
			$(e.target).closest(".itemWrapper").toggleClass("tobeRemoved");
		});
		
		$( "#<?=$fieldName?>_sortableContainer" ).sortable({
			update: function(event, ui) {
// 			      var oldIndex = (ui.item.index()+1);
// 			      var newIndex = ui.placeholder.index();
				
				<?=$fieldName?>_reorder();
			}

		});
	    $( "#<?=$fieldName?>_sortableContainer" ).disableSelection();
	    <?=$fieldName?>_reorder();
	});

	function <?=$fieldName?>_reorder(){
// 		var t=[];

		/*
			var lastIndex = $("#<?=$fieldName?>_sortableContainer .itemWrapper .imageContainer").length-1;
		*/		
		$("#<?=$fieldName?>_sortableContainer .itemWrapper .imageContainer").each(function(i,v){
			var id = $(v).data("id");
			$(".itemOrder",v).html("#" + (i+1));
			$("[name='<?=$fieldName?>_item_order[]']", v).val((i+1));
			
// 			t.push({id:id, i:i});
		});

<?php 

							
 	/* 	$.post("<?=$_SERVER["PHP_SELF"]?>?updatePosition", {t:JSON.stringify(t)}, function(data){
			//nothing					
		});	 */
							?>

	}

	
</script>

<div style="clear:both">
</div>
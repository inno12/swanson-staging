<?php 
	if($isHideLabel ===true){
?>
	<label for="<?=$fieldName?>" class="formLabel control-label" style="opacity:0"><?=$fieldDisplayName?> : </label>
<?php 
	}else{
?>
	<label for="<?=$fieldName?>" class="formLabel control-label" ><?=$fieldDisplayName?> : </label>
<?php 
	}
?>




<?php 
	$text = $fieldTypeOption["text"];
	
	$link = $fieldTypeOption["link"];

	$result = $fieldTypeOption["result"];

	
?>
<div style="display: inline-block;">  
	<button class="btn btn-default" id = "<?=$fieldName?>">
		<?=$text?>
	</button>
	<div id="<?=$fieldName?>_result">
		
	</div>
</div>
<script>


	$(function(){
		var <?=$fieldName?>result = [];
		<?php 
			foreach($result as $rk => $rv){
		?>	
			<?=$fieldName?>result["<?=$rk?>"]  = "<?=$rv?>";
		<?php 
			}
		?>
		
		$("#<?=$fieldName?>").click(function(e){
			e.preventDefault();
			
			$.post("<?=$link?>", function(data){

				createDialogConfirm("更新結果", <?=$fieldName?>result[data], function(){
					location.reload();
				});
				
				//alert(<?=$fieldName?>result[data]);
				//$("#<?=$fieldName?>_result").html(<?=$fieldName?>result[data]); 

			});
			
		});
	});
</script>
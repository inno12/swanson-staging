<?php 
	include_once UTILS_PATH."fieldUtils.php";
	
	$editTable = $fieldTypeOption["table"];
	$editFieldName = $fieldName;
	$editDisplayName = $fieldDisplayName;
?>
					
					


<label for="<?=$editFieldName?>" class="formLabel control-label" style="vertical-align:top" ><?=$editDisplayName?> : </label>
<input type="hidden" name="<?=$editFieldName?>" value="1">

						

	
	
	
	
	
	
					<div class="ui-state-default itemWrapper2" style="display:none" id="itemEditorTemplate_<?=$editFieldName?>2">
						
							<div class="imageContainer2" >
								<input type="hidden" name="<?=$editFieldName?>_item_order[]" value=""/>
								<input type="hidden" name="<?=$editFieldName?>_item_id[]" value=""/>
								<div class="itemOrder" style="font-weight:bold;text-align:left">
									#<?=$i+1?>
								</div>
								<div style="text-align:left" class="fieldHolder"  id="itemEditorTemplate_<?=$editFieldName?>">
									<?php 

										foreach($fieldInfo["child"] as  $fieldInfo2){
// 											$fieldName = $editTable."['".$fieldInfo2["fieldName"]."'][".$i."]";
											$fieldName = $editTable."[".$fieldInfo2["fieldName"]."][]";
											$fieldDisplayName = $fieldInfo2["displayName"];
											
											
											
											if(is_array($fieldInfo2["type"])){
												$fieldType = $fieldInfo2["type"][0];
												$fieldTypeOption = $fieldInfo2["type"][1];
											}else if(is_string($fieldInfo2["type"])){
												$fieldType = $fieldInfo2["type"];
											}
										
											if(in_array($fieldType, array("HIDDEN"))) continue;
										
										?>
																		
															
													<div class="rowContainer">
														<?php 
																include "{$fieldType}.php";
														?>
													</div>
										<?php 
											}
									
										?>
															
														
								
								
								</div>
								
							</div>
							
							<div class="imageOptions">
								<div style="display:inline-block;width:100%">
									<div>
										<div style="margin: 8px;text-align: left;">
											
											<label style="font-weight:100;cursor:pointer;opacity:1 !important;">
												<a class="btn btn-danger "  target="_blank" style="color:#FFF;" >
													<input type="checkbox" class="deleteListItem" name="<?=$editFieldName?>_item_delete[]" value="" style="margin:0px;vertical-align: middle;"/>Delete
													
													
												</a>											
											</label>
										</div>
										
										
									</div>
								</div>
							</div>
						</div>




<div>
	<a class="btn btn-default " onclick="addItemToList_<?=$editFieldName?>()"  >
		Add 
	</a>
</div>
			








<div>

	
	
	<?php
		
		$before = $selectResultSet;
		
		$selectDataSQL = "select * from `{$editTable}` where 1 = 1 and {$selectTable}_id = :id order by display_priority asc";
		$param = array();
		$param["id"] = ADMIN_ACTION_ID1;
		$selectResultDataSet = DB::getInstance()->query($selectDataSQL, $param)->results();
		
		
	?>
	
		<input type="hidden" name="<?=$editFieldName?>_gallerytable" value="<?=$editTable?>" />
	
		<div  style="margin: 16px 8px;">
			
			<div id="<?=$editFieldName?>_sortableContainer" class="sortableContainer">
				
				
				<?php 
					foreach($selectResultDataSet as $i=>$item){
						$itemId = $item->id;
						$itemImg= $item->image;
						
						
						
						
				?>
							
						<div class="ui-state-default itemWrapper2">
						
							<div class="imageContainer2" >
								<input type="hidden" name="<?=$editFieldName?>_item_order[]" value="<?=$i+1?>"/>
								<input type="hidden" name="<?=$editFieldName?>_item_id[]" value="<?=$itemId?>"/>
								<div class="itemOrder" style="font-weight:bold;text-align:left">
									#<?=$i+1?>
								</div>
								<div style="text-align:left" class="fieldHolder">
									<?php 

										foreach($fieldInfo["child"] as  $fieldInfo2){
// 											$fieldName = $editTable."['".$fieldInfo2["fieldName"]."'][".$i."]";
											$fieldName = $editTable."[".$fieldInfo2["fieldName"]."][]";
											$fieldDisplayName = $fieldInfo2["displayName"];
											
											$selectResultSet->$fieldName = $selectResultDataSet[$i]-> $fieldInfo2["fieldName"];
											
											
											if(is_array($fieldInfo2["type"])){
												$fieldType = $fieldInfo2["type"][0];
												$fieldTypeOption = $fieldInfo2["type"][1];
											}else if(is_string($fieldInfo2["type"])){
												$fieldType = $fieldInfo2["type"];
											}
										
											if(in_array($fieldType, array("HIDDEN"))) continue;
										
										?>
																		
															
													<div class="rowContainer">
														<?php 
																include "{$fieldType}.php";
														?>
													</div>
										<?php 
											}
									
										?>
															
														
								
								
								</div>
								
							</div>
							
							<div class="imageOptions">
								<div style="display:inline-block;width:100%">
									<div>
										<div style="margin: 8px;text-align: left;">
											
											<label style="font-weight:100;cursor:pointer;opacity:1 !important;">
												<a class="btn btn-danger "  target="_blank" style="color:#FFF;" >
													<input type="checkbox" class="deleteListItem" name="<?=$editFieldName?>_item_delete[]" value="<?=$itemId?>" style="margin:0px;vertical-align: middle;"/>Delete
													
													
												</a>											
											</label>
										</div>
										
										
									</div>
								</div>
							</div>
						</div>
				
				
				<?php 
					}
				?>
					
					
					
					<?php 
					$selectResultSet = $before;
					
					?>
			</div>

		</div>
					
					
					
					
<div>
	<a class="btn btn-default " onclick="addItemToList_<?=$editFieldName?>()"  >
		Add 
	</a>
</div>
					
					
					
					
	
	
	
	
	
	

</div>

 
				






	<script>
	$(function(){

		$("[name='<?=$editFieldName?>_item_delete[]']").on("change",function(e){
			$(e.target).closest(".itemWrapper2").toggleClass("tobeRemoved");
		});
		
		$( "#<?=$editFieldName?>_sortableContainer" ).sortable({
			update: function(event, ui) {
				
				<?=$editFieldName?>_reorder();
			}

		});
	    $( "#<?=$editFieldName?>_sortableContainer" ).disableSelection();
	    <?=$editFieldName?>_reorder();
	});

	function <?=$editFieldName?>_reorder(){
		$("#<?=$editFieldName?>_sortableContainer .itemWrapper2 .imageContainer2").each(function(i,v){
			var id = $(v).data("id");
			$(".itemOrder",v).html("#" + (i+1));
			$("[name='<?=$editFieldName?>_item_order[]']", v).val((i+1));
			
		});


	}


	function addItemToList_<?=$editFieldName?>(){

		var itemEditor = $("#itemEditorTemplate_<?=$editFieldName?>").html();
		var fieldTemplate = $("#itemEditorTemplate_<?=$editFieldName?>2").html();
		
		$("#<?=$editFieldName?>_sortableContainer").append(
			'<div class="ui-state-default itemWrapper2">'+
				fieldTemplate	+	
			'</div>'
		);
		$("#<?=$editFieldName?>_sortableContainer .itemWrapper2").last().find(".fieldHolder").html(itemEditor);
		
		$("#<?=$editFieldName?>_sortableContainer .itemWrapper2").last().focus();
		 <?=$editFieldName?>_reorder();
		$("#<?=$editFieldName?>_sortableContainer .itemWrapper2").last().get(0).scrollIntoView();
	}

	
</script>
	
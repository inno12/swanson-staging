<label for="<?=$fieldName?>" class="formLabel control-label" ><?=$fieldDisplayName?> : </label>

<div class="stepContainer stepContainerFor_<?=$fieldName?>">
	<?php 
		if(!empty($selectResultSet)){
			$valueList = explode(";", $selectResultSet->$fieldName);
		}
		$data = $fieldTypeOption["data"];
	
		foreach ($valueList as $v_k =>$v){
	?>	
		<div class="stepDiv">
			<span class="stepNum"><?=$v_k+1?></span><input type="text" class="form-control varcharInput stepValue"  name="<?=$fieldName?>[]" placeholder="<?=$fieldTips?>" value="<?=$v?>">
			<a class="<?=$fieldName?>_removeStepBtn"><span class="glyphicon glyphicon-remove " aria-hidden="true"></span></a>
		</div>
	<?php 
		}
	?>
	<div class="stepDiv">
		<span class="stepNum">New</span><input type="text" class="form-control varcharInput newStepToBe"  placeholder="<?=$fieldTips?>" value="">
		<a class="<?=$fieldName?>_addStepBtn"><span class="glyphicon glyphicon-ok " aria-hidden="true"></span></a>
	</div>
	
	
	<div class="<?=$fieldName?>_stepDiv_template" style="display:none">
		<span class="stepNum"></span><input disabled="disabled" type="text" class="form-control varcharInput"  name="<?=$fieldName?>[]" placeholder="<?=$fieldTips?>" value="">
		<a class="<?=$fieldName?>_removeStepBtn"><span class="glyphicon glyphicon-remove " aria-hidden="true"></span></a>
	</div>
</div>






<script>

	$(function(){
		$(".<?=$fieldName?>_removeStepBtn").on("click", function(e){
			$(e.target).closest(".stepDiv").fadeOut(200, function(){this.remove();reorder();});
			
		});

		$(".<?=$fieldName?>_addStepBtn").on("click", function(e){
			var newValue = $(e.target).closest(".stepDiv").find(".newStepToBe").val();
			$(e.target).closest(".stepDiv").find(".newStepToBe").val(""); 
			
			var step = $("<div class='stepDiv'>"+
							$(".<?=$fieldName?>_stepDiv_template").html()+
						"</div>");
			step.find("[disabled]").removeAttr("disabled").addClass("stepValue").val(newValue);
			$(step).insertBefore($(".stepContainerFor_<?=$fieldName?> .stepDiv").last());
			reorder();
		});

	});


	function reorder(){
		$(".stepContainerFor_<?=$fieldName?> .stepValue").each(function(i,v){
			$(v).parent().find(".stepNum").html((i+1));

		});

	}

</script>
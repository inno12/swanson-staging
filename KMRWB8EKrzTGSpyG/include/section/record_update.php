
<?php
if (!array_key_exists( "product_id2", $_POST) ) {
	// echo $_POST["product_id2"][0];
	$_POST["product_id2"][0] = NULL;
}
// var_dump($_POST);
// var_dump($_FILES);
// exit();
$specialHandleType = array("GALLERY","IMAGE","FILE", "TAB_GALLERY", "GALLERY_LIST", "TAB_GALLERY3", "PASSWORD");

$updateTable = $content ["section"] [$sectionMain] ["table"] ;

$updateKeyField = $content ["section"] [$sectionMain] ["keyField"];
$backToPage = $content["backTo"];
// var_dump($updateTable);

// var_dump($updateKeyField);

$typeList = array();
$filePath = array();
foreach( $content ["section"] [$sectionMain]["list"] as $field){

	$fieldName = $field["fieldName"];

	if(is_array($field["type"])){
		$fieldType = $field["type"][0];

	}else if(is_string($field["type"])){
		$fieldType = $field["type"];
	}

	$typeList[$fieldName] = $fieldType;
	if($fieldType == "FILE"){
		$fieldTypeOption = $field["type"][1];
		if(!empty($field["type"][1]["path"])){
			$filePath[$fieldName] = $field["type"][1]["path"];
		}
	}
}


$query = "";
$params = array();
foreach($updateKeyField as $keyField => $keyFieldValue){
	$query .= " AND `{$keyField}` = :{$keyField}";
	$params[$keyField] = constant($keyFieldValue);
}

$dataList = array();
foreach($typeList as $attrName => $temp){



	if(in_array($typeList[$attrName], $specialHandleType)){

		if($typeList[$attrName] == "PASSWORD"){
			$attrValue = md5($_POST[$attrName]);
			$params[$attrName] = $attrValue;
			array_push($dataList, "`{$attrName}` = :{$attrName}");
		}else if($typeList[$attrName] == "IMAGE"||$typeList[$attrName] == "FILE"){



			if(empty( $_FILES[$attrName]['tmp_name'])){

			}else if(!empty( $_FILES[$attrName]['tmp_name'])){
				$fileUpload = new file_upload;

				if(empty($filePath[$attrName])){
					$fileUpload->upload_dir = DOC_ROOT.PATH_TO_ULFILE ; // "files" is the folder for the uploaded files (you have to create this folder)
				}else{
					$fileUpload->upload_dir = DOC_ROOT.ROOT_PATH.$filePath[$attrName] ; // "files" is the folder for the uploaded files (you have to create this folder)
				}


				if($typeList[$attrName] == "IMAGE"){
					$fileUpload->extensions = array(".jpg", ".png", ".gif", ".bmp"); // specify the allowed extensions here
				}else{
					$fileUpload->extensions = array(".doc", ".pdf", ".xls", ".xlxs", ".docx",".jpg", ".png", ".gif", ".bmp", ".xml"); // specify the allowed extensions here
				}

				$fileUpload->max_length_filename = 100; // change this value to fit your field length in your database (standard 100)
				$fileUpload->rename_file = true;

				$fileUpload->the_temp_file = $_FILES[$attrName]['tmp_name'];
				$fileUpload->the_file = $_FILES[$attrName]['name'];
				$fileUpload->http_error = $_FILES[$attrName]['error'];
				if ($fileUpload->the_temp_file != "") {
					$fileUpload->replace = "n";
					$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
					$fileUpload->rename_file =true;
					$fileUpload->upload();
					$imageFile = $fileUpload->file_copy;
				}
				array_push($dataList, "`{$attrName}` = :{$attrName}");
				$params[$attrName] = $imageFile;

				@unlink(DOC_ROOT.PATH_TO_ULFILE.$_POST["existing_".$attrName]);


			}

			if(!empty($_POST["delete_".$attrName])){
				array_push($dataList, "`{$attrName}` = :{$attrName}");
				$params[$attrName] = "";
				@unlink(DOC_ROOT.PATH_TO_ULFILE.$_POST["delete_".$attrName]);

			}

		}else if($typeList[$attrName] == "GALLERY"){
			$galleryTable = $_POST[$attrName."_gallerytable"];

			//update seq
			if(sizeof($_POST[$attrName."_item_id"])>0){

				foreach($_POST[$attrName."_item_id"] as $galleryItemIndex => $galleryItemId){

					$galleryItemParam = array();
					$galleryItemParam["id"] = $galleryItemId;
					$galleryItemParam["display_priority"] = $_POST[$attrName."_item_order"][$galleryItemIndex];

					$updateGalleryItemSeq = "update `{$galleryTable}` set `display_priority` = :display_priority where id = :id";
					$updateGallerySeq = DB::getInstance()->query($updateGalleryItemSeq, $galleryItemParam);
				}

			}

			//delete
			if(sizeof($_POST[$attrName."_item_delete"])>0){


				foreach( $_POST[$attrName."_item_delete"] as $i => $v){

					$tobeDeleteGalleryItemSql = "select * from `{$galleryTable}` where id =:id";
					$tobeDeleteGalleryItem = DB::getInstance()->query($tobeDeleteGalleryItemSql, array("id"=>$v))->singleResults();
					@unlink(DOC_ROOT.PATH_TO_ULFILE.$tobeDeleteGalleryItem->image);

					$deleteGalleryItemSql = "delete from `{$galleryTable}` where id =:id";
					$deleteGalleryItemSet = DB::getInstance()->query($deleteGalleryItemSql, array("id"=>$v));

				}


			}


			//insert new
			if(sizeof($_FILES[$attrName]["name"]) > 0){


				foreach($_FILES[$attrName]["name"] as $index=>$name){

					if(empty($name)){
						continue;
					}

					$fileUpload = new file_upload;
					$fileUpload->upload_dir = DOC_ROOT.PATH_TO_ULFILE ; // "files" is the folder for the uploaded files (you have to create this folder)
					$fileUpload->extensions = array(".jpg", ".png", ".gif", ".bmp", ".pdf"); // specify the allowed extensions here
					$fileUpload->max_length_filename = 100; // change this value to fit your field length in your database (standard 100)
					$fileUpload->rename_file = true;

					$fileUpload->the_temp_file = $_FILES[$attrName]['tmp_name'][$index];
					$fileUpload->the_file = $_FILES[$attrName]['name'][$index];
					$fileUpload->http_error = $_FILES[$attrName]['error'][$index];
					if ($fileUpload->the_temp_file != "") {
						$fileUpload->replace = "n";
						$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
						$fileUpload->rename_file =true;
						$fileUpload->upload();
						$imageFile = $fileUpload->file_copy;
					}


					$insertSQL = "INSERT INTO `{$galleryTable}` (`image`,`{$updateTable}_id`, `display_priority`) VALUES
					( :image, :parent_id, :display_priority)";
					$paramsGallery = array();
					$paramsGallery["image"] = $imageFile;
					$paramsGallery["parent_id"] = ADMIN_ACTION_ID1;
					$paramsGallery["display_priority"] = 0;

					$insertGalleryRecord = DB::getInstance()->query($insertSQL, $paramsGallery);
				}

			}

		}else if($typeList[$attrName] == "TAB_GALLERY"){

			$galleryTable = $_POST[$attrName."_gallerytable"];
			$galleryTable2 = $_POST[$attrName."_gallerytable2"];

			//insert new files
			if(sizeof($_FILES[$attrName]["name"])>0){
				foreach($_FILES[$attrName]["name"] as $i=> $fileNameList){


					if(empty($_POST[$attrName."_tabId"][$i])){
						$insertSQL = "INSERT INTO `{$galleryTable}` (`name`,`{$updateTable}_id`, `display_priority`) VALUES ( :name, :parent_id, :display_priority)";

						$paramsGallery = array();
						$paramsGallery["name"] = $_POST[$attrName."_tabName"][$i];
						$paramsGallery["parent_id"] = ADMIN_ACTION_ID1;
						$paramsGallery["display_priority"] = $i;
						$insertGalleryRecord = DB::getInstance()->query($insertSQL, $paramsGallery);
						$galleryTableId = $insertGalleryRecord->getLastInsertId();
					}else{
						$galleryTableId = $_POST[$attrName."_tabId"][$i];
					}


					if(sizeof($fileNameList) > 0 && !empty($fileNameList)){

						foreach($fileNameList as $i2=>$fileName){

							if(empty($fileName)){
								continue;
							}

							$fileUpload = new file_upload;
							$fileUpload->upload_dir = DOC_ROOT.PATH_TO_ULFILE ; // "files" is the folder for the uploaded files (you have to create this folder)
							$fileUpload->extensions = array(".jpg", ".png", ".gif", ".bmp"); // specify the allowed extensions here
							$fileUpload->max_length_filename = 100; // change this value to fit your field length in your database (standard 100)
							$fileUpload->rename_file = true;

							$fileUpload->the_temp_file = $_FILES[$attrName]['tmp_name'][$i][$i2];
							$fileUpload->the_file = $_FILES[$attrName]['name'][$i][$i2];
							$fileUpload->http_error = $_FILES[$attrName]['error'][$i][$i2];
							if ($fileUpload->the_temp_file != "") {
								$fileUpload->replace = "n";
								$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
								$fileUpload->rename_file =true;
								$fileUpload->upload();
								$imageFile = $fileUpload->file_copy;
							}


							$insertSQL2 = "INSERT INTO `{$galleryTable2}` (`image`,`{$galleryTable}_id`, `display_priority`) VALUES ( :image, :parent_id, :display_priority)";
							$paramsGallery2 = array();
							$paramsGallery2["image"] = $imageFile;
							$paramsGallery2["parent_id"] = $galleryTableId;
							$paramsGallery2["display_priority"] = 0;

							$insertGalleryRecord2 = DB::getInstance()->query($insertSQL2, $paramsGallery2);

						}

					}
				}
			}


			//update
			if(sizeof($_POST[$attrName."_tabId"])>0){
				foreach($_POST[$attrName."_tabId"] as $i => $tabId){

					$tabName = $_POST[$attrName."_tabName"][$i];
					$tabSeq = $_POST["tabseq"][$i];

					$updateTabSql = "update `{$galleryTable}` set name =:name, display_priority =:display_priority where id =:id ";
					$updateGalleryTab = DB::getInstance()->query($updateTabSql, array("name"=>$tabName, "display_priority"=>$tabSeq,"id"=>$tabId));

				}
			}

			if(sizeof($_POST[$attrName."_item_id"])>0){
				foreach($_POST[$attrName."_item_id"] as $tabIndex => $tabIdList){

					foreach($tabIdList as $itemIndex => $itemId){

						$updateTabSql2 = "update `{$galleryTable2}` set display_priority =:display_priority where id =:id ";
						$updateGalleryItem = DB::getInstance()->query($updateTabSql2, array( "display_priority"=>$itemIndex+1,"id"=>$itemId));


					}

				}

			}

			//delete

			if(sizeof($_POST[$attrName."_item_delete"]) > 0){

				foreach( $_POST[$attrName."_item_delete"] as $tabIndex =>$itemList){

					if(sizeof($itemList)>0){

						foreach($itemList as $i=>$itemId){


							$tobeDeleteGalleryItemSql = "select * from `{$galleryTable2}` where id =:id";
							$tobeDeleteGalleryItem = DB::getInstance()->query($tobeDeleteGalleryItemSql, array("id"=>$itemId))->singleResults();
							@unlink(DOC_ROOT.PATH_TO_ULFILE.$tobeDeleteGalleryItem->image);



							$deleteGalleryItemSql = "delete from `{$galleryTable2}` where id =:id ";
							$deleteGalleryItem = DB::getInstance()->query($deleteGalleryItemSql, array("id"=>$itemId));

						}

					}

				}


			}else if(sizeof($_POST[$attrName."_delete"])>0){

				foreach($_POST[$attrName."_delete"] as $i=>$tabId){


					$itemSql =  "select * from `{$galleryTable2}` where {$galleryTable}_id =:pid";
					$tobeDeleteGalleryItem = DB::getInstance()->query($itemSql, array("pid"=>$tabId))->results();

					foreach($tobeDeleteGalleryItem as $item){
						@unlink(DOC_ROOT.PATH_TO_ULFILE.$item->image);
						$deleteGalleryItemSql = "delete from `{$galleryTable2}` where id =:id ";
						$deleteGalleryItem = DB::getInstance()->query($deleteGalleryItemSql, array("id"=>$item->id));
					}


					$deleteTabSql = "delete from `{$galleryTable}` where id =:id ";
					$deleteTab = DB::getInstance()->query($deleteTabSql, array("id"=>$tabId));
				}


			}



		}else if($typeList[$attrName] == "TAB_GALLERY3"){

			$galleryTable = $_POST[$attrName."_gallerytable"];
			$galleryTable2 = $_POST[$attrName."_gallerytable2"];

			//insert new files
			if(sizeof($_FILES[$attrName]["name"])>0){
				foreach($_FILES[$attrName]["name"] as $i=> $fileNameList){


					if(empty($_POST[$attrName."_tabId"][$i])){
						$insertSQL = "INSERT INTO `{$galleryTable}` (`name`,`{$updateTable}_id`, `display_priority`) VALUES ( :name, :parent_id, :display_priority)";

						$paramsGallery = array();
						$paramsGallery["name"] = $_POST[$attrName."_tabName"][$i];
						$paramsGallery["parent_id"] = ADMIN_ACTION_ID1;
						$paramsGallery["display_priority"] = $i;
						$insertGalleryRecord = DB::getInstance()->query($insertSQL, $paramsGallery);
						$galleryTableId = $insertGalleryRecord->getLastInsertId();
					}else{
						$galleryTableId = $_POST[$attrName."_tabId"][$i];
					}


					if(sizeof($fileNameList) > 0 && !empty($fileNameList)){

						foreach($fileNameList as $i2=>$fileName){

							if(empty($fileName)){
								continue;
							}

							$fileUpload = new file_upload;
							$fileUpload->upload_dir = DOC_ROOT.PATH_TO_ULFILE ; // "files" is the folder for the uploaded files (you have to create this folder)
							$fileUpload->extensions = array(".jpg", ".png", ".gif", ".bmp"); // specify the allowed extensions here
							$fileUpload->max_length_filename = 100; // change this value to fit your field length in your database (standard 100)
							$fileUpload->rename_file = true;

							$fileUpload->the_temp_file = $_FILES[$attrName]['tmp_name'][$i][$i2];
							$fileUpload->the_file = $_FILES[$attrName]['name'][$i][$i2];
							$fileUpload->http_error = $_FILES[$attrName]['error'][$i][$i2];
							if ($fileUpload->the_temp_file != "") {
								$fileUpload->replace = "n";
								$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
								$fileUpload->rename_file =true;
								$fileUpload->upload();
								$imageFile = $fileUpload->file_copy;
							}


							$insertSQL2 = "INSERT INTO `{$galleryTable2}` (`image`,`{$galleryTable}_id`, `display_priority`) VALUES ( :image, :parent_id, :display_priority)";
							$paramsGallery2 = array();
							$paramsGallery2["image"] = $imageFile;
							$paramsGallery2["parent_id"] = $galleryTableId;
							$paramsGallery2["display_priority"] = 0;

							$insertGalleryRecord2 = DB::getInstance()->query($insertSQL2, $paramsGallery2);

						}

					}
				}
			}


			//update
			if(sizeof($_POST[$attrName."_tabId"])>0){
				foreach($_POST[$attrName."_tabId"] as $i => $tabId){

					$tabName = $_POST[$attrName."_tabName"][$i];
					$tabSeq = $_POST["tabseq"][$i];

					$updateTabSql = "update `{$galleryTable}` set name =:name, display_priority =:display_priority where id =:id ";
					$updateGalleryTab = DB::getInstance()->query($updateTabSql, array("name"=>$tabName, "display_priority"=>$tabSeq,"id"=>$tabId));

				}
			}

			if(sizeof($_POST[$attrName."_item_id"])>0){
				foreach($_POST[$attrName."_item_id"] as $tabIndex => $tabIdList){

					foreach($tabIdList as $itemIndex => $itemId){

						$updateTabSql2 = "update `{$galleryTable2}` set display_priority =:display_priority where id =:id ";
						$updateGalleryItem = DB::getInstance()->query($updateTabSql2, array( "display_priority"=>$itemIndex+1,"id"=>$itemId));


					}

				}

			}

			//add doc

			if(sizeof($_FILES[$attrName."_item_file"]["name"])>0){

				foreach($_FILES[$attrName."_item_file"]["name"] as $tabIndex => $fileNameList){

					foreach($fileNameList as $fileIndex => $fileName){

						if(!empty($fileName)){

							$galleryItemId = $_POST[$attrName."_item_id"][$tabIndex][$fileIndex];

							$fileUpload = new file_upload;
							$fileUpload->upload_dir = DOC_ROOT.PATH_TO_ULFILE ; // "files" is the folder for the uploaded files (you have to create this folder)
							$fileUpload->extensions = array(".doc", ".pdf", ".xls", ".xlxs", ".docx",".jpg", ".png", ".gif", ".bmp", ".xml"); // specify the allowed extensions here
							$fileUpload->max_length_filename = 100; // change this value to fit your field length in your database (standard 100)
							$fileUpload->rename_file = true;

							$fileUpload->the_temp_file = $_FILES[$attrName."_item_file"]["tmp_name"][$tabIndex][$fileIndex];
							$fileUpload->the_file =  $_FILES[$attrName."_item_file"]['name'][$tabIndex][$fileIndex];
							$fileUpload->http_error =  $_FILES[$attrName."_item_file"]['error'][$tabIndex][$fileIndex];
							if ($fileUpload->the_temp_file != "") {
								$fileUpload->replace = "n";
								$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
								$fileUpload->rename_file =true;
								$fileUpload->upload();
								$uploadedFile = $fileUpload->file_copy;
							}




							$updateTabSql3 = "update `{$galleryTable2}` set file =:file where id =:id ";
							$updateGalleryItemFile = DB::getInstance()->query($updateTabSql3, array( "file"=>$uploadedFile,"id"=>$galleryItemId));

						}
					}
				}
			}



			//delete

			if(sizeof($_POST[$attrName."_item_delete"]) > 0){

				foreach( $_POST[$attrName."_item_delete"] as $tabIndex =>$itemList){

					if(sizeof($itemList)>0){

						foreach($itemList as $i=>$itemId){


							$tobeDeleteGalleryItemSql = "select * from `{$galleryTable2}` where id =:id";
							$tobeDeleteGalleryItem = DB::getInstance()->query($tobeDeleteGalleryItemSql, array("id"=>$itemId))->singleResults();
							@unlink(DOC_ROOT.PATH_TO_ULFILE.$tobeDeleteGalleryItem->image);
							@unlink(DOC_ROOT.PATH_TO_ULFILE.$tobeDeleteGalleryItem->file);



							$deleteGalleryItemSql = "delete from `{$galleryTable2}` where id =:id ";
							$deleteGalleryItem = DB::getInstance()->query($deleteGalleryItemSql, array("id"=>$itemId));

						}

					}

				}


			}else if(sizeof($_POST[$attrName."_delete"])>0){

				foreach($_POST[$attrName."_delete"] as $i=>$tabId){


					$itemSql =  "select * from `{$galleryTable2}` where {$galleryTable}_id =:pid";
					$tobeDeleteGalleryItem = DB::getInstance()->query($itemSql, array("pid"=>$tabId))->results();

					foreach($tobeDeleteGalleryItem as $item){
						@unlink(DOC_ROOT.PATH_TO_ULFILE.$item->image);
						@unlink(DOC_ROOT.PATH_TO_ULFILE.$item->file);
						$deleteGalleryItemSql = "delete from `{$galleryTable2}` where id =:id ";
						$deleteGalleryItem = DB::getInstance()->query($deleteGalleryItemSql, array("id"=>$item->id));
					}


					$deleteTabSql = "delete from `{$galleryTable}` where id =:id ";
					$deleteTab = DB::getInstance()->query($deleteTabSql, array("id"=>$tabId));
				}


			}


		}else if($typeList[$attrName] == "GALLERY_LIST"){



			$galleryTable = $_POST[$attrName."_gallerytable"];
			//update seq
			if(sizeof($_POST[$attrName."_item_id"])>0){

				foreach($_POST[$attrName."_item_id"] as $galleryItemIndex => $galleryItemId){

					$galleryItemParam = array();
					$galleryItemParam["id"] = $galleryItemId;
					$galleryItemParam["display_priority"] = $_POST[$attrName."_item_order"][$galleryItemIndex];

					$updateGalleryItemSeq = "update `{$galleryTable}` set `display_priority` = :display_priority where id = :id";
					$updateGallerySeq = DB::getInstance()->query($updateGalleryItemSeq, $galleryItemParam);
				}

			}


			//delete record
			if(sizeof($_POST[$attrName."_item_delete"])>0){


				foreach( $_POST[$attrName."_item_delete"] as $i => $v){

					$tobeDeleteGalleryItemSql = "select * from `{$galleryTable}` where id =:id";
					$tobeDeleteGalleryItem = DB::getInstance()->query($tobeDeleteGalleryItemSql, array("id"=>$v))->singleResults();
					@unlink(DOC_ROOT.PATH_TO_ULFILE.$tobeDeleteGalleryItem->image);

					$deleteGalleryItemSql = "delete from `{$galleryTable}` where id =:id";
					$deleteGalleryItemSet = DB::getInstance()->query($deleteGalleryItemSql, array("id"=>$v));

				}


			}

			//insert new
			if(sizeof($_FILES[$galleryTable]) > 0){


				//insert new files
				if(sizeof($_FILES[$galleryTable]["name"]["image"])>0){

							foreach($_FILES[$galleryTable]["name"]["image"] as $i2=>$fileName){

								//image

								$uploadedImage ="";
								$uploadedFile ="";
								if(empty($_FILES[$galleryTable]["name"]["image"][$i2])){

								}else{
									$fileUpload = new file_upload;
									$fileUpload->upload_dir = DOC_ROOT.PATH_TO_ULFILE ; // "files" is the folder for the uploaded files (you have to create this folder)
									$fileUpload->extensions = array(".jpg", ".png", ".gif", ".bmp"); // specify the allowed extensions here
									$fileUpload->max_length_filename = 100; // change this value to fit your field length in your database (standard 100)
									$fileUpload->rename_file = true;

									$fileUpload->the_temp_file = $_FILES[$galleryTable]["tmp_name"]["image"][$i2];
									$fileUpload->the_file =  $_FILES[$galleryTable]['name']["image"][$i2];
									$fileUpload->http_error =  $_FILES[$galleryTable]['error']["image"][$i2];
									if ($fileUpload->the_temp_file != "") {
										$fileUpload->replace = "n";
										$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
										$fileUpload->rename_file =true;
										$fileUpload->upload();
										$uploadedImage = $fileUpload->file_copy;
									}
								}

								//file
								if(empty($_FILES[$galleryTable]["name"]["file"][$i2])){

								}else{
									$fileUpload = new file_upload;
									$fileUpload->upload_dir = DOC_ROOT.PATH_TO_ULFILE ; // "files" is the folder for the uploaded files (you have to create this folder)
									$fileUpload->extensions = array(".doc", ".pdf", ".xls", ".xlxs", ".docx",".jpg", ".png", ".gif", ".bmp", ".xml"); // specify the allowed extensions here
									$fileUpload->max_length_filename = 100; // change this value to fit your field length in your database (standard 100)
									$fileUpload->rename_file = true;

									$fileUpload->the_temp_file = $_FILES[$galleryTable]["tmp_name"]["file"][$i2];
									$fileUpload->the_file =  $_FILES[$galleryTable]['name']["file"][$i2];
									$fileUpload->http_error =  $_FILES[$galleryTable]['error']["file"][$i2];
									if ($fileUpload->the_temp_file != "") {
										$fileUpload->replace = "n";
										$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
										$fileUpload->rename_file =true;
										$fileUpload->upload();
										$uploadedFile = $fileUpload->file_copy;
									}
								}



								if(empty($_POST[$attrName."_item_id"][$i2]) && (!empty($uploadedFile) || !empty($uploadedImage))){
									$galleryItemParam=array();
									$insertSQL3 = "INSERT INTO `{$galleryTable}` (`image`,`file`,`{$updateTable}_id`, `display_priority`) VALUES ( :image, :file, :parent_id, :display_priority)";
									$paramsGallery3 = array();
									$paramsGallery3["image"] = $uploadedImage;
									$paramsGallery3["file"] = $uploadedFile;
									$paramsGallery3["parent_id"] = ADMIN_ACTION_ID1;
									$paramsGallery3["display_priority"] = 0;
									$insertGalleryRecord2 = DB::getInstance()->query($insertSQL3, $paramsGallery3);

								}else if(!empty($_POST[$attrName."_item_id"][$i2]) && (!empty($uploadedFile) || !empty($uploadedImage))){
									$updateFieldList= array();
									$galleryItemParam = array();

									if(!empty($_FILES[$galleryTable]["name"]["image"][$i2])){
										$galleryItemParam["image"] = $uploadedImage;
										array_push($updateFieldList, "`image`=:image");
									}


									if(!empty($_FILES[$galleryTable]["name"]["file"][$i2])){
										$galleryItemParam["file"] = $uploadedFile;
										array_push($updateFieldList, "`file`=:file");
									}

									$updateFieldSql = implode(",",$updateFieldList );
									$galleryItemParam["id"] = $_POST[$attrName."_item_id"][$i2];
									$updateGalleryItemSql = "update `{$galleryTable}` set {$updateFieldSql} where id = :id";


									$updateGallerySeq = DB::getInstance()->query($updateGalleryItemSql, $galleryItemParam);
								}





							}

				}




// 				foreach($_FILES[$attrName]["name"] as $index=>$name){

// 					if(empty($name)){
// 						continue;
// 					}

// 					$fileUpload = new file_upload;
// 					$fileUpload->upload_dir = DOC_ROOT.PATH_TO_ULFILE ; // "files" is the folder for the uploaded files (you have to create this folder)
// 					$fileUpload->extensions = array(".jpg", ".png", ".gif", ".bmp", ".pdf"); // specify the allowed extensions here
// 					$fileUpload->max_length_filename = 100; // change this value to fit your field length in your database (standard 100)
// 					$fileUpload->rename_file = true;

// 					$fileUpload->the_temp_file = $_FILES[$attrName]['tmp_name'][$index];
// 					$fileUpload->the_file = $_FILES[$attrName]['name'][$index];
// 					$fileUpload->http_error = $_FILES[$attrName]['error'][$index];
// 					if ($fileUpload->the_temp_file != "") {
// 						$fileUpload->replace = "n";
// 						$fileUpload->do_filename_check = "n"; // use this boolean to check for a valid filename
// 						$fileUpload->rename_file =true;
// 						$fileUpload->upload();
// 						$imageFile = $fileUpload->file_copy;
// 					}


// 					$insertSQL = "INSERT INTO `{$galleryTable}` (`image`,`{$updateTable}_id`, `display_priority`) VALUES
// 					( :image, :parent_id, :display_priority)";
// 					$paramsGallery = array();
// 					$paramsGallery["image"] = $imageFile;
// 					$paramsGallery["parent_id"] = ADMIN_ACTION_ID1;
// 					$paramsGallery["display_priority"] = 0;

// 							$insertGalleryRecord = DB::getInstance()->query($insertSQL, $paramsGallery);
// 				}

			}

			//insert new data

// 			var_dump($_POST[$attrName."_item_id"]);
// 			var_dump($_POST[$galleryTable]);

			if(sizeof($_POST[$attrName."_item_id"]) >0){

				foreach($_POST[$attrName."_item_id"] as $idIndex => $idValue){

					if($idIndex == 0 ){
						continue;
					}

					$galleryFieldList = array();
					$galleryUpdateId = "";
					$galleryFieldParamValue = array();
					foreach($_POST[$galleryTable] as $galleryField =>$galleryFieldValues){

						if($galleryField == "id" || $galleryField=="image" || $galleryField =="file"){
							continue;
						}else{
							array_push($galleryFieldList, $galleryField);
							$galleryFieldParamValue[$galleryField] = $galleryFieldValues[$idIndex];
						}



					}

					if(sizeof($galleryFieldList) > 0){

						if(!empty($idValue)){
							$updateStringList  = array();
							foreach($galleryFieldList as $f){
								array_push($updateStringList, "`{$f}` = :{$f}");
							}

							$updateString = implode(",", $updateStringList);

							$insertSQL4 = "UPDATE `{$galleryTable}` set {$updateString} WHERE id = :id";
							$paramsGallery4 = array_merge(array(), $galleryFieldParamValue);
							$paramsGallery4["id"] = $idValue;
							$insertGalleryRecord4 = DB::getInstance()->query($insertSQL4, $paramsGallery4);


						}else{
							$insertAttr_1 = "`" . implode("`,`", $galleryFieldList)."`";
							$insertAttr_2 = ":" . implode(",:", $galleryFieldList);

							unset($galleryFieldParamValue["id"]);

							$insertSQL4 = "INSERT INTO `{$galleryTable}` ({$insertAttr_1},`{$updateTable}_id`, `display_priority`) VALUES ( {$insertAttr_2}, :parent_id, :display_priority)";
							$paramsGallery4 = array_merge(array(), $galleryFieldParamValue);
							$paramsGallery4["parent_id"] = ADMIN_ACTION_ID1;
							$paramsGallery4["display_priority"] = 0;
							$insertGalleryRecord4 = DB::getInstance()->query($insertSQL4, $paramsGallery4);
						}

					}








				}

			}

		}

	}else{

		$attrValue = $_POST[$attrName];

		if(is_null($attrValue) && $attrValue !="0"){

			continue;
		}


		array_push($dataList, "`{$attrName}` = :{$attrName}");
		if(is_array($attrValue)){
			$params[$attrName] = implode(";",$attrValue);
		}else{
			$params[$attrName] = $attrValue;
		}
	}


}
if(!empty($dataList) && sizeof($dataList)>0){
	$dataString = implode(", ", $dataList);

	$updateSQL = "update `{$updateTable}` set {$dataString} where 1 = 1 {$query}";
	//Execute Query
	$updateResultSet = DB::getInstance()->query($updateSQL, $params);

	// var_dump($updateResultSet);
}


?>
<div class="updateDone">
	Record has been added/updated.

	<div>
		<a href="<?=ADMIN_ROOT_PATH.ADMIN_MODULE."/".ADMIN_SECTION."/edit/".ADMIN_ACTION_ID1?>" type="button" class="btn btn-info">View record</a>
		&nbsp;&nbsp;&nbsp;

		<?php
			if(!empty($backToPage)){
		?>
			<a href="<?=ADMIN_ROOT_PATH.$backToPage?>" type="button" class="btn btn-default">
		<?php
			}else{
		?>
			<a href="<?=ADMIN_ROOT_PATH.ADMIN_MODULE."/"?>" type="button" class="btn btn-default">
		<?php
			}
		?>
				Back to list
			</a>
	</div>

</div>

<?php
	$recordFields = $content ["section"] [$sectionMain] ["list"];
	$recordTabs = $content ["section"] [$sectionMain] ["tabs"];
	$backToPage = $content["backTo"];


	//parse config
	$fieldsDisplayName = array();
	$fieldsName = array();
	$fieldsType = array();
	foreach($recordFields as $fieldInfo){
		if(!$fieldInfo["notInTable"]){
			array_push($fieldsName, $fieldInfo["fieldName"]);
		}
	}

	//prepare SQL
	$selectNameStr = implode("`,`",$fieldsName);
	$selectTable = $content ["section"] [$sectionMain] ["table"] ;
	$selectOrderBy = $content ["section"] [$sectionMain] ["orderby"];

	$recordKeyField = $content ["section"] [$sectionMain] ["keyField"];

	if(ADMIN_ACTION == "new"){

	}else if(ADMIN_ACTION=="edit"){

		$query = "";
		$params = array();
		foreach($recordKeyField as $keyField => $keyFieldValue){
			$query .= " AND {$keyField} = :{$keyField}";
			$params[$keyField] = constant($keyFieldValue);
		}
		$selectSQL = "select `{$selectNameStr}` from {$selectTable} where 1 = 1 {$query}";
		//Execute Query
		$selectResultSet = DB::getInstance()->query($selectSQL, $params)->singleResults();
	}

	// var_dump($selectResultSet);
	// var_dump($recordFields);

	// var_dump($recordTabs);
?>


<div id="formTabs">
  <ul>
  	<?php
  		foreach($recordTabs as $tabId =>$tabName){
			if(empty($tabId))	continue;
	?>
	    <li><a href="<?="#".$tabId?>"><?=$tabName?></a></li>
	<?php
  		}
  	?>
  </ul>

	<?php
	  if(ADMIN_ACTION == "new"){
	?>
		<form class="form-horizontal" action="<?=ADMIN_ROOT_PATH.ADMIN_MODULE."/".ADMIN_SECTION."/add/"?>" method="post" enctype="multipart/form-data">
	<?php
	  }else if(ADMIN_ACTION=="edit"){
	?>
		<form class="form-horizontal" action="<?=ADMIN_ROOT_PATH.ADMIN_MODULE."/".ADMIN_SECTION."/update/".ADMIN_ACTION_ID1?>" method="post" enctype="multipart/form-data">
	<?php
	  }
	?>


  	<?php
  		foreach($recordTabs as $tabId =>$tabName){
			if(empty($tabId))	continue;
	?>
		<div id="<?=$tabId?>" class="editFormTabContainer">
				<div class="form-group">



					<?php
						foreach($recordFields as $fieldInfo){
							if($fieldInfo["isShowInEdit"] === false){
								continue;
							}
							$fieldName = $fieldInfo["fieldName"];
							$fieldDisplayName = $fieldInfo["displayName"];
							// var_dump($fieldDisplayName);
							// var_dump($fieldInfo["type"]);
							// var_dump($fieldInfo["inTab"]);
							// var_dump($fieldInfo["tips"]);
							// var_dump($fieldInfo["hideLabel"]);

							if(is_array($fieldInfo["type"])){
								$fieldType = $fieldInfo["type"][0];
								$fieldTypeOption = $fieldInfo["type"][1];
							}else if(is_string($fieldInfo["type"])){
								$fieldType = $fieldInfo["type"];
							}


							$fieldInTab = $fieldInfo["inTab"];
							$fieldValue = $fieldInfo["inTab"];
							$fieldTips = $fieldInfo["tips"];
							$fieldData = $fieldInfo["data"];
							$isHideLabel = $fieldInfo["hideLabel"];

							if($fieldInTab != $tabId) continue;
							if(in_array($fieldType, array("HIDDEN"))) continue;
					?>



							<div class="rowContainer">
								<?php
										include "fields/edit/{$fieldType}.php";
								?>
							</div>
					<?php
						}
					?>



				</div>
		</div>
	<?php
  		}
  	?>

</div>
	<div>
		<div id="formBackButton">
			<?php
				if(!empty($backToPage)){
			?>
				<a href="<?=ADMIN_ROOT_PATH.$backToPage?>">
			<?php
				}else{
			?>
				<a href="<?=ADMIN_ROOT_PATH.ADMIN_MODULE."/".ADMIN_SECTION."/list/"?>">
			<?php
				}
			?>

				<button type="button" class="btn btn-default">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> &nbsp; Back 返回
				</button>
			</a>
		</div>

		<div id="formActionButton">
			<button type="reset" class="btn btn-default">
			<span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> &nbsp; Reset 重設</button>
			<button type="submit" class="btn btn-info">
			<span class="glyphicon glyphicon-ok white" aria-hidden="true"></span> &nbsp; Submit 提交</button>

		</div>

	</div>
</form>
	<div style="clear:both"></div>
<script>
	$(function() {
		$( "#formTabs" ).tabs();
	});


</script>

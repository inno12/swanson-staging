<?php

// var_dump($_FILES);

$updateTable = $content ["section"] ["manage_category"] ["table"] ;
$updateKeyField = $content ["section"] ["manage_category"] ["keyField"];

$nodeDataJson = $_POST["nodeData"];
$nodeData = json_decode($nodeDataJson);

$level1Order = 1;
$level2Order = 1;
$level3Order = 1;
$level4Order = 1;
$level5Order = 1;

foreach($nodeData as $level1Key => $level1Node){
	
	$level1NodeParentId = 0;
	$level1NodeId = $level1Node->id;
	$level1NodeChildren = $level1Node->children;
	updateNode($level1NodeParentId, 0, $level1Order++, $level1NodeId);
	
	foreach($level1NodeChildren as $level2Key => $level2Node){
		
		$level2NodeParentId = $level1NodeId;
		$level2NodeId = $level2Node->id;
		$level2NodeChildren = $level2Node->children;
		updateNode($level2NodeParentId, 1, $level2Order++, $level2NodeId);
		
		
		foreach($level2NodeChildren as $level3Key => $level3Node){
		
			$level3NodeParentId = $level2NodeId;
			$level3NodeId = $level3Node->id;
			$level3NodeChildren = $level3Node->children;
			updateNode($level3NodeParentId, 2, $level3Order++, $level3NodeId);
		
			
			foreach($level3NodeChildren as $level4Key => $level4Node){
			
				$level4NodeParentId = $level3NodeId;
				$level4NodeId = $level4Node->id;
				$level4NodeChildren = $level4Node->children;
				updateNode($level4NodeParentId, 3, $level4Order++, $level4NodeId);
			
				
				foreach($level4NodeChildren as $level5Key => $level5Node){
						
					$level5NodeParentId = $level4NodeId;
					$level5NodeId = $level5Node->id;
					$level5NodeChildren = $level5Node->children;
					updateNode($level5NodeParentId, 4, $level5Order++, $level5NodeId);
				}
			}
				
		}
		
	}
	
	
	
}



	function updateNode($pid, $nl, $dp, $nid){
		global $updateTable;
		$updateSQL = "update `{$updateTable}` set `parent_id`=:pid, `node_level`=:nl, `display_priority`= :dp where  id =:nid";
		$params = array(
			"nid" =>$nid,
			"pid" =>$pid,
			"dp"  =>$dp,
			"nl"  =>$nl,
		);
		
		$updateResultSet = DB::getInstance()->query($updateSQL, $params);
	}

?>
<div class="updateDone">
	Record has been added/updated.
	
	<div>
		<a href="<?=ADMIN_ROOT_PATH.ADMIN_MODULE."/manage_category/"?>" type="button" class="btn btn-info">View category</a>
	</div>
	
</div>
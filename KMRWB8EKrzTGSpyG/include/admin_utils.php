<?php 
	if (!function_exists("isAdminLogin")) {
		
		
		function isAdminLogin(){
			if($_SESSION){
				return ( array_key_exists(APP_ID."_LOGINID", $_SESSION) && array_key_exists(APP_ID."_NAME", $_SESSION));
			}else{
				return false;
			}
		}
	}

?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="<?=ADMIN_ROOT_PATH?>include/css/admin_global.css"/>
<link rel="stylesheet" type="text/css" href="<?=ADMIN_ROOT_PATH?>include/css/jquery.fancybox.css"/>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" href="<?=ADMIN_ROOT_PATH?>include/css/jquery.datetimepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?=ADMIN_ROOT_PATH?>include/css/jquery.nestable.css"/>
<link rel="stylesheet" type="text/css" href="<?=ADMIN_ROOT_PATH?>include/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="<?=ADMIN_ROOT_PATH?>include/css/jquery-ui.min.css"/>
<link rel="stylesheet" type="text/css" href="<?=ADMIN_ROOT_PATH?>include/css/leftmenu.css"/>
<link rel="stylesheet" type="text/css" href="<?=ADMIN_ROOT_PATH?>include/css/topmenu.css"/>
<link rel="stylesheet" type="text/css" href="<?=ADMIN_ROOT_PATH?>include/css/record.css"/>


  
<?php
$logoContent = array (
		"img" => "images/logo.png",
		"url" => "http://www.swansoncooking.com.hk/" 
);

/*not shown in menu*/
$generalContent = array(
		
		
		"changepw" => array (
				"displayName" => "Change Password",
				type => "SETTING",
				section => array (
						"record" => array (
								"displayName" => "Change Password",
								"orderby"=>"",
								"table" => "user",
								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
								"tabs" =>array("BASIC"=>"Basic Info",
								),
								"list"	=> array(
		
										array(	"displayName"		=>"ID",
												"fieldName"			=>"id",
												"type"				=>"HIDDEN",
												"w"					=>"20px",
												"inTab"				=>"BASIC"
										),
		
		
		
										array(	"displayName"		=>"Password",
												"fieldName"			=>"password",
												"type"				=>array("PASSWORD", array()),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
		
		
		
		
								),
		
		
						),
				)
		)
		
		
		
);

$homeMenu = array (
		"home" => array (
				displayName => "Home",
				type => "HOME",
				section => array (
						"home" => array (
								"displayName" => "Home",
								"table" => array (),
								"fields" => array ()
						),
				)
		)
	);



$pageContentMenu = array (
		
		

		
		
		


// 		"about_tecm" => array (
// 				"displayName" => "關於",
// 				type => "CONTENT",
// 				section => array (
// 						// 						"manage_category" => array (
// 						// 								"displayName" => "test2 Category",
// 						// 								"level" => 3,
// 						// 								"table" => "product_node",
// 						// 								"fields" => array ()
// 						// 						),
// 						"record" => array (
// 								"displayName" => "關於",
// 								"orderby"=>" order by display_priority desc",
// 								"table" => "about_tecm",
// 								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
// 								"tabs" =>array("BASIC"=>"Basic Info",
// 								),
// 								"list"	=> array(
		
// 										array(	"displayName"		=>"ID",
// 												"fieldName"			=>"id",
// 												"type"				=>"HIDDEN",
// 												"w"					=>"20px",
// 												"inTab"				=>"BASIC"
// 										),
		
// 										array(	"displayName"		=>"Ranking(Greater value shown first)",
// 												"fieldName"			=>"display_priority",
// 												"type"				=>array("VARCHAR", array("format"=>"INT")),
// 												"w"					=>"100px",
// 												"inTab"				=>"BASIC",
// 										),
										
		
// 										array(	"displayName"		=>"Title",
// 												"fieldName"			=>"title",
// 												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 										),
										
										
// 										array(	"displayName"		=>"Description",
// 												"fieldName"			=>"content",
// 												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"EN")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
		
// 								),
		
		
// 						),
// 				)
// 		),
		
		
// 		"se_story" => array (
// 				"displayName" => "社企故事",
// 				type => "CONTENT",
// 				section => array (
// 						// 						"manage_category" => array (
// 						// 								"displayName" => "test2 Category",
// 						// 								"level" => 3,
// 						// 								"table" => "product_node",
// 						// 								"fields" => array ()
// 						// 						),
// 						"record" => array (
// 								"displayName" => "社企故事",
// 								"orderby"=>" order by display_priority desc",
// 								"table" => "se_story",
// 								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
// 								"tabs" =>array("BASIC"=>"Basic Info",
// 								),
// 								"list"	=> array(
		
// 										array(	"displayName"		=>"ID",
// 												"fieldName"			=>"id",
// 												"type"				=>"HIDDEN",
// 												"w"					=>"20px",
// 												"inTab"				=>"BASIC"
// 										),
		
// 										array(	"displayName"		=>"Ranking(Greater value shown first)",
// 												"fieldName"			=>"display_priority",
// 												"type"				=>array("VARCHAR", array("format"=>"INT")),
// 												"w"					=>"100px",
// 												"inTab"				=>"BASIC",
// 										),
		
// 										array(	"displayName"		=>"Title",
// 												"fieldName"			=>"title",
// 												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 										),
		
		
// 										array(	"displayName"		=>"Description",
// 												"fieldName"			=>"content",
// 												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"EN")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
										
// 										array(	"displayName"		=>"Image",
// 												"fieldName"			=>"image",
// 												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"100px")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 										),
										
// 										array(	"displayName"		=>"Gallery",
// 												"fieldName"			=>"gallery",
// 												"type"				=>array("GALLERY", array("table"=>"se_story_image")),
// 												"w"					=>"100px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
										
		
// 								),
		
		
// 						),
// 				)
// 		),
		
		


// 		"find_se" => array (
// 				"displayName" => "尋找社企",
// 				type => "CONTENT",
// 				section => array (
// 						// 						"manage_category" => array (
// 						// 								"displayName" => "test2 Category",
// 						// 								"level" => 3,
// 						// 								"table" => "product_node",
// 						// 								"fields" => array ()
// 						// 						),
// 						"record" => array (
// 								"displayName" => "尋找社企",
// 								"orderby"=>" order by display_priority desc",
// 								"table" => "find_se",
// 								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
// 								"tabs" =>array("BASIC"=>"Basic Info",
// 								),
// 								"list"	=> array(
		
// 										array(	"displayName"		=>"ID",
// 												"fieldName"			=>"id",
// 												"type"				=>"HIDDEN",
// 												"w"					=>"20px",
// 												"inTab"				=>"BASIC"
// 										),
		
// 										array(	"displayName"		=>"Ranking(Greater value shown first)",
// 												"fieldName"			=>"display_priority",
// 												"type"				=>array("VARCHAR", array("format"=>"INT")),
// 												"w"					=>"100px",
// 												"inTab"				=>"BASIC",
// 										),
		
// 										array(	"displayName"		=>"Title",
// 												"fieldName"			=>"title",
// 												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 										),
		

// 										array(	"displayName"		=>"主分類",
// 												"fieldName"			=>"category1",
// 												"type"				=>array("SELECT", array("src"=>"LIST", "data"=>array(
// 														"食"=>"食",
// 														"玩"=>"玩",
// 														"買"=>"買",
// 														"個人服務"=>"個人服務",
// 												))),
// 												"w"					=>"100px",
// 												"inTab"				=>"BASIC",
// 										),
										
// 										array(	"displayName"		=>"次分類",
// 												"fieldName"			=>"category2",
// 												"type"				=>array("SELECT", array("src"=>"LIST", "data"=>array(
// 														"二手及回收"							=>			"二手及回收",
// 														"二手店及回收"						=>			"二手店及回收",
// 														"二手服飾及回收"						=>			"二手服飾及回收",
// 														"小食"								=>			"小食",
// 														"小賣部"								=>			"小賣部",
// 														"手工藝及特色 產品"					=>			"手工藝及特色 產品",
// 														"手工藝及特色產品"					=>			"手工藝及特色產品",
// 														"日常及醫療用品"						=>			"日常及醫療用品",
// 														"生活百貨-旅 遊及旅運"				=>			"生活百貨-旅 遊及旅運",
// 														"汽車美容"							=>			"汽車美容",
// 														"汽車維修及美容"						=>			"汽車維修及美容",
// 														"到會"								=>			"到會",
// 														"服飾"								=>			"服飾",
// 														"服飾、健康有機食品"					=>			"服飾、健康有機食品",
// 														"花店"								=>			"花店",
// 														"長者運動課程"						=>			"長者運動課程",
// 														"長者課程/活動"						=>			"長者課程/活動",
// 														"按摩及美容"							=>			"按摩及美容",
// 														"按摩推拿"							=>			"按摩推拿",
// 														"活動"								=>			"活動",
// 														"活動製作"							=>			"活動製作",
// 														"美容及剪髮"							=>			"美容及剪髮",
// 														"食品"								=>			"食品",
// 														"家居清潔 / 汽車美容"				=>			"家居清潔 / 汽車美容",
// 														"旅遊及旅運"							=>			"旅遊及旅運",
// 														"氣球製作"							=>			"氣球製作",
// 														"健康及有機食品"						=>			"健康及有機食品",
// 														"健康及有機食品、手工藝及特色產品"		=>			"健康及有機食品、手工藝及特色產品",
// 														"健康及有機食品、日用品"				=>			"健康及有機食品、日用品",
// 														"教育及旅遊"							=>			"教育及旅遊",
// 														"教育及培訓"							=>			"教育及培訓",
// 														"甜品店"								=>			"甜品店",
// 														"復康及長者用品"						=>			"復康及長者用品",
// 														"搬屋服務"							=>			"搬屋服務",
// 														"滅蟲及消毒"							=>			"滅蟲及消毒",
// 														"農莊及推廣有機耕種及環保"			=>			"農莊及推廣有機耕種及環保",
// 														"餐廳"								=>			"餐廳",
// 														"環保產品"							=>			"環保產品",
// 														"環保產品、花藝及種植"				=>			"環保產品、花藝及種植",
// 														"寵物美容"							=>			"寵物美容",
// 														"麵包西餅店"							=>			"麵包西餅店",
// 														"攝影"								=>			"攝影",
// 												))),
// 												"w"					=>"100px",
// 												"inTab"				=>"BASIC",
// 										),
										
										
// 										array(	"displayName"		=>"經營理念及服務方針",
// 												"fieldName"			=>"concept",
// 												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"CN")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
										
// 										array(	"displayName"		=>"社企故事",
// 												"fieldName"			=>"story",
// 												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"CN")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
										
										
// 										array(	"displayName"		=>"電話",
// 												"fieldName"			=>"contact",
// 												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 										),
										
// 										array(	"displayName"		=>"電郵",
// 												"fieldName"			=>"email",
// 												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
										
// 										array(	"displayName"		=>"Facebook",
// 												"fieldName"			=>"facebook",
// 												"type"				=>array("VARCHAR", array("format"=>"URL")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
		
// 										array(	"displayName"		=>"營業時間",
// 												"fieldName"			=>"officehour",
// 												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"CN")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
// // 										array(	"displayName"		=>"Description",
// // 												"fieldName"			=>"content",
// // 												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"EN")),
// // 												"w"					=>"200px",
// // 												"inTab"				=>"BASIC",
// // 												"isShowInList"		=>false,
// // 										),
		
// 										array(	"displayName"		=>"Image",
// 												"fieldName"			=>"image",
// 												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"100px")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 										),
										
// 										array(	"displayName"		=>"Gallery",
// 												"fieldName"			=>"gallery",
// 												"type"				=>array("GALLERY", array("table"=>"find_se_image")),
// 												"w"					=>"100px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
										
// 										array(	"displayName"		=>"網址",
// 												"fieldName"			=>"homepage",
// 												"type"				=>array("VARCHAR", array("format"=>"URL")),
// 												"w"					=>"100px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
										
										
// 										array(	"displayName"		=>"地址",
// 												"fieldName"			=>"address",
// 												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
// 												"w"					=>"100px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
										
										
// 										array(	"displayName"		=>"Google Map",
// 												"fieldName"			=>"location_map",
// 												"type"				=>array("MAP"),
// 												"w"					=>"100px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
										
										
		
// 								),
		
		
// 						),
// 				)
// 		),
		
		
		
		"theme_highlight" => array (
				"displayName" => "特色主題",
				type => "CONTENT",
				section => array (
						// 						"manage_category" => array (
						// 								"displayName" => "test2 Category",
						// 								"level" => 3,
						// 								"table" => "product_node",
						// 								"fields" => array ()
						// 						),
						"record" => array (
								"displayName" => "特色主題",
								"orderby"=>" order by display_priority desc",
								"table" => "theme_highlight",
								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
								"tabs" =>array("BASIC"=>"Basic Info",
								),
								"list"	=> array(
		
										array(	"displayName"		=>"ID",
												"fieldName"			=>"id",
												"type"				=>"HIDDEN",
												"w"					=>"20px",
												"inTab"				=>"BASIC"
										),
		
										array(	"displayName"		=>"Ranking(Greater value shown first)",
												"fieldName"			=>"display_priority",
												"type"				=>array("VARCHAR", array("format"=>"INT")),
												"w"					=>"100px",
												"inTab"				=>"BASIC",
										),
		
										
										array(	"displayName"		=>"Title",
												"fieldName"			=>"title",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
		
										array(	"displayName"		=>"Image",
												"fieldName"			=>"image",
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
		
		
								),
		
		
						),
				)
		),
		
		

		"ingredients" => array (
				"displayName" => "食譜材料",
				type => "CONTENT",
				section => array (
						// 						"manage_category" => array (
						// 								"displayName" => "test2 Category",
						// 								"level" => 3,
						// 								"table" => "product_node",
						// 								"fields" => array ()
						// 						),
						"record" => array (
								"displayName" => "食譜材料",
								"orderby"=>" order by display_priority desc",
								"table" => "ingredients",
								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
								"tabs" =>array("BASIC"=>"Basic Info",
								),
								"list"	=> array(
		
										array(	"displayName"		=>"ID",
												"fieldName"			=>"id",
												"type"				=>"HIDDEN",
												"w"					=>"20px",
												"inTab"				=>"BASIC"
										),
		
										array(	"displayName"		=>"Ranking(Greater value shown first)",
												"fieldName"			=>"display_priority",
												"type"				=>array("VARCHAR", array("format"=>"INT")),
												"w"					=>"100px",
												"inTab"				=>"BASIC",
										),
		
		
										array(	"displayName"		=>"Title",
												"fieldName"			=>"title",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
		
										array(	"displayName"		=>"Image",
												"fieldName"			=>"image",
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
		
		
								),
		
		
						),
				)
		),
		
		
		
		"recipe" => array (
				"displayName" => "食譜",
				type => "CONTENT",
				section => array (
						
						"record" => array (
								"displayName" => "食譜",
								"orderby"=>" order by display_priority desc",
								"table" => "recipe",
								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
								"tabs" =>array("BASIC"=>"Basic Info",
								),
								"list"	=> array(
		
										array(	"displayName"		=>"ID",
												"fieldName"			=>"id",
												"type"				=>array("VARCHAR", array("format"=>"INT")),
												"w"					=>"20px",
												"inTab"				=>"BASIC"
										),
										
										array(	"displayName"		=>"隱藏",
												"fieldName"			=>"isHide",
												"type"				=>array("RADIO", array("src"=>"LIST", "data"=>array("N"=>"否", "Y"=>"是", ))),
												"w"					=>"80px",
												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"View Count",
												"fieldName"			=>"viewCount",
												"type"				=>array("VARCHAR", array("format"=>"INT")),
												"w"					=>"100px",
												"inTab"				=>"BASIC"
										), 
		
										array(	"displayName"		=>"Ranking(Greater value shown first)",
												"fieldName"			=>"display_priority",
												"type"				=>array("VARCHAR", array("format"=>"INT")),
												"w"					=>"100px",
												"inTab"				=>"BASIC",
										),
		
		
										array(	"displayName"		=>"Title",
												"fieldName"			=>"title",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										array(	"displayName"		=>"材料",
												"fieldName"			=>"ingredients_desc",
												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"EN")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"醃料",
												"fieldName"			=>"sauce",
												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"EN")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"其他內容 1 - 標題",
												"fieldName"			=>"otherDesc_title",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										array(	"displayName"		=>"其他內容 1 - 內文",
												"fieldName"			=>"otherDesc_content",
												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"EN")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										
										array(	"displayName"		=>"其他內容 2- 標題",
												"fieldName"			=>"otherDesc_title2",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										array(	"displayName"		=>"其他內容 2- 內文",
												"fieldName"			=>"otherDesc_content2",
												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"EN")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										
										array(	"displayName"		=>"Steps",
												"fieldName"			=>"steps",
												"type"				=>array("STEPS", array()),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"產品",
												"fieldName"			=>"product_id",
												"type"				=>array("CHECKBOX", array("src"=>"TABLE", "data"=>array("table"=>"product", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc, id asc"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
		
										array(	"displayName"		=>"食物材料",
												"fieldName"			=>"ingredients",
												"type"				=>array("CHECKBOX", array("src"=>"TABLE", "data"=>array("table"=>"ingredients", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										array(	"displayName"		=>"主題",
												"fieldName"			=>"themes",
												"type"				=>array("CHECKBOX", array("src"=>"TABLE", "data"=>array("table"=>"theme_highlight", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										array(	"displayName"		=>"Image (比例: 600:420)",
												"fieldName"			=>"image",
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										array(	"displayName"		=>"小貼士",
												"fieldName"			=>"tips",
												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"EN")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
// 										array(	"displayName"		=>"相關食譜 1",
// 												"fieldName"			=>"related_recipe_1",
// 												"type"				=>array("SELECT", array("src"=>"TABLE", "data"=>array("table"=>"recipe", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
										
// 										array(	"displayName"		=>"相關食譜 2",
// 												"fieldName"			=>"related_recipe_2",
// 												"type"				=>array("SELECT", array("src"=>"TABLE", "data"=>array("table"=>"recipe", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
										
// 										array(	"displayName"		=>"相關食譜 3",
// 												"fieldName"			=>"related_recipe_3",
// 												"type"				=>array("SELECT", array("src"=>"TABLE", "data"=>array("table"=>"recipe", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
										
// 										array(	"displayName"		=>"相關食譜 4",
// 												"fieldName"			=>"related_recipe_4",
// 												"type"				=>array("SELECT", array("src"=>"TABLE", "data"=>array("table"=>"recipe", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
										
										
										array(	"displayName"		=>"人氣食譜",
												"fieldName"			=>"isPopular",
												"type"				=>array("RADIO", array("src"=>"LIST", "data"=>array("Y"=>"是", "N"=>"否"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										 
									
								),
		
		
						),
				)
		),



		"product_node" => array (
				"displayName" => "產品類別",
				type => "HIDDEN",
				"backTo"=>"product/manage_category",
				section => array (
						"record" => array (
								"displayName" => "產品類別",
								"orderby"=>" ",
								"table" => "product_node",
								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
								"tabs" =>array("BASIC"=>"Basic Info",
								),
								"list"	=> array(
		
										array(	"displayName"		=>"ID",
												"fieldName"			=>"id",
												"type"				=>"HIDDEN",
												"w"					=>"20px",
												"inTab"				=>"BASIC"
										),
		
										array(	"displayName"		=>"Name",
												"fieldName"			=>"name",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
		
										array(	"displayName"		=>"Description",
												"fieldName"			=>"description",
												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"EN")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
		
		
		
								),
		
		
						),
				)
		),
		


		"product" => array (
				"displayName" => "產品",
				type => "CONTENT",
				section => array (
						"manage_category" => array (
								"displayName" => "產品類別",
								"level" => 5,
								"table" => "product_node", //need match section
						),
						"record" => array (
								"displayName" => "產品",
								"orderby"=>" order by display_priority desc",
								"table" => "product",
								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
								"tabs" =>array("BASIC"=>"Basic Info",
								),
								"list"	=> array(
		
										array(	"displayName"		=>"ID",
												"fieldName"			=>"id",
												"type"				=>"HIDDEN",
												"w"					=>"20px",
												"inTab"				=>"BASIC"
										),
		
										
										array(	"displayName"		=>"Ranking(Greater value shown first)",
												"fieldName"			=>"display_priority",
												"type"				=>array("VARCHAR", array("format"=>"INT")),
												"w"					=>"100px",
												"inTab"				=>"BASIC",
										),
										
										array(	"displayName"		=>"分類",
												"fieldName"			=>"product_node_id",
												"type"				=>array("SELECT_CATEGORY", array("src"=>"TABLE", "data"=>array("table"=>"product_node", "name"=>"name", "id"=>"id" ))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
										),
		
		
										array(	"displayName"		=>"Title",
												"fieldName"			=>"title",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
		
										array(	"displayName"		=>"Description",
												"fieldName"			=>"description",
												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"EN")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
		
		
										array(	"displayName"		=>"Image",
												"fieldName"			=>"image",
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"Image - shown in recipe(Thumb, 200*200)",
												"fieldName"			=>"image_thumb",
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
		

										array(	"displayName"		=>"Image - shown in mobile slider(Thumb, 200*200)",
												"fieldName"			=>"image_thumb3",
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										

										array(	"displayName"		=>"Share Image",
												"fieldName"			=>"share_image", 
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										

										array(	"displayName"		=>"方便裝",
												"fieldName"			=>"isSmallPackage",
												"type"				=>array("RADIO", array("src"=>"LIST", "data"=>array("N"=>"否","Y"=>"是", ))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
// 										array(	"displayName"		=>"Youtube ID List",
// 												"fieldName"			=>"youtubeIdList",
// 												"type"				=>array("TEXT", array("format"=>"TEXT", "LANG"=>"EN")),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 												"tips"				=>"YOUTUBE_ID1;\nYOUTUBE_ID2;\nYOUTUBE_ID3;",
												
// 										),
		
// 										array(	"displayName"		=>"人氣食譜",
// 												"fieldName"			=>"isPopular",
// 												"type"				=>array("RADIO", array("src"=>"LIST", "data"=>array("Y"=>"是", "N"=>"否"))),
// 												"w"					=>"200px",
// 												"inTab"				=>"BASIC",
// 												"isShowInList"		=>false,
// 										),
								),
		
		
						),
				)
		),
		
		
		

		
		


		"recipe_video" => array (
				"displayName" => "入廚教學短片",
				type => "CONTENT",
				section => array (
						"record" => array (
								"displayName" => "入廚教學短片",
								"orderby"=>" order by display_priority desc",
								"table" => "recipe_video",
								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
								"tabs" =>array("BASIC"=>"Basic Info",
								),
								"list"	=> array(
		
										array(	"displayName"		=>"ID",
												"fieldName"			=>"id",
												"type"				=>"HIDDEN",
												"w"					=>"20px",
												"inTab"				=>"BASIC"
										),
		
		
										array(	"displayName"		=>"Ranking(Greater value shown first)",
												"fieldName"			=>"display_priority",
												"type"				=>array("VARCHAR", array("format"=>"INT")),
												"w"					=>"100px",
												"inTab"				=>"BASIC",
										),
		
		
										array(	"displayName"		=>"Title",
												"fieldName"			=>"title",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
		
										array(	"displayName"		=>"Description",
												"fieldName"			=>"description",
												"type"				=>array("TEXT", array("format"=>"EDITOR", "LANG"=>"EN")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
		
		
										array(	"displayName"		=>"Image",
												"fieldName"			=>"image",
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										array(	"displayName"		=>"食譜連結",
												"fieldName"			=>"recipe_link",
												"type"				=>array("SELECT", array("src"=>"TABLE", "data"=>array("table"=>"recipe", "name"=>"title", "id"=>"id", "order"=>" order by id desc"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										array(	"displayName"		=>"Youtube ID",
												"fieldName"			=>"video_link",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
		
								),
		
		
						),
				)
		),
		
		
		
		

		
		


		
		

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
);

$siteConfigMenu = array (
		


		"mp_setting" => array (
				"displayName" => "首頁設定",
				type => "SETTING",
				section => array (
						"record" => array (
								"displayName" => "首頁設定",
								"orderby"=>"",
								"table" => "pagebanner_setting",
								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
								"tabs" =>array(
// 										"BASIC"=>"Basic",
										"DESKTOP"=>"Desktop",
										"MOBILE"=>"Mobile",
								),
								"list"	=> array(
		
										array(	"displayName"		=>"ID",
												"fieldName"			=>"id",
												"type"				=>"HIDDEN",
												"w"					=>"20px",
												"inTab"				=>"BANNER"
										),
										
										
										array(	"displayName"		=>"Homepage - Banner(1920x600)",
												"fieldName"			=>"homepage",
												"type"				=>array("GALLERY_LIST", array("table"=>"pagesetting_coverimage")),
												"w"					=>"250px",
												"isShowInList"		=>false,
												"inTab"				=>"DESKTOP",
												"child"				=>array(
		
														array(	"displayName"		=>"ID",
																"fieldName"			=>"id",
																"type"				=>"HIDDEN",
														),
		
														array(	"displayName"		=>"Hyperlink",
																"fieldName"			=>"hyperlink",
																"type"				=>array("VARCHAR", array("format"=>"URL_MYSITE")),
														),
		
														array(	"displayName"		=>"Image",
																"fieldName"			=>"image",
																"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"100px")),
														),
		
		
		
												)
										),
		
		
		
										array(	"displayName"		=>"Homepage - Mobile Banner(600x400)",
												"fieldName"			=>"homepage_m",
												"type"				=>array("GALLERY_LIST", array("table"=>"pagesetting_coverimage2")),
												"w"					=>"250px",
												"isShowInList"		=>false,
												"inTab"				=>"MOBILE",
												"isShowInList"		=>false,
												"child"				=>array(
												
														array(	"displayName"		=>"ID",
																"fieldName"			=>"id",
																"type"				=>"HIDDEN",
														),
												
														array(	"displayName"		=>"Hyperlink",
																"fieldName"			=>"hyperlink",
																"type"				=>array("VARCHAR", array("format"=>"URL_MYSITE")),
														),
												
														array(	"displayName"		=>"Image",
																"fieldName"			=>"image",
																"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"100px")),
														),
												
												
												
												)
										),
		
								),
		
		
						),
				)
		),
		
		
		
		
		
		"weekly_highlight" => array (
				"displayName" => "每週推介",
				type => "SETTING",
				section => array (
						// 						"manage_category" => array (
						// 								"displayName" => "test2 Category",
						// 								"level" => 3,
						// 								"table" => "product_node",
						// 								"fields" => array ()
						// 						),
						"record" => array (
								"displayName" => "每週推介",
								"orderby"=>"",
								"table" => "weekly_highlight",
								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
								"tabs" =>array("BASIC"=>"Basic Info",
								),
								"list"	=> array(
		
										array(	"displayName"		=>"ID",
												"fieldName"			=>"id",
												"type"				=>"HIDDEN",
												"w"					=>"20px",
												"inTab"				=>"BASIC"
										),
		
										array(	"displayName"		=>"星期一食譜",
												"fieldName"			=>"r1",
												"type"				=>array("SELECT", array("src"=>"TABLE", "data"=>array("table"=>"recipe", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),		
										array(	"displayName"		=>"星期二食譜",
												"fieldName"			=>"r2",
												"type"				=>array("SELECT", array("src"=>"TABLE", "data"=>array("table"=>"recipe", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										array(	"displayName"		=>"星期三食譜",
												"fieldName"			=>"r3",
												"type"				=>array("SELECT", array("src"=>"TABLE", "data"=>array("table"=>"recipe", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										array(	"displayName"		=>"星期四食譜",
												"fieldName"			=>"r4",
												"type"				=>array("SELECT", array("src"=>"TABLE", "data"=>array("table"=>"recipe", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"星期五食譜",
												"fieldName"			=>"r5",
												"type"				=>array("SELECT", array("src"=>"TABLE", "data"=>array("table"=>"recipe", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										array(	"displayName"		=>"星期六食譜",
												"fieldName"			=>"r6",
												"type"				=>array("SELECT", array("src"=>"TABLE", "data"=>array("table"=>"recipe", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										array(	"displayName"		=>"星期日食譜",
												"fieldName"			=>"r7",
												"type"				=>array("SELECT", array("src"=>"TABLE", "data"=>array("table"=>"recipe", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"最後更新",
												"fieldName"			=>"lastUpdate",
												"type"				=>array("VARCHAR", array("format"=>"DATETIME")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"手動更新",
												"notInTable"		=>true,
												"fieldName"			=>"hyperlink1",
												"type"				=>array("HYPERLINK", array("text"=>"手動更新", "link"=>"http://".$_SERVER["HTTP_HOST"].ROOT_PATH."recordUtils.php?generateWeeklyRecipe", 
														
														"result" =>array("1" => "已成功更新", 
																		 "0" => "更新失敗，請稍後再試"
																	)
														
														
												)),
												
												
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
												"isHideLabel"		=>true,
										),
		
								),
		
		
						),
				)
		),
		

		"latest_promo" => array (
				"displayName" => "最新推介",
				type => "SETTING",
				section => array (
						// 						"manage_category" => array (
						// 								"displayName" => "test2 Category",
						// 								"level" => 3,
						// 								"table" => "product_node",
						// 								"fields" => array ()
						// 						),
						"record" => array (
								"displayName" => "最新推介",
								"orderby"=>"",
								"table" => "latest_promo",
								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
								"tabs" =>array("BASIC"=>"Basic Info",
								),
								"list"	=> array(
		
										array(	"displayName"		=>"ID",
												"fieldName"			=>"id",
												"type"				=>"HIDDEN",
												"w"					=>"20px",
												"inTab"				=>"BASIC"
										),
		
										array(	"displayName"		=>"啟用並顯示",
												"fieldName"			=>"isShow",
												"type"				=>array("RADIO", array("src"=>"LIST", "data"=>array("Y"=>"是", "N"=>"否"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										array(	"displayName"		=>"開始顯示時間",
												"fieldName"			=>"startTime",
												"type"				=>array("VARCHAR", array("format"=>"DATETIME")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										array(	"displayName"		=>"終止顯示時間",
												"fieldName"			=>"endTime",
												"type"				=>array("VARCHAR", array("format"=>"DATETIME")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										
										array(	"displayName"		=>"Title",
												"fieldName"			=>"title",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										array(	"displayName"		=>"食譜",
												"fieldName"			=>"r_id",
												"type"				=>array("SELECT", array("src"=>"TABLE", "data"=>array("table"=>"recipe", "name"=>"title", "id"=>"id", "order"=>"order by display_priority desc"))),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"Youtube ID",
												"fieldName"			=>"youtubeId",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"Image",
												"fieldName"			=>"image",
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										
										
										
										
		
										
								),
		
		
						),
				)
		),
		
		
		

		"product_news" => array (
				"displayName" => "產品推廣",
				type => "SETTING",
				section => array (
						// 						"manage_category" => array (
						// 								"displayName" => "test2 Category",
						// 								"level" => 3,
						// 								"table" => "product_node",
						// 								"fields" => array ()
						// 						),
						"record" => array (
								"displayName" => "產品推廣",
								"orderby"=>"",
								"table" => "product_news",
								"keyField"=> array("id"=>"ADMIN_ACTION_ID1"),
		
								"tabs" =>array(
										"BASIC"=>"Basic Info",
										"TEST"=>"Testing",
								),
								"list"	=> array(
		
										array(	"displayName"		=>"ID",
												"fieldName"			=>"id",
												"type"				=>"HIDDEN",
												"w"					=>"20px",
												"inTab"				=>"BASIC"
										),
		
		
										array(	"displayName"		=>"Youtube ID 1",
												"fieldName"			=>"youtubeId1",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"Youtube 標題 1",
												"fieldName"			=>"title1",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"Youtube 簡介 1",
												"fieldName"			=>"caption1",
												"type"				=>array("TEXT", array("format"=>"TEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"Image 1",
												"fieldName"			=>"image1",
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
		
										array(	"displayName"		=>"Youtube ID 2",
												"fieldName"			=>"youtubeId2",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"Youtube 標題 2",
												"fieldName"			=>"title2",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"Youtube 簡介 2",
												"fieldName"			=>"caption2",
												"type"				=>array("TEXT", array("format"=>"TEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"Image 2",
												"fieldName"			=>"image2",
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										array(	"displayName"		=>"Youtube ID 3",
												"fieldName"			=>"youtubeId3",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
 
										array(	"displayName"		=>"Youtube 標題 3",
												"fieldName"			=>"title3",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"Youtube 簡介 3",
												"fieldName"			=>"caption3",
												"type"				=>array("TEXT", array("format"=>"TEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"Image 3",
												"fieldName"			=>"image3",
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
										array(	"displayName"		=>"Youtube ID 4",
												"fieldName"			=>"youtubeId4",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),

										array(	"displayName"		=>"Youtube 標題 4",
												"fieldName"			=>"title4",
												"type"				=>array("VARCHAR", array("format"=>"PLAINTEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"Youtube 簡介 4",
												"fieldName"			=>"caption4",
												"type"				=>array("TEXT", array("format"=>"TEXT")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
												"isShowInList"		=>false,
										),
										
										array(	"displayName"		=>"Image 4",
												"fieldName"			=>"image4",
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"BASIC",
										),
										
		
		
		
		
										array(	"displayName"		=>"Image test",
												"fieldName"			=>"imageTest",
												"type"				=>array("IMAGE", array("style"=>"FIT_H", "h"=>"80px")),
												"w"					=>"200px",
												"inTab"				=>"TEST",
										),
										
		
								),
		
		
						),
				)
		),
		
		
		
		
		
);

?>




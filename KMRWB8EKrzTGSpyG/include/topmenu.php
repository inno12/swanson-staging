<div style="width: 100%; height: 100%">
	<div id="top_left">
		<?=APP_TITLE?> Admin Panel
	</div><!-- 
	 
 --><div id="top_right">
 		<div style="height:100%;">
 		
 		
			<a href="<?=ROOT_PATH?>" target="_blank">
				<div class="topMenuItem">
							View My Site
				</div>
			</a>
			
			<div id="topmenu_userBtn" class="topMenuItem">
				<span class="glyphicon glyphicon-user white" style=""></span>
				<?=$_SESSION[APP_ID."_NAME"]?>
				<span class="glyphicon glyphicon-chevron-down white" style="vertical-align: middle;"></span>
				
				<div id="topmenu_usermenu"						>
					<a href="<?=ADMIN_ROOT_PATH?>changepw">
						<div class="topMenuSubItem">
							Change Password
						</div>
					</a>
					<a onclick="confirmLogout()">
						<div class="topMenuSubItem">
							Logout
						</div>
					</a>
				</div>
			</div>
 		</div>
	</div>
</div>



<script>


	$(function(){

		$("#topmenu_userBtn").hover(
				function(){
					$("#topmenu_usermenu").fadeIn(50);
				},
				function(){
					$("#topmenu_usermenu").fadeOut(50);
				}
			 );
			
	});

	function confirmLogout(){
		var title = "Logout";
		var message = "Are you going to logout?";
		
		createDialogConfirmCancel(	title,
									message,
									function(){
										logout();	
									},
									null);
	}

	function logout(){

		$.post(	"<?=ADMIN_ROOT_INC_PATH."_api/user/user_api.php"?>?logout",
				{},
				function(data){
					if(data == "SUCCESS"){

						$("html").fadeOut(200, function(){
							window.location="<?=ADMIN_ROOT_PATH?>";
						});
						
					}
				}
		);
		
		
	}

</script>
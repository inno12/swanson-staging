<?php
	include_once 'inc_top_script.php';

	if($_REQUEST["p"] != strip_tags($_REQUEST["p"])){
		//$_REQUEST["p"] = strip_tags($_REQUEST["p"]);
		unset($_REQUEST["p"]);
	}
?>
<!DOCTYPE html>
<html>
	<head>

		<?php
			include_once 'include_script.php';
		?>

		<style>
			.productInfo{
				display:none;
			}


			.title_txt{
				display:inline;
			}
			.productInfo .title{
					position: relative;
				}
				img.prod_title_small_tag{
					position: absolute;
					bottom:20px;
				}
				#ham_soup{
					width: 235px;
					/* background-position-x: 20px; */
					background-image: url(../images/box.png);
				}
			@media(min-width:768px){
				.title_txt{
					display:block;
				}
			}
		@media only screen and (min-width : 768px) and (max-width : 1024px)  {
		 .L-size{
		 	width: 104%;
		 }
		 .M-size img{
		 	width: 94%;
			/* padding-left: 10px; */
		 }
		 #L-chicken{
		 	margin-left: -20px;
		 }
		 #L-pig{
		 	margin-left: -14px;

		 }
		 #M-chicken{
		 	padding-left: 5px !important;
		 }
		 #M-fish{
		 	margin-left: -35px !important;
		 }
		 .productList .container>div{
		 	padding-left: 35px;
		 }


		}
		@media only screen and (min-width : 768px) and (max-width : 991px)  {
			.productList .container>div{
 		 	padding-left: 55px;
 		 }
		 .productSetContainer img{
		 	width: 80%;
		 }
		 #L-chicken{
		 	margin-left: -32px;
		 }
		 #L-pig{
		 	margin-left: -37px;

		 }
		 #M-fish{
		 	margin-left: -50px !important;
			margin-right: -25px;
		 }
		 .M-size img{
		 	width: 75%;

		 }



		}
		</style>



		<?php
			if(!empty($_REQUEST["p"])){

				$rRs = $pdo->prepare("select * from product where product_key = :p");
				$rRs->bindValue("p", $_REQUEST["p"], PDO::PARAM_STR);
				$rRs->execute();
			}
			if(empty($_REQUEST["p"]) || $rRs->rowCount()==0){

				unset($_REQUEST["p"]);
				$rRs = $pdo->prepare("select * from product order by display_priority desc limit 1" );
				$rRs->execute();
			}

			$rRs_row = $rRs->fetch();
			$key = $rRs_row["product_key"];
			$title = $rRs_row["title"];
			$image = $rRs_row["share_image"];
			$shareDesc = $rRs_row["share_desc"];

			$seoTitle = $rRs_row["seo_title"];
			$seoDesc = $rRs_row["seo_desc"];
			$seoKeywords = $rRs_row["seo_keywords"];

			$isSmallPackage = $rRs_row["isSmallPackage"] == "Y";
		?>
		<title><?=$seoTitle?></title>
		<meta name="keywords" content="<?=$seoKeywords?>">
		<meta name="description" content="<?=$seoDesc?>">


		<meta property="og:title" content="<?=APP_TITLE?> - <?=$title?><?=$isSmallPackage ? "(方便裝)":""?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH?>product-list.php?p=<?=$key?>"/>
		<meta property="og:image" content="http://<?=$_SERVER["HTTP_HOST"].PATH_TO_ULFILE.$image?>"/>
		<meta property="og:site_name" content="<?=APP_TITLE?>"/>
		<meta property="og:description" content="<?=$shareDesc?>"/>

		<script>

			<?php
				if(!empty($_REQUEST["p"])){
			?>
				var p = "<?=$_REQUEST["p"]?>";
			<?php
				}else{
			?>
				var p = "CCB_250";
			<?php
				}
			?>


			function sharePage(){
				var u ="http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH?>product-list.php?p=";
				window.open("https://www.facebook.com/sharer/sharer.php?u="+u + p , "pop", "width=600, height=400, scrollbars=no");
			}
		</script>
	</head>

	<body>
		<?php
			include_once 'inc_beginbody_script.php';
		?>
		 <div id="wrapper" class="">
	        <?php
	        	include_once 'inc_sidebar.php';
	        ?>
			<div class="mainContainer "  id="page-content-wrapper" >
				<div style="position:relative;">
				<?php
					include_once 'inc_header.php';
				?>

				<div class="breadcrumb" style="background-color:#f1dc00;margin-bottom:0px;">
					<div class="container swansonBreadcrumb" >
						<img src="images/breadcrumb_home_icon.png"/>&nbsp;<a href="index.php">主頁</a>

						<span class="glyphicon glyphicon-menu-right " aria-hidden="true" ></span>
						 史雲生系列
					</div>

				</div>

				<div class="row productIntro" >
					<div class="container" style="padding:0px;" >
						<div class="title">
							史雲生，在香港擁有超過30年歷史，
							<span class="title_txt">
								佔香港包裝湯超過7成佔有率，深受香港人歡迎。
							</span>
						</div>
						<div class="caption">
							史雲生清雞湯更一直深入民心，是大家的入廚好拍檔。清雞湯以優質上雞熬製，滴滴都是真正鮮雞精華，味道鮮甜可口，適合烹調各種菜式或做湯底。除清雞湯外，史雲生更備有豬骨、鮮魚、瑤柱、金華火腿4款上湯口味，而各款口味均選用優質材料熬製而成，不添加任何防腐劑*，並經低脂處理，不含脂肪。無論做火鍋湯底、湯麵、各款家常小菜以至鮑參翅肚，加入史雲生上湯，頓變成滋味無窮的佳餚。
							<br/><br/>
							近年備受注目的調味雞汁，同樣採用優質靚雞熬製而成，其汁狀質地，用於醃肉，能快速均勻地滲入肉內並鎖住肉汁，醃肉夠入味之餘亦能辟去冰鮮肉的雪藏味，助你輕易煮出滋味靚餸；用來炒餸，則能帶出菜餚鮮味及去除蔬菜的草青味，成為一支全能的入廚好幫手！
							<br/><br/>
							* 於生產過程中不添加防腐劑<br/>
						</div>
					</div>
				</div>

				<div class="row recipeDetail" style="display:none">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row lineContainer">
						<div class="line">
							<div><div class="border"></div></div>
						</div>
						<div class="titleImg">
							<img src="images/product_intro.png" alt=""/>
						</div>
						<div class="line">
							<div><div class="border"></div></div>
						</div>
					</div>
				</div>






				<!--<div class="row productList hidden-xs" >
					<div class="container" style=";height:100%;">
						<div>
							<div class="productSetContainer <?=$_REQUEST["p"]=="CCB_250"? "active":""?>" data-key = "CCB_250">
								<img src="images/product_img/1.png"/>
								<div class="productName" >
										清雞湯250毫升方便裝
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="CCB4_250"? "active":""?>" data-key = "CCB5_250">
								<img src="images/product_img/2.png"/>
								<div class="productName" >
										瑤柱上湯250毫升方便裝
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="CCB5_250"? "active":""?>" data-key = "CCB4_250">
								<img src="images/product_img/3.png"/>
								<div class="productName" >
									金華火腿250毫升方便裝
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="CCB_500"? "active":""?>" data-key = "CCB_500">
								<img src="images/product_img/4.png"/>
								<div class="productName" >
									清雞湯500毫升
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="PBB_500"? "active":""?>" data-key = "PBB_500">
								<img src="images/product_img/5.png"/>
								<div class="productName" >
									豬骨湯500毫升
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="FB_500"? "active":""?>" data-key = "FB_500">
								<img src="images/product_img/6.png"/>
								<div class="productName" >
									鮮魚湯500毫升
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="CCB_1000"? "active":""?>" data-key = "CCB_1000">
								<img src="images/product_img/7.png"/>
								<div class="productName" >
									清雞湯1公升
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="PBB_1000"? "active":""?>" data-key = "PBB_1000">
								<img src="images/product_img/8.png"/>
								<div class="productName" >
									豬骨湯1公升
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="SCS_PACKAGE"? "active":""?>" data-key = "SCS_PACKAGE" style="z-index:30;">
								<img src="images/product_img/9.png"/>
								<div class="productName" >
									調味雞汁8包方便裝
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="SCS_270"? "active":""?>" data-key = "SCS_270" style="z-index:20;">
								<img src="images/product_img/10.png" />
								<div class="productName" >
									調味雞汁270克
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="SCS_500"? "active":""?>" data-key = "SCS_500" style="z-index:10;">
								<img src="images/product_img/11.png"/>
								<div class="productName" >
									調味雞汁504克
								</div>
							</div>


						</div>
					</div>
				</div>-->

				<div class="row productList hidden-xs" >
					<div class="container" style=";height:100%;">
						<div>
							<div class="productSetContainer <?=$_REQUEST["p"]=="CCB_250"? "active":""?>" data-key = "CCB_250">
								<img src="images/product_img/new-pack-1.png"/>
								<div class="productName" >
										清雞湯250毫升方便裝
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="CCB4_250"? "active":""?>" data-key = "CCB5_250">
								<img src="images/product_img/new-pack-2.png"/>
								<div class="productName" >
										瑤柱上湯250毫升方便裝
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="CCB5_250"? "active":""?>" data-key = "CCB4_250">
								<img src="images/product_img/new-pack-3.png"/>
								<div class="productName" id="ham_soup" >
									金華火腿上湯250毫升方便裝
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="SCS_PACKAGE"? "active":""?>" data-key = "SCS_PACKAGE" style="z-index:30;">
								<img src="images/product_img/new-pack-4.png"/>
								<div class="productName" >
									調味雞汁8包方便裝
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="SCS_270"? "active":""?>" data-key = "SCS_270" style="z-index:20;">
								<img src="images/product_img/new-pack-5.png" />
								<div class="productName" >
									調味雞汁270克
								</div>
							</div>

							<div class="productSetContainer <?=$_REQUEST["p"]=="SCS_500"? "active":""?>" data-key = "SCS_500" style="z-index:10;">
								<img src="images/product_img/new-pack-6.png"/>
								<div class="productName" >
									調味雞汁504克
								</div>
							</div>

							<div class="productSetContainer big-productSet-right L-size <?=$_REQUEST["p"]=="CCB_1000"? "active":""?>" data-key = "CCB_1000" id="L-chicken">
								<img src="images/product_img/new-pack-7.png"/>
								<div class="productName" >
									清雞湯1公升
								</div>
							</div>

							<div class="productSetContainer big-productSet-right L-size <?=$_REQUEST["p"]=="PBB_1000"? "active":""?>" data-key = "PBB_1000" id="L-pig">
								<img src="images/product_img/new-pack-8.png"/>
								<div class="productName" >
									豬骨湯1公升
								</div>
							</div>

							<div class="productSetContainer medium-productSet M-size <?=$_REQUEST["p"]=="CCB_500"? "active":""?>" data-key = "CCB_500" style="padding-left:4px;" id="M-chicken">
								<img src="images/product_img/new-pack-9.png"/>
								<div class="productName" >
									清雞湯500毫升
								</div>
							</div>

							<div class="productSetContainer medium-productSet M-size <?=$_REQUEST["p"]=="PBB_500"? "active":""?>" data-key = "PBB_500" id="M-pork">
								<img src="images/product_img/new-pack-11.png"/>
								<div class="productName" >
									豬骨湯500毫升
								</div>
							</div>

							<div style="margin-left:-19px;" class="productSetContainer medium-productSet M-size <?=$_REQUEST["p"]=="FB_500"? "active":""?>" data-key = "FB_500" id="M-fish">
								<img src="images/product_img/new-pack-10.png"/>
								<div class="productName" >
									鮮魚湯500毫升
								</div>
							</div>

						</div>
					</div>
				</div>



				<div class="container recipeDetail " style="position:relative">

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row" style="background-color:#FFF;border-bottom:15px solid #0e847a;margin-bottom:15px;">

						<div class="hidden-lg hidden-md hidden-sm productSliderContainer col-xs-12">
							<div class="productSlider  col-xs-12">
								<?php
									if(array_key_exists("p", $_GET)){
										// $rRs = $pdo->prepare("select * from product order by (product_key = :p) desc");
										$rRs = $pdo->prepare("select * from product order by display_priority desc");

										$rRs->bindValue("p", $_GET["p"], PDO::PARAM_STR);
									}else{
										// $rRs = $pdo->prepare("select * from product");
										$rRs = $pdo->prepare("select * from product order by display_priority desc");
									}

									$rRs->execute();

									//var_dump($rRs_row);


									$cnt = 0;
									while($rRs_row = $rRs->fetch()){
										$image = $rRs_row["image_thumb3"];

										if(empty($image)){
											$image = $rRs_row["image_thumb"];
										}
										$prodId =  $rRs_row["id"];
										$prodKey = $rRs_row["product_key"];
								?>
									<div class="item "  data-id="<?=$prodId?>" data-key = "<?=$prodKey?>" data-index="<?=$cnt?>">
										<img src="<?=PATH_TO_ULFILE.$image?>" style="width:100%;"/>
									</div>
								<?php
										$cnt++;
									}
								?>
							</div>
						</div>

						<div class="row">
							<?php
								if(array_key_exists("p", $_GET)){
									$rRs = $pdo->prepare("select * from product order by (product_key = :p) desc");
									$rRs->bindValue("p", $_GET["p"], PDO::PARAM_STR);
								}else{
									$rRs = $pdo->prepare("select * from product");
								}

								$rRs->execute();

								$cnt = 0;
								while($rRs_row = $rRs->fetch()){
									$pid = $rRs_row["id"];
									$key = $rRs_row["product_key"];
									$title = $rRs_row["title"];
									$description = $rRs_row["description"];
									$image = $rRs_row["image"];
									$isSmallPackage = $rRs_row["isSmallPackage"] == "Y";
							?>
								<div class="productInfo <?=$cnt==1 ? "showing" : ""?>" id="<?=$key?>" <?=$cnt==0 ? "style='display:block'" : ""?> data-id="<?=$pid?>">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 image" >
										<img src="<?=PATH_TO_ULFILE.$image?>" style="width:100%;"/>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 productDetail">
										<div style="display:table-cell;vertical-align: middle;">
											<div class="title">
												<?=$title?>
												<?php
													if($isSmallPackage){
												?>
													<img class="prod_title_small_tag" src='images/tag_smallPackage.jpg'/>
												<?php
													}
												?>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 description" >
												<?=html_entity_decode($description)?>
											</div>
										</div>
									</div>
								</div>
						<?php
								$cnt++;
							}
						?>
						</div>



						<div class="productSliderPadding">
						</div>
						<div class="row detailEnd">
							<img src="images/detail_bottom_image.gif"/>
						</div>
					</div>

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row lineContainer">
						<div class="line">
							<div><div class="border"></div></div>
						</div>
						<div class="titleImg">
							<img src="images/latest_news.png" alt="推廣情報"/>
						</div>
						<div class="line">
							<div><div class="border"></div></div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">

						<?php
							$rRs2 = $pdo->prepare("select * from product_news limit 1");
							$rRs2->execute();

							$rRs2_row = $rRs2->fetch();
							$youtubeList = array();

							if( !empty($rRs2_row["youtubeId1"])){array_push($youtubeList, array("id"=>$rRs2_row["youtubeId1"], "title"=>$rRs2_row["title1"], "caption"=>$rRs2_row["caption1"], "image"=>$rRs2_row["image1"]));}
							if( !empty($rRs2_row["youtubeId2"])){array_push($youtubeList, array("id"=>$rRs2_row["youtubeId2"], "title"=>$rRs2_row["title2"], "caption"=>$rRs2_row["caption2"], "image"=>$rRs2_row["image2"]));}
							if( !empty($rRs2_row["youtubeId3"])){array_push($youtubeList, array("id"=>$rRs2_row["youtubeId3"], "title"=>$rRs2_row["title3"], "caption"=>$rRs2_row["caption3"], "image"=>$rRs2_row["image3"]));}
							if( !empty($rRs2_row["youtubeId4"])){array_push($youtubeList, array("id"=>$rRs2_row["youtubeId4"], "title"=>$rRs2_row["title4"], "caption"=>$rRs2_row["caption4"], "image"=>$rRs2_row["image4"]));}

							foreach($youtubeList as $youtubeInfo){
						?>
						<div class="videoItem col-lg-3 col-md-3 col-sm-6 col-xs-6" style="padding:10px;">

							<a href="http://www.youtube.com/watch?v=<?=$youtubeInfo["id"]?>" class="fancybox-media" rel="albumGallery" style="display: block;">
								<div style="position:relative">

									<?php
										/*
 										<div class="thumb">
 											<img src="http://img.youtube.com/vi/<?=$youtubeInfo["id"]?>/0.jpg" />
										</div>
										*/
									?>
									<div class="thumb">
										<img src="<?=PATH_TO_ULFILE.$youtubeInfo["image"]?>" />
									</div>
									<div class="videoOverlay" style="position:absolute;">
										<div class="bg">
										</div>
										<div>
											<div>
												<img src="images/playVideoBtn.png" style="width:40px"/>
											</div>
										</div>
									</div>
								</div>
							</a>
							<a href="#">
								<div class="title" style="">
									<?=$youtubeInfo["title"]?>
								</div>
							</a>
							<div class="caption" style="">
								<?=$youtubeInfo["caption"]?>
							</div>
						</div>
						<?php
							}
						?>
					</div>


					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row lineContainer">
						<div class="line">
							<div><div class="border"></div></div>
						</div>
						<div class="titleImg">
							<img src="images/related_recipe.png" alt="相關食譜"/>
						</div>
						<div class="line">
							<div><div class="border"></div></div>
						</div>
					</div>

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row recipeContainer">


						<?php
							$rRs = $pdo -> prepare("select distinct r.id, r.title, r.image, r.isPopular, r.product_id, r.product_id2, p.image_thumb from recipe r left join product p on r.product_id = p.id where product_key = :key and r.isHide = 'N' order by rand() limit 4");

							if(!empty($_REQUEST["p"])){
								$rRs->bindValue("key", $_REQUEST["p"], PDO::PARAM_STR);
							}else{
								$rRs->bindValue("key", "CCB_250", PDO::PARAM_STR);
							}

							$rRs->execute();

							while($rRs_row = $rRs->fetch()){
								$rId = $rRs_row["id"];
								$title = $rRs_row["title"];
								$image = $rRs_row["image"];
								$image = empty($rRs_row["image"])? "images/empty_recipe.png" : PATH_TO_ULFILE.$rRs_row["image"];
								$productImage = $rRs_row["image_thumb"];
								$isPopular = $rRs_row["isPopular"] == "Y";
								$productId2 = $rRs_row["product_id2"];
								$product2Image3 = "";
								if(!empty($productId2)){
									$productRs2 = $pdo->prepare("select id, title, image_thumb, image_thumb2, image_thumb3  from `product` where id = :pid2");
									$productRs2 -> bindValue("pid2", $productId2, PDO::PARAM_INT);
									$productRs2 ->execute();
									$productRs2_row = $productRs2 ->fetch();
									$product2Image3 = $productRs2_row["image_thumb3"];
									$isSCS = $productRs2_row["title"] == "調味雞汁8包" ? "product-scs" : "";
								}
						?>
							<div id="<?=$productId2;?>" class="recipeItem col-lg-3 col-md-3 col-sm-6 col-xs-6" data-ga-recipetitle="<?=$title?>">
								<div><?php print_r($rRs3_row);?>
									<div class="image hoverzoom">
										<a href="recipe-detail.php?id=<?=$rId?>">
											<img src="<?=$image?>" alt="<?=$title?>" />
										</a>
										<?php
											if($isPopular){
										?>
											<div class="hotRecipe">
												<img src="images/hot_recipe.png"/>
											</div>
										<?php
											}
										?>
									</div>

									<a href="recipe-detail.php?id=<?=$rId?>">
										<div class="caption">
											<div class="title">
												<?=$title?>
											</div>
											<div class="productIcon productListIcon">
												<? //var_dump($relatedRecipeInfoList); ?>
												<? if(!empty($product2Image3)){ ?>
												<div class="main <?=$isSCS;?>">
													<img src="<?=PATH_TO_ULFILE.$productImage;?>" />
												</div>
												<div class="main2 <?=$isSCS;?>">
													<img src="<?=PATH_TO_ULFILE.$product2Image3;?>" />
												</div>
												<? } else { ?>
												<div>
													<img src="<?=PATH_TO_ULFILE.$productImage;?>" />
												</div>
												<? } ?>
											</div>
										</div>
									</a>
								</div>
							</div>
						<?php
							}
						?>











					</div>
					<? //background-color:#0e847a;?>

					<div style="clear:both;height:45px;">
					</div>

				</div>
			</div>
			<?php
				include_once 'inc_footer.php';
			?>
		</div>
	</body>
</html>

<div class="productSlider_btns" style="display:none;">
	<img class="left sliderArrowBtns" src="images/arrow_left.png"/>
	<img class="right sliderArrowBtns" src="images/arrow_right.png"/>
</div>
<?php
	include_once 'inc_footer_script.php';
?>
<script src="js/jquery.fancybox-media.js"></script>
<script>


	isGettingNewRecipe  = false;
	$(function(){

		/*
		$(window).scroll(function(e) {
		    var productSliderPos = $(".productSlider").parent().offset().top;



		    if( $(this).scrollTop()>= $(".productInfo.showing .description").offset().top +35){

		    	$('.productSlider').removeClass("fixed");

		    	$('.productSlider').addClass("locked");

		    	$('.productSlider').css("top", $(".productInfo.showing .productDetail").height() + $(".productInfo.showing .image").height() + $(".productSlider .owl-height").height() + $(".productSliderPadding").height()-40);

		    }else if ($(this).scrollTop() +75 >= productSliderPos && $('.productSlider').css('position') != 'fixed'){
		    	$('.productSlider').removeClass("locked");
		        $('.productSlider').addClass("fixed")
		        $(".productSliderPadding").css("height", $('.productSlider .owl-height').height());
		    }else if ($(this).scrollTop() +75< productSliderPos && $('.productSlider').css('position') != 'relative') {
		    	$('.productSlider').removeClass("locked");
		    	$('.productSlider').removeClass("fixed");
		    	$(".productSliderPadding").css("height", 0);
		    	$('.productSlider').css("top", 0);
		    }
		});

		*/






		$(".productSetContainer").on("click",function(e){
			e.stopPropagation();
			if(!isGettingNewRecipe){
				isGettingNewRecipe = true;

				var key = $(this).data("key");
				p = key;
				$(".productInfo").hide().removeClass("showing");
				$("#"+key).show().addClass("showing");
				var pid= $("#"+key).data("id");
				$(".recipeContainer").fadeTo(0 ,300, function(){

					getNewRecipe(pid);
				});
			}
		});


		var isAndroid = (/android/gi).test(navigator.appVersion);
	    var isIDevice = (/iphone|ipad/gi).test(navigator.appVersion);

	    if (isAndroid || isIDevice || $(window).width()<768) {

	    }else{

	    	$('.fancybox-media').fancybox({
				openEffect : 'none',
				closeEffect : 'none',
				prevEffect : 'none',
				nextEffect : 'none',

				arrows : false,
				helpers : {
					media : {},
					buttons : {}
				}
			});
	  	}


		var isFocus = "<?=$_REQUEST["p"]?>";
		if(isFocus != ""){
			$(function(){

				var isAndroid = (/android/gi).test(navigator.appVersion);
			    var isIDevice = (/iphone|ipad/gi).test(navigator.appVersion);

			    if (isAndroid || isIDevice || $(window).width()< 768) {
			       offset = $(".navbar-brand>div").height()+ 10;
			    }else{
				   offset = 0;
			  	}


				$('html, body').animate({
			        scrollTop: $(".productInfo").offset().top - offset
			    }, 400, "easeOutExpo");
			});
		}


		$('.productSlider').owlCarousel({
			nav:true,
			items : 3,
			loop: true,
			dots: false,
			startPosition:<?=$cnt-1?>,
			navText:[$(".productSlider_btns>.left"), $(".productSlider_btns>.right")],
		});




		$('.productSlider').on('translated.owl.carousel',function(e){
		    var currentId = $('.productSlider .owl-item.active:eq(1) .item').data("id")
		    var currentKey = $('.productSlider .owl-item.active:eq(1) .item').data("key")

		    if(!isGettingNewRecipe){
				isGettingNewRecipe = true;


				$(".productInfo").hide().removeClass("showing");
				$("#"+currentKey).show().addClass("showing");

				var pid= $("#"+currentKey).data("id");
				$(".recipeContainer").fadeTo(0 ,300, function(){

					getNewRecipe(currentId);
				});
			}
		});


		$('.productSlider .owl-item img').on("click", function(e){

			var currentId = $(e.target).parent().data("id");
		    var currentKey = $(e.target).parent().data("key");

			var currentItem = $(e.target).closest(".owl-item.active")[0];

			$(".productSlider .owl-item.active").each(function(i,v){

				if(currentItem ==v){
					if(i==0){
						$(".productSlider").trigger('prev.owl.carousel');
					}else if(i==2){
						$(".productSlider").trigger('next.owl.carousel');
					}
				}

			});


		});

	});

	function getNewRecipe(pid){



		$.post("recordUtils.php?getRecipe", {
				product: pid,
				count: 4,
				page: 1
			}, function(d){

				var data = JSON.parse(d);

				if(data.status=="SUCCESS"){
					var result = data.r;
					var totalCnt = data.count;
					console.log(result);
					if(result.length > 0){
						$(".recipeContainer").html("");
						for(var x in result){
							var obj = result[x];

							var id = obj.id;
							var title = obj.t;
							var image ;
							if(obj.image == ""){
								image = "images/empty_recipe.png";
							}else{
								image = "<?=PATH_TO_ULFILE?>" + obj.image;
							}

							var thumb = obj.thumb;
							var thumb2 = obj.thumb2;
							console.log(thumb2);
							var isPop = obj.isPop == "Y";
							var isSCS = obj.isSCS;
							console.log(obj);
							var html =

								'<div class="recipeItem col-lg-3 col-md-3 col-sm-6 col-xs-6 lazy" style="display:none">' +
								'	<div>                                                                      ' +
								'		<div class="image hoverzoom">                                                    ' +
								'			<a href="recipe-detail.php?id='+  id+'">                           ' +
								'				<img src="'+ image +'" alt="'+title+'" />                   ' +
								'			</a>                                                               ' +
								'		</div>                                                                 ';

							if(isPop){
								html+=
								'<div class="hotRecipe">														' +
								'	<img src="images/hot_recipe.png"/>											' +
								'</div>																			';
							}
							if(thumb2 !=null){
								html +=
								'		                                                                       ' +
								'		<a href="recipe-detail.php?id='+  id +'">                              ' +
								'			<div class="caption">                                              ' +
								'				<div class="title">                                            ' +
								'					'+  title  +'                                              ' +
								'				</div>                                                         ' +
								'				<div class="productIcon productListIcon">                                      ' +
								'					<div class="main '+ isSCS +'">                                                      ' +
								'						<img src="<?=PATH_TO_ULFILE?>'+  thumb   +'"/>         ' +
								'					</div>                                                     ' +
								'					<div class="'+ isSCS +' '+(id=='230' ? "special" : "")+'">                                                      ' +
								'						<img src="<?=PATH_TO_ULFILE?>'+  thumb2   +'"/>         ' +
								'					</div>                                                     ' +
								'				</div>                                                         ' +
								'			</div>                                                             ' +
								'		</a>                                                                   ' +
								'		<div style="clear: both"></div>                                        ' +
								'	</div>                                                                     ' +
								'</div>                                                                        ';
							}else{
								html +=
								'		                                                                       ' +
								'		<a href="recipe-detail.php?id='+  id +'">                              ' +
								'			<div class="caption">                                              ' +
								'				<div class="title">                                            ' +
								'					'+  title  +'                                              ' +
								'				</div>                                                         ' +
								'				<div class="productIcon">                                      ' +
								'					<div>                                                      ' +
								'						<img src="<?=PATH_TO_ULFILE?>'+  thumb   +'"/>         ' +
								'					</div>                                                     ' +
								'				</div>                                                         ' +
								'			</div>                                                             ' +
								'		</a>                                                                   ' +
								'		<div style="clear: both"></div>                                        ' +
								'	</div>                                                                     ' +
								'</div>                                                                        ';
							}


							$(".recipeContainer").append(html);
						}
						$(".lazy").each(function(i,v){
							$(v).fadeTo(1,400);
						});

					}else{
					}
					isGettingNewRecipe = false;
				}




		});
	}


</script>

@charset "utf-8";
/* CSS Document */
*:not(.fancybox-overlay *){
	transition: all 0.5s ease;
}
html,body{
	margin: 0;
	padding: 0;
	font-family: "Arial", "微軟正黑體", Helvetica, sans-serif; 
	color:#565b54;
	background-color:#0e847a;
	-webkit-font-smoothing: antialiased;
	border:none;
}
@media (min-width:768px){
	body{
	}
	
}

a{
	color:#0e847a;
}

a:hover{
	text-decoration:none;
	opacity:0.8;
	color:#0e847a;
}

.ani{
	-webkit-transition: all 0.3s linear;
	-moz-transition: all 0.3s linear;
	-o-transition: all 0.3s linear;
	transition: all 0.3s linear;
}


p{
	margin: 0 0 0px 0;	
}

a{
	text-decoration:none;
}


img{
	border: 0;
}

.clear{
	clear:both;
}

.pad0{
	padding:0!important;
}

.row{
	margin-left:0px;
	margin-right:0px;
}


.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12{
	padding:0px;
}

.container{
	padding-left:15px;
	padding-right:15px;
}

@media(min-width:768px){
	.container{
		padding-left:0px;
		padding-right:0px;
	}
}

@media (min-width: 1200px){
	.container {
	    width: 1200px;
	}
}
/* start include_header	*/
	.navbar {
		background:none;
		width:100%;
		position:fixed;
		z-index:10;
	    top: 0px;
	    border-bottom: 6px solid #f1dc00;
	    opacity: 0.9;
        border-top: none;
        border-right: none;
        border-left: none;
	    margin:0px;
	}
	@media (min-width:768px){
		 .navbar {
	 		background-color:#0e847a;
	 		position:fixed;
		    border:none;
		    opacity:1;
		    border-radius:0px;
	        padding-bottom: 1px;
		    position:relative;
		 }
	}
	.navbar-default .navbar-toggle{
		float:left;
		background:none;
		border:none;
	    position: absolute;
	    margin:0px;
	    padding:9px 40px 9px 12px;
	}
	.navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover{
		background:none;
	}
	
	.navbar-header{
		/*background-color:#FFF;*/
		background-color:#0e847a;
	}
	
	@media (min-width:768px){
		 .navbar-header{
	 		padding: 0px;
		 }
	}
	
	.navbar-brand{
		float:none;
		padding:0px;
	}
	.navbar-brand>div{
		text-align:center;
	}
	@media(min-width:768px){
		.navbar-brand>div{
			text-align:left;
		}
	}
	
	@media(min-width:768px){
		.navbar-brand div{
			padding:0px 50px;
		}
	}
	@media(min-width:992px){
		.navbar-brand div{
			padding:0px 30px;
		}
	}
	@media(min-width:1200px){
		.navbar-brand div{
			padding:0px;
		}
	}
	
	
	
	.navbar-brand img{
		margin: 10px 0px;
		height:50px;
		display:inline-block;
	}
	@media (min-width:768px){
		.navbar-brand img{
			margin: 10px 0px;
			height:auto;
			display:inline-block;
		}
	}
	
	.navbar-nav{
		margin:0px;
	}
	.navbar-default .navbar-nav>li>a{
		color:#f36f24;
		font-weight:bold;
		font-size:18px;
	}
	
	.navbar-default .navbar-toggle .icon-bar{
		background-color:#002D28;
	}
	.nav>li{
		text-align:center;
		opacity: 0.95;
	}
	
	.navbar-default .navbar-nav>li>a{
		background-color:#0e847a !important;
		padding-left:0px;
		padding-right:0px;
	    color:#FFF !important;
	}
	
	.navbar-default .navbar-nav>li>a:hover{
		/*background-color:#23baad !important;*/
	    color:#fff !important;
	    opacity:0.9 !important;
	}
	
	.navbar-default .collapsed.navbar-toggle:focus, .navbar-default .collapsed.navbar-toggle:hover{
		background-color:inherit;		
	}
	
	.dropdown:hover .dropdown-menu {
	    display: block;
	    margin-top: 0;
	    background-color:#0E847A;
	 }
	 .dropdown:hover .dropdown-menu a{
	    text-align:center;
	    color:#fff !important;
	 }
	 .dropdown:hover .dropdown-menu a:hover{
	    background-color:#f1dc00 ;
	    color :#0E847A !important;
	    font-weight:bold;
	    opacity:1;
	 }
	 
	 #sidebar-wrapper{
		display:block;
	
	 }
	 #sidebar-wrapper .sidebar-nav{
	 	padding:5px 25px 5px 25px;
	 }
	 .sidebar-cancel-btn{
 	    float: right;
	    font-size: 20px;
	    color: rgba(160,160,160,0.6);
	    padding: 4px;
	 	
	 }
	 
	 
	 #sidebar-wrapper .sidebar-bottom {
	 	padding:10px 10%;
	 }
	 
	 #sidebar-wrapper .sidebar-bottom>* {
	 	padding-bottom:10px;
	 }
	 
	 #sidebar-wrapper li{
	 	border-bottom: 1px solid rgba(160,160,160,0.6);
	 }
	 
	 #sidebar-wrapper li:nth-child(2){
	 	border-top: 3px solid #f1dc00;
	 }
  	 #sidebar-wrapper li:last-child{
	 	border-bottom: 3px solid #f1dc00;
	 }
	  	
	 
	 @media (min-width:768px){
	 	#sidebar-wrapper{
			display:none;
		 }
	 }
	 
	@media (min-width: 768px){
		.dropdown:hover .dropdown-menu a{
			text-align:left;
		}
	}
	
	@media (min-width: 768px){
		.navbar-nav {
		    float: right;
		    height:80px;
	        
		}
		.navbar-nav>li{
			height:100%;
			padding-left: 4px;
		}
		.navbar-nav>li>a{
			padding-top: 30px;
	  	 	padding-bottom: 30px;
		}
		
		.nav>li>a{
			padding-left:5px;
			padding-right:5px;
		}
	}
	
	@media (min-width: 840px){
		.navbar-nav>li{
			height:100%;
			padding-left: 16px; 
		}
		.nav>li>a{  
			padding-left:10px;
			padding-right:10px;
		}
	}
	
	@media (min-width: 991px){
		.navbar-nav>li{
			height:100%;
			padding-left: 36px;
		}
		.nav>li>a{
			padding-left:20px;
			padding-right:20px;
		}
	}

	@media (min-width: 1100px){
		.nav>li>a{
			padding-left:20px;
			padding-right:20px;
		}
	}
	
	.navbar .row{
	    padding-left: 0px;
		padding-right: 0px;
		margin:auto;
	}

	@media (min-width: 768px){
		.navbar .row{
			padding-left: 0px;
			padding-right: 0px;
			margin:0px;
		}
	}

	@media (min-width: 1100px){
		.navbar .row{
			margin:auto;
			max-width:1200px;
		}
	}

/* end include_header	*/



/* start include_footer	*/

	#footerContainer{
		background-color: #0f847b;
		padding-left:15px;
		padding-right:15px;
		border-bottom: 8px solid #f0dc01;
	}
	#footerContainer>div{
		padding:0px 0px 55px 0px;;
	}
	
	#footerContainer, 
	#footerContainer a {
		color:#FFF;
	}
	#copyrightContainer{
		text-align:center;
		padding:10px 0px;
	}
	
	

	#footerContainer .footerList .ingredients *,
	#footerContainer .footerList .theme *,
	#footerContainer .footerList .product * {
		padding:2px 0px;
		font-size:13px;
	}
	
	#footerContainer .footerList .title{
		font-weight:bold;
		font-size:14px;
		padding:4px 0px;
	}
	
	#footerContainer .footerList{
		padding-bottom:8px;
		margin-bottom:8px;
		width:32%;
	}
	
	#footerContainer .footerList.ingredientsFooterList{
		width:24%;
	}
	#footerContainer .footerList.themeFooterList{
		width:30%;
	}
	#footerContainer .footerList.productFooterList{
		width:30%;
	}
	
	
	@media(min-width:768px){
		#footerContainer .footerList{
			padding-bottom:8px;
			padding-left:12px;
			margin-bottom:8px;
			width:32%;
		}
	}
		
	#footerContainer .footerList.btnContainer{
		width:100%;
	}

	#footerContainer .footerList.btnContainer>div{
		padding-top:10px;
		text-align:center;
	}

	@media(min-width:768px){
		#footerContainer .footerList.ingredientsFooterList{
			width:19%;
		}
		#footerContainer .footerList.themeFooterList{
			width:19%;
		}
		#footerContainer .footerList.productFooterList{
			width:28%;
		}
		#footerContainer .footerList{
			border-bottom:none;
		    padding-left: 0px;
		}
		#footerContainer .footerList.btnContainer{
			width:29%;
			float: right;
		}		
		#footerContainer .footerList.btnContainer>div{
			text-align:right;
		}
	}
	
	
	
	
	#footerContainer .footerList:last-child,
	#footerContainer .footerList:nth-child(3) {
		border-bottom:none;
	}
	
	.footerListContainer{
		margin-bottom:20px;
		height:250px;
	}
	@media(min-width:768px){
		.footerListContainer{
			margin-bottom:40px;
			height:190px;
		}
		
	}
	
	@media(min-width:768px){
		
		#footerContainer .footerList:first-child{
			/*padding-left:0px;*/
		}
		#footerContainer .footerList:last-child,
		#footerContainer .footerList:nth-child(3){
			border-right:none;
		}
		
	}
	
	#footerContainer .btnContainer{
		padding:0px;
		text-align:center;
		padding-top: 20px;
	}
	
	.btnContainer img{
		padding:8px 0px;
	}
	
	.btnContainer .youtubeBtn{
		height: 48px;
	}
	.btnContainer .facebookBtn{
		height: 48px;
	}
	
	@media(min-width:768px){
		#footerContainer .btnContainer{
			padding:0px;
			margin-top: 100px;
		}
		
		.btnContainer .youtubeBtn{
			height: 48px;
		}
		.btnContainer .facebookBtn{
			height: 48px;
		}
	}
	
	
	#footerContainer hr{
		border-color:#29b1a5;
		border-style: inset;
  	  	border-width: 1px;
	}
	
	#footerContainer hr.vertical{
/*		height: 100%; 
		float:left;
		margin:0px;
		margin-right:10px;
*/
		display:none;		
	}
	@media(min-width:768px){
		#footerContainer hr.vertical{
			display:block;		
			height: 100%; 
			float:left;
			margin:0px;
			margin-right:1%;
		}
	}
	
	#footerContainer .footerLinks a{
		padding: 0px 8px;
		border-right: 1px solid #FFF;
		text-align:center;
		margin:2px 0px;
		float:left
	}
	
	#footerContainer .footerLinks a{
			width:33.3%;
			height:30px;
	}
	@media(min-width:768px){
		#footerContainer .footerLinks a{
			width:auto;
			height:auto;
			font-size:12.5px;
		}	
	}
	
	
	#footerContainer .footerLinks a:last-child{
		border-right:0px;
	}
	
	
	#footerTitle{
		font-size:20px;
	}
	
	#backtotopDiv{
	    text-align: center;
	}
	
	@media(min-width:768px){
		#footerTitle{
			font-size:15px;
			padding-top: 25px;
		}
		
		#backtotopDiv{
			float:right;
		    position: absolute;
    		right: 0px;
			
		}
	}
	
	.footerCopyRight_line1, .footerCopyRight_line2{
		text-align:center;
	}
	
	.footerCopyRight_line1, .footerCopyRight_line2{
		display:block;
	}
	@media(min-width:768px){
		.footerCopyRight_line1, .footerCopyRight_line2{
			display:block;
			text-align:left;
		}
	}
	
/* end include_footer	*/


/* start common */
	.mainContainer{
		background-image:url(../images/pagebg.jpg);		
	}
	
	.mainContainer .container.swansonBreadcrumb img{ 
		 vertical-align: top;
		 margin-left: -3px;
		 padding-right: 1px;
	}
	@media(min-width:768px){
		
		.mainContainer .container.swansonBreadcrumb img{ 
			 vertical-align: top;
			 margin-left: -3px;
			 padding-right: 1px;
			 padding-top:4px;
		}
		
		
		.mainContainer .container.swansonBreadcrumb{
			margin:auto;
			padding-left:205px;			
		}
		
		.mainContainer .container.swansonBreadcrumb,
		.mainContainer .container.swansonBreadcrumb a{
			font-size:18px;
			font-weight:bold;
		    padding-top: 1px;
		}
		
		
	}
	
	.breadcrumb{
		color:#0e847a;
		font-size:16px;
		padding: 5px 15px;
		font-weight:bold;
	    padding-top: 4px;
		padding-bottom:8px;
	    margin-bottom: 18px;
	    background-color:#f1dc00;
	}
	
	@media(min-width:768px){
		.breadcrumb{
			color:#0e847a;
			padding: 5px 15px;
			font-weight:bold;
			padding-bottom:14px;
			padding-top:14px;
		    margin-bottom: 25px;
		    background-color:#f1dc00;
		}
		
		.breadcrumb_mp{
			padding-bottom:12px;
		    background-color:#f1dc00;
		}
	}
	
	.breadcrumb .glyphicon-menu-right{
		font-size: 10px;
	    font-weight: bold;
	    vertical-align: top;
	    padding-top: 6px;
	    padding-left: 4px;
	    padding-right: 4px;
	}
/* end common */


/* recipe */

	.menuContainer_m .searchTool{
		padding:15px 16px 24px 20px;
		background-color:#0f847b;
		color:#FFF;
		font-weight:bold;
	    border-radius: 3px;
		font-size:20px;
	}

	#leftMenu .searchTool{
		padding:15px 16px 24px 20px;
		background-color:#0f847b;
		color:#FFF;
		font-weight:bold;
	    border-radius: 3px;
		font-size:20px;
		margin-bottom:19px;
	}

	#leftMenu .title{
		padding:13px 9px 13px 25px;
		margin:5px 0px;
		background-color:#0f847b;
		color:#FFF;
		font-size:18px;
		font-weight:bold;
	}
	
	#leftMenu .collapsible{
		cursor:pointer;
	}
	#leftMenu .collapsible div:first-child{
		width:80%;
		display:inline-block;
	}
	#leftMenu .collapsible div:last-child{
		width:15%;
		font-size:12px;
		display:inline-block;
		text-align:right;
	    font-weight: bold;
	}
	
	#leftMenu .collapsibleContent{
	    border: 1px solid #0F847B;
	    background-color: #FFF;
	    padding: 5px 15px;
	    font-size: 18px;
	}
	#leftMenu .collapsibleContent{
	}
	#leftMenu .collapsibleContent label,
	#leftMenu .checkboxGroupTitle
	{
		padding: 8px 4px;	
		color: #0F847B;	
		font-weight:bold;
		display:block;
		cursor:pointer;
	}
	#leftMenu .checkboxGroupTitle .arrow{
		padding-right:40%;
	}
	
	#leftMenu .checkboxGroupContent{
		padding-left:30px;
	}
	#leftMenu .collapsibleContent label:hover{
		background-color:#0F847B;
		color:#FFF;
	}
	#leftMenu .collapsibleContent [type='checkbox']{
		font-size:15px;
		margin:5px;
		margin-right:8px;
	}
	
	#leftMenu .collapsible .activeIcon{
		display:none;
	}
	#leftMenu .collapsible.active .activeIcon {
		display:block;
	}
	
	#leftMenu .collapsible .inActiveIcon{
		display:block;
	}

	#leftMenu .collapsible.active .inActiveIcon{
		display:none;
	}
	
	#leftMenu>div:last-child{
		margin-bottom:200px;
	}
	#leftMenu, #leftMenu *{
		transition:none;
	}
	.recipeItem{
		padding:0px 5px;
		margin-bottom: 10px;
		height:auto;
		-webkit-transform-origin: center center;
		transform-origin: center center;
	}
	.recipeItem:nth-child(odd){
		padding-left:0px;
	}
	.recipeItem:nth-child(even){
		padding-right:0px;
	}
	@media (min-width:768px){
		.recipeItem{
			padding:0px 8px;
			margin-bottom: 10px;
		}
		
		.recipeItem:nth-child(odd){
			padding-left:8px;
		}
		.recipeItem:nth-child(even){
			padding-right:8px;
		}
		
	}	
	
	.recipeItem:hover>div{
		/*height:105%;
		margin-top:-5%;
		z-index:10;*/
		
	}
	
	.recipeItem:nth-child(3n){
		/*padding-right:0px;*/
	}
	
	.recipeItem .image{
		background-color:#FFF;
		position: relative;
	}
	.recipeItem .image img{
		width:100%;
		-webkit-transform-origin: center center;
		transform-origin: center center;
	}
	.recipeItem .caption{
		width:100%;
		height:auto;
		background-color:#FFF;
		height: 60px;
	}
	@media (min-width:768px){
		.recipeItem .caption{
			height:100px;	
			max-height:100px;
		}	
	}
	
	.recipeDetail .recipeItem .title,
	.recipeItem .title{
		color:#0F847B;
		display:inline-block;
	    padding: 4px 0px 0px 8px;
	    font-weight: bold;
	    font-size: 18px;
		width:65%;
	} 
	
	
	@media (min-width:768px){
		.recipeItem .title{
			padding:15px 18px;
		}
		
		.recipeItem .title,
		.recipeDetail .recipeItem .title{
			font-size:21px;
			width:65%;
		}
	}
	.recipeItem .productIcon{
		width:30%;
		display:inline-block;
		vertical-align: top;
	}
	@media (min-width:768px){
		.recipeItem .productIcon{
			width:35%;
			display:inline-block;
			vertical-align: top;
		}	
	}
	
	.recipeItem .productIcon div{
		max-width:100px;
		padding:5px 0px;
	    float: right;	
	}
	.recipeItem .productIcon img{
		max-height: 50px;
		max-width: 100px;
	}
	@media (min-width:768px){
		.recipeItem .productIcon img{
			
			max-height: 90px;
		}	
	}
	@media (min-width:768px){
		.recipeItem .productIcon{
			float:right;
			
		}
	
	}
	#wrapper #page-content-wrapper{
		padding: 0px;
		padding-top:75px;
	}
	@media(min-width:768px){
		#wrapper #page-content-wrapper{
			padding: 0px;
		}
		
	}
	
	.hotRecipe{
		position: absolute;
		top:0px;
		left:8px;
		max-width:25%;
	}
	.hotRecipe img{
		width:100%;
	}
	
	.menuContainer_m{
		padding-bottom:16px;
	}
	.menuContainer_m .multiselect-container>li>a>label{
		color: #0F847B;
    	font-weight: bold;
    	font-size:16px;
	}
	
	.menuContainer_m button.multiselect.dropdown-toggle.btn.btn-default{
		border-radius:0px;
		border-color:#0F847B; 
		margin-top:8px;
		width:100%;
	    color: #FFF;
   	 	background-color: #0F847B;
   	 	overflow: hidden;
   	 	padding: 6px 0px;
	}
	.menuContainer_m span.multiselect-selected-text{
		padding:10px 0px;
		font-weight:bold;
		color: #fff;
		font-size:18px;
		white-space: normal;
	} 
	
	.menuContainer_m .dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover {
	    color: #fff;
	    text-decoration: none;
	    background-color: transparent;
	    outline: 0;
	}
	
	.menuContainer_m .btn-group{
		width:100%;
	}
	
	
	.menuContainer_m li.notInGroup>a>label{
		padding: 10px 5px 10px 40px;
	}
	.menuContainer_m li.inGroup>a>label{
	    padding: 10px 5px 10px 80px;
	}
	.menuContainer_m .multiselect-container>li>a>label>input[type=checkbox]{
		margin-left:-20px;
	}
	.caret-container{
	    padding-right: 60%;
		padding-top: 5px;
		padding-bottom: 5px;
	}
	
	.caret-container .caret{
		border-top: 6px dashed;
		border-top: 6px solid\9;
		border-bottom: none;
		border-left: 6px solid transparent;
		border-right: 6px solid transparent;
		
	}
	
	.caret-container.active .caret{
		
		border-top: 6px solid transparent;
		border-bottom: 6px solid transparent;
		border-left: 6px dashed;
		border-left: 6px solid\9;
		border-right: none;
	}
	
	.menuContainer_m .open>.dropdown-menu{
	    width:100%;
        border-radius: 0px;
        border:1px solid #0F847B;
	}
	
	
	
	.pagingContainer{
		text-align:center;
		margin:10px;
		clear:both;
	}
	
	.pageItemContainer{
		display:inline-block;
	}
	
	.pagingContainer .pageItem.left, .pagingContainer  .pageItem.right{
		background-color:#0f847B;
		color:#FFF;
	}
	.pagingContainer .pageItem{
		cursor:pointer;
		padding:5px 8px;
		margin:0px 2px;
		display:inline-block;
		border:1px solid #0f847B;
		background-color:#FFF;
		font-size:16px;
		font-weight:bold;
		color:#0f847B;
	}
	
	.pagingContainer .pageItem.current{
		background-color:#0f847B;
		color:#FFF;
	}
	
	.pagingContainer .pageItem:hover{
		background-color:#0f847B;
		color:#FFF;
	}
	
	
	@media(min-width:768px){
		.pagingContainer{
			text-align:right;
			margin:15px 8px;
			clear:both;
		}
		
		.pageItemContainer{
			display:inline-block;
		}
		
		.pagingContainer .pageItem.left, .pagingContainer  .pageItem.right{
			background-color:#0f847B;
			color:#FFF;
		}
		.pagingContainer .pageItem{
			cursor:pointer;
			padding:5px 11px;
			margin:0px 2px;
			display:inline-block;
			border:1px solid #0f847B;
			background-color:#FFF;
			font-size:16px;
			font-weight:bold;
			color:#0f847B;
		}
		
		.pagingContainer .pageItem.current{
			background-color:#0f847B;
			color:#FFF;
		}
		
		.pagingContainer .pageItem:hover{
			background-color:#0f847B;
			color:#FFF;
		}
			
	}
	
/* recipe */

/* recipe-detail & product*/

	 
	.recipeDetail .title{
		font-size:36px;
		color:#0F847B;
		font-weight:bold;
	}
	.recipeDetail .shareContainer{
		padding:10px 0px;
	}
	@media(min-width:768px){
		.recipeDetail .shareContainer{
		    right: 34px;
		    color:#888;
		    top: 26px;
		    padding: 15px 0px;
		    font-size: 18px;
		}
	}
	@media(min-width:1200px){
		.recipeDetail .shareContainer{
			position: absolute;
		}
	}
	
	@media(min-width:768px){
		.recipeDetail .bigimage{
			min-height:420px;
		}
	}
	
	.recipeDetail .content{
		padding:15px 15px;
	}
	@media(min-width:768px){
		.recipeDetail .content{
			padding:35px 60px;
			min-height:420px;		
		}
	}

	.recipeDetail .shareContainer img{
		padding:0px 2px;
		vertical-align:middle;
	}
	.recipeDetail .title2{
		font-size:24px;
		color:#0F847B;
		font-weight:bold;
		margin-bottom:4px;
		margin-top:0px;
	}
	
	.recipeDetail .productThumb{
		display:block;
		text-align:center;
		clear:both;
		
		position: absolute;
	    right: 0px;
	    top: 60px;
	    width: 100px;
	}
	
	.recipeDetail .productThumb img{
		margin-top:15px ;
	}
	@media(min-width:400px){
		.recipeDetail .productThumb{
			width:150px;
		}	
	}
	@media(min-width:500px){
		.recipeDetail .productThumb{
			width:180px;
		}	
	}
	@media(min-width:768px){
		.recipeDetail .productThumb{
			position: absolute;
			bottom: 0px;
			right: 10px;
			width:120px;
		    top: auto;
		}
	}
	@media(min-width:992px){
		.recipeDetail .productThumb{
			position: absolute;
			bottom: 0px;
			right: 10px;
			width:150px;
		    top: auto;
		}
	}
	
	@media(min-width:1200px){
		.recipeDetail .productThumb{
			position: absolute;
			bottom: 0px;
			right: 10px;
			width:180px;
		    top: auto;
		}
	}
	
	
	
	
	.recipeDetail .stepsContainer{
		padding:15px;	
	}
	
	@media(min-width:768px){
		.recipeDetail .stepsContainer{
			padding:20px 50px;	
		}	
	}
	
	.recipeDetail .steps .number{
		background-color:#0F847B;
		width:24px;
		height:24px;
		color:#FFF;
		font-weight:bold;
		text-align:center;
		line-height:24px;
		font-size:13px;
		border-radius:30px;

		display:inline-block;
    	vertical-align: top;
	}
	.recipeDetail .steps .stepContent{
		display:inline-block;
		font-size:16px;
		width:calc(100% - 30px);
		padding:5px 5px;
	}
	.recipeDetail hr{
		margin:10px 0px;
	}
	.recipeDetail .relatedRecipe{
	}
	
	
	
	@media(min-width:768px){
		
		.recipe_share{
			padding-top:50px;
			z-index:1000;
			width:50px;
			text-align:center;
			left:-50px;
		}
		
	}
	
	
	
/* recipe-detail & product*/


/* productList */
	.productIntro{
		background-image: url('../images/product_bg.jpg');
		min-height:400px;
		background-size: cover;
		
		padding:15px;
	}
	@media(min-width:768px){
		.productIntro{
			padding:40px;
		}
				
	}
	
	.productIntro .title{
		font-size:22px;
		color:#FFF;
		font-weight:bold;
		text-align:left;
	}
	
	@media(min-width:768px){
		.productIntro .title{
			font-size:24px;
		}
				
	}
	
	.productIntro .caption{
		color :#FFF;
		padding:25px 0px;
		font-size:15px;
	}
	
	
	.productList{
		background-image: url('../images/product_bg2.jpg');
		
		background-size: cover;
		padding: 40px 0px;
	}
	
	.productInfo .image{
		padding:15px 15px;
		margin:15px;
	}
	@media(min-width:768px){
		.productInfo .image{
			padding:50px;
			margin:0px;
		}
	}
	
	.productInfo .description{
		padding-top:20px;
		width:100%;
	}
	@media(min-width:768px){
		.productInfo .description{
			padding-top:20px;
			width:100%;
		}
	}
	
	.productInfo .productDetail{
		padding:20px 5px 15px 5px; 
		display:table
	}
	
	@media(min-width:768px){
		.productDetail{
			padding:40px;
			min-height:400px;	
		}
	}
	
	.productInfo .title{
		font-size:22px;
	}
	@media(min-width:458px){
		.productInfo .title{
			font-size:30px;
		}
	}
	
	
	@media(min-width:768px){
		.productInfo .title{
			font-size:30px;
		}
	}
	
	
	@media(min-width:768px){
		.productList{
			height:325px;
		}
		.productList .container{
			padding:0px 0px;
		}
	}
	@media(min-width:992px){
		.productList{
			height:350px;
		}
		.productList .container{
			padding:20px 0px;
		}
	}
	@media(min-width:1200px){
		.productList{
			height:400px;
		}
		
		.productList .container{
			padding:40px 0px;
		}
	}
	
	.productList .container>div{
		position:relative;
		height:100%;
		padding-left:50px;
	}
	.productSetContainer{
		display:inline-block;
		cursor:pointer;
		position: relative;
	}
	
	@media(min-width:768px){
		.productSetContainer.active{
			 	
			 z-index:200;
		}
	}
	
	
	@media(min-width:768px){
		.productSetContainer{
			max-width:14%;
			margin-left: -40px;
		}
	}
	@media(min-width:992px){
		.productSetContainer{
			max-width:10%;
			margin-left: -15px;
		}
	}
	@media(min-width:1200px){
		.productSetContainer{
			max-width:15%;
			margin-left: -22px;
		}
	}
	
	.productSetContainer img{
		width:100%;
	    vertical-align: bottom;
	}

	
	
	.productSetContainer img:hover{
		opacity:0.8;
		z-index:100;
		
	}
	
	
	
	
	
	.productList .productName{
		opacity: 0;
		background-image : url('../images/product_name_bg.png');
		width:200px;
		height:45px;
		text-align:center;
		color:#FFF;
		padding-top:16px;
		font-size:18px;
		font-weight:bold;
		position: absolute;
   	 	margin-left: calc(50% - 100px);
   	 	transition: visibility 0s, opacity 0.2s linear;
	}
	
	.productList .productSetContainer:hover .productName{
		visibility: visible;
  		opacity: 1;
	}
	
	.productSlider{
		/*padding-bottom:25px;*/
	}
	
	.productSlider .item img{
		padding:15px;
	}
	
	.productSlider .owl-stage{
		background-color:#FFF;
	}
	
	.productSlider.fixed{
	    position: fixed;
		top: 72px !important;
		padding: 0px 15px;
		left: 0px;
	}
	
	.productSlider.locked{
	    position: absolute;
	}
	
	.productDescEnd{
		text-align:center;
		padding-top: 40px;
	}
	
	@media(min-width:768px){
		.productDescEnd{
			padding-top: 40px;
		}
	}
	
	.productSlider .owl-carousel {
		position: relative;
	}
	.productSlider .owl-prev,
	.productSlider .owl-next{
		position: absolute;
		top: 40%;
	}
	.productSlider .owl-prev {
		left: -2%;
	}
	.productSlider .owl-next {
		right: -2%;
	}
	.productSlider .owl-controls{
	    margin-top: -25px;
	}
	.productSlider .sliderArrowBtns {
	    width: 60%;
	}
	
	
	
	@media(min-width:992px){
		
		.product_share{
			padding-top:50px;
			z-index:1000;
			position:fixed;
			width:50px;
			text-align:center;
			top:35%;
			transform: translateX(-100%);
		}
		
		
		
		.product_share.shown{
		   opacity:1;
			
		}
		.product_share.hidden{
			/*left:-1000px;*/
			opacity:0;
		}
	}
	
	@media(min-width:992px){
		.product_share{
			top:35%;
		}
		
		
	}
	
/* productList */


/*video Item*/
	.videoItem .thumb img{
		width:100%;
		padding:0px 0px;
	}
	
	.videoItem .title{
		padding:5px 0px;
		color:#0F847B;
		font-size:12px;
		font-weight:bold;
	}
	
	.videoItem .caption{
		font-size:12px;
		padding:0px 0px;
		color:#777;
		height:80px;
		display: -webkit-box;
	    line-height: 1.4;
	    -webkit-line-clamp: 3;
	    -webkit-box-orient: vertical;
	    overflow: hidden;
	    text-overflow: ellipsis;
	    
	}
	@media(min-width:300px){
		.videoItem .title{
			height:50px;
		}
	}
	
	@media(min-width:400px){
		.videoItem .title{
			height:auto
		}
	}
	
	@media(min-width:300px){
		.videoItem .title{
			font-size:13px;
		}
		.videoItem .caption{
			height:80px;
			font-size:13px;
		}
	}
	@media(min-width:400px){
		.videoItem .title{
			font-size:14px;
		}
		.videoItem .caption{
			height:70px;
			font-size:14px;
		}
	}
	@media(min-width:500px){
		.videoItem .title{
			font-size:18px;
		}
		.videoItem .caption{
			height:60px;
		}
	}	
	@media(min-width:768px){
		.videoItem .caption{
			height:40px;
		}
	}
	
	
	.videoItem .videoOverlay{
		
		width:100%;
		height:100%;
		top:0px;
		left:0px;
	}
	.videoItem .videoOverlay>div{
		display:table;
	    width: 100%;
		height: 100%;	
		text-align:center;
		
		position:absolute;
	}
	.videoItem .videoOverlay .bg{
		background-color:#000;
		opacity:0.5;
	}
	
	.videoItem .videoOverlay .bg:hover{
		opacity:0.4;
	}
	
	.videoItem .videoOverlay>div>div{
		display:table-cell;
		vertical-align:middle;
		
	}
/*video Item*/

/* line*/
	.lineContainer{
	    padding: 0px 15px;
	}
	@media(min-width:768px){
		.lineContainer{
		    padding: 0px 0px;
		}
	}

	.lineContainer .line{
		width: calc(50% - 100px);
		float:left;
		height:75px;
	}
	
	.lineContainer .line{
		display:table;
	}
	.lineContainer .line>div{
		display:table-cell;
		vertical-align:middle;
	}
	.lineContainer .line>div>div.border{
		border-top:2px solid #0E847A;
		border-bottom:1px solid #0E847A;
		border-left:none;
		border-right:none;
		padding:3px 0px;
		line-height: 75px;
	}
	
	.lineContainer .titleImg{
		float:left;
		width:200px;
	}
	
	
/*line */
.listResult{
	padding-bottom:25px;
	font-size:16px;
}

/*video*/
.videoItem{
	
}

@media (min-width: 768px){
	.recipeVideoItemContainer .col-sm-6{
		width:48.5% !important;
	}
}

@media (min-width: 992px){
	.recipeVideoItemContainer .col-md-6{
		width:48.5% !important;
	}
}	


@media (min-width: 1200px){
	.recipeVideoItemContainer .col-lg-4{
		width:32% !important;
	}
}








.recipeVideoItemContainer{
	text-align: justify;
    -ms-text-justify: distribute-all-lines;
    text-justify: distribute-all-lines;
    
}

.recipeVideoItem{
	float:none;
	padding-top:10px;
	padding-bottom:10px;
	display:inline-block;
}
.recipeVideoItem>div{
	position:relative;
}
.recipeVideoItem>div>div{
	background-color:#fff;
}
.recipeVideoItem .image img{
		width:100%;
}

.recipeVideoItem .title{
	color:#0E847A;
	font-size:22px;
	line-height:24px;
	font-weight:bold;
	padding:10px 15px;
}


.recipeVideoItem .description{
	color:#808285;
	font-size:15px;
	line-height:26px;
	padding:15px 15px;
    /*letter-spacing: -0.4px;*/
}
.recipeVideoItem .button{
	border:1px solid #d1d3d4;
	border-radius:4px;
	padding:10px 12px;
	font-size:16px;
	font-weight:bold;
	color:#0E847A;
	width:120px;
	text-align:center;
	line-height: 28px;
	display:inline-block
}
.recipeVideoItem .button img{
	padding:4px 3px 4px 3px;
	vertical-align:top;
}

.recipeVideoItem .buttonHolder{
    width: 100%;
    bottom: 32px;
    padding: 15px 23px;
    left: 0px;
}
.recipeVideoItem .buttonHolder>div{
	display:inline-block;
	width:49%;
}

.recipeVideoBackToTopBtn{
	text-align:center;
	position:fixed;
	bottom:0px;
	width:100%;
	left:0px;
	bottom:-100px;
    border-bottom: 12px solid #0e847a;
}
.recipeVideoBackToTopBtn img{
	min-height:50px;
}




@media(min-width:768px){
	.recipeVideoItemContainer{
		text-align: justify;
	    -ms-text-justify: distribute-all-lines;
	    text-justify: distribute-all-lines;
	    
	}
	
	.recipeVideoItem{
		float:none;
		padding-top:10px;
		padding-bottom:10px;
		display:inline-block;
	}
	.recipeVideoItem>div{
		height:600px;
		background-color:#0e847a;
		position:relative;
	}
	.recipeVideoItem>div>div{
		position:absolute;
		width:100%;
		height:100%;
		margin-top:-5px;
		margin-left:-5px;
		
		padding:24px 23px 30px 23px;
	}
	
	
	.recipeVideoItemContainer:after {
	    content: '';
	    width: 100%;
	    display: inline-block;
	    font-size: 0;
	    line-height: 0
	}
	
	.recipeVideoItem .image img{
		width:100%;
	}
	
	.recipeVideoItem .title{
		color:#0E847A;
		font-size:24px;
		line-height:24px;
		font-weight:bold;
		padding:22px 0px;
	}
	
	
	.recipeVideoItem .description{
		color:#808285;
		font-size:16px;
		line-height:26px;
		padding:0px;
	    /*letter-spacing: -0.4px;*/
	}
	.recipeVideoItem .button{
		border:1px solid #d1d3d4;
		border-radius:4px;
		padding:10px 20px;
		font-size:19px;
		font-weight:bold;
		color:#0E847A;
		width:150px;
		text-align:center;
		line-height: 28px;
	}
	.recipeVideoItem .button img{
		padding:4px 3px 4px 3px;
		vertical-align:top;
	}
	
	.recipeVideoItem .buttonHolder{
	    position: absolute;
	    width: 100%;
	    bottom: 32px;
	    padding: 0px 23px;
	    left: 0px;
	}
}


/*video*/

/*cookbook*/
	
	
	.cookbook_bottom{
		background-image:url(../images/cookbook_bottom_bg.jpg);
	}
	
	.cookbookContainer_m{
		padding-bottom:25px;
	}
	
	.cookbookContainer_m .image{
		max-width:100%;
	}
	
	.cookbookContainer_m.cookbook .cookbookItem .title{
		max-width:90%;
	}
	.cookbookContainer_m.cookbook .titleContainer{
		height:70px;
	}
	
	.cookbookContainer_m.cookbookscs .title{
		max-width:90%;
		max-height:90%;
	}
	.cookbookContainer_m.cookbookscs .titleContainer{
		height:60px;
		text-align: center;
	}
	@media(min-width:768px){
		.cookbookItem:hover .cookbook-line-bg{
		    background-repeat: repeat-x;
			background-image:url(../images/cookbook_border.png)
		}	
		.cookbookItem .cookbook-line-bg{
			height:45px;
			display:inline-block;
		    background-repeat: repeat-x;
		    background-position: 0px 35px;
		}
	}
	
	
/*cookbook*/

/*index*/
	.mainpage{
		background-image:url(../images/index_p1_bg.jpg);
	    border-bottom: 15px solid #0e847a;
	    background-size: cover;
	}
	.mp_cookbook_part{
		background-image: url(../images/pagebg.jpg);
	}
	
	.mp_cookbook_partContainer{
		background-image:url(../images/pagebg.jpg);
		background-size: cover;
		padding-top:16px;
	    border-radius: 8px; 
	}
	
	.mp_cookbook_part .cookbook_container .titleImg{
		width:232px;
	}
	@media(min-width:768px){
		
		.mp_cookbook_part{
	       border-radius: 8px;
		   position:relative;
		   
		   padding:10px;
		}
		
		
		.mp_cookbook_part .cookbook_container{
			border:1px dashed  #0E847A
		}
		.mp_cookbook_part .cookbook_container .titleImg{
			margin-top:-37.5px;	
		    width: 100%;
		    text-align: center;
		}
	}
	
	.mp_cookbook_part .videoItem{
		padding:8px 8px 8px 8px;
		
	} 
	
	@media(min-width:768px){
		.mp_cookbook_part .videoItem{
			padding:2px 25px;
		}
	}
	
	.day_recipe{
		background-color:#FFF;
	}
	.day_recipe .title{
		color:#0E847A;
		font-weight:bold;
	}
	
	.top-container{
		padding-left:0px;
		padding-right:0px;
	}
	@media(min-width:768px){
		.top-container{
			padding-top:40px;
		}
	}
	
	.sliderContainer{
		vertical-align: top;
	}
	
	.mainSlider_carousel .owl-carousel {
		position: relative;
	}
	.mainSlider_carousel .owl-prev,
	.mainSlider_carousel .owl-next{
		position: absolute;
		top: 50%;
		margin-top: -10px; 
	}
	.mainSlider_carousel .owl-prev {
		left: 2.5%;
	}
	.mainSlider_carousel .owl-next {
		right: 2.5%;
	}
	.mainSlider_carousel .owl-controls{
		margin-top:-45px;
	}
	
	
	
	.mainSlider_carousel_mobile .owl-carousel {
	}
	.mainSlider_carousel_mobile .owl-prev,
	.mainSlider_carousel_mobile .owl-next{
		position: absolute;
		top: 50%;
		margin-top: -10px; 
	}
	.mainSlider_carousel_mobile .owl-prev {
		left: 2.5%;
	}
	.mainSlider_carousel_mobile .owl-next {
		right: 2.5%;
	}
	.mainSlider_carousel_mobile .owl-controls{
		margin-top:-45px;
	}
	
	
	
	
	
	
	.day_recipe.today .image{
	    width: 70%;
	}
	.weekdayContent{
		width: 30%;
		float: right;
	}
	.weekdayContent .weekday_title{
		width:80%;
		padding-top: 5px;
	}
	@media(min-width:360px){
		.weekdayContent .weekday_title{
			width:80%;
			padding-top: 16px;
		}
	}
	.weekdayContent .weekday_title img{
		max-width:100%;
	}
	
	.weekdayContent .title{
		font-size:12px;
		padding-left:4px;
		padding-top:5px;
		padding-right: 4px;
	}
	@media(min-width:360px){
		.weekdayContent .title{ 
			font-size:16px;
			padding-left:15px;
			padding-top:5px;
			padding-right: 15px;
		}
	}
	@media(min-width:768px){
		.weekdayContent .title{
			font-size:20px;
		}
	}
	
	.weekdayContent .product_thumb{
	    width: 80px;
	    padding:8px;
	    float: right;
	}
	@media(min-width:768px){
		.weekdayContent .product_thumb{
		    width: 80px;
		    padding:0px;
		    float: right;
		}
	}
	.day_recipe .product_thumb img{
		width:100%;
	}
	
	.recipeDownload>div{
		padding-bottom:20px;
	}
	
	@media(min-width:768px){
		.recipeDownload{
			padding-bottom:45px;
		}
	}
	.moreRecipeVideoBtn{ 
		padding:4px 12px;
		display:block;
		text-align:right;
	} 
	
	@media(min-width:768px){
		.moreRecipeVideoBtn{
			text-align:right;
			color:#0E847A;
			font-weight:bold;
			font-size:16px;
			padding:8px 12.5px;
			right:20px;
			display:block;
		}
	}
	
	
	
	
	.sliderContainer{
		width: 100%
	}
	
			
	
	
			
	.owl-theme .owl-controls .owl-dot {
	    display: inline-block;
	}
	.owl-controls .owl-dot span {
	    background: none repeat scroll 0 0 #CCC;
	    border-radius: 20px;
	    display: block;
	    margin: 5px 7px;
	    opacity: 0.99;
	    height: 25px;
	    width: 25px;
	}
	.owl-controls .owl-dot.active span,
	.owl-controls .owl-dot:hover span {
	    background: #0E847A;
	    border: 6px solid #f1dc00
	}
	
	
	@media(min-width:1200px){
		
		.day_recipe{
			height:300px;
		}
		.day_recipe.today .image img{
			height:300px;
		}
		.day_recipe.col-lg-3 {
		    width: 24%;
		}
	} 
	
	@media(min-width:768px){
		
		.sliderContainer.col-sm-8{
			width:65%;
		} 
	}

	@media(min-width:970px){
		.sliderContainer.col-md-8{
			width:65%;
		} 
	}
		
	@media(min-width:1200px){
		.sliderContainer.col-md-8{
			width:74%;
		} 
	}
	
	.weekly_more .day_recipe{
		margin-top:16px;
		display:none
	}
	
	.weekly_more .moreBtn, .weekly_more .hideBtn{
		width:100%;
		text-align:center;
		font-weight:18px;
		color:#0e847a;
		padding:12px;
		margin:15px 0px;
		font-weight:bold;
		border:1px solid #0e847a;
		clear: both;
	}
	
	.weekly_more .hideBtn{
		display:none;
		margin-top:10px;
	}
	
	
	@media(min-width:768px){			
		.mp_cookbook_part{
		}
		
		.moreRecipeVideoTitle{
			height:60px;
			top: 0px;
   			width: 100%;
		    display: block;
		}
		
		.day_recipe{
			vertical-align:top;
			
			position: relative;
		}
		.day_recipe.today .image{
			overflow:hidden
		}
	
		.day_recipe:not(.today) .image img{
			height:200px;
			width:100%;
		}
		
		.weekday_title{
			padding-top:12px;
			width:100%;
		}
		
		.day_recipe .title{
			font-size:20px;
			padding-left:15px;
			padding-top:10px;
			padding-right: 15px;
		}	
		
		.day_recipe .product_thumb{
		    position: absolute;
		    right: 10px;
		    bottom: 5px;
		    width:auto;
		    float:none;
		}
		.day_recipe .product_thumb img{
		    width: 90px;
   			height: 90px;
		}
		
		
		.day_recipe.col-md-6{
			width:48.5%;
		}
		.day_recipe.col-md-3{
			width:24%;
		}
		
		.weekdayContent{
			vertical-align:top;
		}
		.recipeDownload>div{
			padding-bottom:0px;
		}
		.recipeDownload .col-md-6{
			width:48.5%;
		}
		
		.mp_search{
			margin-top:10px;
			padding:4px 0px;
			font-size:15px;
		}
		
	}

	.mp_search{
		
		border:1px solid #FFF;
		text-align:center;
		color:#FFF;
		font-weight:bold;
	}
	.mainpage #searchRecipeTxt{
		border-top-right-radius: 0px;
    	border-bottom-right-radius: 0px;
		
	}
	
	.index-ingredient-item{
		width:50%;
		float:left;
		display:block;
	}
	
	@media(min-width:768px){
		
		.quickSearchBox{
			width:calc(100% / 4.1);
			max-width:260px;
		}
		
		.index-ingredient-item{
			width:calc(100% / 8.25);
			vertical-align:top;
			max-width: 130px;
			display:inline-block;
			float:none;
		}
		
		.index-ingredient-item img{
			width:100%;
		}
	}
	
	
	.mobile_ingredients_carousel .owl-stage-outer{
		z-index:100;
	}
	
	.mobile_ingredients_carousel .owl-controls{
		z-index:0;
		margin-top:0px;
	} 
	.mobile_ingredients_carousel{
		width:90%;
		margin:auto;
	}
	
	
	.mobile_ingredients_carousel .owl-nav,
	.mobile_ingredients_carousel .owl-controls{
		position:relative;
	}
	.mobile_ingredients_carousel .owl-carousel ,
	.mobile_ingredients_carousel{
		position: relative;
	} 
	.mobile_ingredients_carousel .owl-prev,
	.mobile_ingredients_carousel .owl-next{
		position: absolute;
		top: 50%;
		margin-top: -80px; 
	}
	.mobile_ingredients_carousel .owl-prev {
		left: -6.5%;
	}
	.mobile_ingredients_carousel .owl-next {
		right: -6.5%;
		
	}
	.mobile_ingredients_carousel .owl-controls{
	}
	
	.mobile_ingredients_carousel .sliderArrowBtns{
		height:auto; 
	} 
	 
	
	
	
	.mobile_ingredients_carousel .owl-item,
	.mobile_ingredients_carousel .item{
		
	}
	
	
/*index*/

	@media(min-width:768px){
			
		.boxDistribute{
			text-align:justify;
		}
		.boxDistribute>*{
			display:inline-block;
			float:none;
		}
		.boxDistribute:after {
		    content: '';
		    width: 100%;
		    display: inline-block;
		    font-size: 0;
		    line-height: 0
		}
	}
	
	.shadow{
		-webkit-box-shadow: 8px 8px 10px 0 #AAA;
		box-shadow: 8px 8px 10px 0 #AAA;
	}
	
	.hoverzoom{
		overflow:hidden;
	}
	.hoverzoom:hover img{
		-webkit-transition: all 0.5s ease; /* Safari and Chrome */
	    -moz-transition: all 0.5s ease; /* Firefox */
	    -o-transition: all 0.5s ease; /* IE 9 */
	    -ms-transition: all 0.5s ease; /* Opera */
	    transition: all 0.5s ease;
	    
		-webkit-transform:scale(1.15); /* Safari and Chrome */
	    -moz-transform:scale(1.15); /* Firefox */
	    -ms-transform:scale(1.15); /* IE 9 */
	    -o-transform:scale(1.15); /* Opera */
	     transform:scale(1.15);
	}
	
	.sliderArrowBtns{
		/*max-width:50px;
		max-height:50px;*/
	}
	
	@media(min-width:768px){
		.sliderArrowBtns{
			opacity:0.8;	
		}
		
		.sliderArrowBtns:hover{
			opacity:1;	
		}
	}
	
.detailEnd{
	text-align:center; 
}
.detailEnd img{
	width:50%;
}
@media(min-width:768px){
	.detailEnd img{
		width:auto;
	}
}


#shareToWhatsapp{
	width:100%;
}
#shareToWhatsapp .input{
	padding-bottom:20px;
}
#shareToWhatsapp .input .whatsappBtn{
	cursor:pointer;
}

#shareToWhatsapp .title{
	padding-bottom:20px;
	font-weight:bold;
	font-size:20px;
	color:#888;
}

#shareToWhatsapp .image{
	
}
#shareToWhatsapp .image img{
	width:100%;
}


.prod_title_small_tag{
	width:60px;
	margin-top:-75px;
}
@media(min-width:768px){
	.prod_title_small_tag{
		width:50px;
		margin-top:-75px;
	}
}

@media(min-width:992px){
	.prod_title_small_tag{
		width:60px;
		margin-top:-85px;
	}
}

<?php
	include_once 'inc_top_script.php';
?>
<!DOCTYPE html>
<html>
	<head>

		<?php
			include_once 'include_script.php';
		?>

		<title>調味雞汁鮮味炒食譜</title>
		<meta name="keywords" content="調味雞汁食譜,一包煮一餸,醃肉菜式,簡易食譜,家常菜式,史雲生食譜,食譜下載,精選食譜,新手食譜">
		<meta name="description" content="史雲生調味雞汁食譜，精選醃肉菜式。立即下載食譜，隨時隨地煮出滋味靚餸！">

		<style>
			.productInfo{
				display:none;
			}

			.fancybox-outer{
				background-image: url(images/cookbook/cookbook_popup_bg.jpg);
				/*padding:45px 90px;*/
				padding:100px 25px;
				background-size: cover;
				margin-top: 5%;
			}
			.fancybox-outer>*{
				padding:20px;
				background-color:#FFF;
				width:auto !important;
				height:auto !important;
			}

			.fancybox-opened{
				margin-bottom:200px;
			}
			.fancybox-wrap{
				width: 66% !important;
				left: 17% !important;
			}
			a.cookbookItem {
			    position: relative;
			    clear: both;
			}



			@media(min-width:768px){
				.cookbookItem >div>img{
					height:110px;
					margin-top:-25px;

				}

				.cookbookContainer>div:nth-child(2n){
					padding-left:180px;
				}
			}

			@media(min-width:768px){
      /*171016 new cookbook*/
			.cookbookItem-div:nth-child(odd){
				margin-left: 60px;

			}
			.cookbookItem-div:nth-child(even){
				margin-left: 40px;
			}
			.cookbookItem-div:last-child{
				margin-left: -20px;
			}
			.cookbookContainer>div:nth-child(even) .cookbook-line-bg{
				left: 210px;
			}
			.cookbookContainer>div:nth-child(odd) .cookbook-line-bg{
				left: 368px;
			}
			/*171016 new cookbook*/

				.cookbookItem .cookbook-line-bg{
					height:120px;
				    position: absolute;
				    clear: both;
				    top: 50px;
				}
				/*.cookbookContainer>div:nth-child(odd) .cookbook-line-bg{
					left: 500px;
				}*/

				/*.cookbookContainer>div:nth-child(even) .cookbook-line-bg{
					left: 320px;
				}*/

				.cookbookContainer>div:nth-child(1) .cookbook-line-bg{
					/*width:330px;*/
					width: 230px;
				}
				.cookbookContainer>div:nth-child(2) .cookbook-line-bg{
					/*width:460px;*/
					width: 400px;
				}
				.cookbookContainer>div:nth-child(3) .cookbook-line-bg{
					/*width:390px;*/
					width: 145px;
				}
				.cookbookContainer>div:nth-child(4) .cookbook-line-bg{
					/*width:400px;*/
					width: 380px;
				}

				.cookbookContainer>div:nth-child(5) .cookbook-line-bg{
					/*width:210px;*/
					width: 520px;
				}
				.cookbookContainer>div:nth-child(6) .cookbook-line-bg{
					/*width:480px;*/
					width: 400px;
				}
				.cookbookContainer>div:nth-child(7) .cookbook-line-bg{
					/*width:270px;*/
					width: 300px;
				}
				.cookbookContainer>div:nth-child(8) .cookbook-line-bg{
					/*width:310px;*/
					width: 190px;
				}

				.cookbookContainer>div:nth-child(9) .cookbook-line-bg{
					/*width:375px;*/
					width: 365px;
					top: 50px;
				}

				.cookbookContainer>div:nth-child(10) .cookbook-line-bg{
					/*width:445px;*/
					width: 500px;
					top:80px;
				}
				.cookbookContainer>div:nth-child(11) .cookbook-line-bg{
					/*width:205px;*/
					width: 360px;
				}
				.cookbookContainer>div:nth-child(12) .cookbook-line-bg{
					width:390px;
				}
				.cookbookContainer>div:nth-child(13) .cookbook-line-bg{
					/*width:385px;*/
					width: 395px;
				}
				.cookbookContainer>div:nth-child(14) .cookbook-line-bg{
					/*width:275px;*/
					width: 490px;
					top:62px;
				}

				.cookbookContainer>div:nth-child(15) .cookbook-line-bg{
					/*width:245px;*/
					width: 415px;
				}
				.cookbookContainer>div:nth-child(16) .cookbook-line-bg{
					/*width:275px;*/
					width: 430px;
					top:70px;
					left:265px;
				}
			}



			@media(min-width:768px){
				.cookbookContainer{
					padding:25px 0%;
				}
				.cookbookContainer .left, .cookbookContainer .right{
					width:20%;
				}
				.cookbookContainer .image{
					width:135px;
				}

				.cookbookContainer .right {
					text-align:right;
				}
				.cookbookContainer .left {
					text-align:left;
				}

				.cookbookContainer .title{
				    height: 28px;
		    		max-width: 100%;
				}

				.cookbookContainer a, .cookbookContainer a span{
					display:inline-block;
					margin-top:-25px;
				}
			}
			@media(min-width:992px){

				.cookbookContainer{
					padding:25px 40px;
				}
				.cookbookContainer .title{
				    height: 30px;
		    		max-width: 100%;
				}

				.cookbookContainer .left, .cookbookContainer .right{
					min-width:135px;
					width:20%;
				}
			}
			@media(min-width:1200px){

				.cookbookContainer{
					padding:25px 150px;
				}
				.cookbookContainer .title{
				    height: 35px;
		    		max-width: 100%;
				}

				.cookbookContainer .left, .cookbookContainer .right{
					min-width:135px;
					width:20%;
				}
			}
			/*171027*/
			.cookbookContainer_m.cookbookscs .title{
				width: 100%;
				height: auto;
				/*height: 100px;*/
				/*max-height: 200px;*/
			}
		.cookbookContainer_m .image{
			max-width: 90%;
		}


		</style>
		<meta property="og:title" content="史雲生食譜網站 "/>
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH?>cookbook-scs.php"/>
		<meta property="og:image" content="http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH."images/fb-share-banner-scs.jpg"?>"/>
		<meta property="og:site_name" content="<?=APP_TITLE?>"/>
		<meta property="og:description" content="史雲生食譜，教你用簡單食材煮出滋味靚餸。"/>

		<script>
			function sharePage(){
				var u = "http://<?=$_SERVER["HTTP_HOST"].ROOT_PATH?>";
				window.open("https://www.facebook.com/sharer/sharer.php?u="+u , "pop", "width=600, height=400, scrollbars=no");
			}
		</script>
	</head>

	<body>
		<?php
			include_once 'inc_beginbody_script.php';
		?>
		 <div id="wrapper" class="">
	        <?php
	        	include_once 'inc_sidebar.php';
	        ?>
			<div class="mainContainer "  id="page-content-wrapper" >
				<div style="position:relative;" class="page-cookbook-scs">
				<?php
					include_once 'inc_header.php';
				?>

				<div class="breadcrumb" style="background-color:#f1dc00;margin-bottom:0px;">
					<div class="container swansonBreadcrumb" >
						<img src="images/breadcrumb_home_icon.png"/>&nbsp;<a href="index.php">主頁</a>

						<span class="glyphicon glyphicon-menu-right " aria-hidden="true" ></span>
						 食譜下載
					</div>

				</div>

				<div class="row cookbook col-lg-12 col-md-12 col-sm-12 hidden-xs" style="    padding-bottom: 25px;">
					<a href="doc/調味雞汁鮮味炒食譜.pdf"  download = "調味雞汁鮮味炒食譜.pdf">
						<img src="images/Banner_ccs_1920x465_v3.jpg" style="width:100%;"/>
					</a>
				</div>

				<div class="row cookbook hidden-lg hidden-md hidden-sm col-xs-12" style="    padding-bottom: 25px;">
					<a href="doc/調味雞汁鮮味炒食譜.pdf"  download = "調味雞汁鮮味炒食譜.pdf">
						<img src="images/cook-books-list-banner-scs.jpg" style="width:100%;"/>
					</a>
				</div>


				<div class="container recipeDetail">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row lineContainer">
						<div class="line">
							<div><div class="border"></div></div>
						</div>
						<div class="titleImg">
							<img src="images/cookbook.png" alt="下載食譜"/>
						</div>
						<div class="line">
							<div><div class="border"></div></div>
						</div>
					</div>
				</div>


				<div class="row cookbookContainer_m cookbookscs hidden-lg hidden-md" style="text-align:center; ">

					<?php
						$i=1;
						while($i<=16){
					?>

					<a class=" col-sm-6 col-xs-12" href="images/cookbook_scs/css-recipe/ccs-recipes-mobile-<?=$i?>.jpg" target="_blank" >
						<div>
							<img class="image" src="images/cookbook_scs/cookbookscs_img_<?=$i?>_1710.jpg">
						</div>
						<div class="titleContainer">
							<img class="title" src="images/cookbook_scs/mobile-name/cookbookscs<?=$i?>.png">
						</div>
					</a>

					<?php
							$i++;
						}
					?>

				</div>



				<div class="container cookbookContainer hidden-xs hidden-sm" style=" ">

					<?php
						for($i = 1 ; $i <=16 ; $i++){
					?>
							<div class="cookbookItem-div" style="clear: both;width:100%">
								<a class="cookbookItem" href="images/cookbook_scs/ccs-recipe<?=$i?>.jpg" target="_blank" style="width:100%;">
									<img src="images/cookbook_scs/ccs_name<?=$i?>.png">
									<div class="cookbook-line-bg">
									</div>
								</a>
							</div>
					<?php
						}
					?>

				</div>



				<div class="row cookbook_bottom" style="text-align:center;padding:35px 0px;display:none">
					<div>
						<a href="product-list.php">
							<img src="images/cookbook_bottom_p1.png" />
						</a>
					</div>
					<div>
						<img src="images/cookbook_bottom_p2.png" />
					</div>
				</div>



			</div>
			<?php
				include_once 'inc_footer.php';
			?>
		</div>
	</body>
</html>
<?php
	include_once 'inc_footer_script.php';

?>
<script>

	$(function(){

		if( navigator.userAgent.match(/Android/i)
				 || navigator.userAgent.match(/webOS/i)
				 || navigator.userAgent.match(/iPhone/i)
				 || navigator.userAgent.match(/iPad/i)
				 || navigator.userAgent.match(/iPod/i)
				 || navigator.userAgent.match(/BlackBerry/i)
				 || navigator.userAgent.match(/Windows Phone/i)
		 ){
			//
	  	}else{
			$(".cookbookItem").fancybox({
				fitToView: false,
			    maxWidth: "90%",
			    margin:[20, 200,200,200],
				openEffect	: 'fade',
				closeEffect	: 'fade',
				padding:0
			});
		}


	});
</script>

<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
		 <div class="index_body">
			<div class="index_bodyLeft">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( is_front_page() ) { ?>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					<?php } else { ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php } ?>
					
					<div class="entry-content">
						<?php the_content(); ?>
						
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

<?php endwhile; ?>
			
			</div><!--indexBodyLeft--->						

			<?php get_sidebar('recipes'); ?>
		</div><!--index body-->
<?php get_footer(); ?>

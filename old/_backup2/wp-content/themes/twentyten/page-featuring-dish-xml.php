<?php
header ("content-type: text/xml");
if ( have_posts() ) { 
	while ( have_posts() ) { 
		the_post();
		$pid = trim(get_the_content());
	}
	$post = get_post($pid);
	$block = get_post_meta($pid, 'Dish Image (small)');		
	preg_match('/< *[img][^\>]*[src] *= *[\"\']{0,1}([^\"\'\ >]*)/i', $block[0], $img);		
	$strpos = strripos($img[1], "_");
	$image_src = substr($img[1],0,$strpos) . "_M.jpg";	
} 
?>
<?php if ( have_posts() ) : ?>
<results error="0">
	<dish id="<?php echo $pid ?>">	
		<title><![CDATA[<?php echo _e($post->post_title); ?>]]></title>
		<permalink><![CDATA[<?php the_permalink() ; ?>?utm_source=widget_feature&utm_medium=widget_feature_clickthru&utm_campaign=widget]]></permalink>
		<img><?php echo $image_src ?></img>
	</dish>
</results>
<?php else : ?>
<results error="1"></results>
<?php endif; ?>
			

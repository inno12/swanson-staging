<?php
header ("content-type: text/xml");
$queryCatArr = array();
$ingredientsArr = @$_REQUEST['ingredient'];	

if (!empty($ingredientsArr)){
	$ingredientsArr = explode(',', $ingredientsArr);
	$queryCatArr = array_merge ($ingredientsArr, $queryCatArr);
}
query_posts(array('category__in' => $queryCatArr,'nopaging'=>true));
 ?>
 <?php if ( have_posts() ) : ?>
 <results error="0">
	<dishes>
	<?php while ( have_posts() ) : the_post(); ?>
		<dish id="<?php the_ID(); ?>">
			<title><?php the_title(); ?></title>
			<permalink><![CDATA[<?php the_permalink(); ?>?utm_source=widget_result&utm_medium=widget_result_clickthru&utm_campaign=widget]]></permalink>			
		</dish>
	<?php endwhile; // End the loop. Whew. ?>
	</dishes>
</results>
<?php else : ?>
<results error="1"></results>
<?php endif; ?>
<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<div class="index_body">
		<div class="index_bodyLeft">

			<div id="post-0" class="post error404 not-found">
				<h1 class="entry-title"><?php _e( '<!--:en-->Not Found<!--:--><!--:zh--><!--:-->', 'twentyten' ); ?></h1>
				<div class="entry-content">
					<p><?php _e( '<!--:en-->Apologies, but the page you requested could not be found. Perhaps searching will help.<!--:--><!--:zh-->對不起！我們未能帶領你到目標頁面，可能你所輸入的網址並不正確，請重新輸入。<br/>或者請使用關鍵字搜索你的目標頁面。<!--:-->' ); ?></p>
					<?php //get_search_form(); ?>
				</div><!-- .entry-content -->
			</div><!-- #post-0 -->
			
		</div><!--indexBodyLeft--->						
			<?php get_sidebar('recipes'); ?>
		</div><!--index body-->
<?php get_footer(); ?>

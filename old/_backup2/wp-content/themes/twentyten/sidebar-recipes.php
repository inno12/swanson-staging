<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
<script type="text/javascript">
	function submitSearch(){		
		var url = "<?php echo "http://" .$_SERVER['HTTP_HOST']."/recipes/?lang=". qtrans_getLanguage() ?>";
		
		var ingredient = [];
		$.each($('.ingredient:checked'), function() {
			ingredient.push($(this).val()); 			 
		});
				
		var product = [];
		$.each($('.product:checked'), function() {
			product.push($(this).val()); 
		});
		
		var application = [];
		$.each($('.application:checked'), function() {
			application.push($(this).val()); 
		});
			
		$(location).attr('href',url+"&ingredient="+ingredient+"&product="+product+"&application="+application);		
		
		return true;
	}
	
	function changeFilterCb(classObj){
		var firstCb = classObj.parents('div.ui-accordion-content').find(':checkbox:eq(0)');		
		if  (!classObj.attr("checked")){			
			firstCb.attr('checked', false);
		}else {
			var childCb = firstCb.parent().siblings('div').find(':checkbox');					
			//console.log(firstCb);
			//console.log(childCb);
			$.each(childCb, function() {										
				firstCb.attr('checked', true);
				if  (!$(this).attr("checked")){
					firstCb.attr('checked', false);
					return false;					
				}				
			});
		}
	}
	
	function resetAccordionHeader(){
		$(".foodCurrent").removeClass("foodCurrent").addClass("foodInactive");		
		$(".soupCurrent").removeClass("soupCurrent").addClass("soupInactive");
		$(".dishCurrent").removeClass("dishCurrent").addClass("dishInactive");
	}
	

	$(function(){

		// Accordion
		//$("#accordion").accordion({ header: "h3" ,autoHeight: false,fillSpace: true});
		$("#accordion").accordion({ header: "h3"});				
		$("#accordion").accordion("option", "icons", false);
		
		// Set Accordion
		$("#foodDiv").click(function()		{		   		
			resetAccordionHeader();			
			$(this).addClass("foodCurrent").removeClass("foodInactive");
			
		});
		$("#soupDiv").click(function()		{		   		
			resetAccordionHeader();			
			$(this).addClass("soupCurrent").removeClass("soupInactive");
		});
		$("#dishDiv").click(function()		{		   		
			resetAccordionHeader();
			$(this).addClass("dishCurrent").removeClass("dishInactive");
		});
		
		// Select all
		$("#anyIngredient").click(function()		{		   
			$(this).parents('div:eq(1)').find(':checkbox').attr('checked', this.checked);			  
		});
		
		$("#anyProduct").click(function()		{		   
			$(this).parents('div:eq(1)').find(':checkbox').attr('checked', this.checked);			  
		});
		
		$("#anyApplication").click(function()		{		   
			$(this).parents('div:eq(1)').find(':checkbox').attr('checked', this.checked);			  
		});
		
		// Select each
		$(".ingredient").change(function(e,v){				
			changeFilterCb($(this));
		});

		$(".product").change(function(e,v){
			changeFilterCb($(this));
		});

		$(".application").change(function(e,v){
			changeFilterCb($(this));
		});
	 
	});
</script>
		<div class="index_bodyRight">			
        	<div id="accordion">
				<form role="search" method="get" action="" >																	
						<div>
							<h3 id="dishDiv" class="dishCurrent"><a href="">&nbsp;</a></h3>
							<div class="font12_brown dishListing">								
								<div class="listText">
									<input type="checkbox" name="anyApplication" id="anyApplication" value="">
									<label class="listPaddingLeft" for="anyApplication"><?php _e( '<!--:en-->Any<!--:--><!--:zh-->全選<!--:-->')?></label>
								</div>
								<?php 														
									$idObj = get_category_by_slug('application');
									$categories  =get_categories(array('parent'=>$idObj->term_id,'hide_empty'=>0)) ;							
									$i=0;
								?>
								<?php foreach($categories as  $category) :?>
								<div class="listText">
									<input type="checkbox" class="application" name="application<?php echo $i?>" id="application<?php echo $i?>" value="<?php echo $category->cat_ID?>">
									<label class="listPaddingLeft" for="application<?php echo $i?>"><?php echo $category->name?></label>
								</div>
								<?php $i++; ?>		
								<?php endforeach ?>								
							</div>
						</div>	
						<div>
							<h3 id="foodDiv" class="foodInactive"><a href="">&nbsp;</a></h3>
							<div class="font12_brown foodListing">								
								<div class="listText">
									<input  type="checkbox" name="anyIngredient" id="anyIngredient" value="">
									<label class="listPaddingLeft" for="anyIngredient"><?php _e( '<!--:en-->Any<!--:--><!--:zh-->任何材料<!--:-->')?></label>
								</div>
								<?php
									$idObj = get_category_by_slug('ingredient');
									$categories  =get_categories(array('parent'=>$idObj->term_id,'hide_empty'=>0)) ;
									$i=0;															
								?>
								<?php foreach($categories as  $category) :?>
								<div class="listText">
									<input type="checkbox" class="ingredient" name="ingredient<?php echo $i?>" id="ingredient<?php echo $i?>" value="<?php echo $category->cat_ID?>">
									<label class="listPaddingLeft" for="ingredient<?php echo $i?>"><?php echo $category->name?></label>
								</div>
								<?php $i++; ?>		
								<?php endforeach ?>							
							</div>
						</div>
						<div>
							<h3 id="soupDiv" class="soupInactive"><a href="">&nbsp;</a></h3>
							<div class="font12_brown soupListing">								
								<div class="listText">
									<input type="checkbox" name="anyProduct" id="anyProduct" value="">
									<label class="listPaddingLeft" for="anyProduct"><?php _e( '<!--:en-->Any<!--:--><!--:zh-->所有系列<!--:-->')?></label>
								</div>
								<?php 												
									$idObj = get_category_by_slug('product');
									$categories  =get_categories(array('parent'=>$idObj->term_id,'hide_empty'=>0)) ;
									$i=0;
								?>
								<?php foreach($categories as  $category) :?>									
								<div class="<?php if($i%2==0) {echo "listSoupL";} else { echo "listSoupR"; } ?>">									
									<input type="checkbox" class="product" name="product<?php echo $i?>" id="product<?php echo $i?>" value="<?php echo $category->cat_ID?>" />											
										<label for="product<?php echo $i?>">
											<img class="listSoupPhoto" align="texttop" src="/wp-content/uploads/<?php echo $category->slug; ?>.png" alt="<?php echo $category->name; ?>" />													
											<span class="listSoupText"><?php echo $category->name?></span>
										</label>											
								</div>
								<?php $i++; ?>		
								<?php endforeach ?>								
							</div>							
						</div>															
					<div class="accordionBtn"><a href="javascript:submitSearch();"><span class="nodisp"><?php _e( '<!--:en-->Find Recipes<!--:--><!--:zh-->搜尋食譜<!--:-->')?></span></a></div>							
				</form>			
			</div><!--accordion-->       
         	<div class="indexRightFB">
            	<div class="indexRightTitle"><div><?php _e( '<!--:en-->Support us<!--:--><!--:zh-->歡迎支持<!--:-->')?></div><div><img src="<?php bloginfo('template_directory')?>/images/campbellFB.gif" /></div></div>
             	<div class="indexRightFBLike">
					<iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.swansoncooking.com.hk&amp;layout=standard&amp;show_faces=false&amp;width=200&amp;action=like&amp;colorscheme=light&amp;height=50" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:200px; height:50px;" allowTransparency="true"></iframe>					
				</div>
            </div><!--fb-->
            
        </div><!--indexBodyRight-->

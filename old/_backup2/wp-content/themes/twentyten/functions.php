<?php
/**
 * TwentyTen functions and definitions
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, twentyten_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook. The hook can be removed by using remove_action() or
 * remove_filter() and you can attach your own function to the hook.
 *
 * We can remove the parent theme's hook only after it is attached, which means we need to
 * wait until setting up the child theme:
 *
 * <code>
 * add_action( 'after_setup_theme', 'my_child_theme_setup' );
 * function my_child_theme_setup() {
 *     // We are providing our own filter for excerpt_length (or using the unfiltered value)
 *     remove_filter( 'excerpt_length', 'twentyten_excerpt_length' );
 *     ...
 * }
 * </code>
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640;

/** Tell WordPress to run twentyten_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', 'twentyten_setup' );

if ( ! function_exists( 'twentyten_setup' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * To override twentyten_setup() in a child theme, add your own twentyten_setup to your child theme's
 * functions.php file.
 *
 * @uses add_theme_support() To add support for post thumbnails and automatic feed links.
 * @uses register_nav_menus() To add support for navigation menus.
 * @uses add_custom_background() To add support for a custom background.
 * @uses add_editor_style() To style the visual editor.
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_custom_image_header() To add support for a custom header.
 * @uses register_default_headers() To register the default custom header images provided with the theme.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_setup() {

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// Make theme available for translation
	// Translations can be filed in the /languages/ directory
	load_theme_textdomain( 'twentyten', TEMPLATEPATH . '/languages' );

	$locale = get_locale();
	$locale_file = TEMPLATEPATH . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'twentyten' ),
	) );

	// This theme allows users to set a custom background
	add_custom_background();

	// Your changeable header business starts here
	define( 'HEADER_TEXTCOLOR', '' );
	// No CSS, just IMG call. The %s is a placeholder for the theme template directory URI.
	define( 'HEADER_IMAGE', '%s/images/headers/path.jpg' );

	// The height and width of your custom header. You can hook into the theme's own filters to change these values.
	// Add a filter to twentyten_header_image_width and twentyten_header_image_height to change these values.
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'twentyten_header_image_width', 940 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'twentyten_header_image_height', 198 ) );

	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be 940 pixels wide by 198 pixels tall.
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

	// Don't support text inside the header image.
	define( 'NO_HEADER_TEXT', true );

	// Add a way for the custom header to be styled in the admin panel that controls
	// custom headers. See twentyten_admin_header_style(), below.
	add_custom_image_header( '', 'twentyten_admin_header_style' );

	// ... and thus ends the changeable header business.

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	register_default_headers( array(
		'berries' => array(
			'url' => '%s/images/headers/berries.jpg',
			'thumbnail_url' => '%s/images/headers/berries-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Berries', 'twentyten' )
		),
		'cherryblossom' => array(
			'url' => '%s/images/headers/cherryblossoms.jpg',
			'thumbnail_url' => '%s/images/headers/cherryblossoms-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Cherry Blossoms', 'twentyten' )
		),
		'concave' => array(
			'url' => '%s/images/headers/concave.jpg',
			'thumbnail_url' => '%s/images/headers/concave-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Concave', 'twentyten' )
		),
		'fern' => array(
			'url' => '%s/images/headers/fern.jpg',
			'thumbnail_url' => '%s/images/headers/fern-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Fern', 'twentyten' )
		),
		'forestfloor' => array(
			'url' => '%s/images/headers/forestfloor.jpg',
			'thumbnail_url' => '%s/images/headers/forestfloor-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Forest Floor', 'twentyten' )
		),
		'inkwell' => array(
			'url' => '%s/images/headers/inkwell.jpg',
			'thumbnail_url' => '%s/images/headers/inkwell-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Inkwell', 'twentyten' )
		),
		'path' => array(
			'url' => '%s/images/headers/path.jpg',
			'thumbnail_url' => '%s/images/headers/path-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Path', 'twentyten' )
		),
		'sunset' => array(
			'url' => '%s/images/headers/sunset.jpg',
			'thumbnail_url' => '%s/images/headers/sunset-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Sunset', 'twentyten' )
		)
	) );
}
endif;

if ( ! function_exists( 'twentyten_admin_header_style' ) ) :
/**
 * Styles the header image displayed on the Appearance > Header admin panel.
 *
 * Referenced via add_custom_image_header() in twentyten_setup().
 *
 * @since Twenty Ten 1.0
 */
function twentyten_admin_header_style() {
?>
<style type="text/css">
/* Shows the same border as on front end */
#headimg {
	border-bottom: 1px solid #000;
	border-top: 4px solid #000;
}
/* If NO_HEADER_TEXT is false, you would style the text with these selectors:
	#headimg #name { }
	#headimg #desc { }
*/
</style>
<?php
}
endif;

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 *
 * To override this in a child theme, remove the filter and optionally add
 * your own function tied to the wp_page_menu_args filter hook.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'twentyten_page_menu_args' );

/**
 * Sets the post excerpt length to 40 characters.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 *
 * @since Twenty Ten 1.0
 * @return int
 */
function twentyten_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'twentyten_excerpt_length' );

/**
 * Returns a "Continue Reading" link for excerpts
 *
 * @since Twenty Ten 1.0
 * @return string "Continue Reading" link
 */
function twentyten_continue_reading_link() {
	return ' <a href="'. get_permalink() . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyten' ) . '</a>';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and twentyten_continue_reading_link().
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 *
 * @since Twenty Ten 1.0
 * @return string An ellipsis
 */
function twentyten_auto_excerpt_more( $more ) {
	return ' &hellip;' . twentyten_continue_reading_link();
}
add_filter( 'excerpt_more', 'twentyten_auto_excerpt_more' );

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 *
 * @since Twenty Ten 1.0
 * @return string Excerpt with a pretty "Continue Reading" link
 */
function twentyten_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= twentyten_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'twentyten_custom_excerpt_more' );

/**
 * Remove inline styles printed when the gallery shortcode is used.
 *
 * Galleries are styled by the theme in Twenty Ten's style.css.
 *
 * @since Twenty Ten 1.0
 * @return string The gallery style filter, with the styles themselves removed.
 */
function twentyten_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
}
add_filter( 'gallery_style', 'twentyten_remove_gallery_css' );

if ( ! function_exists( 'twentyten_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own twentyten_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
		<div class="comment-author vcard">
			<?php echo get_avatar( $comment, 40 ); ?>
			<?php printf( __( '%s <span class="says">says:</span>', 'twentyten' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
		</div><!-- .comment-author .vcard -->
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em><?php _e( 'Your comment is awaiting moderation.', 'twentyten' ); ?></em>
			<br />
		<?php endif; ?>

		<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
				/* translators: 1: date, 2: time */
				printf( __( '%1$s at %2$s', 'twentyten' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'twentyten' ), ' ' );
			?>
		</div><!-- .comment-meta .commentmetadata -->

		<div class="comment-body"><?php comment_text(); ?></div>

		<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div><!-- .reply -->
	</div><!-- #comment-##  -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'twentyten' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __('(Edit)', 'twentyten'), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;

/**
 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
 *
 * To override twentyten_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @since Twenty Ten 1.0
 * @uses register_sidebar
 */
function twentyten_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Primary Widget Area', 'twentyten' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The primary widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => __( 'Secondary Widget Area', 'twentyten' ),
		'id' => 'secondary-widget-area',
		'description' => __( 'The secondary widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 3, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'First Footer Widget Area', 'twentyten' ),
		'id' => 'first-footer-widget-area',
		'description' => __( 'The first footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 4, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Second Footer Widget Area', 'twentyten' ),
		'id' => 'second-footer-widget-area',
		'description' => __( 'The second footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 5, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Third Footer Widget Area', 'twentyten' ),
		'id' => 'third-footer-widget-area',
		'description' => __( 'The third footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 6, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Fourth Footer Widget Area', 'twentyten' ),
		'id' => 'fourth-footer-widget-area',
		'description' => __( 'The fourth footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
/** Register sidebars by running twentyten_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'twentyten_widgets_init' );

/**
 * Removes the default styles that are packaged with the Recent Comments widget.
 *
 * To override this in a child theme, remove the filter and optionally add your own
 * function tied to the widgets_init action hook.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'twentyten_remove_recent_comments_style' );

if ( ! function_exists( 'twentyten_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post—date/time and author.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_posted_on() {
	printf( __( '<span class="%1$s">Posted on</span> %2$s <span class="meta-sep">by</span> %3$s', 'twentyten' ),
		'meta-prep meta-prep-author',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
			get_permalink(),
			esc_attr( get_the_time() ),
			get_the_date()
		),
		sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
			get_author_posts_url( get_the_author_meta( 'ID' ) ),
			sprintf( esc_attr__( 'View all posts by %s', 'twentyten' ), get_the_author() ),
			get_the_author()
		)
	);
}
endif;

if ( ! function_exists( 'twentyten_posted_in' ) ) :
/**
 * Prints HTML with meta information for the current post (category, tags and permalink).
 *
 * @since Twenty Ten 1.0
 */
function twentyten_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
		$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyten' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
		$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyten' );
	} else {
		$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyten' );
	}
	// Prints the string, replacing the placeholders.
	printf(
		$posted_in,
		get_the_category_list( ', ' ),
		$tag_list,
		get_permalink(),
		the_title_attribute( 'echo=0' )
	);
}
endif;

// Print Custom Field Template Values
//function printCustomField($theField) {
//	global $post;
//	$block = get_post_meta($post->ID, $theField);
//	if($block){
//		foreach(($block) as $blocks) {
//			echo $blocks;
//		}
//	}
//}

// Get Custom Field Template Values
function getCustomField($theField, $id=null, $shortCode=true) {
	global $post;
	$r = "";
	if ($id==null){
		$block = get_post_meta($post->ID, $theField);
	} else {
		$block = get_post_meta($id, $theField);
	}
	
	
	if($block){
		foreach(($block) as $blocks) {
			//echo $blocks;
			$r = $blocks;
		}
	}
	
	if ($shortCode){		
		$r = sanitize_text_field($r);
		$r = apply_filters('the_content', $r );
	}
	
	return $r;
}

function sayHello() {
	return "Hello, world!";
}
add_shortcode('hw', 'sayHello');

function renderDifficulty($level, $on, $off, $max=5) {
	$result = '';	 
	for ($i=0; $i<$max;$i++){
		if ($i>=$level){
			$result .= "<img src='/wp-content/themes/twentyten/images/$off' width='11' height='9' />";
		}else{
			$result .= "<img src='/wp-content/themes/twentyten/images/$on' width='11' height='9' />";			
		}
	}
	return $result;
}
function getProduct($post_id=null){
	
	if (empty($post_id)){
		global $post;
		$post_id = $post->ID;
	}

	foreach((get_the_category($post_id)) as $cat) {		
		$p_id = $cat->parent;	
		$parent_parent_slug = get_category($p_id)->slug;				
		if ($parent_parent_slug == "product") {				
			$parent_id = $cat->cat_ID;			
		} 
	}	
	foreach((get_the_category($post_id)) as $cat) {
		$p_id = $cat->parent;	
		if ($parent_id == $p_id) {
			$product = $cat;
		}
	}
	
	return $product;

}

function trimStr( $string, $length){
	if ( strlen($string)>$length)
		return substr($string,0,$length) . '...';			
	else 
		return $string;
}

function mbTrimStr( $string, $length){
/*	//no mb_string extension at prod server
	if ( mb_strlen($string)>$length)
		return mb_substr($string,0,$length) . '...';			
	else 
		return $string;
		*/
	preg_match('/(.{1,'.$length.'})/u', $string, $m);
	$trimStr = $m[0];
	if($trimStr!=$string){
		return $trimStr.'...';
	}else{
		return $string;
	}	
}

function renderDishes($attr) {
	global $q_config;

	$post_id = $attr['id'];
	$post = get_post( $post_id, 'ARRAY_A' );	
	$difficulty = getCustomField('Difficulty', $post_id, false);	
	$lead_time = getCustomField('Lead Time', $post_id, false);
	
	$post_url = get_bloginfo('url') .'/'.$post['post_name'].'/';
	$post_url = qtrans_convertURL($post_url, $q_config['language']);
	
	$product = getProduct($post_id);
	
	if ($attr['type'] == 'main'){
	
		$dish_image = getCustomField('Dish Image (large)', $post_id, false);	
		
		$output = '<div class="ContentTop">							
							<div id="mainContent">														
									<div class="mainKV">'.$dish_image.'</div><!--mainKV-->
									<a href="'.$post_url.'" class="mainKVLink" title="'.__($post['post_title']).'">
									<div class="mainCaption">
										<div class="mainCaption_title"><h2>'. __($post['post_title']).'</h2></div><!--title-->
										<div class="mainCaption_detail">';
						if ($difficulty != "NA") 
							$output .= '<div class="mainCaption_diff font12_white"><span class="diff_field1">'. __( '<!--:en-->Difficulty<!--:--><!--:zh-->難度<!--:-->') .'</span>'.renderDifficulty($difficulty, 'diff_5.png', 'diff_0.png').'</div>';
						if ($lead_time != "NA") 
							$output .='<div class="mainCaption_time font12_white"><span class="time_field1">'. __( '<!--:en-->Time<!--:--><!--:zh-->時間<!--:-->') .'</span><span>'.$lead_time. __( '<!--:en--> minutes<!--:--><!--:zh-->分鐘<!--:-->') .'</span></div>';
						$output .=	'</div><!--detail--></div><!--caption-->									
									<div class="mainSoup"><img src="/wp-content/uploads/'. $product->slug .'_L.png" alt="'. $product->name .'" title="'. $product->name .'"/></div><!--soup-->
									</a>
							</div><!--mainContent-->							
						</div><!--contentTop-->
						<div class="ContentBottom">';
	} else {
		$dish_image = getCustomField('Dish Image (small)', $post_id, false);	
		$output .= '<div class="smallpic">							
							<div class="smallpic_pic">'.$dish_image.'</div><!--pic-->
							<a href="'.$post_url.'" class="smallpicLink" title="'.__($post['post_title']).'" >
								<div class="smallpic_detail">
									<div class="smallpic_detail_c1">'.trimStr(__($post['post_title']), 37).'</div>
									<div class="smallpic_detail_c2">';
					if ($difficulty != "NA") 
						$output .= '<div class="smallpic_detail_c2_txt1">'.renderDifficulty($difficulty, 'smallpic_diff.gif', 'smallpic_diff_0.gif').'</div>';
					if ($lead_time != "NA") 
						$output .= '<div class="smallpic_detail_c2_txt2">'.$lead_time. __( '<!--:en--> minutes<!--:--><!--:zh-->分鐘<!--:-->') . '</div>';
					$output .= '</div><!--c2-->
								<div class="smallpic_detail_c3"></div>
								<div class="smallpic_soup"><img src="/wp-content/uploads/'. $product->slug .'_S.png" alt="'. $product->name .'" title="'. $product->name .'"/></div>
								</div><!--pic-->
							</a>
						</div><!--smallpic-->';
	}
	
	//$r = apply_filters('the_content', $r );
	return $output;
}
add_shortcode('dish', 'renderDishes');

/* start shortcode function for content */
function renderIngredient($attr,$content=null) {			
	$r = sanitize_text_field($content);		
	$output .= '<div class="ingredients"><div class="ingredientsTitle">'. __( '<!--:en-->Ingredients<!--:--><!--:zh-->材料<!--:-->') .'</div><div class="ingreBox">'.do_shortcode($r).'</div></div><!--method-->';			
	return $output;
}
add_shortcode('ingredient', 'renderIngredient');

function renderFood($attr, $content=null) {	
	$output .= '<div class="ingreRow"><span class="ingreName">'.$attr['name'].'</span><span class="ingreVol">'.$attr['vol'].'</span></div>';		
	return $output;
}
add_shortcode('food', 'renderFood'); 

function renderMethod($attr,$content=null) {			
	$r = sanitize_text_field($content);	
	$output .= '<div class="method"><div class="methodTitle">'. __( '<!--:en-->Steps<!--:--><!--:zh-->做法<!--:-->') .'</div><ul>'.do_shortcode($r).'</ul></div><!--method-->';		
	
	return $output;
}
add_shortcode('method', 'renderMethod');

function renderStep($attr, $content=null) {	
	$output .= '<li class="methodSteps"><span class="methodStepsNo">'.$attr['index'].'</span><span class="methodStepsTxt">'.$content.'</span></li>';		
	return $output;
}
add_shortcode('step', 'renderStep');
/* end shortcode function for content */


/* start shortcode function for blogger method*/
function renderBloggerAnchor($attr,$content=null) {	
	global $q_config;
	if ($q_config['language'] == 'en'){
		$title = trimStr($attr['title'], 22);
	}else{
		$title = $attr['title'];
		// no blogger for swanson
		/*if(empty($attr['blogger'])){
			$output .= '<div class="otherMethodBox">
								<div class="otherMethodPhoto"><a href="'. $attr['link'] .'"><img src="'. $attr['img'].'" title="'.$attr['title'].'"/></a></div>
								<div class="otherMethodText">								
									<a href="'. $attr['link'] .'" title="'.$attr['title'].'">'.$title.'</a></div>                        
							</div><!--methodBox-->';
		}else{
			$output .= '<div class="otherMethodBox">
								<div class="otherMethodPhoto"><a href="#blogger"><img src="'. $attr['img'].'" /></a></div>
								<div class="otherMethodText">
									<a href="#blogger">'.$title.'<br />
									<span class="otherMethodPersonName">'.trimStr($attr['blogger'],8).'</span><span class="otherMethodArrow"></span></a></div>                        
							</div><!--methodBox-->';
		}*/
		$output .= '<div class="otherMethodBox">
								<div class="otherMethodPhoto"><a href="'. $attr['link'] .'"><img src="'. $attr['img'].'" title="'.$attr['title'].'"/></a></div>
								<div class="otherMethodText">								
									<a href="'. $attr['link'] .'" title="'.$attr['title'].'">'.$title.'</a></div>                        
							</div><!--methodBox-->';
	}
	
	return $output;
}
add_shortcode('anchor', 'renderBloggerAnchor');

/* start shortcode function for related recipe method*/
function renderRelatedRecipe($attr,$content=null) {	
	global $q_config;
	if ($q_config['language'] == 'en')
		$title = trimStr($attr['title'], 22);
	else
		$title = mbTrimStr($attr['title'], 9);
		$link = $attr['link'];
		$link = (empty($link))?'#':$link;
		$output .= '<div class="otherMethodBox">
									<div class="otherMethodPhoto"><a href="'.$link.'"><img src="'. $attr['img'].'" title="'.$attr['title'].'"/></a></div>
									<div class="otherMethodText">		
										<a href="'.$link.'">'.$title.'<br />
										</a></div>                        
								</div><!--methodBox-->';
	return $output;
}
add_shortcode('relatedRecipe', 'renderRelatedRecipe');

function renderBloggerMethod($attr,$content=null) {			
	$r = sanitize_text_field($content);		
	$output .= '<div class="bloggerMethodDetail">'.do_shortcode($r).'</div><!--method detail-->';		
	return $output;
}
add_shortcode('other', 'renderBloggerMethod');

function renderPhotoArea($attr,$content=null) {		
	$add_class = '';
	if(empty($attr['desc'])){ $add_class.=' no_bloggerMethodTips'; }
	
	if($attr['size']=='small'){ $add_class.=' photo_size_small'; }
	
	# allow absolute url
	$img = get_bloginfo('template_directory') .'/'. $attr['img'];
	if(strpos($attr['img'],'http://') !== false){ $img = $attr['img']; }
	if(strpos($attr['img'],'/wp-content/uploads/') === 0){ $img = $attr['img']; }
	
	global $q_config; 
	$trimTitle = $attr['title'];
	if($q_config['language'] != 'en'){ $trimTitle = mbTrimStr($attr['title'], 9); }
	
	$output .= '<div class="bloggerMethodPhotoArea'.$add_class.'">												
						<div class="bloggerMethodPhoto"><img src="'.$img.'" alt="'.$attr['title'].'" /></div>
						<div class="bloggerMethodTitle">'.$trimTitle.'</div>	
						<div class="printableBloggerMethodTitle">'.$attr['title'].'</div>
						<div class="bloggerMethodTips">'.$attr['desc'].'</div>
					</div><!--photoArea-->';
	return $output;
}
add_shortcode('photo_area', 'renderPhotoArea');

function renderOtherIngredient($attr,$content=null) {			
	$output .= '<div class="bloggerMethodIngre"><div class="bloggerMethodIngreTitle">'. __( '<!--:en-->Ingredients<!--:--><!--:zh-->材料<!--:-->') .'</div><div class="bloggerMethodIngreDetail">'.do_shortcode($content).'</div></div><!--ingre-->';			
	return $output;
}
add_shortcode('other_ingredient', 'renderOtherIngredient');

function renderOtherFoodName($attr, $content=null) {	
	$output .= '<div class="bloggerMethodIngreLine">'.$content.'</div>';		
	return $output;
}
add_shortcode('other_name', 'renderOtherFoodName');

function renderOtherMethod($attr,$content=null) {			
	$output .= '<div class="bloggerMethodMethod"><div class="bloggerMethodMethodTitle">'. __( '<!--:en-->Steps<!--:--><!--:zh-->做法<!--:-->') .'</div>'.do_shortcode($content).'</div><!--method method-->';			
	return $output;
}
add_shortcode('other_method', 'renderOtherMethod');

function renderOtherStep($attr, $content=null) {		
	$output .= '<div class="bloggerMethodMethodSteps"><span class="bloggerMethodMethodNo">'.$attr['index'].'</span><span class="bloggerMethodMehtodTxt">'.$content.'</span></div>';		
	return $output;
}
add_shortcode('other_step', 'renderOtherStep');

function renderLink($attr, $content=null) {		
	$output .= '<div class="bloggerMethodMoreLink">
						<div class="bloggerMethodMore">'. __( '<!--:en-->Related Recipe<!--:--><!--:zh-->相關食譜<!--:-->') .'</div>
						<div class="bloggerMethodLink"><a href="'.$content.'">'.substr($content,0,40) .' ' .substr($content,40,66) .' '.substr($content,66,99). '</a></div>
					</div><!--more-->';		
	return $output;
}
add_shortcode('link', 'renderLink');

function renderLink2($attr, $content=null) {		
	$output .= '<div class="bloggerMethodMoreLink">
						<div class="bloggerMethodMore">'. __( '<!--:en-->&nbsp;<!--:--><!--:zh-->&nbsp;<!--:-->') .'</div>
						<div class="bloggerMethodLink"><a href="'.$content.'" target="_blank">'.$content. '</a></div>
					</div><!--more-->';		
	return $output;
}
add_shortcode('link2', 'renderLink2');

function render_related_recipe_link($attr, $content=null) {	
	$output .= '<div class="bloggerMethodMoreLink">
						<div class="bloggerMethodMore">'. __( '<!--:en-->Related recipes<!--:--><!--:zh-->相關食譜<!--:-->') .'</div>
						<div class="bloggerMethodLink"><a href="'.$content.'" target="_blank">'.$content. '</a></div>
					</div><!--more-->';							
	return $output;
}
add_shortcode('related_recipe_link', 'render_related_recipe_link');

function render_related_recipe_link2($attr, $content=null) {		
	$output .= '<div class="bloggerMethodMoreLink">
						<div class="bloggerMethodMore">'. __( '<!--:en-->&nbsp;<!--:--><!--:zh-->&nbsp;<!--:-->') .'</div>
						<div class="bloggerMethodLink"><a href="'.$content.'" target="_blank">'.$content. '</a></div>
					</div><!--more-->';		
	return $output;
}
add_shortcode('related_recipe_link2', 'render_related_recipe_link2');

function render_blogger_link($attr, $content=null) {	
	$output .= '<div class="bloggerMethodMoreLink">
						<div class="bloggerMethodMore">'. __( '<!--:en-->Related recipes<!--:--><!--:zh-->食譜作者<!--:-->') .'</div>
						<div class="bloggerMethodLink"><a href="'.$content.'" target="_blank">'.$content. '</a></div>
					</div><!--more-->';							
	return $output;
}
add_shortcode('blogger_link', 'render_blogger_link');

function render_blogger_link2($attr, $content=null) {		
	$output .= '<div class="bloggerMethodMoreLink">
						<div class="bloggerMethodMore">'. __( '<!--:en-->&nbsp;<!--:--><!--:zh-->&nbsp;<!--:-->') .'</div>
						<div class="bloggerMethodLink"><a href="'.$content.'" target="_blank">'.$content. '</a></div>
					</div><!--more-->';		
	return $output;
}
add_shortcode('blogger_link2', 'render_blogger_link2');

/* shortcode function for blogger method */

function renderBloggerDish($attr, $content=null) {			
	$output .= '<div class="smallpic">							
						<div class="smallpic_pic"><img src="'.$attr['img'].'" alt="'.$attr['title'].'" width="320" height="200" /></div><!--pic-->
						<a href="'.$attr['url'].'" class="smallpicLink" title="'.$attr['title'].'" >
							<div class="smallpic_detail">
								<div class="smallpic_detail_c1">'.trimStr(__($attr['title']), 37).'</div>';				
			$output .= '	<div class="smallpic_soup"><img src="/wp-content/uploads/'. $attr['src'] .'_S.png" alt="'. $attr['soup'] .'" title="'. $attr['soup'] .'" width="45" height="70"/></div>
							</div><!--pic-->
						</a>
					</div><!--smallpic-->';
	return $output;
}
add_shortcode('blogger_dish', 'renderBloggerDish');

/* shortcode function for blogger method */

function getAttribute($attrib, $tag){
  //get attribute from html tag
  $re = '/'.$attrib.'=["\']?([^"\' ]*)["\' ]/is';
  preg_match($re, $tag, $match);
  if($match){
	return urldecode($match[1]);
  }else {
	return false;
  }
}
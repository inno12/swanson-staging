<?php
/**
 * The loop that displays posts.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop.php or
 * loop-template.php, where 'template' is the loop context
 * requested by a template. For example, loop-index.php would
 * be used if it exists and we ask for the loop with:
 * <code>get_template_part( 'loop', 'index' );</code>
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<?php /* If there are no posts to display, such as an empty archive page */ ?>
<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post error404 not-found">
		<h1 class="entry-title"><?php _e( 'Not Found', 'twentyten' ); ?></h1>
		<div class="entry-content">
			<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyten' ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-0 -->
<?php endif; ?>

<?php
	/* Start the Loop.
	 *
	 * In Twenty Ten we use the same loop in multiple contexts.
	 * It is broken into three main parts: when we're displaying
	 * posts that are in the gallery category, when we're displaying
	 * posts in the asides category, and finally all other posts.
	 *
	 * Additionally, we sometimes check for whether we are on an
	 * archive page, a search page, etc., allowing for small differences
	 * in the loop on each template without actually duplicating
	 * the rest of the loop that is shared.
	 *
	 * Without further ado, the loop:
	 */ ?>
<?php while ( have_posts() ) : the_post(); ?>
	<?php if ($post->post_type != "page") : ?>		
		<? 
			$dish_image = getCustomField('Dish Image (small)', null, false);				
			$difficulty = getCustomField('Difficulty', null, false);
			$lead_time = getCustomField('Lead Time', null, false);		
			$custom_template = getCustomField('Custom template', null, false);			
			$product = getProduct();	

			
			global $q_config;
			if($q_config['language']=='en'){
				$post_title = mbTrimStr(__($post->post_title), 20);
			}else{
				$post_title = mbTrimStr(__($post->post_title), 9);
			}
			
			$add_class = '';
			if(strpos($custom_template, 'small')!==false){
				$add_class = ' small_template';
			}
			
		?>		
		<div class="smallpic">
			<div class="smallpic_pic<?php echo $add_class;?>"><?php echo $dish_image?></div><!--pic-->
			<a href="<?php the_permalink(); ?>" class="smallpicLink" title="<?php the_title(); ?>">
			<div class="smallpic_detail">
				<div class="smallpic_detail_c1"><?=$post_title?></div>
				<div class="smallpic_detail_c2">
				<?if ($difficulty != "NA") :?>
					<div class="smallpic_detail_c2_txt1">
						<?php echo renderDifficulty($difficulty, 'smallpic_diff.gif', 'smallpic_diff_0.gif')?>
					</div>
				<?endif?>
				<?if ($lead_time != "NA") :?>
					<div class="smallpic_detail_c2_txt2"><?php echo $lead_time?><?php _e( '<!--:en--> minutes<!--:--><!--:zh-->分鐘<!--:-->')?></div>
				<?endif?>
				</div><!--c2-->
			<div class="smallpic_detail_c3"></div>
			<div class="smallpic_soup"><img src="/wp-content/uploads/<?php echo $product->slug;?>_S.png" alt="<?php echo $product->name; ?>" title="<?php echo $product->name; ?>" width="45" height="70" /></div>
			</div><!--pic--></a>
		</div><!--smallpic-->			
	<?php endif; // This was the if statement that broke the loop into three parts based on categories. ?>

<?php endwhile; // End the loop. Whew. ?>



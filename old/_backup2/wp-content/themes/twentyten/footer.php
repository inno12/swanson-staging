	
	<div id="footer">
    	<div class="footerLink"><a href="<?php echo qtrans_convertURL(get_bloginfo('url').'/tc-and-privacy-policies/', $q_config['language']);?>"><?php echo _e('<!--:en-->Terms &amp; Conditions and Privacy Policies<!--:--><!--:zh-->條款、細則及私隱政策<!--:-->') ?></a><span style="padding:0px 10px 0px 10px;">|</span><a href="mailto:HK_CS@campbellsoup.com"><?php echo _e('<!--:en-->Contact Us<!--:--><!--:zh-->聯絡我們<!--:-->') ?></a> </div>
        <div class="footerTxt">&reg; Registered trade mark of Campbell Soup Company    &copy; 2012 Campbell Soup Asia Limited</div>
    </div><!--footer-->

</div><!--wrapper-->
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
<script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-57656614-5');ga('send','pageview');
</script>
</body>
</html>
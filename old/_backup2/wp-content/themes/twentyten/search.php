<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div class="index_body">
			<div class="index_bodyLeft">			
<?php if ( have_posts() ) : ?>
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyten' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
				<?php
				/* Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called loop-search.php and that will be used instead.
				 */
				 get_template_part( 'loop', 'recipes' );
				?>
				<div class="resultPaging">
					<?php get_pagination() ?>
				</div><!--resultPaging-->
<?php else : ?>
				<div id="post-0" class="post no-results not-found">
					<h2 class="entry-title"><?php _e( '<!--:en-->Nothing Found<!--:--><!--:zh--><!--:-->' ); ?></h2>
					<div class="entry-content">
						<p><?php _e( '<!--:en-->Sorry, but nothing matched your search criteria. Please try again with some different keywords.<!--:--><!--:zh-->對不起！我們未能找到符合搜索條件之結果，請以其他關鍵字詞重新搜索。<!--:-->' ); ?>
						<?php //get_search_form(); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-0 -->
<?php endif; ?>
			</div><!--indexBodyLeft--->						

			<?php get_sidebar('recipes'); ?>
		</div><!--index body-->
		
<?php get_footer(); ?>

(function($) {
	$.fn.ellipsis = function(enableUpdating){
		var s = document.documentElement.style;
		//if (!('textOverflow' in s || 'OTextOverflow' in s)) {
			var isEllipsis = false;
			this.each(function(){
				var el = $(this);
				if(el.css("overflow") == "hidden"){
									
					var originalText = el.html();
					var w = el.width();
					
					
					var t = $(this.cloneNode(true)).hide().css({
                        'position': 'absolute',
                        'width': 'auto',
                        'overflow': 'visible',
                        'max-width': '100%'
                    });
					
					el.after(t);					
					var text = originalText;
					
					while(text.length > 0 && t.width() > el.width()){
						text = text.substr(0, text.length - 1);
						t.html(text + "...");
						isEllipsis = true;
					}
					el.html(t.html());
					
					t.remove();
					
					if(enableUpdating == true){
						var oldW = el.width();
						setInterval(function(){
							if(el.width() != oldW){
								oldW = el.width();
								el.html(originalText);
								el.ellipsis();
							}
						}, 200);
					}
				}				
			});					
			
			return isEllipsis;
	//	} else {
			
	//		return false;
	//	}
	};
})(jQuery);
var flashObj;

//some common functions
function pageOnLoad() {
	getFlashObj();
}

function getFlashObj() {
    if (navigator.appName.indexOf("Microsoft") != -1) {
        flashObj = window["home"];
    }
    else {
        flashObj = document["home"];
    }
}

function openUrl(url){
	window.open(url);
}

function openWinAndTrack(url,trackname,winOptions){
	ecTrack(trackname);
	if(winOptions){
		window.open(url,"newWin",winOptions);
	}else{
		window.open(url);
	}
}

/*
*	Get GET parameters on the url
*/
function queryString(key) {
	hu = window.location.search.substring(1);
	gy = hu.split("&");
	for (i=0;i<gy.length;i++) {
		ft = gy[i].split("=");
		if (ft[0] == key) {
			return ft[1];
		}
	}
}

function ecTrack(code){
	// use the following for google analytics	
	_gaq.push(['_trackPageview', code]);
}
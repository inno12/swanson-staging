<!DOCTYPE html>
<?php global $page, $paged, $q_config, $wp_query; 
	if ($q_config['language'] == 'en' && $_SERVER['HTTP_HOST'] != 'swanson.e-crusade.com' && !current_user_can('edit_posts')){
		header( 'Location: '. qtrans_convertURL($url, 'zh') ) ;
	}
	
	$block = get_post_meta($post->ID, 'Dish Image (small)');			
	preg_match('/< *[img][^\>]*[src] *= *[\"\']{0,1}([^\"\'\ >]*)/i', $block[0], $img);	
	$image_src = $img[1];	
	
	$page = get_page_by_path("latest-recipes");	
	$campaignEnable = get_post_meta($page->ID, 'Campaign Enable', true);	
	
	if ($post->post_name == "latest-recipes" && $campaignEnable =="1")
		$campaignStartAt = get_post_meta($page->ID, 'Campaign Start At', true);
?>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="title" content="<?php echo $post->post_title; ?>"/>
<link rel="image_src" href="<?php echo $image_src ?>"/>
<link rel="shortcut icon" href="<?php bloginfo( 'template_directory' ); ?>/swanson.ico" />
<meta name="description" content="<?php _e( '<!--:en-->With 5 variants of Swanson Broth, you can have enormous cooking ideas! Please visit our site and see how to cook in a nice and easy way!<!--:--><!--:zh-->史雲生五大上湯，煮乜都主意多多。即刻上黎睇吓點樣煮得輕鬆又好味。<!--:-->')?>">
<meta name="keywords" content="金寶, 金寶湯, 湯, 金寶 香港, 金寶湯 香港, 史雲生, 史雲生湯, 史雲生湯底, 史雲生上湯, 史雲生香港, 史雲生 食譜, 上湯, 湯底, 食譜, 簡單食譜, 簡易食譜, 簡便食譜, 食乜餸, 今晚食乜餸, 今晚食乜餸連菜譜, 煮乜餸, 今日煮乜餸, 今晚煮乜餸, 網上食譜, 食譜網, 煮食, 烹飪, 簡易煮食, 牛, 豬, 家禽, 其他肉類, 魚, 蝦, 蟹, 海味, 其他海鮮, 蔬菜, 豆腐, 蛋, 菇類, 250ml 方便裝:一盒煮一餸, 家常小菜, 節慶食品, 飯, 粥, 粉麵, 羹, 小食, 火鍋, 清雞湯, 豬骨湯, 鮮魚湯, 瑤柱上湯, 金華火腿上湯, 馬來喇沙湯米線, 京都炸醬拉面 , 四川担担麵, 上海酸辣湯麵, 冬菜魚滑湯粉絲, 魚腐米線, 獅子狗卷蟹柳蛋米粉, 木瓜雪耳肉片湯, 蘋果排骨湯, 越南雞絲湯河, 三絲竹笙浸玉子豆腐, 糯米飯, 沙鍋雲吞雞, 蟹肉蛋白浸翡翠苗, 龍鳳豆腐羹, 瑤柱魚肚羹, 上湯蒸蛋, 冬瓜粒湯泡飯, 雞湯油飯配浸雞腿, 蒜子浸莧菜, campbell, campbells, soup, campbell hk, campbells hk, campbell hong kong, campbells hong kong, Swanson, Swanson broth, Swansonbroth, Swanson hk, Swanson hong kong, Swanson recipe, Swanson cooking, Swansoncooking, broth, recipe, simple recipes; easy cooking recipes, quick and easy recipes, quick cooking recipes, easy dinner, easy meal, online recipe; online recipes, recipes, cook, cooking, easy cooking, Beef, Pork, Poultry, Other Meats, Fish, Shrimp, Crab, Dried Seafood, Other Seafood, Vegetable, Tofu, Egg, Mushrooms, 250ml, Dishes, Seasonal dishes, rice, porridge, noodles, thick soup, side dishes, hot pot, chicken broth, pork broth, fish broth, scallop broth, ham broth, Malaysian Laksa, Ramen with Spicy Meat Sauce, Dan Dan Noodles, Shanghai Sour and Spicy Noodles , Preserved Cabbage and Fish Paste Soup with Vermicelli, Rice Noodles with Fish Puff, Japanese Style Vermicelli, Papaya, Snow Fungus and Pork Soup, Spare Ribs and Apple Soup, Vietnamese Style Chicken Rice Noodles, Egg Tofu with Bamboo Fungus in Ham Broth, Glutinous Rice with Four Treasures, Wonton Chicken in Casserole, Green House Bean Sprouts with Crab Meat and Egg White in Broth, Tofu with diced Prawns and Chicken Soup, Dried Scallop & Dried Fish Maw Soup, Steamed Egg Custard with Chicken Broth, Winter Melon with Rice in Chicken Broth, Flavoured Rice with Chicken Broth & Braised Chicken Leg in Broth , Vegetable in Chicken Broth">
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed. 
	 */	

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
	
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory'); ?>/css/smoothness/jquery-ui-1.8.4.custom.css" />
<? if ($q_config['language'] == 'en') :?>
	<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo( 'template_directory' ); ?>/css/en/Style.css" />	
<? else:?>
	<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo( 'template_directory' ); ?>/css/Style.css" />	
<? endif;?>	
<link rel="stylesheet" type="text/css" media="print" href="<?php bloginfo( 'template_directory' ); ?>/css/Print.css" />	

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-ui-1.8.4.custom.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.text-overflow.js"></script>
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	//if ( is_singular() && get_option( 'thread_comments' ) )
		//wp_enqueue_script( 'comment-reply' );

		
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19162634-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();  
</script>
<!--[if IE 6]>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_directory' ); ?>/css/ie6hack.css" />	
<style type="text/css">
img, div { behavior: url(/wp-content/themes/twentyten/images/iepngfix.htc) }
<? if ($q_config['language'] == 'en') :?>
.smallpic_detail_c1{width:expression( this.clientWidth > 220 ? 220 : "auto")}
.resultPrev{width:70px;}
.resultPageNo{width:520px;}
.smallpic_detail_c2_txt2{width:65px;}
.mainCaption{
	background-image:url("/wp-content/themes/twentyten/images/en/mainCaptionbg_ie6.png");
	background-position:left bottom;
	background-repeat:no-repeat;
	height:115px;
	overflow:hidden;
	padding:365px 0 0 135px;
	position:absolute;
	width:555px;
}
.mainCaption_detail{
	padding-top:0px;
}
.recipeCaptionBg{
	position:absolute;
	width:692px;
	height:180px;
	overflow:hidden;
	padding:350px 0px 0px 0px;
}
.recipeCaption{
	position:absolute;
	width:555px;
	height:180px;	
	overflow:hidden;
	padding:350px 0px 0px 135px;	
	bbackground-image:url("/wp-content/themes/twentyten/images/en/recipes/recipeCaptionbg_ie6.gif");
	bbackground-position:bottom left;
	bbackground-repeat:no-repeat;	
}
.recipeCaption_title{
	padding-top:5px;
}
<?endif?>
</style> 
<![endif]-->

</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({appId: '172498922775114', status: true, cookie: true,
             xfbml: true});
  };
  (function() {
    var e = document.createElement('script'); e.async = true;
    e.src = document.location.protocol +
      '//connect.facebook.net/en_US/all.js';
    document.getElementById('fb-root').appendChild(e);
  }());
</script>
<?php if ($campaignEnable == '1') :?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/common.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/swfobject/swfobject.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/swfaddress/swfaddress.js"></script>
<script type="text/javascript">
 function expandBannerAd(ContainerDivID) {
	var docHeight = $(document).height();
   $("body").append("<div id='overlay'></div>");
   $("#overlay")
	  .height(docHeight)
	  .css({
		 'opacity' : 0.9,
		 'position': 'absolute',
		 'top': 0,
		 'left': 0,
		 'background-color': 'white',
		 'width': '100%',
		 'z-index': 2000
	  });	   
   $('#ContainerDivID').css('height','650px').css('width','1100px');   
   document.getElementById(ContainerDivID).className = "alignCenter flashBannerAd";
   
   // for ff 3.0 and 3.5
   if ($.browser.mozilla && ($.browser.version.indexOf("1.9.1")==0 || $.browser.version.indexOf("1.9.0")==0 )) {
		window.scrollTo(0,400);
	}
 }
 function collapseBannerAd(ContainerDivID) {	
	$("#overlay").remove();
	$('#ContainerDivID').css('height','303px').css('width','196px');   
	document.getElementById(ContainerDivID).className = "flashBannerAd";
	
	// for ff 3.0 and 3.5
   if ($.browser.mozilla && ($.browser.version.indexOf("1.9.1")==0 || $.browser.version.indexOf("1.9.0")==0 )) {
		window.scrollTo(0,0);
	}
 }
</script>
<div class="flashBannerAd" id="ContainerDivID">
	<div id="flashContent">
	</div>
</div>
<script type="text/javascript">	
	var debug = queryString("debug") ? queryString("debug") : false;	
	var startAt = queryString("startAt") ? queryString("startAt") : "<?php echo $campaignStartAt ?>";
	var flashvars = {debug:debug, startAt:startAt};
	var params = {wmode:"transparent",allowScriptAccess:"always"};
	var attributes = {};
	swfobject.embedSWF("<?php bloginfo('template_directory'); ?>/swf/home.swf", "flashContent", "1100", "650", "9.0.124","swf/expressinstall.swf", flashvars, params, attributes);		
</script>
<?php endif;?>
<div id="Wrapper">
	<div id="Header">
    	<div id="topNav">
     		 <div id="leftNav">
            	 <div id="mainmenu">
                    <ul id="menu">
                        <li id="recipes" class="main"><a href="<?php echo qtrans_convertURL(get_bloginfo('url'), $q_config['language']);?>/" class="menu_link current"><span class="nodisp"><h3><?php _e( '<!--:en-->Latest Recipes<!--:--><!--:zh-->最新食譜<!--:-->')?></h3></span></a></li>                        
                        <li id="aboutSwanson" class="main"><a href="<?php _e( '<!--:en-->http://www.campbellsoup.com.hk/corporate/en/our-brands/swanson/<!--:--><!--:zh-->http://www.campbellsoup.com.hk/corporate/zh/our-brands/swanson/<!--:-->')?>" target="_blank" class="menu_link"><span class="nodisp"><h3><?php _e( '<!--:en-->About Swanson<!--:--><!--:zh-->史雲生概覽<!--:-->')?></h3></span></a></li>
                    </ul><!--menu-->
                    </div><!--mainmenu-->
              	</div><!--leftNav-->
      <div id="rightNav" >
            	<div class="rightLinks" style="float:right;">
				<? if ($q_config['language'] == 'en') :?>					
					<a class="whiteLink" href="http://www.campbellskitchen.com.hk/en/" target="_blank">Campbell's Kitchen</a>
					<a class="whiteLink" href="http://www.campbellsoup.com.hk/corporate/en/" target="_blank">Campbell’s Hong Kong</a>
					<a href="<?php echo qtrans_convertURL($url, 'zh');?>" class="whiteLink">中文</a>
				<? else:?>					
					<a class="whiteLink" href="http://www.campbellskitchen.com.hk/zh/" target="_blank">金寶湯食譜網站</a>
					<a class="whiteLink" href="http://www.campbellsoup.com.hk/corporate/zh/" target="_blank">金寶湯公司網站</a>
					<!--<a href="<?php echo qtrans_convertURL($url, 'en');?>" class="whiteLink">ENG</a>-->
				<? endif;?>					                
                </div>
            </div><!--right-->
        </div><!--topNav-->
        
        <div id="logoSearch">
	        <div id="index"><a class="logo" href="<?php echo qtrans_convertURL(get_bloginfo('url'), $q_config['language']);?>/"><h1>Swanson's Cooking</h1></a></div><!--logo-->
			<div id="printableLogo"><img src="/wp-content/themes/twentyten/images/<?=($q_config['language'] == 'en')?"en/":""?>logo.jpg" alt="Swanson's Cooking" /></div><!--logo-->
	        <div id="search">
				<?php get_search_form(); ?>				
	        </div><!--search--> 	
        </div><!--logoSearch-->
    </div><!--Header-->
	<!--[if IE 6]>
	<div id="BrowserMessage" class="indexRightWidgetTxt">
		<?php echo _e('<!--:en-->*To achieve the best browsing experience, please upgrade to new browser such as internet Explorer 7 or above, Mozilla Firefox 3.5 or above, or Google Chrome 6 or above.<!--:--><!--:zh-->*請注意，閣下現正使用的瀏覽器因版本問題而未能提供此網頁的最佳瀏覽體驗。建議更新至較新版本的瀏覽器(如 Internet Explorer 7 或以上)，或安裝其他新版本之瀏覽器(如 Google Chrome 6 或以上，及 Mozilla Firefox 3.5或以上) ，以達至最佳瀏覽效果。<!--:-->') ?>		
	</div>
	<![endif]--> 	
	
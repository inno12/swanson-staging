<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div class="index_body">
			<div class="index_bodyLeft">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<? 
					$dish_image = getCustomField('Dish Image (large)', null, false);	
					$dish_image = preg_replace('/alt=\".*\"/i', 'alt="'.$post->post_title.'"', $dish_image);
					$difficulty = getCustomField('Difficulty', null, false);
					$lead_time = getCustomField('Lead Time', null, false);
					//$bloggerAnchor = getCustomField('Blogger Anchor ('.$q_config['language'].')');
					//$bloggerMethod = getCustomField('Blogger Method ('.$q_config['language'].')');					
					$product = getProduct();		
					$relatedRecipe = getCustomField('Related Recipe ('.$q_config['language'].')');	
					$relatedRecipeMethod = getCustomField('Related Recipe Method ('.$q_config['language'].')');				
					$trimTitle = ($q_config['language'] == 'en')?mbTrimStr(the_title('','',false),50):mbTrimStr(the_title('','',false),9);
				?>
				<div class="ContentTop">
					<div id="mainContent" class="small-photo-recipe">
						<div class="mainKV"><?php echo $dish_image?></div><!--mainKV-->
						<div class="mainSoup"><img src="/wp-content/uploads/<?php echo $product->slug;?>_M.png" alt="<?php echo  $product->name; ?>" title="<?php echo  $product->name; ?>"/></div><!--soup-->	
						<!--mainKV-->
                        <!--[if IE 6]>
                        <style>
                            .recipeCaption{padding-top:360px;}
                        </style>
                        <![endif]-->
						<div class="recipeCaptionBg"><!--<img src="/wp-content/themes/twentyten/images/<?=($q_config['language'] == 'en')?"en/":""?>recipes/recipeCaptionbg.png">-->
						<div class="recipeCaption">				
							<div class="mainCaption_title"><h2><?php echo $trimTitle; ?></h2></div><!--title-->
							<div class="mainCaption_detail">							
							<?if ($difficulty != "NA") :?>
								<div class="mainCaption_diff font12_white">
									<span class="diff_field1"><?php _e( '<!--:en-->Difficulty<!--:--><!--:zh-->難度<!--:-->')?></span>
									<span><?php echo renderDifficulty($difficulty, 'small-recipe-diff_5.png', 'small-recipe-diff_0.png')?></span>				  
								</div>
							<?endif?>
							<?if ($lead_time != "NA") :?>
								<span class="time_field1"><?php _e( '<!--:en-->Time<!--:--><!--:zh-->時間<!--:-->')?></span><span class="time_field1"><?php echo $lead_time?><?php _e( '<!--:en--> minutes<!--:--><!--:zh-->分鐘<!--:-->')?></span>
							<?endif?>
							</div><!--detail-->
							
							
							<div class="recipesFunctionBar">
								<div class="recipesFbBtn">		
									<iframe src="http://www.facebook.com/plugins/like.php?href=<?php echo urlencode(qtrans_convertURL($url, $q_config['language']));?>&amp;layout=button_count&amp;show_faces=false&amp;width=80&amp;action=like&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
								</div><!--FbBtn-->
								<div style="clear:both;"></div>
								<div class="recipesPrintBtn"><a href="javascript:window.print()"><span class="nodisp"><?php _e( '<!--:en-->Print<!--:--><!--:zh-->列印<!--:-->')?></span></a></div><!--printBtn-->
								<!--<div class="recipesWidgetBtn"><span class="nodisp">下載桌面工具</span></div>--><!--widget-->
								<div class="recipesAddThisBtn">
									<!-- AddThis Button BEGIN -->
									<a target="_blank" class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4c909aed58695a6f"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a>
									<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4c909aed58695a6f"></script>
									<!-- AddThis Button END -->								
								</div><!--addthis-->
								
							</div><!--functionBar-->
						
						</div>
						</div><!--caption-->
						
						<? //if (!empty($relatedRecipe)):?>	
						<? if (false):?>
						<div class="otherMethod">
							<div class="otherMethodTitle"><?php _e( '<!--:en-->Related<br />Recipes<!--:--><!--:zh-->相關食譜<!--:-->')?></div>	
							<?php echo $relatedRecipe;?>
						</div><!--otherMethod-->
						<? endif; ?>
						
					</div><!--mainContent-->				
				</div><!--contentTop-->
				<div class="printableCaption">
					<div><h2><?php the_title(); ?></h2></div><!--title-->
					<div>
						<?if ($difficulty != "NA") :?>
						<div>
							<span class="diff_field1"><?php _e( '<!--:en-->Difficulty<!--:--><!--:zh-->難度<!--:-->')?> <?php echo $difficulty?></span>					  
						</div>
						<?endif?>
						<?if ($lead_time != "NA") :?>
						<div><span class="time_field1"><?php _e( '<!--:en-->Time<!--:--><!--:zh-->時間<!--:-->')?> <?php echo $lead_time?><?php _e( '<!--:en--> minutes<!--:--><!--:zh-->分鐘<!--:-->')?></span></div>
						<?endif?>
					</div>
				</div>						
				<div class="ContentBottomRecipe" >				
					<div class="ingreMethod">
						<?php the_content(); ?>						
					</div><!--ingreMethod-->
					
					<? //if (!empty($relatedRecipeMethod)):?>		
					<? if (false):?>			
					<div class="bloggerMethod">						
						<a name="relatedRecipe">&nbsp;</a>
						<div class="bloggerTitle"><!--<img src="/wp-content/themes/twentyten/images/recipes/campbellLogo.gif" />--><?php _e( '<!--:en-->Related recipes<!--:--><!--:zh-->相關食譜<!--:-->')?></div>												
						<?php echo $relatedRecipeMethod;?>
					</div><!--relatedRecipeMethod-->
					<? endif; ?>
					
					<div class="recipeFacebook">
						<div class="recipeFbTitle"><span class="nodisp"><?php _e( '<!--:en-->Share Your Ideas<!--:--><!--:zh-->分享你的廚藝心思<!--:-->')?></span></div>
						<div class="recipeFbComment"><fb:comments numposts="5" width="650"></fb:comments></div>
					</div><!--fb comment-->

				</div><!--ContentBottom-->


<?php endwhile; // end of the loop. ?>
			<div style="padding-bottom:80px;"></div>	
			</div><!--indexBodyLeft--->						
			
			<?php get_sidebar('recipes'); ?>
		</div><!--index body-->
		
<?php get_footer(); ?>

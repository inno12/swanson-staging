<?php 
	
	$templateID = getCustomField('Custom template', null, false);
	if(empty($templateID)){ $templateID = 'default'; }
	$templateFile = TEMPLATEPATH."/single-template-$templateID".".php";
	
	if (file_exists($templateFile)) {
		include($templateFile);
	}else{
		include(TEMPLATEPATH."/single-template-default".".php");
	}
?>
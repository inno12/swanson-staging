<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<script type='text/javascript'>
var ebRand = Math.random()+'';
ebRand = ebRand * 1000000;
//<![CDATA[ 
document.write('<scr'+'ipt src="HTTP://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&amp;ActivityID=107323&amp;rnd=' + ebRand + '"></scr' + 'ipt>');
//]]>
</script>
<noscript>
<img width="1" height="1" style="border:0" src="HTTP://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&amp;ActivityID=107323&amp;ns=1"/>
</noscript>
	<div class="index_body">
		<div class="index_bodyLeft">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div class="entry-content">
					<?php the_content(); ?>				
				</div><!-- .entry-content -->
					
<?php endwhile; ?>

				</div>
				<div class="index_more">
					<a href="<?php echo qtrans_convertURL(get_bloginfo('url').'/recipes/', $q_config['language']);?>">
						<?php echo _e('<!--:en-->View More<!--:--><!--:zh-->檢閱所有食譜<!--:-->') ?>
					</a>
				</div><!--indexMore-->			
			</div><!--indexBodyLeft--->						

			<?php get_sidebar('recipes'); ?>
		</div><!--index body-->
<?php get_footer(); ?>
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<script type="text/javascript">
	function showMore(){		
		$('#showMore').toggle();
		$('#resultCategoryTxt').toggle();
		$('#resultCategoryAllTxt').toggle();
	}
	
</script>		
		<div class="index_body">
			<div class="index_bodyLeft">
				
				<?php 
					$queryCatArr = array();
					$ingredientsArr = @$_REQUEST['ingredient'];		
					if (!empty($ingredientsArr)){
						$ingredientsArr = explode(',', $ingredientsArr);
						$queryCatArr = array_merge ($queryCatArr, $ingredientsArr);
					}
					$productRangeArr = @$_REQUEST['product'];
					if (!empty($productRangeArr)){
						$productRangeArr = explode(',', $productRangeArr);
						$queryCatArr = array_merge ($queryCatArr, $productRangeArr);
					}
					
					$applicationArr = @$_REQUEST['application'];
					if (!empty($applicationArr)){
						$applicationArr = explode(',', $applicationArr);
						$queryCatArr = array_merge ($queryCatArr, $applicationArr);
					}
						
					if ($q_config['language'] == 'en')
						$seperator = ", ";
					else if ($q_config['language'] == 'zh'){
						$seperator = "、";
					}
					
					query_posts(array('category__in' => $queryCatArr, 'paged'=>get_query_var('paged')));
				
					$queryCatNameArr = array();
					foreach($queryCatArr as $catId) {
						array_push($queryCatNameArr, get_cat_name( $catId));
					}
					$queryCatStr = implode($seperator, $queryCatNameArr);
					/*if ( count($queryCatNameArr)<=9 ) {
						$queryCatStr = implode($seperator, $queryCatNameArr);
					} else {
						$queryCatStr = implode($seperator, array_slice($queryCatNameArr,0,9));
						$queryOtherCatStr = implode($seperator, array_slice($queryCatNameArr,9,count($queryCatNameArr)-1));
					}	*/		
					
					//var_dump($queryCatNameArr);
					//get_category_link( $category->term_id )
				?>
				
<?php if ( have_posts() ) : ?>
				<?php if (!empty($queryCatArr)) : ?>
				<div class="ContentTop">
					<div class="resultTop">
						<div>
							<?php 
							if (isset($_GET['product']) && $_GET['product'] != ""){
								$product = explode(",", $_GET['product']);
								$prod_id = intval($product[0]);

								$idObj = get_category_by_slug('product');
								$categories  = get_categories(array('parent'=>$idObj->term_id,'hide_empty'=>0)) ;

								foreach($categories as $category){
									if( $category->cat_ID == $prod_id){
										$custom_field = get_field('banner', $category );
										break;
									}

								}
								//echo $custom_field;
								echo "<img src='".$custom_field."' alt='' style='margin-bottom:20px;'>";
							}else if (isset($_GET['application']) && $_GET['application'] != ""){
								$application = explode(",", $_GET['application']);
								$app_id = intval($application[0]);

								$idObj = get_category_by_slug('application');
								$categories  = get_categories(array('parent'=>$idObj->term_id,'hide_empty'=>0)) ;

								foreach($categories as $category){
									if( $category->cat_ID == $app_id){
										$custom_field = get_field('banner', $category );
										break;
									}

								}
								//echo $custom_field;
								echo "<img src='".$custom_field."' alt='' style='margin-bottom:20px;'>";
							}
							?>
						</div>
						<div class="resultTopTitle"><?php echo _e( '<!--:en-->Search Result<!--:--><!--:zh-->搜尋結果<!--:-->'); ?></div>
						<div class="resultDetail">

							<div class="resultCategory">
								<div class="font_green" style="float:left"><?php echo _e( '<!--:en-->Category Selected:&nbsp;<!--:--><!--:zh-->搜索類別︰<!--:-->'); ?></div>
								<div id="resultCategoryTxt" class="resultCategoryTxt"><?php echo $queryCatStr?></div><span class="font_blue nodisp" id="showMore"><a href="javascript:showMore();" id="toggle_link"><?php echo _e( '<!--:en-->&nbsp;More<!--:--><!--:zh-->更多<!--:-->'); ?></a></span>							
								<script type="text/javascript">
									var isEllipsis = $('#resultCategoryTxt').ellipsis();
									if (isEllipsis){
										$('#showMore').show();
									}else {
										<? if ($q_config['language'] == 'en') :?>
										$('#resultCategoryTxt').css("width","500px");
										<? else: ?>
										$('#resultCategoryTxt').css("width","450px");
										<?endif;?>
									}									
								</script>							
								<div id="resultCategoryAllTxt" class="resultCategoryTxt nodisp"><?php echo $queryCatStr?> <a href="javascript:showMore();" id="toggle_link"><?php echo _e( '<!--:en-->Hide<!--:--><!--:zh-->隱藏<!--:-->'); ?></a></div>							
							</div>
							<div class="resultTotal">
								<? if ($q_config['language'] == 'en') :?>
									<span class="font_green"><?php echo $wp_query->found_posts;?> Recipes in total.</span>
								<? else: ?>
									<span class="font_green">共 <?php echo $wp_query->found_posts;?> 款食譜</span>
								<?endif;?>
							</div>
						</div><!--resultDetail-->
					</div><!--resultTop-->            
				</div><!--contentTop-->
				<?php endif; ?>
				<!-- page-receipes 2015 -->
				<div class="ContentBottom">
				<?php				
				/* Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called loop-search.php and that will be used instead.
				 */
				 get_template_part( 'loop', 'recipes' );
				?>
					<div class="resultPaging">
						<?php get_pagination() ?>
					</div><!--resultPaging-->
				
				</div><!--ContentBottom-->
<?php else : ?>
				<div id="post-0" class="post no-results not-found">
					<h2 class="entry-title"><?php _e( '<!--:en-->Nothing Found<!--:--><!--:zh-->未能找到<!--:-->' ); ?></h2>
					<div class="entry-content">
						<p><?php _e( '<!--:en-->Sorry, but nothing matched your search criteria. Please try again with some different keywords.<!--:--><!--:zh-->對不起！我們未能找到符合搜索條件之結果，請以其他條件重新搜索。<!--:-->' ); ?>
						
						</p>
						<?php //get_search_form(); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-0 -->
<?php endif; ?>
			</div><!--indexBodyLeft--->						

			<?php get_sidebar('recipes'); ?>
		</div><!--index body-->
		
<?php get_footer(); ?>

<div id="footerContainer">
	<div class="container">

		<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 row" style="font-weight: bold;">

			<div id="backtotopDiv" >
				<a onclick="backToTop()">
					<img src="<?=ROOT_PATH?>images/backtotop.jpg"  style="cursor:pointer; width:135px;" />
				</a>
			</div>
			<div id="footerTitle">
				史雲生食譜
			</div>
		</div>

		<div class=" footerListContainer">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 row footerList ingredientsFooterList" >
				<div class="title">食材</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ingredients">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div><a href="<?=ROOT_PATH?>recipe-list.php?i=1">豬</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?i=3">雞</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?i=2">牛</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?i=4">其他肉類</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?i=5">蛋及豆腐</a></div>
					<!-- </div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" > -->
						<div><a href="<?=ROOT_PATH?>recipe-list.php?i=6">海鮮</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?i=7">菜及菇類</a></div>
					</div>
				</div>
			</div>

			<hr class="vertical"/>

			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 row footerList themeFooterList" >
				<div class="title">菜式</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 theme">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div><a href="<?=ROOT_PATH?>recipe-list.php?t=1">新手菜式</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?t=2">家常小菜</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?t=3">節慶菜式</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?t=4">粥粉麵飯</div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?t=5">火鍋推介</a></div>
					<!-- </div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> -->
						<div><a href="<?=ROOT_PATH?>recipe-list.php?t=6">輕盈健康浸菜</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?t=7">湯/羹</a></div>
					</div>
				</div>
			</div>

			<hr class="vertical"/>

			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 row footerList productFooterList"  >
				<div class="title">史雲生系列</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div><a href="<?=ROOT_PATH?>recipe-list.php?c=CCB">清雞湯</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?p=1" style="color:#CCC;">&nbsp;清雞湯250mL</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?p=2" style="color:#CCC;">&nbsp;清雞湯500mL</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?p=3" style="color:#CCC;">&nbsp;清雞湯1L</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?p=4">瑤柱上湯250mL</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?p=6">金華火腿上湯250mL</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?p=8">鮮魚湯500mL</a></div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div><a href="<?=ROOT_PATH?>recipe-list.php?c=PBB">豬骨湯</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?p=5" style="color:#CCC;">&nbsp;豬骨湯500mL</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?p=7" style="color:#CCC;">&nbsp;豬骨湯1L</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?p=9,10,11">調味雞汁</a></div>
					</div>
				</div>
			</div>

			<hr class="vertical"/>

			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 row footerList cookingMethodFooterList" >
				<div class="title">煮法</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ingredients">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
						<div><a href="<?=ROOT_PATH?>recipe-list.php?m=1">煎</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?m=2">炒</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?m=3">蒸</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?m=4">炆</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?m=6">燴</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?m=7">湯浸</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?m=8">煮</a></div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
						<div><a href="<?=ROOT_PATH?>recipe-list.php?m=9">煲/火鍋</a></div>
						<div><a href="<?=ROOT_PATH?>recipe-list.php?m=10">其他</a></div>
					</div>
				</div>
			</div>



			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 row footerList btnContainer" style="border:none;">
				<hr class="hidden-lg hidden-md hidden-sm col-xs-12"/>
				<div>
					<a class=""  href="https://www.youtube.com/channel/UCAQGuP8ViNiIZIb8CcDQ3nQ/videos" target="_blank"  style="margin-right:8px;">
						<img src="<?=ROOT_PATH?>images/youtubeBtn.png" class="youtubeBtn"/>
					</a>

					<a onclick ="sharePage()" style="margin-right:1px;cursor:pointer;">
						<img src="<?=ROOT_PATH?>images/facebookBtn.png" class="facebookBtn"/>
					</a>
				</div>

			</div>
		</div>
			<div style="clear:both"></div>
	<hr class="col-lg-12 col-md-12 col-sm-12 hidden-xs" style="    margin-top: 10px;"/>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="font-size:11.5px;padding-top:5px;padding-left:0px;">
				<span class="footerCopyRight_line1">&reg;&nbsp;Registered trademark of Campbell Soup Company.</span>

				<span class="footerCopyRight_line2">&copy;&nbsp;2018 Campbell Soup Asia Limited.</span>
			</div>

			<div class="col-lg-8 col-md-8 col-sm-8 hidden-xs footerLinks" style="text-align:right;">
				<div style="float: right;padding-top: 4px;">
					<a href="<?=ROOT_PATH?>index.php">
						主頁
					</a>
					<a href="<?=ROOT_PATH?>recipe-list.php">
						食譜搜尋
					</a>
					<a href="<?=ROOT_PATH?>recipe-video-tsui.php">
						鮮味炒教室
					</a>
					<a href="<?=ROOT_PATH?>recipe-video.php">
						入廚教學短片
					</a>
					<a href="<?=ROOT_PATH?>product-list.php">
						史雲生系列
					</a>
					<a href="<?=ROOT_PATH?>cookbook.php">
						1盒煮1餸簡易食譜下載
					</a>
					<a href="<?=ROOT_PATH?>cookbook-scs.php">
						調味雞汁鮮味炒食譜下載
					</a>
					<a href="https://docs.google.com/forms/d/e/1FAIpQLSfc99LkzptSOzJUhhYz-f2UY6Icll90QYegzcIXhZ2R6lKR-A/viewform" target="_blank">
						有獎問卷
					</a>
					<a href="<?=ROOT_PATH?>event_list.php">
						鮮味炒試食地點
					</a>
				</div>
			</div>
		</div>
	</div>

</div>

	<script>
		function backToTop(){
			$('html,body').animate({
	            scrollTop: 0
	        }, 400);
		}
	</script>

	<div style="display:none;">
		<!-- Google Code for Remarketing Tag -->
		<!--------------------------------------------------
		Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
		--------------------------------------------------->
		<script type="text/javascript">
// 		var google_tag_params = {
// 			dynx_itemid: 'REPLACE_WITH_VALUE',
// 			dynx_itemid2: 'REPLACE_WITH_VALUE',
// 			dynx_pagetype: 'REPLACE_WITH_VALUE',
// 			dynx_totalvalue: 'REPLACE_WITH_VALUE',
// 		};
		</script>
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 974311933;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:none;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/974311933/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
	</div>

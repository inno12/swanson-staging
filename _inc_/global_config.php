<?php
ini_set('session.save_path',"/var/www/html/session");
session_start();

	/*App info*/
	define( APP_ID, "Swanson_Cooking_HK");
	define( APP_TITLE, "Swanson Cooking (Hong Kong)");
	
	/*Path info*/
	define ( DOC_ROOT,  realpath($_SERVER["DOCUMENT_ROOT"]) );
	
	define ( ROOT_PATH, "/" );
	
	define ( INC_PATH, ROOT_PATH."_inc_/" );
	define ( UTILS_PATH, DOC_ROOT.INC_PATH."_utils_/" );
	define ( PATH_TO_ULFILE, ROOT_PATH."_ul/" );
	
	define ( ADMIN_ROOT_PATH, ROOT_PATH."KMRWB8EKrzTGSpyG/" );
	define ( ADMIN_ROOT_INC_PATH, ROOT_PATH."KMRWB8EKrzTGSpyG/include/" );
	
	/*DB info*/

	define ( DB_PLATFORM, 	"mysql" );
	define ( DB_HOST, 		"localhost" );
	define ( DB_USERNAME,	"root" );
	define ( DB_PASSWORD, 	"root" );
	define ( DB_INSTANCE, 	"swanson_webdb" );
	
	define ( DEBUG_MODE, false);
	
	error_reporting(0);
?>

<?php
	require_once "../global_config.php";
	require_once DOC_ROOT.INC_PATH."init.php";
	
	if(array_key_exists("deleteRecord", $_GET)){
		
		$deleteTable = $_POST["table"];
		$deleteWhere = $_POST["where"];
		echo deleteRecord($deleteTable, $deleteWhere);
	}

	function deleteRecord($table, $where){
		if(!empty($table) && !empty($where)){
			
			
			$deleteSQL = "delete from " . $table . " where ". $where; 
			
			DB::getInstance()->query($deleteSQL);
			
			if(!DB::getInstance()->error()){

				
				
				return json_encode(returnObj(true, "", ""));
			}
			
		}else{
			return "UNSUCCESS";
		}
		
	
	}
	
	
	function returnObj($isSuccess, $message, $value){
		return array(
			"success"=> $isSuccess,
			"msg" => $message, 
			"value"	=> $value
		);
	};
	
?>
<?php
	function outputImageByDimension($pathToFile, $imageFileName, $width="100%", $height="auto", $className="", $imageId="", $otherArray=array()){
?>
		<img <?=!empty($imageId)? "id='".$imageId."'" : ""?> src="<?=$pathToFile."/".$imageFileName?>" class="<?=$className?>"	style="width:<?=$width?>;height:<?=$height?>"	<?=otherArrayToString($otherArray)?>/> 
<?php 
	}
	
	function outputCropImageByDimension($pathToFile, $imageFileName, $width="100%", $height="auto", $className="", $imageId="", $otherArray=array()){
?>
	<div
		<?=!empty($imageId)? "id='".$imageId."'" : ""?>
	 	style="
		width:<?=$width?>;
		height:<?=$height?>;
		overflow: hidden;
		background-image: url(<?=$pathToFile."/".$imageFileName?>);
		background-repeat: no-repeat;
		background-position: center center;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover; 
		class="<?=$className?>" 
		<?=otherArrayToString($otherArray)?> 
		">	
	</div>
<?php 
	}
		
	function outputImageIncludePadding($pathToFile, $imageFileName, $width="100%", $height="auto", $className="", $imageId="", $otherArray=array()){

?>
	<div style="display:table;width:100%;height:100%">
		<div style="display:table-cell;vertical-align:middle;text-align:center">
			<img <?=!empty($imageId)? "id='".$imageId."'" : ""?> width="<?=$width?>" height="<?=$height?>" class="<?=$className?>" src="<?=$pathToFile."/".$imageFileName?>" <?=otherArrayToString($otherArray)?> >					
		</div>
	</div>
<?php 

			
	}
	
	function otherArrayToString($otherArray){

		$otherAttr = "";
		if(!empty($otherArray)){
			foreach($otherArray as $k=>$v){
				$otherAttr.= " ".$k."=\"".$v."\"";
			}
		}
		return $otherAttr;
	}
?>



	
	
	
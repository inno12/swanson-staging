<script>
	function createDialogConfirm(title, content, okFn){
		
		$.fancybox.open({
			href:"<?=INC_PATH?>js/dialog_popup.html",
			type:"ajax",
			beforeShow:function(){
				var popup = $("#popup_confirm");
	
				$("#popup_title", popup).html(title);
				$("#popup_message", popup).html(content);
				$("#popup_btnContainer_yesno").hide();
				$("#popup_confirm_ok").on("click", function(e){
					if(okFn){
						okFn();
					}
					$.fancybox.close();
				});
			}
		}); 
	}
	
	
	
	function createDialogConfirmCancel(title, content, trueFn, falseFn){
		
		
		$.fancybox.open({
			href:"<?=INC_PATH?>js/dialog_popup.html",
			type:"ajax",
			beforeShow:function(){
				var popup = $("#popup_confirm");
	
				$("#popup_title", popup).html(title);
				$("#popup_message", popup).html(content);
				$("#popup_btnContainer_ok").hide();
				$("#popup_confirm_yes").on("click", function(e){
					if(trueFn){
						trueFn();
					}
					$.fancybox.close();
				});
				$("#popup_confirm_yes_danger").hide();
				$("#popup_confirm_no").on("click", function(e){
					if(falseFn){
						falseFn();
					}
					$.fancybox.close();
				});
			}
		}); 
		
		
		
	}

	
	function createDialogConfirmCancel_danger(title, content, trueFn, falseFn){
			
			
			$.fancybox.open({
				href:"<?=INC_PATH?>js/dialog_popup.html",
				type:"ajax",
				beforeShow:function(){
					var popup = $("#popup_confirm");
		
					$("#popup_title", popup).html(title);
					$("#popup_message", popup).html(content);
					$("#popup_btnContainer_ok").hide();
					$("#popup_confirm_yes_danger").on("click", function(e){
						if(trueFn){
							trueFn();
						}
						$.fancybox.close();
					});
					
					$("#popup_confirm_yes").hide();
					$("#popup_confirm_no").on("click", function(e){
						if(falseFn){
							falseFn();
						}
						$.fancybox.close();
					});
				}
			}); 
			
			
			
		}
</script>
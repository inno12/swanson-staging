<?php



	

	class DB{
		
		private static $_instance = null;
		private $_pdo,
				$_query,
				$_error = false,
				$_results,
				$_count = 0;	
				
		private function __construct(){
		
			try{
				
				$this->_pdo = new PDO(DB_PLATFORM.":host=".DB_HOST.";dbname=".DB_INSTANCE, DB_USERNAME, DB_PASSWORD);
				$this->_pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
				$this->_pdo->exec("set names utf8");
			}catch(PDOException $e){
				die($e->getMessage());
			}
		
			//echo " DB Connected";
		}
		
		
		public static function getInstance(){
		
			if(!isset(self::$_instance)){
				
				self::$_instance = new DB();
			
			}
			return self::$_instance;
		}
	
	
		public function query($sql, $params = array()){
		
			$this->_error = false;
			if($this->_query = $this->_pdo->prepare($sql)){
			
				if(count($params)){
					
					foreach($params as $k=>$p){
												
						if(is_numeric($p)){
							$this->_query->bindValue($k, $p, PDO::PARAM_INT);
						}else if(is_string($p)){
							$this->_query->bindValue($k, $p, PDO::PARAM_STR);
						}
					}
				}
				
				
				
				
				
				
				
				
				if($this->_query->execute()){
					
					
					$this->_count = $this->_query->rowCount();
					
					if($this->_query->columnCount() > 0 ){
						$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
					}
					

				}else{
					$this->_error = true;
					
					var_dump($this->_pdo->errorInfo());
					echo "query: ".$sql;
					var_dump($params);
				}
			}
			
			return $this;
		}
		
		
		
		/*
		public function get($table, $where, $fields = "*"){
			return $this->action("SELECT ".$fields, $table, $where);
		}
		
		public function delete($table, $where){
			return $this->action("DELETE", $table, $where);
		}
		
		
		private function action($action, $table, $where = array()){
			
			$whereSql = "";
			$whereValues = array();
			
			foreach($where as $key=>$cond){
				
				if(count($cond) ===3){
						
					$validOperators = array("=", ">", "<",">=", "<=", "in");
				
					$field = $cond[0];
					$operator = $cond[1];
					$value = $cond[2];
				
					if(in_array($operator, $validOperators)){
				
						if($key==0){
							$whereSql = "where ";
						}else{
							$whereSql .= " and ";
						}
						
						$whereSql .= "{$field} {$operator} ?";
						
						array_push($whereValues, $value);
							
					}
				}
				
			}
			
			$sql = "{$action} FROM {$table} {$whereSql} ";

			if(!$this->query($sql, $whereValues)->error()){
				
				return $this;
			
			}else{
				var_dump($this->_pdo->errorInfo());
			}
			
			
		
			return false;
		}
		*/
		public function results(){
		
			return $this->_results;
		}
		
		public function getLastInsertId(){
		
			return $this->_pdo->lastInsertId();
		}
		
		public function singleResults(){
		
			return $this->_results[0];
		}
		
		
		public function error(){
		
			return $this->_error;
		}
	
		public function count(){
			return $this->_count;
		}
		
		
		
		
		
		
		
		
		
		
		
	}
?>
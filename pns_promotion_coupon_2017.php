<?php

	include_once 'inc_top_script.php';

?>

<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>
		Swanson Cooking PNS Coupon
	</title>
	
	<style>
		body,html{
			margin:0px;
			padding:0px;
			border:none;
		}
		
		img{
			width:100%;
			max-width:750px;
		}
		
		@media(min-width:768px){
			img{
				height:100%;
				width:auto;
			}
		}
	</style>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-57656614-6', 'auto');
	  ga('send', 'pageview');
	  
	 
 
	</script>
</head>
<?php
	$date = date('Ymd', strtotime(date("Y-m-d H:i:s")));
	
	$rRs = $pdo->prepare("SELECT * 
		FROM  `coupon_list` 
		WHERE  `validDate` = CURDATE( ) 
		");
	$rRs->execute();
	
	$rRs_row = $rRs->fetch();
	$image = $rRs_row["image"];
	
	
?>

<?php
	if(!empty($image)){
?>
	<img src="<?=PATH_TO_ULFILE.$image?>?<?=$date?>" style=""/>
<?php
	}
?>


